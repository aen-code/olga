/**
------------------------------------------------------------------------
  This file is part of the OLGA project.
  Copyright © 2017 by Enver Akhmedov.
  E-mail: aen-code@yandex.ru
  For conditions of distribution and use, see copyright notice in OLGA.h
------------------------------------------------------------------------
**/

#include "ol_List.h"

/**
    --------------------------------------- Node -------------------------------------------
    >                                                                                      <
    >                           Узел кольцевого списка.                                    <
    >   Кольцевые списки удобны, например, в геометрии, где многоугольник хранится в       <
    >   виде кольцевого списка вершин в порядке обхода. Узел такого списка является        <
    >                           объектом класса Node:                                      <
    >                                                                                      <
    ----------------------------------------------------------------------------------------
**/

ol_Node::ol_Node()
{
    prev = next = this;
}

ol_Node::~ol_Node()
{}

ol_Node* ol_Node::Next(void)
{
    return next;
}

ol_Node* ol_Node::Prev(void)
{
    return prev;
}

ol_Node* ol_Node::Insert(ol_Node *b)
{
    ol_Node *c = next;
    b->next = c;
    b->prev = this;
    next = b;
    c->prev = b;

    return b;
}

ol_Node* ol_Node::Remove(void)
{
    prev->next = next;
    next->prev = prev;
    next = prev = this;

    return  this;
}

void ol_Node::Splice(ol_Node *b)
{
    ol_Node *a = this;
    ol_Node *an = a->next;
    ol_Node *bn = b->next;
    a->next = bn;
    b->next = an;
    an->prev = b;
    bn->prev = a;
}
