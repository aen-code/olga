/**
------------------------------------------------------------------------
  This file is part of the OLGA project.
  Copyright © 2017 by Enver Akhmedov.
  E-mail: aen-code@yandex.ru
  For conditions of distribution and use, see copyright notice in OLGA.h
------------------------------------------------------------------------
**/

#ifndef OL_FRUSTUM_H_INCLUDED
#define OL_FRUSTUM_H_INCLUDED

#include <GL/gl.h>
#include <math.h>
#include "ol_Vector_Math.h"

// We create an enum of the sides so we don't have to call each side 0 or 1.
// This way it makes it more understandable and readable when dealing with frustum sides.
enum ol_FrustumSide
{
    OL_FRUSTUM_RIGHT	= 0,	// The RIGHT side of the frustum
    OL_FRUSTUM_LEFT		= 1,	// The LEFT	 side of the frustum
    OL_FRUSTUM_BOTTOM	= 2,	// The BOTTOM side of the frustum
    OL_FRUSTUM_TOP		= 3,	// The TOP side of the frustum
    OL_FRUSTUM_BACK		= 4,	// The BACK	side of the frustum
    OL_FRUSTUM_FRONT	= 5		// The FRONT side of the frustum
};

// Like above, instead of saying a number for the ABC and D of the plane, we
// want to be more descriptive.
enum ol_PlaneData
{
    A = 0,		// The X value of the plane's normal
    B = 1,		// The Y value of the plane's normal
    C = 2,		// The Z value of the plane's normal
    D = 3		// The distance the plane is from the origin
};

// This will allow us to create an object to keep track of our frustum
class ol_Frustum
{
public:
    // Call this every time the camera moves to update the frustum
    void CalculateFrustum();

    // This takes a 3D point and returns TRUE if it's inside of the frustum
    bool PointInFrustum(ol_Vector3d);

    // This takes a 3D point and a radius and returns TRUE if the sphere is inside of the frustum
    bool SphereInFrustum(float x, float y, float z, float radius);

    // This takes the center and half the length of the cube.
    bool CubeInFrustum( float x, float y, float z, float size );

private:

    // This holds the A B C and D values for each side of our frustum.
    float frustum[6][4];
};

void NormalizePlane(float frustum[6][4], int side);

#endif // OL_FRUSTUM_H_INCLUDED
