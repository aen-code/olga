/**
------------------------------------------------------------------------
  This file is part of the OLGA project.
  Copyright © 2017 by Enver Akhmedov.
  E-mail: aen-code@yandex.ru
  For conditions of distribution and use, see copyright notice in OLGA.h
------------------------------------------------------------------------
**/

#ifndef OL_MATH_H_INCLUDED
#define OL_MATH_H_INCLUDED

// Эта функция возвращает число в степени два, большее, чем число a
unsigned int Next_P2(unsigned int a);

#endif // OL_MATH_H_INCLUDED
