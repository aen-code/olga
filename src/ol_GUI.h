/**
------------------------------------------------------------------------
  This file is part of the OLGA project.
  Copyright © 2017 by Enver Akhmedov.
  E-mail: aen-code@yandex.ru
  For conditions of distribution and use, see copyright notice in OLGA.h
------------------------------------------------------------------------
**/

#ifndef OL_GUI_H_INCLUDED
#define OL_GUI_H_INCLUDED

#include <map>
#include <list>

#include "ol_Graphics.h"
#include "core/ol_System.h"
#include "ol_Math.h"
#include "stdio.h"

#include <ft2build.h>
#include FT_FREETYPE_H

using namespace std;

class ol_GUIManager; // Декларация для GUI Managera
class ol_GUIButton;  // Декларация кнопки
class ol_GUILabeledEditBox;
class ol_GUIEditBox;

enum ol_GUIControlStates
{
    OL_GUI_STATE_DISABLED,
    OL_GUI_STATE_STANDARD,
    OL_GUI_STATE_SELECTED,
    OL_GUI_STATE_PRESSED,
    OL_GUI_STATE_FOCUSED
};

// Передается в качестве параметра при позиционировании ГУИ контрола
// чтобы определить какая часть рамки гуи должна совпасть с заданными координатами
enum ol_GUIPositionPoints
{
    OL_GUI_POS_TOPLEFT,
    OL_GUI_POS_TOPMID,
    OL_GUI_POS_TOPRIGHT,
    OL_GUI_POS_CENTLEFT,
    OL_GUI_POS_CENTMID,
    OL_GUI_POS_CENTRIGHT,
    OL_GUI_POS_BOTLEFT,
    OL_GUI_POS_BOTMID,
    OL_GUI_POS_BOTRIGHT
};

enum ol_GUIButtonStyle
{
	OL_GUI_BUTOTN_VOID,			// не генерирует никаких событий, просто "знает" что она нажата
	OL_GUI_BUTTON_SIMPLE,		// реагирует на нажатие
	OL_GUI_BUTTON_STANDARD,		// реагирует на отжатие. в нажатом состоянии изменяется текстура и надпись смещается
	OL_GUI_BUTTON_MENUITEM,		// по поведению похожа на симпл, заголовок позиционирует по левому краю
	OL_GUI_BUTTON_LOCKED		// не генерирует никаких событий. Никак не реагирует. Всегда возвращает true.
};

enum ol_GUIFileDialogType
{
    OL_GUI_FILEDIALOG_OPEN,
    OL_GUI_FILEDIALOG_SAVE
};

// расположение эдита относительно скроллера
enum ol_GUIEdScrollPos
{
    OL_GUI_EDSCROLL_BEGIN, // со стороны мин. знач.
    OL_GUI_EDSCROLL_END    // со стороны макс. знач.
};

enum ol_GUIScrollBoxType
{
    OL_GUI_SCROLLBOX_HORIZONTAL,
    OL_GUI_SCROLLBOX_VERTICAL
};

enum ol_CharSets
{
    OL_CHARSET_COMMON,
    OL_CHARSET_kYRILIC,
    OL_CHARSET_kYRILIC_BASIC,
    OL_CHARSET_LATIN,
    OL_CHARSET_LATIN_BASIC
};

// Условное расположение, слева, справа... создано для широкого применения
enum ol_RelPos
{
    OL_POS_LEFT,
    OL_POS_RIGHT,
    OL_POS_UP,
    OL_POS_DOWN,
    OL_POS_CENTER
};

// Тип оси. Используется для того чтобы определить откуда брать значения для подписи шкалы
enum ol_AxisType
{
    OL_AXIS_X,
    OL_AXIS_Y
};

enum ol_RectSides
{
    OL_RECT_SIDE_LEFT,
    OL_RECT_SIDE_RIGHT,
    OL_RECT_SIDE_TOP,
    OL_RECT_SIDE_BOTTOM
};

vector<unsigned int> addCharSet(vector<unsigned int> codes, ol_CharSets charset);

// Структура для хранения параметров символа
struct ol_Symbol
{
    GLuint ind;      // индекс дисплей листа для отображения
    unsigned short width;    // ширина в пикселях
    unsigned short height;   // высота в пикселях
    unsigned short hn;       // расстояние на которое символ смещен вниз
    unsigned short hp;       // расстояние на которое символ выпирает наверх (height - hn)
};

// Вектор индексов дисплей листов. Используется для отображения строк без конвертации
typedef vector<GLuint> ol_GLString;
// Более сложный тип содержит не только строку в кодировке энджина, но и размеры описывающего прямоугольника
struct ol_GLFormatedString
{
    vector<GLuint> str;
    float width;
    float height;
};

class ol_Font;

// Поля элемента управления
class ol_GUIMargins
{
public:
    ol_GUIMargins(void)
    {
        top = bottom = left = right = 0.0f;
    }
    ol_GUIMargins(float left, float top, float right, float bottom)
    {
        this->left = left;
        this->top = top;
        this->right = right;
        this->bottom = bottom;
    }
    ~ol_GUIMargins() {}

    ol_GUIMargins& operator=(ol_GUIMargins m)
    {
        left = m.left;
        top = m.top;
        right = m.right;
        bottom = m.bottom;

        return *this;
    }

    float top, bottom, left, right;
};

struct ol_GUITheme
{
    const char* fontFileName;
    unsigned int fontSize;
    ol_Font *font;
    ol_Color bgrColor;
    ol_Color bgrColor1;
    ol_Color pressColor;
    ol_Color brdColor;
    ol_Color selectColor;
    ol_Color fontColor;

    short   ctrlDist; // стандартное расстояние между соседними контролами

    ol_GUIMargins   editBoxMargins;               // Отступы от краев области, куда происходит вывод текста
    short           editBoxLabShift;              // Отступ заголовка эдит бокса от края элемента
    short           editBoxHeight;                // Стандартная высота эдит бокса
    ol_Color        editBoxBgColor;               // Background colour of editBox

    short           scrollerThickness;            // Ширина ползунка или высота в случае вертикального скролбокса

    ol_GUIMargins   winClientMrgs;                // Отступы клиентской части окна
    short           winHeadHeight;                // высота заголовка окна
    short           winHeadButSize;               // размер кнопок заголовка окна(закрыть, свернуть, развернуть...) 1 число, так как кнопки квадратные
    short           winHeadButShift;              // отступ центра кнопки от края окна

    short           buttHeight;                   // стандартная высота кнопки
    short           buttLabHorShift;              // отступ заголовка кнопки от края (лево/право)
    short           buttLabVertShift;             // отступ заголовка кнопки от края (верх/низ)
    ol_Color        buttLabStandardColor;         // обычный цвет заголовка кнопки
    ol_Color        buttLabSelectedColor;         // цвет заголовка в выделенном состоянии

    short           fileDialogWidth;
    short           fileDialogHeight;
    wstring         fileDiagPathCaption;           // заголовок эдита с полным именем директории
    wstring         fileDiagOpenCaption;           // заголовок диалога "открыть файл"
    wstring         fileDiagOpenOKCaption;         // надпись на кнопке "открыть"
    wstring         fileDiagSaveCaption;           // заголовок диалога "сохранить файл"
    wstring         fileDiagSaveOKCaption;         // надпись на кнопке "сохранить"
};

/**
    --------------------------------- Quad 2D ----------------------------------------------
    >                                                                                      <
    >               Простой прямоугольник. Знает свои вершины.                             <
    >                                                                                      <
    ----------------------------------------------------------------------------------------
**/

class ol_Quad2d
{
public:
    ol_Quad2d() {}
    ~ol_Quad2d() {}

    ol_Vector2df botLeft;      // Нижний левый угол
    ol_Vector2df topLeft;      // Верхний левый угол
    ol_Vector2df topRight;     // Верхний правый угол
    ol_Vector2df botRight;     // Правый нижний угол
};

/**
    ------------------------------- 2D Animation -------------------------------------------
    >                                                                                      <
    >                                Класс анимации.                                       <
    >      Сам ничего не рендерит. Позволяет загрузить текстуру, разбить её на кадры и     <
    >           переключать кадры по команде или циклически, с заданным фпс.               <
    >                                                                                      <
    ----------------------------------------------------------------------------------------
**/

class ol_Animation2d
{
protected:
    vector<ol_Quad2d*> frames;

    unsigned short fps;				// скорость проигрывания анимации кадров/с
    unsigned short framesCount;		// общее количество кадров в анимации
    short repeat;					// количество раз которое нужно повторить анимацю. Отрицательное значение включает циклическую анимацию
    unsigned short curRepeat;		// количество раз которое уже прошло

    ol_TimeMonitor *period;			// интерфейс для засечек времени
    bool animating;					// проигрывается ли анимация в данный момент

public:
    GLuint  texID;
    unsigned int texWidth;
    unsigned int texHeight;

    unsigned short curFNum;			// номер текущего кадра
    ol_Quad2d *curFrame;

    ol_Animation2d();
    ~ol_Animation2d();

    /* инкрементирует все счетчики; должна вызываться каждый фрейм, ответственность
     * за это берет на себя класс содержащий анимейшн или унаследованный от него.*/
    void UpdateAnimation(void);
    void StartAnimation(void);
    void StopAnimation(void);
    void Animate(short repeat = -1);
    bool AssignAnimation(ol_Texture tex);
    bool HasAnimation(void);
    void setFPS(unsigned short fps);
    void setCurFrame(unsigned int frame);
    // циклически прокручивает кадры. Возвращает тру, если перескочила на начальный кадр
    bool IncrementFrame(void);
    bool isAnimating(void);
};

/**
    --------------------------------- Control points ---------------------------------------
    >                                                                                      <
    >    Ключевые вершины элемента управления, по которым можно его позиционировать        <
    >                                                                                      <
    ----------------------------------------------------------------------------------------
**/

class ol_GUIControlPoints: public ol_Quad2d
{
public:
    ol_GUIControlPoints();
    ~ol_GUIControlPoints() {}

    ol_Vector2df topMid;       // Середина верхней стороны
    ol_Vector2df centLeft;     // Середина левой стороны
    ol_Vector2df centMid;      // Центр
    ol_Vector2df centRight;    // Середина правой стороны
    ol_Vector2df botMid;       // Середина нижней грани

    void Init(ol_Vector2df origin, float width, float height, ol_GUIPositionPoints keyPoint = OL_GUI_POS_BOTLEFT);
};

/**
    --------------------------------- 2D Area ----------------------------------------------
    >                                                                                      <
    >  Двумерный прямоугольник. "Знает" свои углы, середины сторон, центр, длину и ширину. <
    >  Может определить попадает ли точка внутрь.                                          <
    >                                                                                      <
    ----------------------------------------------------------------------------------------
**/

class ol_Area2d: public ol_GUIControlPoints
{
public:
    float width;                 // Ширина
    float height;                // высота

    ol_Area2d() {}
    virtual ~ol_Area2d() {}

    // в результате сложения получается прямоугольник содержащий обе области
    ol_Area2d operator+(ol_Area2d &are);

    void Init(ol_Vector2df origin, float width, float height, ol_GUIPositionPoints keyPoint = OL_GUI_POS_BOTLEFT);
    void getSide(ol_RectSides side, ol_Vector2df &v1, ol_Vector2df &v2);
    void getSide(ol_RectSides side, ol_Vector2dld &v1, ol_Vector2dld &v2);
    ol_Vector2df &getPosPoint(ol_GUIPositionPoints keyPoint);
    virtual void Shift(ol_Vector2df &dir); // сдвигает все точки Area по вектору dir
    bool isInside(ol_Vector2df &v);
};

/**
    Расчитывает положение объекта "подпись" относительно контрола
    control - подписываемый элемент управления
    controlPoint - "ключевая точка" контрола возле которой будет подпись;
    delta - расстояние на которое отстоит подпись;
    pos, labelPoint - возвращаемые координата и "ключевая точка" подписи
**/
void CalcLabelRelPos(ol_Area2d *control, ol_GUIPositionPoints controlPoint, float delta, ol_Vector2df &pos, ol_GUIPositionPoints &labelPoint);

/**
    --------------------------------- Simple Font ------------------------------------------
    >                                                                                      <
    >                                Простой шрифт.                                        <
    >       Сам инициализирует латинские символы и символы общего назначения;              <
    >       не поддерживает юникод; позволяет осуществлять многострочный вывод.            <
    >                                                                                      <
    ----------------------------------------------------------------------------------------
**/

class ol_SimpleFont
{
protected:
    float height;              // высота шрифта в пикселях
    float size;                // размер шрифта
    GLuint *textures;          // массив указателей на текстуры
    GLuint list_base;          // Содержит указатель на список отображения

    ol_Graphics *grm;           // Указатель на графический интерфейс. Используется при отрисовке

    map<wchar_t, ol_Symbol*> charmap; // карта символов. Хранит соответствие unicode кода символа коду дисплей листа
    vector<unsigned int> charcodes;  // коды символов, из которых нужно построить charmap. Удаляется после создания шрифта

    unsigned int char_count;    // количество символов

    void Make_Dlist(FT_Face face, wchar_t ch, ol_Symbol *s, GLuint list_base, GLuint *tex_base);
    void Destroy();

    // заменяет в codes значения юникод на дисплей лист, а также вычисляет прямоугольную область отрисовки
    virtual void ConvertCharCodes(char codes[], unsigned int len); // тоже самое, только для char

public:
    ol_SimpleFont(ol_Graphics *grm);
    virtual ~ol_SimpleFont();

    bool BuildFont_ft(FT_Library  *ft_lib, const char *fname, unsigned int h);

    virtual void Print(float x, float y, const char *fmt, ...);

    float getHeight(void);
    float getSize(void);
};

/**
    ----------------------------------- Font -----------------------------------------------
    >                                                                                      <
    >                               Продвинутый шрифт.                                     <
    >       Поддерживает юникод; при инициализации необходимо передать ему массив          <
    >  с кодировками; не поддерживает многострочный вывод; поддерживает относительное      <
    >                   позиционирование при выводе (layout)                               <
    >                                                                                      <
    ----------------------------------------------------------------------------------------
**/

class ol_Font: public ol_SimpleFont
{
public:
    ol_Area2d printArea;   // прямоугольная область описанная вокруг выводимой строки. Инициализируется в момент вывода

    ol_Font(ol_Graphics *grm, vector<unsigned int> charCodes);
    virtual ~ol_Font();

    // Записывает в indexes номера дисплей листов соответствующих codes, а также вычисляет прямоугольную область отрисовки
    virtual void ConvertCharCodes(const wchar_t *codes, GLuint indexes[], unsigned int len, float &width, float &height);
    // проверяет в таблице символов есть ли такой. если да- возвращает тру и ширину символа в пикселях
    bool isValidCharCode(wchar_t c);
    // Продвинутая ф-я вывода. Аналог printf. Поддерживает форматированный вывод и разбор параметров а также позиционирование по ключевым точкам
    virtual void Print(float x, float y, ol_GUIPositionPoints keyPoint, const wchar_t *fmt, ...);
    // Выводит текст предварительно переведенный в индексы дисплей листов (внутренняя кодировка OLGA). Нет разбора параметров. Зато более быстрая
    void TextOut(float x, float y, GLuint lists[], unsigned short length);
    // Нечто среднее между Print и TextOut. Также выводит заранее подготовленную строку, но может её позиционировать по ключевым точкам
    void FormatedTextOut(float x, float y, ol_GLFormatedString &str, ol_GUIPositionPoints keyPoint);
    // вычисляет сдвиг в GL координатах виртуальной каретки в строке str позиции pos
    float getCaretShift(const wchar_t *str, unsigned short len, unsigned short pos);
};

/**
    --------------------------------- GUI Control ------------------------------------------
    >                                                                                      <
    >           Класс элемента управления пользовательского интерфейса.                    <
    >                   Базовый для всех элементов управления                              <
    >                                                                                      <
    ----------------------------------------------------------------------------------------
**/

class ol_GUIControl: public ol_Area2d, public ol_Animation2d
{
protected:
    ol_Graphics *graph;
    bool brdVisible;	// прорисовывать контур или нет
    bool bgrVisible;	// прорисовывать фон или нет
    bool visible;	// виден элемент или нет

    ol_Color brdColor;	// цвет рамки
    ol_Color bgColor;	// цвет градиента фона 1
    ol_Color bgColor1;	// цвет градиента фона 2

    // показывает происходит ли движение в данном фрэйме
    bool moving;
    // направление движения. Вектор, который является результирующим
    // вектором для всех сил действующих на объект. По умолчанию нулевой
    ol_Vector2df moveDir;
    
    bool justShowed; // Becomes true when Show() function is called
    
    ol_GUITheme *theme;

    void DrawBackgrnd(void);
    void DrawBorder(void);

public:
    friend class ol_GUIManager;
    ol_GUIControlStates state;
    static unsigned int guiControlsCount;
    int ID;                 // Уникальный идентификатор
    const char *name;       // Имя объекта
    // если по контролу кликнули, он запоминает координаты курсора
    // это нужно для создания в дальнейшем контроллов, которые можно двигать мышкой
    ol_Vector2df lastClickPoint;

    // Конструктор и деструктор
    ol_GUIControl(ol_Graphics *graph, ol_GUITheme *theme, ol_Vector2df origin, float width, float height, ol_GUIPositionPoints keyPoint = OL_GUI_POS_BOTLEFT, const char *name = "GUI Control");
    virtual ~ol_GUIControl();

    void setID(int id);
    void setName(const char *name);
    void showBorder(bool flag);
    void showBackground(bool flag);
    bool isVisible(void);

    bool AssignAnimation(ol_Texture tex);

    virtual void setState(ol_GUIControlStates state);
    virtual void Show(void);
    virtual void Hide(void);

    virtual void Draw(void);
    virtual void Update(void);
    virtual bool CheckEvents(ol_Event *event, ol_GUIEvent *guiEvent);

    virtual ol_Vector2df setPosition(ol_Vector2df pos, ol_GUIPositionPoints posPoint);
    // Сдвигает элемент по направлению dir. В отличие от Shift фактический сдвиг происходит только в апдэйте
    virtual void Move(ol_Vector2df dir);
};

/**
    --------------------------------- GUI Panel --------------------------------------------
    >                                                                                      <
    >                                Класс панели.                                         <
    >  Служит контейнером для других элементов управления. Берет на себя функции ГУИ       <
    >    менеджера. Вызывает Дро, Апдейт и дэлит для элементов, которые содержит.          <
    >                                                                                      <
    ----------------------------------------------------------------------------------------
**/

class ol_GUIPanel: public ol_GUIControl
{
protected:
    vector<ol_GUIControl*> items;
    unsigned int itemsCount;

    virtual void RefreshFocus(unsigned int focusedControl);
public:
    ol_GUIPanel(ol_Graphics *graph, ol_GUITheme *theme, ol_Vector2df origin, float width, float height, ol_GUIPositionPoints keyPoint = OL_GUI_POS_BOTLEFT, const char *name = "GUI Panel");
    virtual ~ol_GUIPanel();

    virtual void Show(void);
    virtual void Hide(void);

    virtual void addItem(ol_GUIControl *item);
    virtual void Draw(void);
    virtual void Update(void);
    virtual bool CheckEvents(ol_Event *event, ol_GUIEvent *guiEvent);
    virtual void Shift(ol_Vector2df &dir);
};

/**
    --------------------------------- GUI Window -------------------------------------------
    >                                                                                      <
    >                                Класс окна.                                           <
    >      По сути панель, у которой по умолчанию есть заголовок и кнопка закрыть.         <
    >                    Окно можно двигать за заголовок                                   <
    >                                                                                      <
    ----------------------------------------------------------------------------------------
**/

class ol_GUIWindow: public ol_GUIPanel
{
protected:
    ol_GUIButton *header;
    ol_GUIButton *closeButton;

public:
    ol_Area2d *clientArea;

    ol_GUIWindow(ol_Graphics *graph, ol_GUITheme *theme, ol_Vector2df origin, float width, float height, wstring caption, ol_GUIPositionPoints keyPoint = OL_GUI_POS_BOTLEFT, const char *name = "GUI Window");
    virtual ~ol_GUIWindow();

    virtual bool CheckEvents(ol_Event *event, ol_GUIEvent *guiEvent);
    virtual void Draw(void);
    virtual void Shift(ol_Vector2df &dir);
};

/**
    --------------------------------- GUI Items List ---------------------------------------
    >                                                                                      <
    >                               Список элементов.                                      <
    >      По сути панель, которая содержит набор контролов, которые можно добавлять.      <
    >                                                                                      <
    ----------------------------------------------------------------------------------------
**/

class ol_GUIItemsList: public ol_GUIPanel
{
protected:
    ol_Vector2df nextPosPoint; // точка куда будет добавлен следующий элемент. Можно было бы вычислять динамически, но постигла ЖОПА!
public:
    ol_GUIItemsList(ol_Graphics *graph, ol_GUITheme *theme, ol_Vector2df origin, float width, float height, ol_GUIPositionPoints keyPoint = OL_GUI_POS_BOTLEFT, const char *name = "GUI Items List");
    virtual ~ol_GUIItemsList();

    virtual void addItem(ol_GUIControl *item);
};

/**
    --------------------------------- GUI File Browser -------------------------------------
    >                                                                                      <
    >                               Проводник файлов.                                      <
    >      Выполнен на основе Items List. Позволяет перемещаться по файловой системе.      <
    >                                                                                      <
    ----------------------------------------------------------------------------------------
**/

class ol_GUIFileBrowser: public ol_GUIItemsList
{
protected:
    vector<ol_FileNames> files;
    vector<string> curPath;
    string curFile; // имя текущего выбранного файла
    int FindControl(ol_GUIControl *ctrl);
    bool showHidden;
    float scrollVel;

public:
    ol_GUIFileBrowser(ol_Graphics *graph, ol_GUITheme *theme, ol_Vector2df origin, float width, float height, ol_GUIPositionPoints keyPoint = OL_GUI_POS_BOTLEFT, const char *name = "GUI File Browser");
    virtual ~ol_GUIFileBrowser();

    string getCurrentPath(void);
    string getCurrentFile(void);
    void getFilesList(vector<string> &fnames);
    virtual void BuildFilesList(void);
    virtual bool CheckEvents(ol_Event *event, ol_GUIEvent *guiEvent);
};

/**
    --------------------------------- GUI File Dialog --------------------------------------
    >                                                                                      <
    >                       Диалог открытия/сохранения файлов.                             <
    >                                                                                      <
    ----------------------------------------------------------------------------------------
**/

class ol_GUIFileDialog: public ol_GUIWindow
{
protected:
    ol_GUIFileDialogType dialogType;

    ol_GUIControl *callerCtrl;   // контрол, который вызвал диалог
    ol_GUILabeledEditBox *pathEdit;
    ol_GUIEditBox *fnameEdit;
    ol_GUIButton *okButton;
    ol_GUIFileBrowser *browser;

public:
    ol_GUIFileDialog(ol_Graphics *graph, ol_GUITheme *theme, ol_Vector2df origin, float width, float height, wstring caption, ol_GUIFileDialogType type, ol_GUIPositionPoints keyPoint = OL_GUI_POS_BOTLEFT, const char *name = "GUI File Dialog");
    virtual ~ol_GUIFileDialog();

    // возвращает путь к текущей директории
    string getPath(void);
    // возвращает имя выбранного файла. В случае когда full = true, полное имя файла
    string getFileName(bool full);
    // сохраняет в fnames список всех файлов в директории и возвращает тру, если таковые имеются.
    bool getAllFiles(bool full, vector<string> &fnames);

    virtual bool CheckEvents(ol_Event *event, ol_GUIEvent *guiEvent);

    virtual void Show(ol_GUIControl *caller);
    ol_GUIControl *getCaller(void);
};

/**
    --------------------------------- GUI Label --------------------------------------------
    >                                                                                      <
    >                            Статическая надпись.                                      <
    >         Хранит текст и постоянно его выводит максимально быстрым способом.           <
    >                                                                                      <
    ----------------------------------------------------------------------------------------
**/

class ol_GUILabel: public ol_GUIControl
{
private:
    wstring text;           		// строка которую выводит метка
    vector<GLuint> dlists;  		// списки отображения соответствующий символам строки
    unsigned short length;  		// количество символов
    ol_Vector2df origin;            	// координата позиционирования
    ol_GUIPositionPoints posPoint;  	// точка относительно которой позиционируется метка

    ol_Font *font;

public:
    ol_Color color;          // цвет метки

    ol_GUILabel(ol_Graphics *graph, ol_GUITheme *theme, ol_Vector2df origin, float width, float height, wstring text, ol_GUIPositionPoints keyPoint = OL_GUI_POS_BOTLEFT, const char *name = "GUI Label");
    virtual ~ol_GUILabel();

    virtual bool CheckEvents(ol_Event *event, ol_GUIEvent *guiEvent);
    virtual void Draw(void);
    void setText(wstring text);
};

/**
    --------------------------------- GUI Button -------------------------------------------
    >                                                                                      <
    >                           Класс простейшей кнопки.                                   <
    >         Генерирует Баттон_Клик, если нажата, а затем отжата левая клавиша мыши.      <
    >                                                                                      <
    ----------------------------------------------------------------------------------------
**/

class ol_GUIButton: public ol_GUIControl
{
protected:
    ol_GUILabel *caption;            // заголовок кнопки
    ol_Vector2df capPos;             // точка позиционирования заголовка
    ol_GUIPositionPoints capAlign;   // каким местом метка должна встать на capPos
    ol_Vector2df vPressed;           // точка в которую перемещается надпись при нажатии
    ol_GUIButtonStyle style;         // стиль кнопки. Определяет поведения и способ отрисовки

public:
    ol_Color labStandardColor;       // цвет заголовка кнопки в обычном состоянии
    ol_Color labSelectedColor;       // цвет заголовка кнопки в выделенном состоянии

    ol_GUIButton(ol_Graphics *graph, ol_GUITheme *theme, ol_Vector2df origin, float width, float height, wstring caption, ol_GUIButtonStyle style = OL_GUI_BUTTON_STANDARD, ol_GUIPositionPoints keyPoint = OL_GUI_POS_BOTLEFT, const char *name = "GUI Button");
    virtual ~ol_GUIButton();

    bool isPressed(void);
    virtual bool CheckEvents(ol_Event *event, ol_GUIEvent *guiEvent);
    virtual void Draw(void);
    virtual void Shift(ol_Vector2df &dir);
    virtual void setState(ol_GUIControlStates state);
    virtual void setStyle(ol_GUIButtonStyle style);
    virtual void setCaption(wstring text);
    virtual void setCaption(string text);
    virtual void setCaption(int val);
    virtual void setCaption(double val);
};

/**
    --------------------------------- GUI EditBox ------------------------------------------
    >                                                                                      <
    >                              Класс поля ввода.                                       <
    >                                                                                      <
    ----------------------------------------------------------------------------------------
**/

class ol_GUIEditBox: public ol_GUIControl
{
protected:
    wstring text;                // текст выводимый эдитом
    ol_Font *font;                // указатель на шрифт для печати
    ol_GUIMargins mrg;            // отступы. используются для инициализации области печати
    ol_Area2d printArea;          // область печати
    ol_Vector2df printOrigin;     // координаты точки относительно которой печатается текст

    ol_TimeMonitor *caretBlinkPer;   // засечки времени для мигания каретки
    bool caretBlink;                // когда тру, каретка рисуется. меняется каждую засечку времени
    unsigned short caretPos;        // на каком символе находится каретка
    float caretShift;               // x координата каретки в абсолютных GL координатах
    float caretShift_last;          // предыдущее положение каретки. Нужно чтобы узнать на сколько произошел сдвиг

    // расчитывает положение каретки и корректирует положение текста относительно эдита
    // isBackSpase нужна потому что при нажатии Backspase нужен дополнительный просчет
    void adjustCaret(bool isBackSpase);

public:
    ol_GUIEditBox(ol_Graphics *graph, ol_GUITheme *theme, ol_Vector2df origin, float width, float height, ol_GUIPositionPoints keyPoint = OL_GUI_POS_BOTLEFT, const char *name = "GUI EditBox");
    virtual ~ol_GUIEditBox();

    virtual bool CheckEvents(ol_Event *event, ol_GUIEvent *guiEvent);
    virtual void Draw(void);
    virtual void setState(ol_GUIControlStates state);
    virtual void Shift(ol_Vector2df &dir);

    void setText(wstring text);
    void setText(string text);
    void setText(int val);
    void setText(double val);
    wstring getText(void);
    string getStringText(void);
    double getDoubleVal(void);
    int getIntVal(void);
};

/**
    ------------------------------ GUI Labeled EditBox -------------------------------------
    >                                                                                      <
    >                      Класс поля ввода с готовой подписью.                            <
    >                                                                                      <
    ----------------------------------------------------------------------------------------
**/

class ol_GUILabeledEditBox: public ol_GUIEditBox
{
protected:
    ol_GUILabel *caption;
public:
    ol_GUILabeledEditBox(ol_Graphics *graph, ol_GUITheme *theme, ol_Vector2df origin, float width, float height, wstring caption, ol_GUIPositionPoints labelPoint, ol_GUIPositionPoints keyPoint = OL_GUI_POS_BOTLEFT, const char *name = "GUI Labeled Edit");
    virtual ~ol_GUILabeledEditBox();

    virtual void Draw(void);
    virtual ol_Vector2df setPosition(ol_Vector2df pos, ol_GUIPositionPoints posPoint);
    virtual void Shift(ol_Vector2df &dir);
};

/**
    ------------------------------ GUI Text Box --------------------------------------------
    >                                                                                      <
    >                 Многострочное поле вывода с переносом строк.                         <
    >                                                                                      <
    ----------------------------------------------------------------------------------------
**/

class ol_GUITextBox: public ol_GUIControl
{
protected:
    ol_Font *font;
    vector<ol_GLString> lines;

    ol_Vector2df caret;  // точка относительно которой выводится строка (левый нижний угол как правило)
    float caretStep;    // шаг сдвига каретки
    float scroll;       // сдвиг всего текста
    float lineInterval; // межстрочный интервал

    void ResetCaret(void); // сбрасывает положение каретки в начальное с учетом скрола

public:
    ol_GUITextBox(ol_Graphics *graph, ol_GUITheme *theme, ol_Vector2df origin, float width, float height, ol_GUIPositionPoints keyPoint = OL_GUI_POS_BOTLEFT, const char *name = "GUI Text Box");
    virtual ~ol_GUITextBox();

    virtual void Draw(void);
    void addLine(wstring text);  // просто добавляет строку без переноса слов
    void addText(wstring text);  // добавляет текст с переносом
    void setLineInterval(float interval);
};

/**
    ------------------------------ GUI Scroll Box ------------------------------------------
    >                                                                                      <
    >       Полоса прокрутки. Позволяет плавно менять значения в заданном диапазоне.       <
    >       Всегда знает на сколько % относительно своей длины сдвинут ползунок.           <
    >                                                                                      <
    ----------------------------------------------------------------------------------------
**/

class ol_GUIScrollBox: public ol_GUIPanel
{
protected:
    ol_GUIScrollBoxType type;

    ol_GUIButton *scroller;

    float l100;         // длина в пикселях при 100% заполнении скрола
    float range;        // диапазон изменения значений (max - min)
    float scrollVel;    // скорость прокрутки при прокручивании скроллером

    // диапазон изменения значения
    float minVal;   // минимальное значение
    float maxVal;   // максимальное значение
    float curVal;   // текущее значение, изменяемое ползунком
    float curPer;   // текущее значение в процентах

    // Устанавливает положение ползунка, если значение скроллера изменено через setValue или setPersentage
    void RepositionScroller(void);

public:
    ol_GUIScrollBox(ol_Graphics *graph, ol_GUITheme *theme, ol_Vector2df origin, float width, float height, ol_GUIScrollBoxType type, ol_GUIPositionPoints keyPoint = OL_GUI_POS_BOTLEFT, const char *name = "GUI Scroll Box");
    virtual ~ol_GUIScrollBox();

    virtual bool CheckEvents(ol_Event *event, ol_GUIEvent *guiEvent);

    void setValue(float val);
    void setMinimum(float minVal);
    void setMaximum(float maxVal);
    void setPercentage(float per);

    float getValue(void);
    float getMinimum(void);
    float getMaximum(void);
    float getPercentage(void);
};

/**
    ------------------------------ GUI Edit Scroll Box -------------------------------------
    >                                                                                      <
    >              Составной контрол. Состоит из скроллера и эдита.                        <
    >               Можно менять значения и скроллером и эдитом.                           <
    >                                                                                      <
    ----------------------------------------------------------------------------------------
**/

class ol_GUIEdScrollBox: public ol_GUIPanel
{
protected:
    ol_GUIScrollBoxType type;

    ol_GUIScrollBox *scroll;
    ol_GUIEditBox *edit;
    ol_GUILabel *caption;

public:
    /*
        edSize - это размер эдит бокса в пикселях по ширине в случае горизонтального скролбокса и по высоте в случае вертикального
        scSize - это размер скрол бокса в пикселях по высоте в случае горизонтального скролбокса и по ширине в случае вертикального
        width, height - это размерности всего контрола(описывающая рамка). Если скрол или эдит не помещаются - их размеры будут уменьшены
    */
    ol_GUIEdScrollBox(ol_Graphics *graph, ol_GUITheme *theme, ol_Vector2df origin, float width, float height, float scSize, float edSize, ol_GUIScrollBoxType type, ol_GUIEdScrollPos relPos, ol_GUIPositionPoints keyPoint = OL_GUI_POS_BOTLEFT, const char *name = "GUI Edit Scroll Box");
    virtual ~ol_GUIEdScrollBox();
    virtual bool CheckEvents(ol_Event *event, ol_GUIEvent *guiEvent);

    void setLabel(wstring text, ol_GUIPositionPoints point);

    void setValue(float val);
    void setMinimum(float minVal);
    void setMaximum(float maxVal);
    void setPercentage(float per);

    float getValue(void);
    float getMinimum(void);
    float getMaximum(void);
    float getPercentage(void);
};

/**
    ---------------------------------- GUI Image -------------------------------------------
    >                                                                                      <
    >             Класс изображения. Просто постоянно отображает картинку                  <
    >       Фактически является базовым гуи контролом с минимальными доработками:          <
    >                    по умолчанию загружается анимация с 1 кадром.                     <
    >                                                                                      <
    ----------------------------------------------------------------------------------------
**/

class ol_GUIImage: public ol_GUIControl
{
protected:
public:
    ol_GUIImage(ol_Graphics *graph, ol_GUITheme *theme, ol_Vector2df origin, float width, float height, ol_Texture tex, ol_GUIPositionPoints keyPoint = OL_GUI_POS_BOTLEFT, const char *name = "GUI Image");
    virtual ~ol_GUIImage();

    virtual void Update(void);
};

/**
    --------------------------------- GUI Diagram ------------------------------------------
    >                                                                                      <
    >                    Класс для отображения графиков функций.                           <
    >                                                                                      <
    ----------------------------------------------------------------------------------------
**/

// Шаблоны для стилей отображения диаграмм
enum ol_DiagDotStyle
{
    OL_DIAG_SIMPLE,    // просто линия
    OL_DIAG_TIRANGLES, // треугольники в вершинах
    OL_DIAG_QUADS,     // квадраты
    OL_DIAG_CIRCLES    // окружности
};

// Стили отрисовки линии графика диаграммы
enum ol_DiagLineStyle
{
    OL_DIAG_LINE_SOLID   = 0xffff,    // сплошная
    OL_DIAG_LINE_DOTTED  = 0x0f0f,    // точечная
    OL_DIAG_LINE_DASHED  = 0x00ff,    // пунктирная
    OL_DIAG_LINE_DASHDOT = 0xf0ff     // штрихпунктирная
};

class ol_GUIDiagram: public ol_GUIControl
{
private:
    ol_Font *font;     // Шрифт отрисовки всех надписей в диаграммы

    struct Diagram    // содержит настройки конкретной диаграммы
    {
        vector<ol_Vector2dld> func;      // реальные значения точек диаграммы
        ol_Color color;                  // цвет
        GLfloat lineThickness;          // толщина линии отрисовки диаграммы
        ol_DiagDotStyle dotStyle;        // как отображать узловые точки графика
        ol_DiagLineStyle lineStyle;      // стиль отображения линии графика
    };

    struct Axes
    {
        ol_Vector2dld v1, v2;                // координаты вершин оси для отрисовки
        vector<long double> divs;           // деления оси. В зависимости от типа, X или Y координаты рисок на шкале
        vector<ol_GLFormatedString> text;    // Подпись каждой отдельной риски в кодировке энджина
        unsigned int divNum;                // количество делений шкалы
        float        divLen;                // размер рисок делений шкалы
        ol_RelPos valPos;                    // расположение подписи деления
        ol_Vector2df divDir;                 // вектор, показывающий в каком направлении нужно рисовать риску от точки divs[i] размером divLen
        ol_AxisType type;                    // тип оси. Какие значения она показывает
        ol_Color color;                      // цвет
    };

    vector<Diagram> diagrams;      // Графики функций для отображения

    Axes xAxis;                         // Шкала X
    Axes yAxis;                         // Шкала Y

    ol_GUIMargins mrg;                   // отступы области отображения от края самого элемента управления
    ol_Area2d drawArea;                  // непосредственно область отображения графика
    ol_Vector2dld zero;                  // начало координат (в GL координатах)
    ol_Vector2dld axisCross;             // точка пересечения осей координат (в GL координатах)
    ol_Vector2dld crosshair;             // точка перекрестья для определения значения в любой точке системы координат. Наводится мышкой
    bool needRedrawCross;               // становится тру, если наведена мышка
    ol_Vector2dld crossVal;              // значение точки в системе координат, на которое указывает crosshair

    ol_Vector2dld k;                      // коэффициент калибровки
    ol_Vector2dld Min, Max;               // вспомогательные переменные, содержат самые мин. и макс. значения всех значений всех диаграмм
    ol_Vector2dld funcMin, funcMax;       // минимальные и максимальные значения точек графиков
    ol_Vector2dld fixedMin, fixedMax;     // граничные значения самой системы координат. Используются в отсутствии диаграмм. Сохраняются, если
    // диапазон значений диаграммы находится внутри fixedMin, fixedMax

    void DrawDiagram(Diagram diag);
    void DrawAxis(Axes &ax);    // отрисовка шкал
    void DrawZeroAxis(void);    // отрисовка пунктиром осей координат

    void InitDrawArea(void);    // Пересчитывает область отрисовки на основе отступов (mrg)

    void Calibrate(void);               // калибровка. Определяет коэффициент калибровки и реальные координаты начала координат
    void CalcAxis(void);                // расчитывает GL координаты точек осей координат и их делений
    void CalcAxisDivisions(Axes &ax);   // расчет градуировки оси

    void FindMinMax(vector<ol_Vector2dld> &func, ol_Vector2dld &Min, ol_Vector2dld &Max); // возвращает мин. и макс. значения ф-ии и аргумента func
    void RecalcMinMax(void);            // производит перекалибровку по минимуму и максимуму

public:
    bool needFixedScale; 	// если true, диаграмма не система координат не масштабируется автоматически

    // Конструктор и деструктор
    ol_GUIDiagram(ol_Graphics *graph, ol_GUITheme *theme, ol_Vector2df origin, float width, float height, ol_GUIPositionPoints keyPoint = OL_GUI_POS_BOTLEFT, const char *name = "GUI Diagram");
    virtual ~ol_GUIDiagram();

    virtual ol_Vector2df setPosition(ol_Vector2df pos, ol_GUIPositionPoints posPoint);
    virtual void Shift(ol_Vector2df &dir);
    virtual void Draw(void);
    virtual bool CheckEvents(ol_Event *event, ol_GUIEvent *guiEvent);

    // добавляет диаграмму к отображению и возвращает её ID
    int AddFunc(vector<ol_Vector2dld> func, ol_Color color = OL_COLOR_BLACK, float lineThickness = 1.0f, ol_DiagLineStyle lineStyle = OL_DIAG_LINE_SOLID, ol_DiagDotStyle dotStyle = OL_DIAG_SIMPLE);
    void setPoints(int ID, vector<ol_Vector2dld> &points);
    void ClearAll(void);
    void setScale(long double minX, long double maxX, long double minY, long double maxY);
    void setAxisDivisionsNum(ol_AxisType type, unsigned int num);
};

/**
    --------------------------------- GUI Manager ------------------------------------------
    >                                                                                      <
    >     Класс управляющий всеми шрифтами и элементами управления польз. интерфейса.      <
    >                                                                                      <
    ----------------------------------------------------------------------------------------
**/

class ol_GUIManager
{
private:
    ol_Font *sysFont;       // Стандартный шрифт
    FT_Library  ft_lib;    // Объект фритайпа для создания шрифтов
    ol_Graphics *graph;

    unsigned int controlsCount;

    list<ol_GUIControl*> controls;
    vector<ol_SimpleFont*> fonts;
    vector<ol_GUITheme*> themes;

    ol_GUIEvent *guiEvent;

public:
    ol_Area2d winArea;      // клиентская часть окна

    ol_GUIManager(ol_Graphics *graph);
    ~ol_GUIManager();

    void DrawAll(void);
    void UpdateAll(void);
    ol_GUIEvent* CheckGUIEvents(ol_Event *event);

    ol_GUIEvent* getLastEvent(void);

    ol_SimpleFont* CreateSimpleFont(const char *fileName, unsigned int height);
    ol_Font* CreateFont(const char *fileName, unsigned int height, vector<unsigned int> codes);
    ol_GUITheme* getDefaultTheme(void);
    bool InitDefaultTheme(const char *fileName);
    // Инициализирует winArea для того чтобы ГУИ менеджер всегда знал точные координаты углов и центра клиентской части окна
    void RefreshWindowArea(float width, float height);
    // просто добавляет готовый указатель в список
    void addControl(ol_GUIPanel *parent, ol_GUIControl *control);
    // Totally deletes an object. Its destructor is called. Memory is freed.
    void delControl(ol_GUIControl *control);
    // выделяют память под заданный тип элемента и добавляют его в список
    ol_GUIControl*           addGUIControl(ol_GUIPanel *parent, ol_Vector2df origin, float width, float height, ol_GUIPositionPoints keyPoint = OL_GUI_POS_BOTLEFT, const char *name = "GUI Control");
    ol_GUIPanel*             addGUIPanel(ol_GUIPanel *parent, ol_Vector2df origin, float width, float height, ol_GUIPositionPoints keyPoint = OL_GUI_POS_BOTLEFT, const char *name = "GUI Panel");
    ol_GUIImage*             addGUIImage(ol_GUIPanel *parent, ol_Vector2df origin, float width, float height, ol_Texture tex, ol_GUIPositionPoints keyPoint = OL_GUI_POS_BOTLEFT, const char *name = "GUI Image");
    ol_GUIWindow*            addGUIWindow(ol_Vector2df origin, float width, float height, wstring caption, ol_GUIPositionPoints keyPoint = OL_GUI_POS_BOTLEFT, const char *name = "GUI Window");
    ol_GUIFileBrowser*       addGUIFileBrowser(ol_GUIPanel *parent, ol_Vector2df origin, float width, float height, ol_GUIPositionPoints keyPoint = OL_GUI_POS_BOTLEFT, const char *name = "GUI File Browser");
    ol_GUIFileDialog*        addGUIFileDialog(ol_GUIFileDialogType type);
    ol_GUIDiagram*           addGUIDiagram(ol_GUIPanel *parent, ol_Vector2df origin, float width, float height, ol_GUIPositionPoints keyPoint = OL_GUI_POS_BOTLEFT, const char *name = "GUI Diagram");
    ol_GUILabel*             addGUILabel(ol_GUIPanel *parent, ol_Vector2df origin, wstring text = L"Label", ol_GUIPositionPoints keyPoint = OL_GUI_POS_BOTLEFT, const char *name = "GUI Label");
    ol_GUIButton*            addGUIButton(ol_GUIPanel *parent, ol_Vector2df origin, float width, float height, wstring caption = L"Button", ol_GUIButtonStyle style = OL_GUI_BUTTON_STANDARD, ol_GUIPositionPoints keyPoint = OL_GUI_POS_BOTLEFT, const char *name = "GUI Button");
    ol_GUIEditBox*           addGUIEditBox(ol_GUIPanel *parent, ol_Vector2df origin, float width, float height, ol_GUIPositionPoints keyPoint = OL_GUI_POS_BOTLEFT, const char *name = "GUI Edit Box");
    ol_GUILabeledEditBox*    addGUILabeledEditBox(ol_GUIPanel *parent, ol_Vector2df origin, float width, float height, wstring caption, ol_GUIPositionPoints labelPoint, ol_GUIPositionPoints keyPoint = OL_GUI_POS_BOTLEFT, const char *name = "GUI labeled Edit");
    ol_GUITextBox*           addGUITextBox(ol_GUIPanel *parent, ol_Vector2df origin, float width, float height, ol_GUIPositionPoints keyPoint = OL_GUI_POS_BOTLEFT, const char *name = "GUI Text Box");
    ol_GUIScrollBox*         addGUIScrollBox(ol_GUIPanel *parent, ol_Vector2df origin, float width, float height, ol_GUIScrollBoxType type, ol_GUIPositionPoints keyPoint = OL_GUI_POS_BOTLEFT, const char *name = "GUI Scroll Box");
    ol_GUIEdScrollBox*       addGUIEdScrollBox(ol_GUIPanel *parent, ol_Vector2df origin, float width, float height, float scSize, float edSize, ol_GUIScrollBoxType type, ol_GUIEdScrollPos relPos, ol_GUIPositionPoints keyPoint = OL_GUI_POS_BOTLEFT, const char *name = "GUI Edit Scroll Box");
};

#endif // OL_GUI_H_INCLUDED
