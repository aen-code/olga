/**
------------------------------------------------------------------------
  This file is part of the OLGA project.
  Copyright © 2017 by Enver Akhmedov.
  E-mail: aen-code@yandex.ru
  For conditions of distribution and use, see copyright notice in OLGA.h
------------------------------------------------------------------------
**/

#include "ol_Frustum.h"

void NormalizePlane(float frustum[6][4], int side)
{
    // Here we calculate the magnitude of the normal to the plane (point A B C)
    // Remember that (A, B, C) is that same thing as the normal's (X, Y, Z).
    // To calculate magnitude you use the equation:  magnitude = sqrt( x^2 + y^2 + z^2)
    float magnitude = (float)sqrt( frustum[side][A] * frustum[side][A] +
                                   frustum[side][B] * frustum[side][B] +
                                   frustum[side][C] * frustum[side][C] );

    // Then we divide the plane's values by it's magnitude.
    // This makes it easier to work with.
    frustum[side][A] /= magnitude;
    frustum[side][B] /= magnitude;
    frustum[side][C] /= magnitude;
    frustum[side][D] /= magnitude;
}

void ol_Frustum::CalculateFrustum()
{
    float   proj[16];	// This will hold our projection matrix
    float   modl[16];	// This will hold our modelview matrix
    float   clip[16];	// This will hold the clipping planes

    // glGetFloatv() is used to extract information about our OpenGL world.
    // Below, we pass in GL_PROJECTION_MATRIX to abstract our projection matrix.
    // It then stores the matrix into an array of [16].
    glGetFloatv( GL_PROJECTION_MATRIX, proj );

    // By passing in GL_MODELVIEW_MATRIX, we can abstract our model view matrix.
    // This also stores it in an array of [16].
    glGetFloatv( GL_MODELVIEW_MATRIX, modl );

    // Now that we have our modelview and projection matrix, if we combine these 2 matrices,
    // it will give us our clipping planes.  To combine 2 matrices, we multiply them.

    clip[ 0] = modl[ 0] * proj[ 0] + modl[ 1] * proj[ 4] + modl[ 2] * proj[ 8] + modl[ 3] * proj[12];
    clip[ 1] = modl[ 0] * proj[ 1] + modl[ 1] * proj[ 5] + modl[ 2] * proj[ 9] + modl[ 3] * proj[13];
    clip[ 2] = modl[ 0] * proj[ 2] + modl[ 1] * proj[ 6] + modl[ 2] * proj[10] + modl[ 3] * proj[14];
    clip[ 3] = modl[ 0] * proj[ 3] + modl[ 1] * proj[ 7] + modl[ 2] * proj[11] + modl[ 3] * proj[15];

    clip[ 4] = modl[ 4] * proj[ 0] + modl[ 5] * proj[ 4] + modl[ 6] * proj[ 8] + modl[ 7] * proj[12];
    clip[ 5] = modl[ 4] * proj[ 1] + modl[ 5] * proj[ 5] + modl[ 6] * proj[ 9] + modl[ 7] * proj[13];
    clip[ 6] = modl[ 4] * proj[ 2] + modl[ 5] * proj[ 6] + modl[ 6] * proj[10] + modl[ 7] * proj[14];
    clip[ 7] = modl[ 4] * proj[ 3] + modl[ 5] * proj[ 7] + modl[ 6] * proj[11] + modl[ 7] * proj[15];

    clip[ 8] = modl[ 8] * proj[ 0] + modl[ 9] * proj[ 4] + modl[10] * proj[ 8] + modl[11] * proj[12];
    clip[ 9] = modl[ 8] * proj[ 1] + modl[ 9] * proj[ 5] + modl[10] * proj[ 9] + modl[11] * proj[13];
    clip[10] = modl[ 8] * proj[ 2] + modl[ 9] * proj[ 6] + modl[10] * proj[10] + modl[11] * proj[14];
    clip[11] = modl[ 8] * proj[ 3] + modl[ 9] * proj[ 7] + modl[10] * proj[11] + modl[11] * proj[15];

    clip[12] = modl[12] * proj[ 0] + modl[13] * proj[ 4] + modl[14] * proj[ 8] + modl[15] * proj[12];
    clip[13] = modl[12] * proj[ 1] + modl[13] * proj[ 5] + modl[14] * proj[ 9] + modl[15] * proj[13];
    clip[14] = modl[12] * proj[ 2] + modl[13] * proj[ 6] + modl[14] * proj[10] + modl[15] * proj[14];
    clip[15] = modl[12] * proj[ 3] + modl[13] * proj[ 7] + modl[14] * proj[11] + modl[15] * proj[15];

    // Now we actually want to get the sides of the frustum.  To do this we take
    // the clipping planes we received above and extract the sides from them.

    // This will extract the RIGHT side of the frustum
    frustum[OL_FRUSTUM_RIGHT][A] = clip[ 3] - clip[ 0];
    frustum[OL_FRUSTUM_RIGHT][B] = clip[ 7] - clip[ 4];
    frustum[OL_FRUSTUM_RIGHT][C] = clip[11] - clip[ 8];
    frustum[OL_FRUSTUM_RIGHT][D] = clip[15] - clip[12];

    // Now that we have a normal (A,B,C) and a distance (D) to the plane,
    // we want to normalize that normal and distance.

    // Normalize the RIGHT side
    NormalizePlane(frustum, OL_FRUSTUM_RIGHT);

    // This will extract the LEFT side of the frustum
    frustum[OL_FRUSTUM_LEFT][A] = clip[ 3] + clip[ 0];
    frustum[OL_FRUSTUM_LEFT][B] = clip[ 7] + clip[ 4];
    frustum[OL_FRUSTUM_LEFT][C] = clip[11] + clip[ 8];
    frustum[OL_FRUSTUM_LEFT][D] = clip[15] + clip[12];

    // Normalize the LEFT side
    NormalizePlane(frustum, OL_FRUSTUM_LEFT);

    // This will extract the BOTTOM side of the frustum
    frustum[OL_FRUSTUM_BOTTOM][A] = clip[ 3] + clip[ 1];
    frustum[OL_FRUSTUM_BOTTOM][B] = clip[ 7] + clip[ 5];
    frustum[OL_FRUSTUM_BOTTOM][C] = clip[11] + clip[ 9];
    frustum[OL_FRUSTUM_BOTTOM][D] = clip[15] + clip[13];

    // Normalize the BOTTOM side
    NormalizePlane(frustum, OL_FRUSTUM_BOTTOM);

    // This will extract the TOP side of the frustum
    frustum[OL_FRUSTUM_TOP][A] = clip[ 3] - clip[ 1];
    frustum[OL_FRUSTUM_TOP][B] = clip[ 7] - clip[ 5];
    frustum[OL_FRUSTUM_TOP][C] = clip[11] - clip[ 9];
    frustum[OL_FRUSTUM_TOP][D] = clip[15] - clip[13];

    // Normalize the TOP side
    NormalizePlane(frustum, OL_FRUSTUM_TOP);

    // This will extract the BACK side of the frustum
    frustum[OL_FRUSTUM_BACK][A] = clip[ 3] - clip[ 2];
    frustum[OL_FRUSTUM_BACK][B] = clip[ 7] - clip[ 6];
    frustum[OL_FRUSTUM_BACK][C] = clip[11] - clip[10];
    frustum[OL_FRUSTUM_BACK][D] = clip[15] - clip[14];

    // Normalize the BACK side
    NormalizePlane(frustum, OL_FRUSTUM_BACK);

    // This will extract the FRONT side of the frustum
    frustum[OL_FRUSTUM_FRONT][A] = clip[ 3] + clip[ 2];
    frustum[OL_FRUSTUM_FRONT][B] = clip[ 7] + clip[ 6];
    frustum[OL_FRUSTUM_FRONT][C] = clip[11] + clip[10];
    frustum[OL_FRUSTUM_FRONT][D] = clip[15] + clip[14];

    // Normalize the FRONT side
    NormalizePlane(frustum, OL_FRUSTUM_FRONT);
}

bool ol_Frustum::PointInFrustum(ol_Vector3d p)
{
    // Go through all the sides of the frustum
    for(int i = 0; i < 6; i++ )
    {
        // Calculate the plane equation and check if the point is behind a side of the frustum
        if(frustum[i][A] * p.x + frustum[i][B] * p.y + frustum[i][C] * p.z + frustum[i][D] <= 0)
        {
            // The point was behind a side, so it ISN'T in the frustum
            return false;
        }
    }

    // The point was inside of the frustum (In front of ALL the sides of the frustum)
    return true;
}

bool ol_Frustum::SphereInFrustum( float x, float y, float z, float radius )
{
    // Go through all the sides of the frustum
    for(int i = 0; i < 6; i++ )
    {
        // If the center of the sphere is farther away from the plane than the radius
        if( frustum[i][A] * x + frustum[i][B] * y + frustum[i][C] * z + frustum[i][D] <= -radius )
        {
            // The distance was greater than the radius so the sphere is outside of the frustum
            return false;
        }
    }

    // The sphere was inside of the frustum!
    return true;
}

bool ol_Frustum::CubeInFrustum( float x, float y, float z, float size )
{
    for(int i = 0; i < 6; i++ )
    {
        if(frustum[i][A] * (x - size) + frustum[i][B] * (y - size) + frustum[i][C] * (z - size) + frustum[i][D] > 0)
            continue;
        if(frustum[i][A] * (x + size) + frustum[i][B] * (y - size) + frustum[i][C] * (z - size) + frustum[i][D] > 0)
            continue;
        if(frustum[i][A] * (x - size) + frustum[i][B] * (y + size) + frustum[i][C] * (z - size) + frustum[i][D] > 0)
            continue;
        if(frustum[i][A] * (x + size) + frustum[i][B] * (y + size) + frustum[i][C] * (z - size) + frustum[i][D] > 0)
            continue;
        if(frustum[i][A] * (x - size) + frustum[i][B] * (y - size) + frustum[i][C] * (z + size) + frustum[i][D] > 0)
            continue;
        if(frustum[i][A] * (x + size) + frustum[i][B] * (y - size) + frustum[i][C] * (z + size) + frustum[i][D] > 0)
            continue;
        if(frustum[i][A] * (x - size) + frustum[i][B] * (y + size) + frustum[i][C] * (z + size) + frustum[i][D] > 0)
            continue;
        if(frustum[i][A] * (x + size) + frustum[i][B] * (y + size) + frustum[i][C] * (z + size) + frustum[i][D] > 0)
            continue;

        // If we get here, it isn't in the frustum
        return false;
    }

    return true;
}
