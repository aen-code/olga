/**
------------------------------------------------------------------------
  This file is part of the OLGA project.
  Copyright © 2017 by Enver Akhmedov.
  E-mail: aen-code@yandex.ru
  For conditions of distribution and use, see copyright notice in OLGA.h
------------------------------------------------------------------------
**/

#ifndef OL_BASIC_SHAPES_H_INCLUDED
#define OL_BASIC_SHAPES_H_INCLUDED

#include <vector>
#include "ol_Geometry.h"
#include "ol_GUI.h"

using namespace std;

// Класс для создания геометрических примитивов. Результат возвращается в виде стандартного ol_Mesh3D
class ol_ShapeGenerator
{
private:
    ol_Vector3d tmp;     // буфер для вершины
    ol_Face tmf;         // буфер для полигона
    ol_Vector2df tmt;    // буфер для текстурной координаты
    vector<ol_Vector3d> points;      // временный массив вершин
    vector<ol_Face> faces;           // временный массив полигонов
    vector<ol_Vector2df> texcoords;  // временный массив текстурных координат

    // Очищает все вспомогательные массивы
    void Refresh(void);
    // Инициализирует pMesh
    void InitMesh(ol_Mesh3D *pMesh);

public:
    ol_ShapeGenerator() {}
    ~ol_ShapeGenerator() {}

	// Generates flat quad in 3D space.
    ol_Mesh3D* CreateQuad3D(float side);
	// Generates flat rectangle in 3d space.
    ol_Mesh3D* CreateRectangle3d(float width, float length);
    // Generates cube with texture mapping for a sky box.
    ol_Mesh3D* CreateSBCube(float side);
    // Generates sphere.
    ol_Mesh3D* CreateSphere(float r, unsigned int  n);
};
#endif // OL_BASIC_SHAPES_H_INCLUDED
