/**
------------------------------------------------------------------------
  This file is part of the OLGA project.
  Copyright © 2017 by Enver Akhmedov.
  E-mail: aen-code@yandex.ru
  For conditions of distribution and use, see copyright notice in OLGA.h
------------------------------------------------------------------------
**/

#include "ol_Math.h"

unsigned int Next_P2(unsigned int a)
{
    unsigned int rval = 1;
    while(rval<a) rval<<=1;

    return rval;
}
