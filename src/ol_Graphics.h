/**
------------------------------------------------------------------------
  This file is part of the OLGA project.
  Copyright © 2017 by Enver Akhmedov.
  E-mail: aen-code@yandex.ru
  For conditions of distribution and use, see copyright notice in OLGA.h
------------------------------------------------------------------------
**/

#ifndef OL_GRAPHICS_H_INCLUDED
#define OL_GRAPHICS_H_INCLUDED

#include <GL/gl.h>
#include <GL/glu.h>
#include <map>
#include "ol_Vector_Math.h"
#include "ol_File_System.h"
#include "ol_Resource.h"

/**
    ------------------------------------- Color --------------------------------------------
    >                                                                                      <
    >  Хранит r, g, b, a составляющие цвета. Позволяет задавать цвет целочисленными        <
    >  значениями (от 0 до 255) и вещественными значениями (от 0 до 1). Имеет              <
    >  перегруженный оператор присваивания.                                                <
    >                                                                                      <
    ----------------------------------------------------------------------------------------
**/

class ol_Color
{
public:
    float r, g, b, a;

    ol_Color() {}

    ol_Color(float r, float g, float b, float a = 1.0f)
    {
        this->r = r;
        this->g = g;
        this->b = b;
        this->a = a;
    }

    ol_Color(int r, int g, int b, int a = 255)
    {
        this->r = (float)r/255;
        this->g = (float)g/255;
        this->b = (float)b/255;
        this->a = (float)a/255;
    }

    ol_Color& operator= (ol_Color c)
    {
        r = c.r;
        g = c.g;
        b = c.b;
        a = c.a;
        return *this;
    }
};

#define  OL_COLOR_RED           ol_Color(1.0f, 0.0f, 0.0f, 1.0f)
#define  OL_COLOR_GREEN         ol_Color(0.0f, 1.0f, 0.0f, 1.0f)
#define  OL_COLOR_BLUE          ol_Color(0.0f, 0.0f, 1.0f, 1.0f)
#define  OL_COLOR_YELLOW        ol_Color(1.0f, 1.0f, 0.0f, 1.0f)
#define  OL_COLOR_PURPLE        ol_Color(1.0f, 0.0f, 1.0f, 1.0f)
#define  OL_COLOR_LIGHT_BLUE    ol_Color(0.0f, 1.0f, 1.0f, 1.0f)
#define  OL_COLOR_WHITE         ol_Color(1.0f, 1.0f, 1.0f, 1.0f)
#define  OL_COLOR_BLACK         ol_Color(0.0f, 0.0f, 0.0f, 1.0f)
#define  OL_COLOR_GREY          ol_Color(0.3f, 0.3f, 0.3f, 1.0f)
#define  OL_COLOR_LIGHT_GREY    ol_Color(0.6f, 0.6f, 0.6f, 1.0f)
#define  OL_COLOR_ORANGE        ol_Color(255, 167, 0, 255)

class ol_Line
{
public:
    ol_Vector3d start;
    ol_Vector3d end;
    ol_Color col;
};

/**
    --------------------------------- ol_Material -------------------------------------------
    >                                                                                      <
    >           Содержит параметры материала и методы для работы с ним.                    <
    >                                                                                      <
    ----------------------------------------------------------------------------------------
**/

class ol_Material
{
private:
    void setColorVec(GLfloat vec[4], ol_Color col);

    void setAmbient(ol_Color col);
    void setDiffuse(ol_Color col);
    void setSpecular(ol_Color col);
    void setColor(ol_Color col);

public:
    GLuint tex;
    GLfloat ambient[4];
    GLfloat diffuse[4];
    GLfloat specular[4];
    GLfloat color[4];
    GLubyte shine;
    ol_MaterialType type;
    ol_RenderParams render;
    bool lightning;
    bool hasTexture;
    bool renderNormals;
    bool renderDirections;

    ol_Material()
    {
        setAmbient(ol_Color(0.3f, 0.3f, 0.3f));
        setDiffuse(ol_Color(0.7f, 0.7f, 0.7f));
        setSpecular(ol_Color(1.0f, 1.0f, 1.0f));
        setColor(ol_Color(0.7f, 0.7f, 0.7f));
        shine = 128;
        type = OL_MATERIAL_TYPE_STANDART;
        render = OL_RENDER_SOLID;
        lightning = false;
        hasTexture = false;
        renderNormals = false;
        renderDirections = false;
    }
    ~ol_Material() {}

    void setMaterialParam(ol_MaterialParams type, ol_Color param);
    void setMaterialParam(ol_MaterialParams type, bool param);
    void setMaterialParam(ol_MaterialParams type, int param);
    void setMaterialParam(ol_MaterialParams type, GLuint param);
    void setMaterialType(ol_MaterialType param);
    void setRenderMode(ol_RenderParams param);
    void setRenderFlags(ol_RenderFlags type, bool param);
};

/**
    --------------------------------- ol_Graphics -------------------------------------------
    >                                                                                      <
    >   Отвечает за загрузку текстур и моделей. Имеет несколько простых функций рисования  <
    >   геометрических примитивов. Отвечает за переход от 3Д рисования к 2Д и наоборот.    <
    >                                                                                      <
    ----------------------------------------------------------------------------------------
**/

class ol_Graphics
{
private:
    // загрузчик ресурсов
    ol_ResourceManager loaderRes;
    // вектор с основными цветами, чтобы их можно было перебирать
    vector<ol_Color> colorPalette;
    // номер индекса следующего элемента массива цветов
    unsigned int nextColor;

    // создает текстуру из файла и возвращает её индекс
    bool LoadTexture(const char *fileName, ol_TextureImage *texture);
    
    map<const char*, ol_Texture> texMap;

public:
    ol_Graphics();
    ~ol_Graphics();

    // Простая функция, в которой сохраняется матрица проекции,
    // затем делаем мировые координаты идентичными с координатами окна.
    void PushScreenCoordinateMatrix(void);
    // Восстановить координаты матрицы проекции.
    void PopProjectionMatrix(void);
    // возвращает индекс текстуры. Если текстура не была загружена - предварительно загружает
    GLuint getTexID(const char *fileName);
    // возвращает структуру, содержащую длину, ширину, индекс текстуры. Если текстура не была загружена - предварительно загружает
    ol_Texture getTexture(const char *fileName);
    // Создает текстуру
    bool BuildTexture(ol_TextureImage *texture);
    // возвращает указатель на меш. Если он не создан - предварительно загружает его
    ol_Mesh3D* getMesh(const char *fileName);
    // сохраняет модель в файл
    bool saveMesh(ol_Mesh3D *mesh, const char *fileName);
    // считывает данные из файла и заполняет структуру текстуры, но текстура не создается
    ol_TextureImage* LoadImage(const char *fileName);
    // сохраняет изображение в файл
    bool SaveImage(const char *fileName, ol_TextureImage *tex);
    // Создает и возвращает переменную типа ol_Texture из переменной tex
    ol_Texture TextureImageToTexture(ol_TextureImage *tex);
    // отрисовывает линию заданного цвета по двум трехмерным вершинам
    void DrawLine3d(ol_Vector3d start, ol_Vector3d end, ol_Color col)  const;
    // отрисовывает массив линий
    void DrawLines3D(vector <ol_Line> lines);
    // рисует оси системы координат длины length
    void DrawCoordSys(float length);
    // Создает объект типа материал с базовым набором параметров
    ol_Material* CreateDefaultMaterial(void);
    // Вызывает ф-ю glColor3f
    void ol_GLColor(ol_Color color);
    // выбирает следующий цвет из палитры. В дальнейшем будут добавлены палитры и возможность переключаться между ними
    ol_Color getNextColor(void);
    // отрисовывает двумерную окружность
    void DrawCircle(ol_Vector2df center, float radius, int vertNum = 16);
};

#endif // OL_GRAPHICS_H_INCLUDED
