/**
------------------------------------------------------------------------
  This file is part of the OLGA project.
  Copyright © 2017 by Enver Akhmedov.
  E-mail: aen-code@yandex.ru
  For conditions of distribution and use, see copyright notice in OLGA.h
------------------------------------------------------------------------
**/

#include "ol_Quaternion.h"

/**
    --------------------------------- Quaternion -------------------------------------------
    >                                                                                      <
    >  Используется для вычисления трехмерного вращения. x, y, z - ось вращения, w - угол. <
    >  Кватернионы удобны, когда необходимо задать сложное вращение вокруг нескольких      <
    >  осей. Для каждого вращения задается отдельный кватернион. Результирующее вращение   <
    >  будет произведением отдельных кватернионов. Кроме того, из кватерниона можно        <
    >  получить матрицу вращения.                                                          <
    >                                                                                      <
    ----------------------------------------------------------------------------------------
**/

ol_Quaternion QuaternionFromAxisAngle(ol_Vector3d axis, float angle)
{
    ol_Quaternion result;
    float sinAngle;

    angle *= 0.5f;
    axis = Normalize(axis);
    sinAngle = sin(angle);
    result.x = (axis.x * sinAngle);
    result.y = (axis.y * sinAngle);
    result.z = (axis.z * sinAngle);
    result.w = cos(angle);
    return result;
}

void QuaternionToAxisAngle(ol_Quaternion quat, ol_Vector3d * axis, float * angle)
{
    float sinAngle;

    QuaternionNormalize(&quat);
    sinAngle = sqrt(1.0f - (quat.w * quat.w));
    if (fabs(sinAngle) < 0.0005f) sinAngle = 1.0f;
    axis->x = (quat.x / sinAngle);
    axis->y = (quat.y / sinAngle);
    axis->z = (quat.z / sinAngle);
    *angle = (acos(quat.w) * 2.0f);
}

void QuaternionToMatrix(ol_Quaternion quat, float matrix[16])
{
    matrix[0]  = (1.0f - (2.0f * ((quat.y * quat.y) + (quat.z * quat.z))));
    matrix[1]  =         (2.0f * ((quat.x * quat.y) + (quat.z * quat.w)));
    matrix[2]  =         (2.0f * ((quat.x * quat.z) - (quat.y * quat.w)));
    matrix[3]  = 0.0f;
    matrix[4]  =         (2.0f * ((quat.x * quat.y) - (quat.z * quat.w)));
    matrix[5]  = (1.0f - (2.0f * ((quat.x * quat.x) + (quat.z * quat.z))));
    matrix[6]  =         (2.0f * ((quat.y * quat.z) + (quat.x * quat.w)));
    matrix[7]  = 0.0f;
    matrix[8]  =         (2.0f * ((quat.x * quat.z) + (quat.y * quat.w)));
    matrix[9]  =         (2.0f * ((quat.y * quat.z) - (quat.x * quat.w)));
    matrix[10] = (1.0f - (2.0f * ((quat.x * quat.x) + (quat.y * quat.y))));
    matrix[11] = 0.0f;
    matrix[12] = 0.0f;
    matrix[13] = 0.0f;
    matrix[14] = 0.0f;
    matrix[15] = 1.0f;
}

void QuaternionRotate(ol_Quaternion *quat, ol_Vector3d axis, float angle)
{
    ol_Quaternion rotationQuat;

    rotationQuat = QuaternionFromAxisAngle(axis, angle);
    *quat = *quat*rotationQuat;
}

void QuaternionNormalize(ol_Quaternion * quat)
{
    float magnitude;

    magnitude = sqrt((quat->x * quat->x) + (quat->y * quat->y) + (quat->z * quat->z) + (quat->w * quat->w));
    quat->x /= magnitude;
    quat->y /= magnitude;
    quat->z /= magnitude;
    quat->w /= magnitude;
}

void QuaternionInvert(ol_Quaternion * quat)
{
    float length;

    length = (1.0f / ((quat->x * quat->x) +
                      (quat->y * quat->y) +
                      (quat->z * quat->z) +
                      (quat->w * quat->w)));
    quat->x *= -length;
    quat->y *= -length;
    quat->z *= -length;
    quat->w *= length;
}

ol_Vector3d QuaternionMultiplyVector(ol_Quaternion *quat, ol_Vector3d *vector)
{
    ol_Quaternion vectorQuat, inverseQuat, resultQuat;
    ol_Vector3d resultVector;

    vectorQuat = QuaternionFromVector(*vector);
    inverseQuat = *quat;
    QuaternionInvert(&inverseQuat);

    resultQuat = vectorQuat * inverseQuat;
    resultQuat = *quat*resultQuat;

    resultVector = QuaternionToVector(resultQuat);

    return resultVector;
}

ol_Quaternion QuaternionSLERP(ol_Quaternion start, ol_Quaternion end, float alpha)
{
    float startWeight, endWeight, difference;
    ol_Quaternion result;

    difference = ((start.x * end.x) + (start.y * end.y) + (start.z * end.z) + (start.w * end.w));
    if ((1.0f - difference) > SLERP_TO_LERP_SWITCH_THRESHOLD)
    {
        float theta, oneOverSinTheta;

        theta = acos(fabs(difference));
        oneOverSinTheta = (1.0f / sin(theta));
        startWeight = (sin(theta * (1.0f - alpha)) * oneOverSinTheta);
        endWeight = (sin(theta * alpha) * oneOverSinTheta);
        if (difference < 0.0f)
        {
            startWeight = -startWeight;
        }
    }
    else
    {
        startWeight = (1.0f - alpha);
        endWeight = alpha;
    }
    result.x = ((start.x * startWeight) + (end.x * endWeight));
    result.y = ((start.y * startWeight) + (end.y * endWeight));
    result.z = ((start.z * startWeight) + (end.z * endWeight));
    result.w = ((start.w * startWeight) + (end.w * endWeight));
    QuaternionNormalize(&result);
    return result;
}

ol_Quaternion QuaternionFromVector(ol_Vector3d v)
{
    ol_Quaternion result;

    result.x = v.x;
    result.y = v.y;
    result.z = v.z;
    result.w = 0.0f;

    return result;
}

ol_Vector3d QuaternionToVector(ol_Quaternion q)
{
    ol_Vector3d result;

    result.x = q.x;
    result.y = q.y;
    result.z = q.z;

    return result;
}

ol_Vector3d RotateVector(ol_Vector3d axis, ol_Vector3d vector, float angle)
{
    ol_Quaternion q, q1, q_v;
    ol_Vector3d result;

    // получаем кватернион вращения и инвертированный кватернион
    q = QuaternionFromAxisAngle(axis, angle*PI/180);
    q1 = q;
    QuaternionInvert(&q1);

    // вращаем вектор
    // получаем кватернион из вращаемого вектора
    q_v = QuaternionFromVector(vector);

    q = q*q_v;
    q = q*q1;

    result = QuaternionToVector(q);
    result = Normalize(result);

    return result;
}
