/**
------------------------------------------------------------------------
  This file is part of the OLGA project.
  Copyright © 2017 by Enver Akhmedov.
  E-mail: aen-code@yandex.ru
  For conditions of distribution and use, see copyright notice in OLGA.h
------------------------------------------------------------------------
**/

#include "ol_Particle_System.h"

/**
    --------------------------------- Particle ---------------------------------------------
    >                                                                                      <
    >                     Класс частицы для создания спец. эффектов.                       <
    >                                                                                      <
    ----------------------------------------------------------------------------------------
**/

ol_Particle::ol_Particle()
{
}

ol_Particle::ol_Particle(ol_Vector2df pos, ol_Vector2df speed, double bornTime)
{
    this->pos = pos;
    this->bornTime = bornTime;
    this->vel = speed;
}

ol_Particle::~ol_Particle()
{
}

/**
    --------------------------------- Particle Emitter -------------------------------------
    >                                                                                      <
    >     Излучатель частиц. Создает, удаляет, отрисовывает частицы.                       <
    >                                                                                      <
    ----------------------------------------------------------------------------------------
**/

ol_ParticleEmitter::ol_ParticleEmitter(ol_Graphics *graph, ol_Vector2df pos, double freq, double lifeTime)
{
    this->graph = graph;
    this->timer = ol_CoreParams::timer;
    this->pos = pos;
    this->lifeTime = lifeTime;
    this->freq = freq;
    period = timer->addTimeMonitor(1.0/freq);
    affector = NULL;
    srand(68);
    partCount =0;
}

ol_ParticleEmitter::~ol_ParticleEmitter()
{
    for(list<ol_Particle*>::iterator it = particles.begin(); it!=particles.end(); ++it)
        delete (*it);

    particles.clear();
    timer->RemoveTimeMonitior(period);

#ifdef _OL_DEBUG_BUILD_
    printf("Particle Emitter destructor\n");
#endif // _OL_DEBUG_BUILD_
}

void ol_ParticleEmitter::Draw(void)
{
    glDisable(GL_TEXTURE_2D);
    graph->ol_GLColor(OL_COLOR_BLACK);
    glPointSize(5);
    glBegin(GL_POINTS);

    for(list<ol_Particle*>::iterator it = particles.begin(); it!=particles.end(); ++it)
    {
        glVertex2f((*it)->pos.x, (*it)->pos.y);
    }

    glEnd();
}

void ol_ParticleEmitter::Update(void)
{
    list<ol_Particle*>::iterator it;

    curTime = timer->readTimer();

    for(it = particles.begin(); it!=particles.end(); ++it)
    {
        if((curTime - (*it)->bornTime)>=lifeTime)
        {
            particles.erase(it);
            delete (*it);
            break;
        }
    }

    if(period->itIsTime)
    {
        ol_Particle *newPart = new ol_Particle(pos+ol_Vector2df(rand()%150, rand()%150), ol_Vector2df(rand()%15-7, rand()%15-7), curTime);
        particles.push_back(newPart);
        partCount++;
        if(affector) affector->Affect(newPart);
    }

    for(it = particles.begin(); it!=particles.end(); ++it)
        (*it)->Update();
}

void ol_ParticleEmitter::addAffector(ol_PhysicsAffector *affector)
{
    this->affector = affector;
}

/**
    --------------------------------- Particle Manager -------------------------------------
    >                                                                                      <
    >                   Класс управляющий всеми частицами сцены.                           <
    >   Позволяет добавлять источники частиц, а также автоматически рендерит все частицы.  <
    >                                                                                      <
    ----------------------------------------------------------------------------------------
**/

ol_ParticleManager::ol_ParticleManager(ol_Graphics *graph)
{
    this->graph = graph;
    emittersCount = 0;
}

ol_ParticleManager::~ol_ParticleManager()
{
    for(unsigned int i=0; i<emittersCount; i++)
        delete emitters[i];
#ifdef _OL_DEBUG_BUILD_
    printf("Particle Manager destructor\n");
#endif // _OL_DEBUG_BUILD_
}

void ol_ParticleManager::addObject(ol_ParticleEmitter *emi)
{
    if(!emi) return;

    emittersCount++;
    emitters.push_back(emi);
}

ol_ParticleEmitter* ol_ParticleManager::addEmitter(ol_Vector2df pos, double freq, double lifeTime)
{
    ol_ParticleEmitter *pemi = new ol_ParticleEmitter(graph, pos, freq, lifeTime);
    if(!pemi) return NULL;
    addObject(pemi);
    return pemi;
}

void ol_ParticleManager::DrawAll(void)
{
    for(unsigned int i=0; i<emittersCount; i++)
        emitters[i]->Draw();
}

void ol_ParticleManager::UpdateAll(void)
{
    for(unsigned int i=0; i<emittersCount; i++)
        emitters[i]->Update();
}
