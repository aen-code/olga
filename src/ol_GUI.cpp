/**
------------------------------------------------------------------------
  This file is part of the OLGA project.
  Copyright © 2017 by Enver Akhmedov.
  E-mail: aen-code@yandex.ru
  For conditions of distribution and use, see copyright notice in OLGA.h
------------------------------------------------------------------------
**/

#include "ol_GUI.h"

unsigned int ol_GUIControl::guiControlsCount = 0; // выделяем память под статический член

std::vector<unsigned int> addCharSet(std::vector<unsigned int> codes, ol_CharSets charset)
{
    switch(charset)
    {
    case OL_CHARSET_COMMON:
        break;
    case OL_CHARSET_kYRILIC:
        for(unsigned int i = 0x0400; i<= 0xa69f; i++)
            codes.push_back(i);
        break;
    case OL_CHARSET_kYRILIC_BASIC:
        for(unsigned int i = 0x0410; i<= 0x044f; i++)
            codes.push_back(i);
        break;
    case OL_CHARSET_LATIN:
        break;
    case OL_CHARSET_LATIN_BASIC:
        for(unsigned int i = 0x0020; i<= 0x007e; i++)
            codes.push_back(i);
        break;
    }
    return codes;
}

void CalcLabelRelPos(ol_Area2d *control, ol_GUIPositionPoints controlPoint, float delta, ol_Vector2df &pos, ol_GUIPositionPoints &labelPoint)
{
    pos = control->topLeft+ol_Vector2df(0,delta);
    labelPoint = OL_GUI_POS_BOTLEFT;

    switch(controlPoint)
    {
    case OL_GUI_POS_TOPLEFT:
        pos = control->topLeft+ol_Vector2df(0,delta);
        labelPoint = OL_GUI_POS_BOTLEFT;
        break;
    case OL_GUI_POS_TOPMID:
        pos = control->topMid+ol_Vector2df(0,delta);
        labelPoint = OL_GUI_POS_BOTMID;
        break;
    case OL_GUI_POS_TOPRIGHT:
        pos = control->topRight+ol_Vector2df(0,delta);
        labelPoint = OL_GUI_POS_BOTRIGHT;
        break;
    case OL_GUI_POS_CENTRIGHT:
        pos = control->centRight+ol_Vector2df(delta,0);
        labelPoint = OL_GUI_POS_CENTLEFT;
        break;
    case OL_GUI_POS_CENTMID:
        break;
    case OL_GUI_POS_BOTRIGHT:
        pos = control->botRight+ol_Vector2df(0,-delta);
        labelPoint = OL_GUI_POS_TOPRIGHT;
        break;
    case OL_GUI_POS_BOTMID:
        pos = control->botMid+ol_Vector2df(0,-delta);
        labelPoint = OL_GUI_POS_TOPMID;
        break;
    case OL_GUI_POS_BOTLEFT:
        pos = control->botLeft+ol_Vector2df(0,-delta);
        labelPoint = OL_GUI_POS_TOPLEFT;
        break;
    case OL_GUI_POS_CENTLEFT:
        pos = control->centLeft+ol_Vector2df(-delta,0);
        labelPoint = OL_GUI_POS_CENTRIGHT;
        break;
    }
}

/**
    --------------------------------- Simple Font ------------------------------------------
    >                                                                                      <
    >                                Простой шрифт.                                        <
    >       Сам инициализирует латинские символы и символы общего назначения;              <
    >       не поддерживает юникод; позволяет осуществлять многострочный вывод.            <
    >                                                                                      <
    ----------------------------------------------------------------------------------------
**/

ol_SimpleFont::ol_SimpleFont(ol_Graphics *grm)
{
    this->grm = grm;
    char_count = 0;
    charcodes = addCharSet(charcodes, OL_CHARSET_LATIN_BASIC);
    size = 0.0f;
    height = 0.0f;
}

ol_SimpleFont::~ol_SimpleFont()
{
    Destroy();
}

void ol_SimpleFont::ConvertCharCodes(char codes[], unsigned int len)
{
    for(unsigned int i=0; i<len; i++)
        codes[i] = charmap[codes[i]]->ind;
}

void ol_SimpleFont::Destroy(void)
{
    glDeleteLists(list_base,char_count);
    glDeleteTextures(char_count,textures);
    delete [] textures;

    map<wchar_t, ol_Symbol*>::const_iterator itr;

    for(itr=charmap.begin(); itr!=charmap.end(); itr++)
        delete itr->second;

#ifdef _OL_DEBUG_BUILD_
    printf("Simple Font destructor\n");
#endif
}

float ol_SimpleFont::getHeight(void)
{
    return height;
}

float ol_SimpleFont::getSize(void)
{
    return size;
}

void ol_SimpleFont::Make_Dlist(FT_Face face, wchar_t ch, ol_Symbol *s, GLuint list_base, GLuint *tex_base )
{
    int error;
    FT_GlyphSlot  slot = face->glyph;

    error = FT_Set_Char_Size(face, 0, size*64, 300, 300);
    if(error) printf("Error (Freetype): Unable to set char size!\n");

    error = FT_Set_Pixel_Sizes(face, 0, size);
    if(error) printf("Error (Freetype): Unable to set pixel size!\n");

    error = FT_Load_Char(face, ch, FT_LOAD_RENDER);
    if(error) printf("Error (Freetype): FT_Load_Char failed\n");

    // С помощью этой ссылки, получаем легкий доступ до растра.
    FT_Bitmap &bitmap = slot->bitmap;

    // Используем нашу вспомогательную функцию для вычисления ширины и высоты
    // текстуры для нашего растра.
    unsigned int gw =  slot->metrics.width>>6;   // Ширина растра глипса
    unsigned int gh =   slot->metrics.height>>6; // Высота растра глипса
    unsigned int width = Next_P2(gw);            // Ширина текстуры
    unsigned int height = Next_P2(gh);           // Высота текстуры

    // Выделим память для данных текстуры.
    GLubyte* expanded_data = new GLubyte[ 2 * width * height];

    for(unsigned int j=0; j <height; j++)
    {
        for(unsigned int i=0; i < width; i++)
        {
            expanded_data[2*(i+j*width)]= expanded_data[2*(i+j*width)+1] = (i>=gw || j>=gh)? 0: bitmap.buffer[i + gw*j];
        }
    }

    ol_TextureImage *tex = new ol_TextureImage();
    tex->bpp = 16;
    tex->width = width;
    tex->height = height;
    tex->imageData = expanded_data;

    if(grm->BuildTexture(tex))
        tex_base[s->ind] = tex->texID;

    // После создания текстуры, мы больше не нуждаемся в промежуточных данных.
    // ol_TextureImage expanded data удалит сам
    delete tex;

    // Создать список отображения
    glNewList(list_base + s->ind, GL_COMPILE);
    glBindTexture(GL_TEXTURE_2D, tex_base[s->ind]);

    // Сдвинем вниз в том случае, если растр уходит вниз строки.
    // Это истинно только для символов, таких как 'g' или 'y'.

    float dp = slot->metrics.horiBearingY>>6;
    float dy = gh - dp;                 // расстояние на которое символ смещен вниз
    float dx = slot->advance.x >> 6;    // общая ширина печатной области. Расстояние перед символом + ширина + расстояние после

    glPushMatrix();
    glTranslatef(0, -dy, 0);

    /*
        Вычислим какая часть нашей текстуры будет заполнена пустым пространством.
        Мы рисуем только ту часть текстуры, в которой находится символ, и сохраняем
        информацию в переменных x и y, затем, когда мы рисуем четырехугольник,
        мы будем только ссылаться на ту часть текстуры, в которой непосредственно
        содержится символ.
    */
    float x = (float)gw/(float)width;
    float y = (float)gh/(float)height;

    // Рисуем текстурированный четырехугольник.
    glBegin(GL_QUADS);
    glTexCoord2d(0, 0);
    glVertex2f(0, gh);

    glTexCoord2d(0, y);
    glVertex2f(0, 0);

    glTexCoord2d(x, y);
    glVertex2f(gw, 0);

    glTexCoord2d(x,0);
    glVertex2f(gw, gh);
    glEnd();

    glPopMatrix();

    glTranslatef(dx, 0, 0);

    // Завершим создание списка отображения
    glEndList();

    s->width = dx;
    s->height = gh;
    s->hn = dy;
    if(s->hn <= s->height) s->hp = s->height - s->hn;
    else s->hp = 0;
}

bool ol_SimpleFont::BuildFont_ft(FT_Library *ft_lib, const char *fname, unsigned int h)
{
    char_count = charcodes.size();

    // Выделим память для идентификаторов текстуры.
    textures = new GLuint[char_count];

    size = h;

    // Объект для хранения шрифта.
    FT_Face face;

    int error = FT_New_Face(*ft_lib, fname, 0, &face );

    if ( error == FT_Err_Unknown_File_Format )
    {
        printf("Error (Freetype): The font file could be opened and read, but it appears that its font format is unsupported\n");
        return false;
    }
    else if (error)
    {
        printf("Error (Freetype): The font file could not be opened or read, or simply it is broken\n");
        return false;
    }

    // Здесь попросим OpenGL, чтобы он выделил память для
    // всех текстур и списков отображения, которые нам нужны.
    list_base = glGenLists(char_count);
    glGenTextures(char_count, textures);

    height = 0.0f;

    for(unsigned i=0; i<char_count; i++)
    {
        ol_Symbol *sb = new ol_Symbol();

        sb->ind = i;
        sb->width = sb->height = 0;

        Make_Dlist(face, wchar_t(charcodes[i]), sb, list_base, textures);

        charmap[wchar_t(charcodes[i])] = sb;

        if(sb->height >=height) height = sb->height; // вычисляем максимальную высоту шрифта в пикселях
    }

    charcodes.clear();

    // Уничтожим шрифт.
    FT_Done_Face(face);

    printf("Font Created (%d symbols): %s\n", char_count, fname);

    return true;
}

void ol_SimpleFont::Print(float x, float y, const char *fmt, ...)
{
    glLoadIdentity();
    // Мы хотим систему координат, в которой расстояние измеряется в пикселях.
    grm->PushScreenCoordinateMatrix();

    // Сделаем высоту немного больше, что бы оставить место между линиями.
    float h = size/.64f;

    char  text[256];           // Сохраним нашу строку
    va_list  ap;               // Указатель на лист аргументов

    if(fmt == NULL)            // Если это не текст
        *text=0;               // Тогда ничего не делать
    else
    {
        va_start(ap, fmt);        // Разбор строки на переменные
        vsprintf(text, fmt, ap);  // И конвертировать символы в числа
        va_end(ap);               // Результат сохранить в текст
    }

    // Разделим текст на строки.
    const char *start_line = text;
    vector<string> lines;

    const char *c;
    for(c = text; *c; c++)
    {
        if(*c == '\n')
        {
            string line;
            for(const char *n = start_line; n<c; n++)
                line.append(1,*n);
            lines.push_back(line);
            start_line = c+1;
        }
    }

    if(start_line)
    {
        string line;
        for(const char *n = start_line; n<c; n++)
            line.append(1, *n);
        lines.push_back(line);
    }

    glPushAttrib(GL_LIST_BIT | GL_CURRENT_BIT  | GL_ENABLE_BIT | GL_TRANSFORM_BIT);
    glMatrixMode(GL_MODELVIEW);
    glDisable(GL_LIGHTING);
    glPolygonMode(GL_FRONT_AND_BACK, GL_FILL);
    glEnable(GL_TEXTURE_2D);
    glDisable(GL_DEPTH_TEST);
    glEnable(GL_BLEND);

    glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);
    glListBase(list_base);

    ol_Matrix44f modelview_matrix;
    glGetFloatv(GL_MODELVIEW_MATRIX, modelview_matrix);

    /*
        На каждой строчке мы сбрасываем матрицу вида модели
        Поэтому строки будут начинаться с правильной позиции.
        Отмечу, что сброс надо делать до сдвига вниз на h, поскольку затем каждый
        символ рисуется и это модифицирует текущую матрицу, поэтому следующий
        символ будет нарисован прямо после него.
    */

    unsigned int slen;

    for(unsigned int i=0; i<lines.size(); i++)
    {
        glPushMatrix();
        glLoadIdentity();
        glTranslatef((int)x, int(y-h*i), 0);
        glMultMatrixf(modelview_matrix);

        strcpy(text, lines[i].c_str());
        slen = strlen(text);
        ConvertCharCodes(text, slen);

        glCallLists(slen, GL_UNSIGNED_BYTE, text);

        glPopMatrix();
    }

    glPopAttrib();
    grm->PopProjectionMatrix();
}

/**
    ----------------------------------- Font -----------------------------------------------
    >                                                                                      <
    >                               Продвинутый шрифт.                                     <
    >       Поддерживает юникод; при инициализации необходимо передать ему массив          <
    >  с кодировками; не поддерживает многострочный вывод; поддерживает относительное      <
    >                   позиционирование при выводе (layout)                               <
    >                                                                                      <
    ----------------------------------------------------------------------------------------
**/

ol_Font::ol_Font(ol_Graphics *grm, std::vector<unsigned int> charCodes): ol_SimpleFont(grm)
{
    charcodes = charCodes;
}

ol_Font::~ol_Font()
{
#ifdef _OL_DEBUG_BUILD_
    printf("Font destructor\n");
#endif
}

void ol_Font::Print(float x, float y, ol_GUIPositionPoints keyPoint, const wchar_t *fmt, ...)
{
    wchar_t *text = new wchar_t[256];   // буфер для хранения результирующей строки
    va_list  ap;                        // Указатель на лист аргументов

    if(fmt == NULL)
        *text=0;
    else
    {
        va_start(ap, fmt);              // Разбор строки на переменные
        vswprintf(text, 256, fmt, ap);  // И конвертировать символы в числа
        va_end(ap);                     // Результат сохранить в текст
    }

    float width, height;
    unsigned int slen = wcslen(text);
    if(!slen) return;

    GLuint lists[256];
    ConvertCharCodes(text, lists, slen,  width, height);

    printArea.Init(ol_Vector2df(x,y), width, height, keyPoint);

    TextOut(printArea.botLeft.x, printArea.botLeft.y, lists, slen);

    delete[] text;
}

void ol_Font::TextOut(float x, float y, GLuint lists[], unsigned short length)
{
    glPushAttrib(GL_LIST_BIT | GL_CURRENT_BIT  | GL_ENABLE_BIT );
    glEnable(GL_TEXTURE_2D);
    glListBase(list_base);

    glPushMatrix();
    glLoadIdentity();

    //glTranslatef((int)v.x, (int)v.y, 0); // с учетом выпирания
    glTranslatef((int)x, (int)y, 0);

    glCallLists(length, GL_UNSIGNED_INT, lists);

    glPopMatrix();
    glPopAttrib();
}

void ol_Font::FormatedTextOut(float x, float y, ol_GLFormatedString &str, ol_GUIPositionPoints keyPoint)
{
    printArea.Init(ol_Vector2df(x,y), str.width, str.height, keyPoint);
    TextOut(printArea.botLeft.x, printArea.botLeft.y, str.str.data(), str.str.size());
}

void ol_Font::ConvertCharCodes(const wchar_t *codes, GLuint indexes[], unsigned int len, float &width, float &height)
{
    ol_Symbol *sb = charmap[codes[0]];
    // не учитываем выпирание вниз
    //maxn = sb->hn;

    float maxp = sb->hp;
    width = 0;
    for(unsigned int i=0; i<len; i++)
    {
        sb = charmap[codes[i]];
        if(!sb) continue;

//        codes[i] = sb->ind;
        indexes[i] = sb->ind;
        width += sb->width;

        // не учитываем выпирание вниз
        //if(sb->hn > maxn) maxn = sb->hn;
        if(sb->hp > maxp) maxp = sb->hp;
    }

    // с учетом выпирания
    // height = maxn + maxp;
    height = maxp;
    // не учитываем выпирание вниз
    //dy = maxn;
}

bool ol_Font::isValidCharCode(wchar_t c)
{
    ol_Symbol *sb = charmap[c];
    if(!sb) return false;
    return true;
}

float ol_Font::getCaretShift(const wchar_t *str, unsigned short len, unsigned short pos)
{
    ol_Symbol *sb;
    float cShift = 0;

    if(pos<=0) return 0.0f;

    for(unsigned int i=0; i<len; i++)
    {
        sb = charmap[str[i]];
        if(!sb) continue;

        cShift += sb->width;

        if((i+1) == pos) break;
    }

    return cShift;
}

/**
    --------------------------------- Control points ---------------------------------------
    >                                                                                      <
    >    Ключевые вершины элемента управления, по которым можно его позиционировать        <
    >                                                                                      <
    ----------------------------------------------------------------------------------------
**/

ol_GUIControlPoints::ol_GUIControlPoints()
{
    Init(ol_Vector2df(0.0f, 0.0f), 10.0f, 5.0f);
}

void ol_GUIControlPoints::Init(ol_Vector2df origin, float width, float height, ol_GUIPositionPoints keyPoint)
{
    ol_Vector2df right = ol_Vector2df(width, 0);
    ol_Vector2df up = ol_Vector2df(0, height);
    ol_Vector2df half_up = up/2;
    ol_Vector2df half_right = right/2;

    switch(keyPoint)
    {
    case OL_GUI_POS_TOPLEFT:
        botLeft = origin - up;
        break;
    case OL_GUI_POS_TOPMID:
        botLeft = origin - half_right - up;
        break;
    case OL_GUI_POS_TOPRIGHT:
        botLeft = origin - right - up;
        break;
    case OL_GUI_POS_CENTLEFT:
        botLeft = origin - half_up;
        break;
    case OL_GUI_POS_CENTMID:
        botLeft = origin - half_right - half_up;
        break;
    case OL_GUI_POS_CENTRIGHT:
        botLeft = origin - right - half_up;
        break;
    case OL_GUI_POS_BOTLEFT:
        botLeft = origin;
        break;
    case OL_GUI_POS_BOTMID:
        botLeft = origin - half_right;
        break;
    case OL_GUI_POS_BOTRIGHT:
        botLeft = origin - right;
        break;
    }

    centLeft    = botLeft + half_up;
    topLeft     = botLeft + up;
    topMid      = topLeft + half_right;
    topRight    = topLeft + right;
    centRight   = topRight - half_up;
    botRight    = topRight - up;
    botMid      = botLeft + half_right;
    centMid     = botLeft + half_up + half_right;
}

/**
    ------------------------------- 2D Animation -------------------------------------------
    >                                                                                      <
    >                                Класс анимации.                                       <
    >      Сам ничего не рендерит. Позволяет загрузить текстуру, разбить её на кадры и     <
    >           переключать кадры по команде или циклически, с заданным фпс.               <
    >                                                                                      <
    ----------------------------------------------------------------------------------------
**/

ol_Animation2d::ol_Animation2d()
{
    curFrame = NULL;
    framesCount = 0;
    fps = 1;
    animating = false;
    curFNum = 0;
    curRepeat = 0;
    repeat = -1;
    period = ol_CoreParams::timer->addTimeMonitor(1.0/fps);
}

ol_Animation2d::~ol_Animation2d()
{
    for(unsigned int i=0; i<frames.size(); i++)
    {
        delete frames[i];
    }
    frames.clear();
    ol_CoreParams::timer->RemoveTimeMonitior(period);
}

bool ol_Animation2d::isAnimating(void)
{
    return animating;
}

bool ol_Animation2d::HasAnimation(void)
{
    if(curFrame) return true;
    return false;
}

void ol_Animation2d::setCurFrame(unsigned int frame)
{
    if((frame<0)||(frame>=framesCount)) return;

    curFrame = frames[frame];
}

bool ol_Animation2d::AssignAnimation(ol_Texture tex)
{
    if(tex.texID == -1)
    {
        printf("Error: Animation is not assigned\n");
        return false;
    }

    texWidth = tex.width;
    texHeight = tex.height;
    texID = tex.texID;
    fps = tex.fps;

    period->timeInterval = (double)1/fps;

    if(framesCount>0)
    {
        for(unsigned int i=0; i<framesCount; i++)
            delete frames[i];

        frames.clear();
    }

    framesCount = tex.width_amount * tex.height_amount;

    float kw = 1.0f/tex.width_amount;
    float kh = 1.0f/tex.height_amount;

    for(int j=0; j<tex.height_amount; j++)
        for(int i=0; i<tex.width_amount; i++)
        {
            ol_Quad2d *rect = new ol_Quad2d();

            rect->botLeft.y = rect->botRight.y = j*kh;
            rect->topLeft.y = rect->topRight.y = (j+1)*kh;

            rect->botLeft.x = rect->topLeft.x = i*kw;
            rect->botRight.x = rect->topRight.x = (i+1)*kw;

            frames.push_back(rect);
        }

    setCurFrame(0);

    printf("Animation created (%d frames)\n", (int)frames.size());
    return true;
}

void ol_Animation2d::setFPS(unsigned short fps)
{
    this->fps = fps;
    period->timeInterval = (double)1/fps;
}

void ol_Animation2d::Animate(short repeat)
{
    this->repeat = repeat;
    curRepeat = 0;
    animating = true;
}

void ol_Animation2d::StartAnimation(void)
{
    repeat = -1;
    animating = true;
}

void ol_Animation2d::StopAnimation(void)
{
    animating = false;
}

bool ol_Animation2d::IncrementFrame(void)
{
    curFNum++;
    if(curFNum >= framesCount)
    {
        curFNum = 0;
        setCurFrame(curFNum);
        return true;
    }
    setCurFrame(curFNum);
    return false;
}

void ol_Animation2d::UpdateAnimation(void)
{
    if(!animating) return;

    if(period->itIsTime)
    {
        if(IncrementFrame() && repeat>0)
        {
            curRepeat++;
            if(curRepeat>= repeat) animating = false;
        }
    }
}

/**
    --------------------------------- 2D Area ----------------------------------------------
    >                                                                                      <
    >  Двумерный прямоугольник. "Знает" свои углы, середины сторон, центр, длину и ширину. <
    >  Может определить попадает ли точка внутрь.                                          <
    >                                                                                      <
    ----------------------------------------------------------------------------------------
**/

ol_Area2d ol_Area2d::operator+(ol_Area2d &are)
{
    ol_Area2d result;
    ol_Vector2df lowleft, upright;
    float w,h;

    lowleft.x = std::min(botLeft.x, are.botLeft.x);
    lowleft.y = std::min(botLeft.y, are.botLeft.y);

    upright.x = std::max(topRight.x, are.topRight.x);
    upright.y = std::max(topRight.y, are.topRight.y);

    w = upright.x - lowleft.x;
    h = upright.y - lowleft.y;

    result.Init(lowleft, w, h, OL_GUI_POS_BOTLEFT);

    return result;
}

void ol_Area2d::Init(ol_Vector2df origin, float width, float height, ol_GUIPositionPoints keyPoint)
{
    this->height = height;
    this->width = width;

    ol_GUIControlPoints::Init(origin, width, height, keyPoint);
}

ol_Vector2df& ol_Area2d::getPosPoint(ol_GUIPositionPoints posPoint)
{
    switch(posPoint)
    {
    case OL_GUI_POS_TOPLEFT:
        return topLeft;
        break;
    case OL_GUI_POS_TOPMID:
        return topMid;
        break;
    case OL_GUI_POS_TOPRIGHT:
        return topRight;
        break;
    case OL_GUI_POS_CENTLEFT:
        return centLeft;
        break;
    case OL_GUI_POS_CENTMID:
        return centMid;
        break;
    case OL_GUI_POS_CENTRIGHT:
        return centRight;
        break;
    case OL_GUI_POS_BOTLEFT:
        return botLeft;
        break;
    case OL_GUI_POS_BOTMID:
        return botMid;
        break;
    case OL_GUI_POS_BOTRIGHT:
        return botRight;
        break;
    }
    return botLeft;
}

void ol_Area2d::getSide(ol_RectSides side, ol_Vector2df &v1, ol_Vector2df &v2)
{
    switch(side)
    {
    case OL_RECT_SIDE_BOTTOM:
        v1 = botLeft;
        v2 = botRight;
        break;
    case OL_RECT_SIDE_LEFT:
        v1 = botLeft;
        v2 = topLeft;
        break;
    case OL_RECT_SIDE_TOP:
        v1 = topLeft;
        v2 = topRight;
        break;
    case OL_RECT_SIDE_RIGHT:
        v1 = topRight;
        v2 = botRight;
        break;
    }
}

void ol_Area2d::getSide(ol_RectSides side, ol_Vector2dld &v1, ol_Vector2dld &v2)
{
    ol_Vector2df bufv1, bufv2;
    getSide(side, bufv1, bufv2);

    v1.x = bufv1.x;
    v1.y = bufv1.y;
    v2.x = bufv2.x;
    v2.y = bufv2.y;
}

void ol_Area2d::Shift(ol_Vector2df &dir)
{
    topLeft = topLeft + dir;
    topMid = topMid + dir;
    topRight = topRight + dir;
    centLeft = centLeft + dir;
    centMid = centMid + dir;
    centRight = centRight + dir;
    botLeft = botLeft + dir;
    botMid = botMid + dir;
    botRight = botRight + dir;
}

bool ol_Area2d::isInside(ol_Vector2df &v)
{
    if(v.isAbove(botLeft) && v.isBelow(topRight)) return true;
    else return false;
}

/**
    --------------------------------- GUI Control ------------------------------------------
    >                                                                                      <
    >           Класс элемента управления пользовательского интерфейса.                    <
    >                   Базовый для всех элементов управления                              <
    >                                                                                      <
    ----------------------------------------------------------------------------------------
**/

ol_GUIControl::ol_GUIControl(ol_Graphics *graph, ol_GUITheme *theme, ol_Vector2df origin, float width, float height, ol_GUIPositionPoints keyPoint, const char *name)
{
    this->graph = graph;
    this->theme = theme;
    this->name = name;
    guiControlsCount++;
    this->ID = guiControlsCount;
    brdVisible = true;
    bgrVisible = true;
    visible = true;
    brdColor = theme->brdColor;
    bgColor = theme->bgrColor;
    bgColor1 = theme->bgrColor1;

    ol_Area2d::Init(origin, width, height, keyPoint);

    state = OL_GUI_STATE_STANDARD;
    moving = false;
    moveDir = ol_Vector2df(0.0f, 0.0f);
    justShowed = false;
}

ol_GUIControl::~ol_GUIControl()
{
#ifdef _OL_DEBUG_BUILD_
    printf("GUI Control destructor ( ID = %d )\n", ID);
#endif // _OL_DEBUG_BUILD_
}
void ol_GUIControl::setID(int id)
{
    ID = id;
}

void ol_GUIControl::setName(const char *name)
{
    this->name = name;
}

void ol_GUIControl::Show(void)
{
    visible = true;
    justShowed = true;
}

void ol_GUIControl::Hide(void)
{
    visible = false;
}
bool ol_GUIControl::isVisible(void)
{
    return visible;
}

void ol_GUIControl::showBorder(bool flag)
{
    brdVisible = flag;
}

void ol_GUIControl::showBackground(bool flag)
{
    bgrVisible = flag;
}

void ol_GUIControl::setState(ol_GUIControlStates state)
{
    this->state = state;

    switch(state)
    {
	case OL_GUI_STATE_STANDARD:
	    bgColor = theme->bgrColor;
	    bgColor1 = theme->bgrColor1;
	break;

	case OL_GUI_STATE_SELECTED:
	    bgColor = theme->selectColor;
	    bgColor1 = theme->selectColor;
	break;

	case OL_GUI_STATE_FOCUSED:
	    bgColor = theme->selectColor;
	    bgColor1 = theme->selectColor;
	break;

	case OL_GUI_STATE_PRESSED:
	    bgColor = theme->pressColor;
	    bgColor1 = theme->pressColor;;
	break;

	case OL_GUI_STATE_DISABLED:
	    bgColor = theme->pressColor;
	    bgColor1 = theme->pressColor;;
	break;
    }
}

ol_Vector2df ol_GUIControl::setPosition(ol_Vector2df pos, ol_GUIPositionPoints posPoint)
{
    ol_Vector2df v = pos - this->getPosPoint(posPoint);
    Shift(v);

    return v;
}

void ol_GUIControl::Move(ol_Vector2df dir)
{
    moving = true;
    moveDir = moveDir + dir;
}

void ol_GUIControl::Update(void)
{
    if(moving)
    {
        Shift(moveDir);
        moveDir = ol_Vector2df(0.0f, 0.0f);
        moving = false;
    }
}

bool ol_GUIControl::CheckEvents(ol_Event *event, ol_GUIEvent *guiEvent)
{
    if(!visible || state == OL_GUI_STATE_DISABLED) return false;

    ol_Vector2df v = event->getCurPos();

    if(isInside(v)) // если курсор находится над элементом управления
    {
        guiEvent->control1 = this;
        guiEvent->control2 = NULL;

        switch(event->getMType())
        {
        case OL_MEV_LBUTTONDOWN:
            guiEvent->type = OL_GUIEV_LBUTTONDOWN;
            lastClickPoint = event->getCurPos();
            return true;
            break;
        case OL_MEV_LBUTTONUP:
            guiEvent->type = OL_GUIEV_LBUTTONUP;
            return true;
            break;
        case OL_MEV_RBUTTONDOWN:
            guiEvent->type = OL_GUIEV_RBUTTONDOWN;
            lastClickPoint = event->getCurPos();
            return true;
            break;
        case OL_MEV_RBUTTONUP:
            guiEvent->type = OL_GUIEV_RBUTTONUP;
            return true;
            break;
        case OL_MEV_MBUTTONDOWN:
            guiEvent->type = OL_GUIEV_MBUTTONDOWN;
            lastClickPoint = event->getCurPos();
            return true;
            break;
        case OL_MEV_MBUTTONUP:
            guiEvent->type = OL_GUIEV_MBUTTONUP;
            return true;
            break;
        case OL_MEV_WHEEL:
            guiEvent->type = OL_GUIEV_SCROLL;
            return true;
            break;
        default:
            guiEvent->type = OL_GUIEV_MOUSEMOVE;
            return true;
            break;
        }
    }

    return false;
}

bool ol_GUIControl::AssignAnimation(ol_Texture tex)
{
    if(ol_Animation2d::AssignAnimation(tex))
    {
        showBackground(false);
        showBorder(false);
        return true;
    }
    else
    {
        if(!curFrame)
        {
            showBackground(true);
            showBorder(true);
        }

        return false;
    }
}

void ol_GUIControl::DrawBackgrnd(void)
{
    glDisable(GL_TEXTURE_2D);

    glBegin(GL_QUADS);
    graph->ol_GLColor(bgColor);
    glVertex2f((float)topLeft.x, (float)topLeft.y);
    glVertex2f((float)topRight.x, (float)topRight.y);
    graph->ol_GLColor(bgColor1);
    glVertex2f((float)botRight.x, (float)botRight.y);
    glVertex2f((float)botLeft.x, (float)botLeft.y);
    glEnd();
    glEnable(GL_TEXTURE_2D);
}

void ol_GUIControl::DrawBorder(void)
{
    glDisable(GL_TEXTURE_2D);
    graph->ol_GLColor(brdColor);
    glBegin(GL_LINE_LOOP);
    glVertex2f((float)topLeft.x, (float)topLeft.y);
    glVertex2f((float)topRight.x, (float)topRight.y);
    glVertex2f((float)botRight.x, (float)botRight.y);
    glVertex2f((float)botLeft.x, (float)botLeft.y);
    glEnd();
    glEnable(GL_TEXTURE_2D);
}

void ol_GUIControl::Draw(void)
{
    if(!visible) return;

    if(curFrame)
    {
        glEnable(GL_TEXTURE_2D);
        graph->ol_GLColor(OL_COLOR_WHITE);
        glBindTexture(GL_TEXTURE_2D, texID);

        glBegin(GL_QUADS);
        glTexCoord2d(curFrame->botLeft.x, curFrame->botLeft.y);
        glVertex2f((float)botLeft.x, (float)botLeft.y);

        glTexCoord2d(curFrame->topLeft.x, curFrame->topLeft.y);
        glVertex2f((float)topLeft.x, (float)topLeft.y);

        glTexCoord2d(curFrame->topRight.x, curFrame->topRight.y);
        glVertex2f((float)topRight.x, (float)topRight.y);

        glTexCoord2d(curFrame->botRight.x, curFrame->botRight.y);
        glVertex2f((float)botRight.x, (float)botRight.y);
        glEnd();
    }

    if(bgrVisible) DrawBackgrnd();
    if(brdVisible) DrawBorder();
}

/**
    --------------------------------- GUI Panel --------------------------------------------
    >                                                                                      <
    >                                Класс панели.                                         <
    >  Служит контейнером для других элементов управления. Берет на себя функции ГУИ       <
    >    менеджера. Вызывает Дро, Апдейт и делит для элементов, которые содержит.          <
    >                                                                                      <
    ----------------------------------------------------------------------------------------
**/

ol_GUIPanel::ol_GUIPanel(ol_Graphics *graph, ol_GUITheme *theme, ol_Vector2df origin, float width, float height, ol_GUIPositionPoints keyPoint, const char *name): ol_GUIControl(graph, theme, origin, width, height, keyPoint, name)
{
    itemsCount = 0;
    bgrVisible = true;
    brdVisible = true;
}

ol_GUIPanel::~ol_GUIPanel()
{
    if(itemsCount>0)
        for(unsigned int i = 0; i<itemsCount; i++)
            delete items[i];

#ifdef _OL_DEBUG_BUILD_
    printf("GUI Panel Destructor ( ID = %d )\n", ID);
#endif // _OL_DEBUG_BUILD_
}

void ol_GUIPanel::addItem(ol_GUIControl *item)
{
    if(!item)
    {
        printf("Failed to add GUI control to panel\n");
        return;
    }

    items.push_back(item);
    itemsCount++;
}

void ol_GUIPanel::Show(void)
{
    ol_GUIControl::Show();
    for(unsigned int i=0; i<itemsCount; i++)
        items[i]->Show();
}

void ol_GUIPanel::Hide(void)
{
    ol_GUIControl::Hide();
    for(unsigned int i=0; i<itemsCount; i++)
        items[i]->Hide();
}

void ol_GUIPanel::RefreshFocus(unsigned int focusedControl)
{
    for(unsigned int i=0; i<itemsCount; i++)
    {
        if(i==focusedControl) continue;

        items[i]->setState(OL_GUI_STATE_STANDARD);
    }
}

bool ol_GUIPanel::CheckEvents(ol_Event *event, ol_GUIEvent *guiEvent)
{
    bool panelEvent = ol_GUIControl::CheckEvents(event, guiEvent);
    bool ctrlEvent = false;

    if(itemsCount>0)
    {
        for(int i=itemsCount-1; i>=0; i--)
            if(items[i]->CheckEvents(event, guiEvent))
            {
                ctrlEvent = true;
                if(guiEvent->type != OL_GUIEV_MOUSEMOVE)
                {
                    RefreshFocus(i);
                    break;
                }
            }
    }

    if(panelEvent || ctrlEvent) return true;

    return false;
}

void ol_GUIPanel::Draw(void)
{
    if(!visible) return;
    ol_GUIControl::Draw();
    if(itemsCount<=0) return;

    glScissor(botLeft.x, botLeft.y, width, height);
    glEnable(GL_SCISSOR_TEST);

    for(unsigned int i=0; i<itemsCount; i++)
        items[i]->Draw();

    glDisable(GL_SCISSOR_TEST);
}

void ol_GUIPanel::Shift(ol_Vector2df &dir)
{
    ol_GUIControl::Shift(dir);
    for(unsigned int i=0; i<itemsCount; i++)
        items[i]->Shift(dir);
}

void ol_GUIPanel::Update(void)
{
    ol_GUIControl::Update();
    for(unsigned int i=0; i<itemsCount; i++)
        items[i]->Update();
}

/**
    --------------------------------- GUI Window -------------------------------------------
    >                                                                                      <
    >                                Класс окна.                                           <
    >      По сути панель, у которой по умолчанию есть заголовок и кнопка закрыть.         <
    >                    Окно можно двигать за заголовок                                   <
    >                                                                                      <
    ----------------------------------------------------------------------------------------
**/

ol_GUIWindow::ol_GUIWindow(ol_Graphics *graph, ol_GUITheme *theme, ol_Vector2df origin, float width, float height, wstring caption, ol_GUIPositionPoints keyPoint, const char *name):ol_GUIPanel(graph,theme,origin,width,height,keyPoint, name)
{
    header = new ol_GUIButton(graph, theme, topLeft, this->width, theme->winHeadHeight, caption, OL_GUI_BUTOTN_VOID, OL_GUI_POS_TOPLEFT, "window header");
    header->showBackground(true);
    header->showBorder(true);
    this->addItem(header);

    ol_Vector2df tmpPos = header->centRight;
    tmpPos.x = tmpPos.x - theme->winHeadButShift;
    closeButton = new ol_GUIButton(graph, theme, tmpPos, theme->winHeadButSize, theme->winHeadButSize, L"", OL_GUI_BUTTON_STANDARD, OL_GUI_POS_CENTMID, "Close button");
    closeButton->showBackground(true);
    closeButton->showBorder(true);
    this->addItem(closeButton);

    // вычисляем левый нижний угол клиентской области окна
    tmpPos = this->botLeft + ol_Vector2df(theme->winClientMrgs.left, theme->winClientMrgs.bottom);
    // вычисляем ширину и высоту клиентской части окна
    float clW = this->width - theme->winClientMrgs.left - theme->winClientMrgs.right;
    float clH = this->height - theme->winClientMrgs.bottom - theme->winClientMrgs.top;

    clientArea = new ol_Area2d();
    clientArea->Init(tmpPos, clW, clH, OL_GUI_POS_BOTLEFT);
}

ol_GUIWindow::~ol_GUIWindow()
{
    delete clientArea;
#ifdef _OL_DEBUG_BUILD_
    printf("GUI Window Destructor ( ID = %d )\n", ID);
#endif // _OL_DEBUG_BUILD_
}

void ol_GUIWindow::Draw(void)
{
    ol_GUIPanel::Draw();
    /*
        // Отрисовка клиентской части окна. Можно удалить вместе с ф-ей дро
        if(!visible) return;
        glDisable(GL_TEXTURE_2D);
        graph->ol_GLColor(OL_COLOR_GREEN);
        glBegin(GL_QUADS);
            glVertex2f(clientArea->botLeft.x, clientArea->botLeft.y);
            glVertex2f(clientArea->topLeft.x, clientArea->topLeft.y);
            glVertex2f(clientArea->topRight.x, clientArea->topRight.y);
            glVertex2f(clientArea->botRight.x, clientArea->botRight.y);
        glEnd();
    */
}

void ol_GUIWindow::Shift(ol_Vector2df &dir)
{
    ol_GUIPanel::Shift(dir);
    clientArea->Shift(dir);
}

bool ol_GUIWindow::CheckEvents(ol_Event *event, ol_GUIEvent *guiEvent)
{
    if(header->isPressed())
    {
        ol_Vector2df dir = event->getCurPos()-header->lastClickPoint;
        this->Shift(dir);
        header->lastClickPoint = event->getCurPos();
    }

    if(ol_GUIPanel::CheckEvents(event, guiEvent))
    {
        if(guiEvent->type != OL_GUIEV_MOUSEMOVE) guiEvent->winEvent = true;

        if((guiEvent->control1 == closeButton)&&(guiEvent->type == OL_GUIEV_BUTTONCLICKED))
        {
            header->setState(OL_GUI_STATE_STANDARD);
            closeButton->setState(OL_GUI_STATE_STANDARD);
            this->Hide();
            return false;
        }

        return true;
    }

    return false;
}

/**
    --------------------------------- GUI File Dialog --------------------------------------
    >                                                                                      <
    >                       Диалог открытия/сохранения файлов.                             <
    >                                                                                      <
    ----------------------------------------------------------------------------------------
**/

ol_GUIFileDialog::ol_GUIFileDialog(ol_Graphics *graph, ol_GUITheme *theme, ol_Vector2df origin, float width, float height, wstring caption, ol_GUIFileDialogType type, ol_GUIPositionPoints keyPoint, const char *name):
    ol_GUIWindow(graph,theme,origin,width,height,caption,keyPoint,name)
{
    dialogType = type;

    wstring editCap;    // заголовок эдита
    wstring okCap;      // заголовок кнопки
    ol_Vector2df dp;     // начальная точка позиционирования контрола
    float cntWidth;     // используется для задания ширины контрола
    float cntHeight;    // используется для задания высоты контрола

    switch(type)
    {
    case OL_GUI_FILEDIALOG_OPEN:
        okCap = theme->fileDiagOpenOKCaption;
        break;
    case OL_GUI_FILEDIALOG_SAVE:
        okCap = theme->fileDiagSaveOKCaption;
        break;
    }

    editCap = theme->fileDiagPathCaption;

    dp = ol_Vector2df(theme->winClientMrgs.right, -theme->winClientMrgs.bottom);
    okButton = new ol_GUIButton(graph, theme, botRight-dp, 0, theme->buttHeight, okCap, OL_GUI_BUTTON_STANDARD, OL_GUI_POS_BOTRIGHT);

    dp = ol_Vector2df(theme->winClientMrgs.left, theme->winClientMrgs.bottom);
    cntWidth = this->width - theme->winClientMrgs.left - theme->winClientMrgs.right - theme->ctrlDist - okButton->width;
    fnameEdit = new ol_GUIEditBox(graph, theme, botLeft+dp, cntWidth, theme->editBoxHeight, OL_GUI_POS_BOTLEFT);
    fnameEdit->setText(L"");

    dp = fnameEdit->topLeft + ol_Vector2df(0, theme->ctrlDist);
    cntWidth = this->width - theme->winClientMrgs.left - theme->winClientMrgs.right ;
    pathEdit = new ol_GUILabeledEditBox(graph, theme, dp, cntWidth, theme->editBoxHeight, editCap, OL_GUI_POS_TOPLEFT, OL_GUI_POS_BOTLEFT);

    cntWidth = this->width - theme->winClientMrgs.left - theme->winClientMrgs.right;
    cntHeight = this->height - theme->winClientMrgs.top - 75;
    dp = ol_Vector2df(-theme->winClientMrgs.left, theme->winClientMrgs.top);
    browser = new ol_GUIFileBrowser(graph, theme, topLeft - dp, cntWidth, cntHeight, OL_GUI_POS_TOPLEFT);

    pathEdit->setText(browser->getCurrentPath());

    this->addItem(okButton);
    this->addItem(fnameEdit);
    this->addItem(pathEdit);
    this->addItem(browser);

    Hide();
}

ol_GUIFileDialog::~ol_GUIFileDialog()
{
#ifdef _OL_DEBUG_BUILD_
    printf("GUI File Dialog ( ID = %d )\n", ID);
#endif // _OL_DEBUG_BUILD_
}

bool ol_GUIFileDialog::CheckEvents(ol_Event *event, ol_GUIEvent *guiEvent)
{
    if(ol_GUIWindow::CheckEvents(event, guiEvent))
    {
        if((guiEvent->control1 == okButton)&&(guiEvent->type == OL_GUIEV_BUTTONCLICKED))
        {
            if(dialogType == OL_GUI_FILEDIALOG_OPEN)
                guiEvent->type = OL_GUIEV_FILEOPENED;

            if(dialogType == OL_GUI_FILEDIALOG_SAVE)
            {
                if(fnameEdit->getStringText() == "") return false;
                guiEvent->type = OL_GUIEV_FILESAVED;
            }

            guiEvent->control1 = this;
            this->Hide();
        }

        if(guiEvent->control1 == browser)
        {
            if(guiEvent->type == OL_GUIEV_FBCHANGEDPATH) pathEdit->setText(browser->getCurrentPath());
            if(guiEvent->type == OL_GUIEV_FBFILESELECTED && dialogType == OL_GUI_FILEDIALOG_OPEN)
                fnameEdit->setText(browser->getCurrentFile());
        }
        return true;
    }
    return false;
}

void ol_GUIFileDialog::Show(ol_GUIControl *caller)
{
    callerCtrl = caller;
    ol_GUIPanel::Show();
}

ol_GUIControl* ol_GUIFileDialog::getCaller(void)
{
    return callerCtrl;
}

string ol_GUIFileDialog::getPath(void)
{
    return browser->getCurrentPath();
}

string ol_GUIFileDialog::getFileName(bool full)
{
    if(full) return browser->getCurrentPath() + "/" + fnameEdit->getStringText();
    else return fnameEdit->getStringText();
}

bool ol_GUIFileDialog::getAllFiles(bool full, vector<string> &fnames)
{
    fnames.clear();

    if(full)
    {
        string pt;
        vector<string> fn;
        pt = browser->getCurrentPath();
        browser->getFilesList(fn);
        if(fn.size()<=0) return false;

        for(unsigned int i=0; i<fn.size(); i++)
            fnames.push_back(pt + "/" + fn[i]);
    }
    else
    {
        browser->getFilesList(fnames);
        if(fnames.size()>0) return true;
        else return false;
    }

    return true;
}


/**
    --------------------------------- GUI Items List ---------------------------------------
    >                                                                                      <
    >                               Список элементов.                                      <
    >      По сути панель, которая содержит набор контролов, которые можно добавлять.      <
    >                                                                                      <
    ----------------------------------------------------------------------------------------
**/

ol_GUIItemsList::ol_GUIItemsList(ol_Graphics *graph, ol_GUITheme *theme, ol_Vector2df origin, float width, float height, ol_GUIPositionPoints keyPoint, const char *name): ol_GUIPanel(graph,theme,origin,width,height,keyPoint,name)
{}

ol_GUIItemsList::~ol_GUIItemsList()
{
#ifdef _OL_DEBUG_BUILD_
    printf("GUI Items List Destructor ( ID = %d )\n", ID);
#endif // _OL_DEBUG_BUILD_
}

void ol_GUIItemsList::addItem(ol_GUIControl *item)
{
    ol_GUIPanel::addItem(item);

    if(!item) return;

    if(itemsCount == 1) item->setPosition(this->topLeft, OL_GUI_POS_TOPLEFT);
    if(itemsCount > 1) item->setPosition(nextPosPoint, OL_GUI_POS_TOPLEFT);

    nextPosPoint = item->botLeft;
}

/**
    --------------------------------- GUI File Browser -------------------------------------
    >                                                                                      <
    >                               Проводник файлов.                                      <
    >      Выполнен на основе Items List. Позволяет перемещаться по файловой системе.      <
    >                                                                                      <
    ----------------------------------------------------------------------------------------
**/

ol_GUIFileBrowser::ol_GUIFileBrowser(ol_Graphics *graph, ol_GUITheme *theme, ol_Vector2df origin, float width, float height, ol_GUIPositionPoints keyPoint, const char *name):ol_GUIItemsList(graph,theme,origin,width,height,keyPoint,name)
{
    curPath.push_back("/home");
    curFile = "";
    showHidden = false;
    scrollVel = 10.0f;
    BuildFilesList();
}

ol_GUIFileBrowser::~ol_GUIFileBrowser()
{
#ifdef _OL_DEBUG_BUILD_
    printf("GUI File Browser Destructor ( ID = %d )\n", ID);
#endif // _OL_DEBUG_BUILD_
}

string ol_GUIFileBrowser::getCurrentPath(void)
{
    string s;
    for(unsigned int i=0; i<curPath.size(); i++)
        s=s+curPath[i];
    return s;
}

string ol_GUIFileBrowser::getCurrentFile(void)
{
    return curFile;
}

void ol_GUIFileBrowser::getFilesList(vector<string> &fnames)
{
    for(unsigned int i=0; i<files.size(); i++)
        if(files[i].type == OL_DIR_TYPE_FILE)
            fnames.push_back(files[i].name);
}

int ol_GUIFileBrowser::FindControl(ol_GUIControl *ctrl)
{
    for(unsigned int i=0; i<itemsCount; i++)
        if(items[i] == ctrl) return i;

    return -1;
}

void ol_GUIFileBrowser::BuildFilesList(void)
{
    for(unsigned short i=0; i<items.size(); i++)
        delete items[i];

    items.clear();

    itemsCount = 0;

    ol_GUIButton *but;
    wstring ws;

    curFile = "";

    if(!ListDir(getCurrentPath(), files, showHidden)) return;

    for(unsigned short i=0; i<files.size(); i++)
    {
        String_to_Wstring(files[i].name, ws);
        but = new ol_GUIButton(this->graph, this->theme, ol_Vector2df(0,0), this->width, 20, ws, OL_GUI_BUTTON_MENUITEM);
        this->addItem(but);
    }
}

bool ol_GUIFileBrowser::CheckEvents(ol_Event *event, ol_GUIEvent *guiEvent)
{
    if(ol_GUIPanel::CheckEvents(event, guiEvent))
    {
        if(guiEvent->type == OL_GUIEV_BUTTONCLICKED)
        {
            int n = FindControl(guiEvent->control1);

            if(n<0) return false;

            if(files[n].type == OL_DIR_TYPE_FILE)
            {
                curFile = files[n].name;
                guiEvent->type = OL_GUIEV_FBFILESELECTED;
                guiEvent->control1 = this;
                return true;
            }
            if(files[n].name == "..") curPath.pop_back();
            if(files[n].type == OL_DIR_TYPE_DIR && files[n].name != "..")  curPath.push_back("/" + files[n].name);

            printf("BuildFilesList, curPath = '%s'\n", this->getCurrentPath().c_str());
            BuildFilesList();
            guiEvent->type = OL_GUIEV_FBCHANGEDPATH;
            guiEvent->control1 = this;
            return true;
        }

        if(guiEvent->type == OL_GUIEV_SCROLL)
        {
            ol_Vector2df vShift(0.0f, 0.0f);
            for(unsigned int i=0; i<itemsCount; i++)
            {
                vShift.y = event->getMouseWheel()*(-scrollVel);
                items[i]->Shift(vShift);
            }
        }
    }

    return false;
}

/**
    --------------------------------- GUI Label --------------------------------------------
    >                                                                                      <
    >                            Статическая надпись.                                      <
    >         Хранит текст и постоянно его выводит максимально быстрым способом.           <
    >                                                                                      <
    ----------------------------------------------------------------------------------------
**/

ol_GUILabel::ol_GUILabel(ol_Graphics *graph, ol_GUITheme *theme, ol_Vector2df origin, float width, float height, wstring text, ol_GUIPositionPoints keyPoint, const char *name): ol_GUIControl(graph, theme, origin, width, height, keyPoint, name)
{
    this->font = theme->font;
    brdVisible = bgrVisible = false;
    this->origin = origin;
    this->posPoint = keyPoint;

    color = theme->fontColor;
    setText(text);
}

ol_GUILabel::~ol_GUILabel()
{
#ifdef _OL_DEBUG_BUILD_
    printf("GUI Label Destructor ( ID = %d )\n", ID);
#endif // _OL_DEBUG_BUILD_
}

bool ol_GUILabel::CheckEvents(ol_Event *event, ol_GUIEvent *guiEvent)
{
    return false;
}

void ol_GUILabel::setText(wstring text)
{
    this->text = text;
    length = text.size();

    if(length<=0) return;

    dlists.resize(length);
    float w, h;

    font->ConvertCharCodes(text.data(), dlists.data(), length, w, h);

    ol_Area2d::Init(origin, w, h, posPoint);
}

void ol_GUILabel::Draw(void)
{
    if(!visible) return;
    ol_GUIControl::Draw();

    graph->ol_GLColor(color);
    font->TextOut(botLeft.x, botLeft.y, dlists.data(), length);
}

/**
    --------------------------------- GUI Button -------------------------------------------
    >                                                                                      <
    >                           Класс простейшей кнопки.                                   <
    >         Генерирует Баттон_Клик, если нажата, а затем отжата левая клавиша мыши.      <
    >                                                                                      <
    ----------------------------------------------------------------------------------------
**/

ol_GUIButton::ol_GUIButton(ol_Graphics *graph, ol_GUITheme *theme, ol_Vector2df origin, float width, float height, wstring caption, ol_GUIButtonStyle style, ol_GUIPositionPoints keyPoint, const char *name): ol_GUIControl(graph, theme, origin, width, height, keyPoint, name)
{
    this->style = style;
    this->caption = new ol_GUILabel(graph, theme, origin, 1, 1, caption, keyPoint, name);

    float w = width;
    float h = height;

    if(width<=0) w = this->caption->width + theme->buttLabHorShift*2;
    if(height<=0) h = this->caption->height + theme->buttLabVertShift*2;

    ol_Area2d::Init(origin, w, h, keyPoint);

    if(style == OL_GUI_BUTTON_MENUITEM)
    {
        capPos = this->centLeft + ol_Vector2df(theme->buttLabHorShift, 0);
        capAlign = OL_GUI_POS_CENTLEFT;
    }
    else
    {
        capPos = this->centMid;
        capAlign = OL_GUI_POS_CENTMID;
    }

    this->caption->setPosition(capPos, capAlign);
    vPressed = capPos + ol_Vector2df(1.0f, -1.0f);

    labStandardColor = theme->buttLabStandardColor;
    labSelectedColor = theme->buttLabSelectedColor;
}

ol_GUIButton::~ol_GUIButton()
{
    delete caption;
#ifdef _OL_DEBUG_BUILD_
    printf("GUI Button destructor ( ID = %d )\n", ID);
#endif // _OL_DEBUG_BUILD_
}

bool ol_GUIButton::isPressed(void)
{
    if(state == OL_GUI_STATE_PRESSED) return true;
    else return false;
}

void ol_GUIButton::setState(ol_GUIControlStates state)
{
    ol_GUIControl::setState(state);

    if(style == OL_GUI_BUTOTN_VOID) return;

    switch(state)
    {
    case OL_GUI_STATE_DISABLED:
        setCurFrame(3);
        caption->color = OL_COLOR_GREY;
        break;
    case OL_GUI_STATE_FOCUSED:
        break;
    case OL_GUI_STATE_PRESSED:
        setCurFrame(2);
        if(style == OL_GUI_BUTTON_STANDARD) caption->setPosition(vPressed, OL_GUI_POS_CENTMID);
        caption->color = labSelectedColor;
        break;
    case OL_GUI_STATE_SELECTED:
        setCurFrame(1);
        if(style == OL_GUI_BUTTON_STANDARD) caption->setPosition(centMid, OL_GUI_POS_CENTMID);
        caption->color = labSelectedColor;
        break;
    case OL_GUI_STATE_STANDARD:
        setCurFrame(0);
        if(style == OL_GUI_BUTTON_STANDARD) caption->setPosition(centMid, OL_GUI_POS_CENTMID);
        caption->color = labStandardColor;
        break;
    }
}

void ol_GUIButton::setStyle(ol_GUIButtonStyle style)
{
	this->style = style;
	if(style == OL_GUI_BUTOTN_VOID) setCurFrame(4);
}

bool ol_GUIButton::CheckEvents(ol_Event *event, ol_GUIEvent *guiEvent)
{
    if(state == OL_GUI_STATE_DISABLED) return false;

    if(ol_GUIControl::CheckEvents(event, guiEvent))
    {
        if(style == OL_GUI_BUTTON_LOCKED) return true;

        switch(guiEvent->type)
        {
        case OL_GUIEV_LBUTTONDOWN:
            setState(OL_GUI_STATE_PRESSED);

            if(style == OL_GUI_BUTTON_SIMPLE)
            {
                guiEvent->type = OL_GUIEV_BUTTONPRESSED;
                return true;
            }
            break;

        case OL_GUIEV_LBUTTONUP:
            if((state == OL_GUI_STATE_PRESSED)&&(style == OL_GUI_BUTTON_STANDARD || style == OL_GUI_BUTTON_MENUITEM))
            {
                guiEvent->type = OL_GUIEV_BUTTONCLICKED;
                setState(OL_GUI_STATE_SELECTED);
                return true;
            }
            setState(OL_GUI_STATE_SELECTED);
            break;

        case OL_GUIEV_MOUSEMOVE:
            if(state == OL_GUI_STATE_STANDARD) setState(OL_GUI_STATE_SELECTED);
            return true;
            break;

		case OL_GUIEV_RBUTTONDOWN:
			return true;
			break;
		case OL_GUIEV_RBUTTONUP:
			return true;
			break;

        default:
            break;
        }
    }
    else
    {
        if(state == OL_GUI_STATE_SELECTED)
            setState(OL_GUI_STATE_STANDARD);
        if((!event->getKey(OL_KEY_LBUTTON))&&(state == OL_GUI_STATE_PRESSED))
            setState(OL_GUI_STATE_STANDARD);
    }

    return false;
}

void ol_GUIButton::Draw(void)
{
    if(!visible) return;
    ol_GUIControl::Draw();

    caption->Draw();
}

void ol_GUIButton::Shift(ol_Vector2df &dir)
{
    ol_GUIControl::Shift(dir);
    vPressed = vPressed + dir;
    caption->Shift(dir);
}

void ol_GUIButton::setCaption(wstring text)
{
	caption->setText(text);
}

void ol_GUIButton::setCaption(string text)
{
	wstring ws;
	String_to_Wstring(text, ws);
	caption->setText(ws);
}

void ol_GUIButton::setCaption(int val)
{
	string s = IntToString(val);
	setCaption(s);
}

void ol_GUIButton::setCaption(double val)
{
	string s = DoubleToString(val);
	setCaption(s);
}


/**
    --------------------------------- GUI EditBox ------------------------------------------
    >                                                                                      <
    >                              Класс поля ввода.                                       <
    >                                                                                      <
    ----------------------------------------------------------------------------------------
**/

ol_GUIEditBox::ol_GUIEditBox(ol_Graphics *graph, ol_GUITheme *theme, ol_Vector2df origin, float width, float height, ol_GUIPositionPoints keyPoint, const char *name): ol_GUIControl(graph, theme, origin, width, height, keyPoint, name)
{
    text = L"Edit Box text";
    caretPos = text.size();
    font = theme->font;

    adjustCaret(true);
    caretBlinkPer = NULL;
    caretBlink = false;
    caretShift_last = caretShift;
    
    setState(OL_GUI_STATE_STANDARD);

    mrg = theme->editBoxMargins;
    
    bgColor = theme->editBoxBgColor;
    bgColor1 = theme->editBoxBgColor;

    printArea.Init(botLeft+ol_Vector2df(mrg.left, mrg.bottom), width - mrg.left - mrg.right, height - mrg.top - mrg.bottom, OL_GUI_POS_BOTLEFT);
    printOrigin = printArea.centLeft;
}

ol_GUIEditBox::~ol_GUIEditBox()
{
#ifdef _OL_DEBUG_BUILD_
    printf("GUI Edit Box destructor ( ID = %d )\n", ID);
#endif // _OL_DEBUG_BUILD_
}

void ol_GUIEditBox::setText(wstring text)
{
    if(state == OL_GUI_STATE_FOCUSED) return;
    this->text =  text;
    caretPos = this->text.size();

    caretShift = font->getCaretShift(text.c_str(), text.size(), caretPos);

    if(caretShift<=printArea.width) printOrigin = printArea.centLeft;
    if(caretShift>=printArea.width) printOrigin = printArea.centRight- ol_Vector2df(caretShift, 0);
}

void ol_GUIEditBox::setText(string text)
{
    if(state == OL_GUI_STATE_FOCUSED) return;
    wstring ws;
    String_to_Wstring(text, ws);
    setText(ws);
}

void ol_GUIEditBox::setText(int val)
{
    if(state == OL_GUI_STATE_FOCUSED) return;
    char buffer[sizeof(int)*8+1]; // в мануале написано, что так по-любому хватит места
    sprintf(buffer,"%d",val);
    string s(buffer);
    setText(s);
}

void ol_GUIEditBox::setText(double val)
{
    if(state == OL_GUI_STATE_FOCUSED) return;
    char buffer[50]; // ну, надеюсь хватит места

    if((val>99999.0) || (val<-99999.0) || ((val>-0.0001)&&(val<0.0001)&&(val!=0)))
        sprintf(buffer,"%e",val);
    else sprintf(buffer,"%f",val);

    string s(buffer);
    setText(s);
}

wstring ol_GUIEditBox::getText(void)
{
    return text;
}

string ol_GUIEditBox::getStringText(void)
{
    string str = "ERROR"; // Если конвертация в string не сработает тогда сохранится надпись ошибка
    Wstring_to_String(text, str);
    return str;
}

double ol_GUIEditBox::getDoubleVal(void)
{
    string str;
    Wstring_to_String(text, str);
    return atof(str.c_str());
}

int ol_GUIEditBox::getIntVal(void)
{
    string str;
    Wstring_to_String(text, str);
    return atoi(str.c_str());
}

void ol_GUIEditBox::setState(ol_GUIControlStates state)
{
    ol_GUIControl::setState(state);

    switch(state)
    {
    case OL_GUI_STATE_DISABLED:
        setCurFrame(2);
        break;
    case OL_GUI_STATE_FOCUSED:
        setCurFrame(1);
        caretBlinkPer = ol_CoreParams::timer->addTimeMonitor(0.5);
	bgColor = theme->editBoxBgColor;
	bgColor1 = theme->editBoxBgColor;
        break;
    case OL_GUI_STATE_SELECTED:
        setCurFrame(1);
        break;
    case OL_GUI_STATE_STANDARD:
        setCurFrame(0);
        ol_CoreParams::timer->RemoveTimeMonitior(caretBlinkPer);
        caretBlinkPer = NULL;
        caretBlink = false;
       	bgColor = theme->editBoxBgColor;
	bgColor1 = theme->editBoxBgColor;
	break;
    case OL_GUI_STATE_PRESSED:
        break;
    }
}

bool ol_GUIEditBox::CheckEvents(ol_Event *event, ol_GUIEvent *guiEvent)
{
    if(state == OL_GUI_STATE_DISABLED) return false;

    if(caretBlinkPer)
        if(caretBlinkPer->itIsTime)
        {
            if(caretBlink) caretBlink = false;
            else caretBlink = true;
        }

    if(ol_GUIControl::CheckEvents(event, guiEvent))
    {
        switch(guiEvent->type)
        {
        case OL_GUIEV_LBUTTONDOWN:
            setState(OL_GUI_STATE_FOCUSED);
            break;

        case OL_GUIEV_MOUSEMOVE:
            if(state == OL_GUI_STATE_STANDARD)
                setState(OL_GUI_STATE_SELECTED);
            break;

        default:
            break;
        }
    }
    else
    {
        if((event->getMType() == OL_MEV_LBUTTONDOWN)&&(state == OL_GUI_STATE_FOCUSED)) setState(OL_GUI_STATE_STANDARD);
        if(state == OL_GUI_STATE_SELECTED) setState(OL_GUI_STATE_STANDARD);
    }

    // обработка ввода символов и каретки
    if((event->getType() == OL_IETYPE_KEYBOARD)&&(event->getKType() == OL_KEV_KEYDOWN)&&(state == OL_GUI_STATE_FOCUSED))
    {
        switch(event->getKeyCode())
        {
        case OL_KEY_BACK:
            if(caretPos<=0) break;
            text.erase(caretPos-1, 1);
            caretPos--;
            caretBlink = true;
            adjustCaret(true);
            break;
        case OL_KEY_LEFT:
            if(caretPos<=0) break;
            caretPos--;
            caretBlink = true;
            adjustCaret(false);
            break;
        case OL_KEY_RIGHT:
            if(caretPos>=text.size()) break;
            caretPos++;
            caretBlink = true;
            adjustCaret(false);
            break;
        case OL_KEY_DELETE:
            if(caretPos>(text.size()-1)) break;
            text.erase(caretPos, 1);
            adjustCaret(false);
            break;
        case OL_KEY_RETURN:
            guiEvent->type = OL_GUIEV_EDITBOXENTER;
            guiEvent->control1 = this;
            setState(OL_GUI_STATE_STANDARD);
            return true;
            break;
        default:
            if(event->getCharCode()>0)
            {
                wchar_t charCode = event->getCharCode();

                if(font->isValidCharCode(charCode))
                {
                    wstring::iterator it;
                    it = text.begin();
                    text.insert(it + caretPos, charCode);
                    caretPos++;
                    adjustCaret(false);
                }
            }
            break;
        }
    }

    return false;
}

void ol_GUIEditBox::adjustCaret(bool isBackSpase)
{
    caretShift_last = caretShift;
    caretShift = font->getCaretShift(text.c_str(), text.size(), caretPos);
    float dx = printOrigin.x + caretShift;
    if(dx>=printArea.centRight.x) printOrigin.x = printOrigin.x - dx + printArea.centRight.x;
    if(dx<=printArea.centLeft.x) printOrigin.x = printOrigin.x + printArea.centLeft.x - dx;
    if((isBackSpase)&&(printOrigin.x<printArea.centLeft.x)) printOrigin.x = printOrigin.x + caretShift_last - caretShift;
}

void ol_GUIEditBox::Shift(ol_Vector2df &dir)
{
    ol_GUIControl::Shift(dir);
    printArea.Shift(dir);
    printOrigin = printOrigin + dir;
}

void ol_GUIEditBox::Draw(void)
{
    if(!visible) return;
    ol_GUIControl::Draw();

    glScissor(printArea.botLeft.x-1, printArea.botLeft.y-1, printArea.width+1, printArea.height+1);
    glEnable(GL_SCISSOR_TEST);
    graph->ol_GLColor(theme->fontColor);
    font->Print(printOrigin.x, printOrigin.y, OL_GUI_POS_CENTLEFT, L"%ls", text.c_str());

    if(caretBlink)
    {
        glDisable(GL_TEXTURE_2D);
        glBegin(GL_LINES);
        glVertex2f(printOrigin.x + caretShift, printArea.topLeft.y); // +1 чтобы обрезка не перекрывала каретку в крайнем левом положении
        glVertex2f(printOrigin.x + caretShift, printArea.botLeft.y);
        glEnd();
    }
    glDisable(GL_SCISSOR_TEST);
}

/**
    ------------------------------ GUI Labeled EditBox -------------------------------------
    >                                                                                      <
    >                      Класс поля ввода с готовой подписью.                            <
    >                                                                                      <
    ----------------------------------------------------------------------------------------
**/

ol_GUILabeledEditBox::ol_GUILabeledEditBox(ol_Graphics *graph, ol_GUITheme *theme, ol_Vector2df origin, float width, float height, wstring caption, ol_GUIPositionPoints labelPoint, ol_GUIPositionPoints keyPoint, const char *name)
    :ol_GUIEditBox(graph,theme,origin,width,height,keyPoint,name)
{
    ol_Vector2df initPos;
    ol_GUIPositionPoints initPoint;
    CalcLabelRelPos(this, labelPoint, theme->editBoxLabShift, initPos, initPoint);
    this->caption = new ol_GUILabel(graph,theme,initPos,1,1,caption,initPoint);

    setPosition(origin, keyPoint);
}

ol_GUILabeledEditBox::~ol_GUILabeledEditBox()
{
    delete caption;
#ifdef _OL_DEBUG_BUILD_
    printf("GUI Labeled Edit Box destructor ( ID = %d )\n", ID);
#endif // _OL_DEBUG_BUILD_
}

ol_Vector2df ol_GUILabeledEditBox::setPosition(ol_Vector2df pos, ol_GUIPositionPoints posPoint)
{
    ol_Vector2df v;
    ol_Area2d a;

    a = *this + *caption;

    v = pos - a.getPosPoint(posPoint);

    this->Shift(v);

    return v;
}

void ol_GUILabeledEditBox::Shift(ol_Vector2df &dir)
{
    ol_GUIEditBox::Shift(dir);
    caption->Shift(dir);
}

void ol_GUILabeledEditBox::Draw()
{
    ol_GUIEditBox::Draw();
    caption->Draw();
}

/**
    ------------------------------ GUI Text Box --------------------------------------------
    >                                                                                      <
    >                 Многострочное поле вывода с переносом строк.                         <
    >                                                                                      <
    ----------------------------------------------------------------------------------------
**/

ol_GUITextBox::ol_GUITextBox(ol_Graphics *graph, ol_GUITheme *theme, ol_Vector2df origin, float width, float height, ol_GUIPositionPoints keyPoint, const char *name):
    ol_GUIControl(graph, theme, origin, width, height, keyPoint, name)
{
    font = theme->font;
    scroll = 0.0f;
    lineInterval = 1.2;
    caretStep = font->getHeight() * lineInterval;
    ResetCaret();
}

ol_GUITextBox::~ol_GUITextBox()
{
#ifdef _OL_DEBUG_BUILD_
    printf("GUI Text Box destructor ( ID = %d )\n", ID);
#endif // _OL_DEBUG_BUILD_
}

void ol_GUITextBox::setLineInterval(float interval)
{
    lineInterval = interval;
    caretStep = font->getHeight() * lineInterval;
}

void ol_GUITextBox::ResetCaret(void)
{
    caret = topLeft;
    caret.y = caret.y - caretStep - scroll;
}

void ol_GUITextBox::Draw(void)
{
    ol_GUIControl::Draw();

    ResetCaret();
    graph->ol_GLColor(theme->fontColor);
    for(unsigned int i=0; i<lines.size(); i++)
    {
        font->TextOut(caret.x, caret.y, lines[i].data(), lines[i].size());
        caret.y = caret.y - caretStep;
    }
}

void ol_GUITextBox::addLine(wstring text)
{
    ol_GLString buf;
    float w, h;

    int length = text.size();
    if(length<=0) return;

    buf.resize(length);
    font->ConvertCharCodes(text.data(), buf.data(), length, w, h);
    lines.push_back(buf);
}

void ol_GUITextBox::addText(wstring text)
{
    vector<wstring> words;   // текст разбитый по словам
    ol_GLString bufW;        // буфер для хранения слова
    ol_GLString bufL;        // буфер для хранения строки

    float w, h;              // ширина, высота слова
    float wL = 0;            // ширина строки

    SplitSring((wchar_t*)text.c_str(), L" ", words);

    for(unsigned int i=0; i<words.size(); i++)
    {
        words[i] = words[i] + L" ";
        bufW.resize(words[i].length());
        font->ConvertCharCodes(words[i].data(), bufW.data(), words[i].length(), w, h);
        if((wL + w)<= this->width)
        {
            wL+=w;
            // добавляем в bufL bufW
            bufL.reserve(bufL.size() + bufW.size());
            bufL.insert(bufL.end(), bufW.begin(), bufW.end());
        }
        else
        {
            lines.push_back(bufL);

            bufL.clear();
            wL = 0;

            wL+=w;
            // добавляем в bufL bufW
            bufL.reserve(bufL.size() + bufW.size());
            bufL.insert(bufL.end(), bufW.begin(), bufW.end());

        }
    }

    lines.push_back(bufL);
}

/**
    ------------------------------ GUI Scroll Box ------------------------------------------
    >                                                                                      <
    >       Полоса прокрутки. Позволяет плавно менять значения в заданном диапазоне.       <
    >       Всегда знает на сколько % относительно своей длины сдвинут ползунок.           <
    >                                                                                      <
    ----------------------------------------------------------------------------------------
**/

ol_GUIScrollBox::ol_GUIScrollBox(ol_Graphics *graph, ol_GUITheme *theme, ol_Vector2df origin, float width, float height, ol_GUIScrollBoxType type, ol_GUIPositionPoints keyPoint, const char *name):
    ol_GUIPanel(graph, theme, origin, width, height, keyPoint, name)
{
    this->type = type;

    scrollVel = 5.0f;
    curVal = 0.0f;
    minVal = 0.0f;
    maxVal = 1.0f;
    range = maxVal - minVal;

    switch(type)
    {
    case OL_GUI_SCROLLBOX_HORIZONTAL:
        scroller = new ol_GUIButton(graph, theme, botLeft, theme->scrollerThickness, this->height, L"", OL_GUI_BUTOTN_VOID, OL_GUI_POS_BOTLEFT, "Scroller");
        l100 = width - scroller->width;
        break;
    case OL_GUI_SCROLLBOX_VERTICAL:
        scroller = new ol_GUIButton(graph, theme, botLeft, this->width, theme->scrollerThickness, L"", OL_GUI_BUTOTN_VOID, OL_GUI_POS_BOTLEFT, "Scroller");
        l100 = height - scroller->height;
        break;
    }

    this->addItem(scroller);
}

ol_GUIScrollBox::~ol_GUIScrollBox()
{
#ifdef _OL_DEBUG_BUILD_
    printf("GUI Scroll Box Destructor ( ID = %d )\n", ID);
#endif // _OL_DEBUG_BUILD_
}

bool ol_GUIScrollBox::CheckEvents(ol_Event *event, ol_GUIEvent *guiEvent)
{
    if(ol_GUIPanel::CheckEvents(event, guiEvent))
    {
        ol_Vector2df shDir(0.0f, 0.0f);

        switch(type)
        {
        case OL_GUI_SCROLLBOX_HORIZONTAL:
            if(guiEvent->type == OL_GUIEV_SCROLL)
            {
                shDir.y = 0.0f;
                shDir.x = event->getMouseWheel()*(-scrollVel);
                scroller->Shift(shDir);
            }

            if(scroller->isPressed())
            {
                shDir.y = 0.0f;
                shDir.x = event->getCurPos().x - scroller->lastClickPoint.x;
                scroller->Shift(shDir);
                scroller->lastClickPoint = event->getCurPos();
            }

            if(scroller->centLeft.x < centLeft.x) scroller->setPosition(centLeft, OL_GUI_POS_CENTLEFT);
            if(scroller->centRight.x > centRight.x) scroller->setPosition(centRight, OL_GUI_POS_CENTRIGHT);

            curPer = (scroller->centLeft.x - centLeft.x)*100.0f/l100;
            break;

        case OL_GUI_SCROLLBOX_VERTICAL:
            if(guiEvent->type == OL_GUIEV_SCROLL)
            {
                shDir.y = event->getMouseWheel()*(scrollVel);
                shDir.x = 0.0f;
                scroller->Shift(shDir);
            }

            if(scroller->isPressed())
            {
                shDir.y = event->getCurPos().y - scroller->lastClickPoint.y;
                shDir.x = 0.0f;
                scroller->Shift(shDir);
                scroller->lastClickPoint = event->getCurPos();
            }

            if(scroller->botMid.y < botMid.y) scroller->setPosition(botMid, OL_GUI_POS_BOTMID);
            if(scroller->topMid.y > topMid.y) scroller->setPosition(topMid, OL_GUI_POS_TOPMID);

            curPer = (scroller->botMid.y - botMid.y)*100.0f/l100;

            break;
        }

        curVal = minVal + range*curPer/100.0f;

        guiEvent->type = OL_GUIEV_SCROLLBOXCHANGED;
        guiEvent->control1 = this;

        return true;
    }

    return false;
}

void ol_GUIScrollBox::RepositionScroller(void)
{
    ol_Vector2df shDir(0.0f, 0.0f);

    switch(type)
    {
    case OL_GUI_SCROLLBOX_HORIZONTAL:
        shDir.x = l100*curPer/100.0f;
        shDir = centLeft + shDir;
        scroller->setPosition(shDir, OL_GUI_POS_CENTLEFT);
        break;

    case OL_GUI_SCROLLBOX_VERTICAL:
        shDir.y = l100*curPer/100.0f;
        shDir = botMid + shDir;
        scroller->setPosition(shDir, OL_GUI_POS_BOTMID);
        break;
    }
}

void ol_GUIScrollBox::setValue(float val)
{
    curVal = val;
    if(val <= minVal) curVal = minVal;
    if(val >= maxVal) curVal = maxVal;

    curPer = curVal*100/maxVal;
    RepositionScroller();
}

void ol_GUIScrollBox::setPercentage(float per)
{
    this->curPer = per;
    if(per <= 0) curPer = 0;
    if(per >= 100) curPer = 100;

    curVal = minVal + range*curPer/100.0f;
    RepositionScroller();
}

void ol_GUIScrollBox::setMinimum(float minVal)
{
    this->minVal = minVal;
    range = maxVal - minVal;
    RepositionScroller();
}

void ol_GUIScrollBox::setMaximum(float maxVal)
{
    this->maxVal = maxVal;
    range = maxVal - minVal;
    RepositionScroller();
}

float ol_GUIScrollBox::getValue(void)
{
    return curVal;
}

float ol_GUIScrollBox::getMinimum(void)
{
    return minVal;
}

float ol_GUIScrollBox::getMaximum(void)
{
    return maxVal;
}

float ol_GUIScrollBox::getPercentage(void)
{
    return curPer;
}

/**
    ------------------------------ GUI Edit Scroll Box -------------------------------------
    >                                                                                      <
    >              Составной контрол. Состоит из скроллера и эдита.                        <
    >               Можно менять значения и скроллером и эдитом.                           <
    >                                                                                      <
    ----------------------------------------------------------------------------------------
**/

ol_GUIEdScrollBox::ol_GUIEdScrollBox(ol_Graphics *graph, ol_GUITheme *theme, ol_Vector2df origin, float width, float height, float scSize, float edSize, ol_GUIScrollBoxType type, ol_GUIEdScrollPos relPos, ol_GUIPositionPoints keyPoint, const char *name):
    ol_GUIPanel(graph, theme, origin, width, height, keyPoint, name)
{
    this->type = type;
    float edW, edH, scW, scH;
    switch(type)
    {
    case OL_GUI_SCROLLBOX_HORIZONTAL:
        edH = height;
        edW = edSize;
        scH = scSize;
        if(scSize > height) scH = height;
        if(scSize < 3) scH = 3.0f;
        scW = width - edW - theme->ctrlDist;

        switch(relPos)
        {
        case OL_GUI_EDSCROLL_BEGIN:
            scroll = new ol_GUIScrollBox(graph, theme, this->centRight, scW, scH, type, OL_GUI_POS_CENTRIGHT);
            edit = new ol_GUIEditBox(graph, theme, this->centLeft, edW, edH, OL_GUI_POS_CENTLEFT);
            break;

        case OL_GUI_EDSCROLL_END:
            scroll = new ol_GUIScrollBox(graph, theme, this->centLeft, scW, scH, type, OL_GUI_POS_CENTLEFT);
            edit = new ol_GUIEditBox(graph, theme, this->centRight, edW, edH, OL_GUI_POS_CENTRIGHT);
            break;
        }
        break;

    case OL_GUI_SCROLLBOX_VERTICAL:
        edH = edSize;
        edW = width;
        scH = height - edH - theme->ctrlDist;
        scW = scSize;
        if(scSize > width) scW = width;
        if(scSize < 3) scW = 3.0f;

        switch(relPos)
        {
        case OL_GUI_EDSCROLL_BEGIN:
            scroll = new ol_GUIScrollBox(graph, theme, this->topMid, scW, scH, type, OL_GUI_POS_TOPMID);
            edit = new ol_GUIEditBox(graph, theme, this->botMid, edW, edH, OL_GUI_POS_BOTMID);
            break;

        case OL_GUI_EDSCROLL_END:
            scroll = new ol_GUIScrollBox(graph, theme, this->botMid, scW, scH, type, OL_GUI_POS_BOTMID);
            edit = new ol_GUIEditBox(graph, theme, this->topMid, edW, edH, OL_GUI_POS_TOPMID);
            break;
        }
        break;
    }

    this->addItem(scroll);
    this->addItem(edit);

    edit->setText(scroll->getValue());
    caption = NULL;
}

ol_GUIEdScrollBox::~ol_GUIEdScrollBox()
{
#ifdef _OL_DEBUG_BUILD_
    printf("GUI Edit Scroll Box Destructor ( ID = %d )\n", ID);
#endif // _OL_DEBUG_BUILD_
}

bool ol_GUIEdScrollBox::CheckEvents(ol_Event *event, ol_GUIEvent *guiEvent)
{
    if(ol_GUIPanel::CheckEvents(event, guiEvent))
    {
        if((guiEvent->control1 == scroll)&&(guiEvent->type == OL_GUIEV_SCROLLBOXCHANGED))
        {
            edit->setText(scroll->getValue());
        }

        if((guiEvent->control1 == edit)&&(guiEvent->type == OL_GUIEV_EDITBOXENTER))
        {
            scroll->setValue((float)edit->getDoubleVal());
        }

        guiEvent->control1 = this;
        guiEvent->type = OL_GUIEV_SCROLLBOXCHANGED;
        return true;
    }

    return false;
}

void ol_GUIEdScrollBox::setLabel(wstring text, ol_GUIPositionPoints point)
{
    ol_Vector2df initPos;
    ol_GUIPositionPoints initPoint;
    CalcLabelRelPos(this, point, theme->ctrlDist, initPos, initPoint);
    caption = new ol_GUILabel(graph, theme, initPos, 1, 1, text, initPoint);
    addItem(caption);
}

void ol_GUIEdScrollBox::setValue(float val)
{
    scroll->setValue(val);
    edit->setText(scroll->getValue());
}

void ol_GUIEdScrollBox::setMinimum(float minVal)
{
    scroll->setMinimum(minVal);
}
void ol_GUIEdScrollBox::setMaximum(float maxVal)
{
    scroll->setMaximum(maxVal);
}
void ol_GUIEdScrollBox::setPercentage(float per)
{
    scroll->setPercentage(per);
    edit->setText(scroll->getValue());
}

float ol_GUIEdScrollBox::getValue(void)
{
    return scroll->getValue();
}
float ol_GUIEdScrollBox::getMinimum(void)
{
    return scroll->getMinimum();
}
float ol_GUIEdScrollBox::getMaximum(void)
{
    return scroll->getMaximum();
}
float ol_GUIEdScrollBox::getPercentage(void)
{
    return scroll->getPercentage();
}

/**
    ---------------------------------- GUI Image -------------------------------------------
    >                                                                                      <
    >             Класс изображения. Просто постоянно отображает картинку                  <
    >       Фактически является базовым гуи контролом с минимальными доработками:          <
    >                    по умолчанию загружается анимация с 1 кадром.                     <
    >                                                                                      <
    ----------------------------------------------------------------------------------------
**/

ol_GUIImage::ol_GUIImage(ol_Graphics *graph, ol_GUITheme *theme, ol_Vector2df origin, float width, float height, ol_Texture tex, ol_GUIPositionPoints keyPoint, const char *name): ol_GUIControl(graph, theme, origin, width, height, keyPoint, name)
{
    if(AssignAnimation(tex)) brdVisible = bgrVisible = false;
}

ol_GUIImage::~ol_GUIImage()
{
#ifdef _OL_DEBUG_BUILD_
    printf("GUI Image Destructor ( ID = %d )\n", ID);
#endif // _OL_DEBUG_BUILD_
}

void ol_GUIImage::Update(void)
{
    ol_GUIControl::Update();
    UpdateAnimation();
}

/**
    --------------------------------- GUI Diagram ------------------------------------------
    >                                                                                      <
    >                    Класс для отображения графиков функций.                           <
    >                                                                                      <
    ----------------------------------------------------------------------------------------
**/

ol_GUIDiagram::ol_GUIDiagram(ol_Graphics *graph, ol_GUITheme *theme, ol_Vector2df origin, float width, float height, ol_GUIPositionPoints keyPoint, const char *name): ol_GUIControl(graph, theme, origin, width, height, keyPoint, name)
{
    needFixedScale = true;

    k = ol_Vector2dld(1.0f, 1.0f);

    mrg.bottom =    theme->font->getHeight() + 10.0f;
    mrg.left   =    100.0f;
    mrg.top    =    mrg.bottom;
    mrg.right  =    20.0f;

    InitDrawArea();

    font = theme->font;
    bgColor = OL_COLOR_WHITE;        // цвет фона

    xAxis.color = yAxis.color = OL_COLOR_BLACK;
    xAxis.divNum = yAxis.divNum = 10;
    xAxis.divLen = yAxis.divLen = 5.0f;
    xAxis.valPos = OL_POS_DOWN;
    yAxis.valPos = OL_POS_LEFT;
    xAxis.divDir = ol_Vector2df(0.0f, -1.0f);
    yAxis.divDir = ol_Vector2df(-1.0f, 0.0f);
    xAxis.type = OL_AXIS_X;
    yAxis.type = OL_AXIS_Y;

    fixedMin = ol_Vector2dld(0,0);
    fixedMax = ol_Vector2dld(1,1);
    Min = fixedMin;
    Max = fixedMax;

    crosshair = ol_Vector2dld(0,0);
    needRedrawCross = false;

    Calibrate();
    CalcAxis();
}

ol_GUIDiagram::~ol_GUIDiagram()
{
#ifdef _OL_DEBUG_BUILD_
    printf("GUI Diagram destructor ( ID = %d )\n", ID);
#endif // _OL_DEBUG_BUILD_
}

void ol_GUIDiagram::InitDrawArea(void)
{
    drawArea.Init(botLeft + ol_Vector2df(mrg.left, mrg.bottom), width - mrg.left - mrg.right, height - mrg.top - mrg.bottom);
}

void ol_GUIDiagram::Shift(ol_Vector2df &dir)
{
    ol_GUIControl::Shift(dir);

    drawArea.Shift(dir);
    Calibrate();
    CalcAxis();
}

ol_Vector2df ol_GUIDiagram::setPosition(ol_Vector2df pos, ol_GUIPositionPoints posPoint)
{
    ol_Vector2df v = ol_GUIControl::setPosition(pos, posPoint);

    drawArea.Shift(v);

    Calibrate();
    CalcAxis();

    return v;
}

bool ol_GUIDiagram::CheckEvents(ol_Event *event, ol_GUIEvent *guiEvent)
{
    needRedrawCross = false;

    if(ol_GUIControl::CheckEvents(event, guiEvent))
    {
        ol_Vector2df mPos = event->getCurPos();
        crosshair.x = (long double)mPos.x;
        crosshair.y = (long double)mPos.y;

        crossVal = crosshair - zero;
        crossVal.x = crossVal.x/k.x;
        crossVal.y = crossVal.y/k.y;

        if(drawArea.isInside(mPos))  needRedrawCross = true;
    }

    return false;
}

void ol_GUIDiagram::FindMinMax(vector<ol_Vector2dld> &func, ol_Vector2dld &Min, ol_Vector2dld &Max)
{
    if(func.size() == 0) return;

    Min.x = func[0].x;
    Min.y = func[0].y;
    Max.x = func[0].x;
    Max.y = func[0].y;

    for(unsigned int i=1; i<func.size(); i++)
    {
        if(func[i].x < Min.x) Min.x = func[i].x;
        if(func[i].y < Min.y) Min.y = func[i].y;
        if(func[i].x > Max.x) Max.x = func[i].x;
        if(func[i].y > Max.y) Max.y = func[i].y;
    }
}

void ol_GUIDiagram::RecalcMinMax(void)
{
    ol_Vector2dld tmpMin, tmpMax;

    Min = fixedMin;
    Max = fixedMax;

    for(unsigned int i=0; i<diagrams.size(); i++)
    {
        FindMinMax(diagrams[i].func, tmpMin, tmpMax);
        if(0==i)
        {
            funcMin = tmpMin;
            funcMax = tmpMax;
            continue;
        }

        funcMin.CompareMin(tmpMin);
        funcMax.CompareMax(tmpMax);
    }

    if(!needFixedScale)
    {
        Min = funcMin;
        Max = funcMax;
    }
    else
    {
        Min.CompareMin(funcMin);
        Max.CompareMax(funcMax);
    }

    Calibrate();
    CalcAxis();
}

void ol_GUIDiagram::Calibrate(void)
{
    k.x = drawArea.width/(Max.x - Min.x);
    k.y = drawArea.height/(Max.y - Min.y);

    zero.y = drawArea.botLeft.y + drawArea.height -  Max.y * k.y;
    zero.x = drawArea.botLeft.x + drawArea.width - Max.x * k.x;

    axisCross = zero;
}

void ol_GUIDiagram::CalcAxisDivisions(Axes &ax)
{
    ol_Vector2dld v = ax.v2 - ax.v1;
    long double step = v.Length()/ax.divNum;
    long double markVal = 0.0f;          	// значение риски
    long double markPos;		            // положение риски
    ol_GLFormatedString sig;			        // текст подписи
    wchar_t sigBuff[40];    			    // буфер для промежуточного хранения текста подписи
    int sigLen;             			    // количество символов текста подписи

    ax.divs.clear();
    ax.text.clear();
    for(unsigned int i=0; i<=ax.divNum; i++)
    {
        switch(ax.type)
        {
        case OL_AXIS_X:
            markPos = ax.v1.x + step*i;
            markVal = (markPos - zero.x)/k.x;
            break;
        case OL_AXIS_Y:
            markPos = ax.v1.y + step*i;
            markVal = (markPos - zero.y)/k.y;
            break;
        }
        // сохраняем положение риски
        ax.divs.push_back(markPos);
        // выводим значение подписи в wchar_t буфер с использованием нужного формата
        if((markVal>99999.0) || (markVal<-99999.0) || ((markVal>-0.0001)&&(markVal<0.0001)&&(markVal!=0)))
            sigLen = swprintf(sigBuff, 40, L"%Le", markVal);
        else sigLen = swprintf(sigBuff, 40, L"%4.2Lf", markVal);

        // выделяем достаточно места для хранения результирующей строки в кодировке энджина
        sig.str.resize(sigLen);
        // переводим в кодировку энджина
        font->ConvertCharCodes(sigBuff, sig.str.data(), sigLen, sig.width, sig.height);
        // запоминаем подпись
        ax.text.push_back(sig);
    }
}

void ol_GUIDiagram::CalcAxis(void)
{
    drawArea.getSide(OL_RECT_SIDE_BOTTOM, xAxis.v1, xAxis.v2);
    CalcAxisDivisions(xAxis);

    drawArea.getSide(OL_RECT_SIDE_LEFT, yAxis.v1, yAxis.v2);
    CalcAxisDivisions(yAxis);
}

void ol_GUIDiagram::setScale(long double minX, long double maxX, long double minY, long double maxY)
{
    fixedMin = ol_Vector2dld(minX, minY);
    fixedMax = ol_Vector2dld(maxX, maxY);

    RecalcMinMax();
    Calibrate();
    CalcAxis();
}

void ol_GUIDiagram::ClearAll(void)
{
    diagrams.clear();
    Min = fixedMin;
    Max = fixedMax;
    Calibrate();
    CalcAxis();
}

void ol_GUIDiagram::setAxisDivisionsNum(ol_AxisType type, unsigned int num)
{
    switch(type)
    {
    case OL_AXIS_X:
        xAxis.divNum = num;
        break;
    case OL_AXIS_Y:
        yAxis.divNum = num;
        break;
    }

    Calibrate();
    CalcAxis();
}

int ol_GUIDiagram::AddFunc(vector<ol_Vector2dld> func, ol_Color color, float lineThickness, ol_DiagLineStyle lineStyle, ol_DiagDotStyle dotStyle)
{
    if(func.size()<=0) return -1;

    Diagram tmp;
    tmp.func = func;
    tmp.lineThickness = lineThickness;
    tmp.color = color;
    tmp.lineStyle = lineStyle;
    tmp.dotStyle = dotStyle;

    diagrams.push_back(tmp);

    RecalcMinMax();
    Calibrate();
    CalcAxis();

    return diagrams.size();
}

void ol_GUIDiagram::setPoints(int ID, vector<ol_Vector2dld> &points)
{
    if(ID<=0) return;

    diagrams[ID-1].func = points;

    RecalcMinMax();
    Calibrate();
    CalcAxis();
}

void ol_GUIDiagram::DrawDiagram(Diagram diag)
{
    // задаем цвет
    graph->ol_GLColor(diag.color);
    // задаем толщину
    glLineWidth(diag.lineThickness);
    // включаем режим пунктирной рисовки
    glEnable(GL_LINE_STIPPLE);
    glLineStipple(1,diag.lineStyle);
    // рисуем диаграмму
    glBegin(GL_LINE_STRIP);
    for(unsigned int i=0; i<diag.func.size(); i++)
        glVertex2f(zero.x + diag.func[i].x*k.x, zero.y + diag.func[i].y*k.y);
    glEnd();
    // отключаем режим пунктирной рисовки
    glDisable(GL_LINE_STIPPLE);

    // рисуем узловые точки
    if(diag.dotStyle != OL_DIAG_SIMPLE)
    {
        glPointSize(6);
        glBegin(GL_POINTS);
        for(unsigned int i=0; i<diag.func.size(); i++)
            glVertex2f(zero.x + diag.func[i].x*k.x, zero.y + diag.func[i].y*k.y);
        glEnd();

    }
    //возвращаем нормальный размер точки
    glPointSize(1);
    //возвращаем нормальную толщину линии
    glLineWidth(1);
}

void ol_GUIDiagram::DrawAxis(Axes &ax)
{
    ol_Vector2df risV1, risV2;

    graph->ol_GLColor(ax.color);

    glBegin(GL_LINES);

    glVertex2f(ax.v1.x, ax.v1.y);
    glVertex2f(ax.v2.x, ax.v2.y);

    for(unsigned int i=0; i<ax.divs.size(); i++)
    {
        switch(ax.type)
        {
        case OL_AXIS_X:
            risV1 = ol_Vector2df(ax.divs[i], ax.v1.y);
            break;
        case OL_AXIS_Y:
            risV1 = ol_Vector2df(ax.v1.x, ax.divs[i]);
            break;
        }
        risV2 = risV1 + ax.divDir * ax.divLen;

        glVertex2f(risV1.x, risV1.y);
        glVertex2f(risV2.x, risV2.y);
    }

    glEnd();

    switch(ax.type)
    {
    case OL_AXIS_X:
        for(unsigned int i=0; i<ax.divs.size(); i++)
        {
            font->FormatedTextOut(ax.divs[i], ax.v1.y - ax.divLen - 5, ax.text[i], OL_GUI_POS_TOPMID);
        }
        break;
    case OL_AXIS_Y:
        for(unsigned int i=0; i<ax.divs.size(); i++)
        {
            font->FormatedTextOut(ax.v1.x - 7, ax.divs[i], ax.text[i], OL_GUI_POS_CENTRIGHT);
        }
        break;
    }
}

void ol_GUIDiagram::DrawZeroAxis(void)
{
    ol_Vector2dld tmpV1, tmpV2;

    glEnable(GL_LINE_STIPPLE);
    glLineStipple(1,0xf0f0);

    tmpV1 = xAxis.v1;
    tmpV2 = xAxis.v2;
    tmpV1.y = tmpV2.y = axisCross.y;

    glBegin(GL_LINES);

    if(axisCross.y >= xAxis.v1.y)
    {
        glVertex2f(tmpV1.x, tmpV1.y);
        glVertex2f(tmpV2.x, tmpV2.y);
    }

    tmpV1 = yAxis.v1;
    tmpV2 = yAxis.v2;
    tmpV1.x = tmpV2.x = axisCross.x;
    if(axisCross.x >= yAxis.v1.x)
    {
        glVertex2f(tmpV1.x, tmpV1.y);
        glVertex2f(tmpV2.x, tmpV2.y);
    }

    // рисуем границы области отрисовки
    tmpV1 = yAxis.v2;
    tmpV2.x = xAxis.v2.x;
    tmpV2.y = yAxis.v2.y;
    glVertex2f(tmpV1.x, tmpV1.y);
    glVertex2f(tmpV2.x, tmpV2.y);

    tmpV1 = tmpV2;
    tmpV2 = xAxis.v2;
    glVertex2f(tmpV1.x, tmpV1.y);
    glVertex2f(tmpV2.x, tmpV2.y);

    // а также перекрестье
    if(needRedrawCross)
    {
        tmpV1.x = yAxis.v1.x;
        tmpV1.y = crosshair.y;
        tmpV2.x = xAxis.v2.x;
        tmpV2.y = crosshair.y;
        glVertex2f(tmpV1.x, tmpV1.y);
        glVertex2f(tmpV2.x, tmpV2.y);

        tmpV1.x = tmpV2.x = crosshair.x;
        tmpV1.y = xAxis.v1.y;
        tmpV2.y = yAxis.v2.y;
        glVertex2f(tmpV1.x, tmpV1.y);
        glVertex2f(tmpV2.x, tmpV2.y);
    }

    glEnd();
    glDisable(GL_LINE_STIPPLE);
}

void ol_GUIDiagram::Draw(void)
{
    if(!visible) return;
    ol_GUIControl::Draw();

    glDisable(GL_TEXTURE_2D);

    graph->ol_GLColor(OL_COLOR_LIGHT_GREY);
    DrawZeroAxis();

    if(needRedrawCross) font->Print(drawArea.topRight.x, drawArea.topRight.y+5, OL_GUI_POS_BOTRIGHT,L"(%Le; %Le)", crossVal.x, crossVal.y);

    for(unsigned int i=0; i<diagrams.size(); i++)
        DrawDiagram(diagrams[i]);

    DrawAxis(xAxis);
    DrawAxis(yAxis);
    glEnable(GL_TEXTURE_2D);
}

/**
    --------------------------------- GUI Manager ------------------------------------------
    >                                                                                      <
    >     Класс управляющий всеми шрифтами и элементами управления польз. интерфейса.      <
    >                                                                                      <
    ----------------------------------------------------------------------------------------
**/

ol_GUIManager::ol_GUIManager(ol_Graphics *graph)
{
    this->graph = graph;

    int error = FT_Init_FreeType(&ft_lib);
    if(error) printf("Error (Freetype): unable to init library\n");

    controlsCount = 0;

    guiEvent = new ol_GUIEvent();
}

ol_GUIManager::~ol_GUIManager()
{
    FT_Done_FreeType(ft_lib);

    delete guiEvent;

    for(unsigned int i=0; i<fonts.size(); i++)
        delete fonts[i];
    fonts.clear();

    for(unsigned int i=0; i<themes.size(); i++)
        delete themes[i];

    for(list<ol_GUIControl*>::iterator it=controls.begin(); it!=controls.end(); ++it)
        delete (*it);
    controls.clear();
}

bool ol_GUIManager::InitDefaultTheme(const char *fileName)
{
    ol_GUITheme *theme = new ol_GUITheme();
    // Базовые цвета если нет текстуры  ----------------
    theme->bgrColor = ol_Color(230, 230, 230);		// Background gradient color 1
    theme->bgrColor1 = ol_Color(190, 190, 190);		// Background gradient color 2
    theme->pressColor = ol_Color(170, 170, 170);	// Background color for pressed control
    theme->brdColor = ol_Color(100, 100, 100);		// Border color
    theme->selectColor = ol_Color(230, 230, 230);	// Background color for selected control
    // Шрифт -------------------------------------------
    theme->fontColor = OL_COLOR_BLACK;
    theme->fontFileName = fileName;
    theme->fontSize = 12;
    // общие размеры -----------------------------------
    theme->ctrlDist = 5;
    // Поля эдитбокса ----------------------------------
    theme->editBoxHeight = 25;
    theme->editBoxMargins.left     = 4.0f;
    theme->editBoxMargins.top      = 4.0f;
    theme->editBoxMargins.right    = 4.0f;
    theme->editBoxMargins.bottom   = 4.0f;
    theme->editBoxLabShift         = 4;
    theme->editBoxBgColor          = ol_Color(255, 255, 255); // White background for editBox
    // Скролбокс ---------------------------------------
    theme->scrollerThickness           = 10;
    // Поля клиентской части окна ----------------------
    theme->winClientMrgs.left      = 3.0f;
    theme->winClientMrgs.top       = 25.0f;
    theme->winClientMrgs.right     = 3.0f;
    theme->winClientMrgs.bottom    = 3.0f;
    theme->winHeadButSize          = 12;
    theme->winHeadHeight           = 20;
    theme->winHeadButShift         = 10;
    // Параметры кнопки --------------------------------
    theme->buttHeight = 25;
    theme->buttLabHorShift = 7;
    theme->buttLabVertShift = 6;
    theme->buttLabStandardColor = OL_COLOR_BLACK;
    theme->buttLabSelectedColor = OL_COLOR_BLUE;
    // Параметры диалогового окна --------------------------------
    theme->fileDialogWidth = 400;
    theme->fileDialogHeight = 300;
    theme->fileDiagPathCaption      = L"Расположение: ";            // заголовок эдита с полным именем файла
    theme->fileDiagOpenCaption      = L"Открыть файл";              // заголовок диалога "открыть файл"
    theme->fileDiagOpenOKCaption    = L"Открыть";                   // надпись на кнопке "открыть"
    theme->fileDiagSaveCaption      = L"Сохранить файл";            // заголовок диалога "сохранить файл"
    theme->fileDiagSaveOKCaption    = L"Сохранить";                 // надпись на кнопке "сохранить"
    // Инициализация кодировок -------------------------
    vector<unsigned int> codes;
    codes = addCharSet(codes, OL_CHARSET_kYRILIC_BASIC);
    codes = addCharSet(codes, OL_CHARSET_LATIN_BASIC);

    ol_Font *font = CreateFont(theme->fontFileName, theme->fontSize, codes);

    if(!font)
    {
        printf("Error: unable to init default theme\n");
        delete theme;
        return false;
    }

    theme->font = font;
    codes.clear();
    themes.push_back(theme);

    return true;
}

ol_GUITheme* ol_GUIManager::getDefaultTheme(void)
{
    if(themes.size()>=1)
        return themes[0];
    else return NULL;
}

void ol_GUIManager::RefreshWindowArea(float width, float height)
{
    winArea.Init(ol_Vector2df(0.0f, 0.0f), width, height, OL_GUI_POS_BOTLEFT);
}

ol_SimpleFont* ol_GUIManager::CreateSimpleFont(const char *fileName, unsigned int height)
{
    ol_SimpleFont *font = new ol_SimpleFont(graph);

    if(font->BuildFont_ft(&ft_lib, fileName, height))
    {
        fonts.push_back(font);
        return font;
    }
    else
    {
        delete font;
        printf("Error: unable to create font\n");
        return NULL;
    }
}

ol_Font* ol_GUIManager::CreateFont(const char *fileName, unsigned int height, vector<unsigned int> codes)
{
    if(codes.empty()) return NULL;

    ol_Font *font = new ol_Font(graph, codes);

    if(font->BuildFont_ft(&ft_lib, fileName, height))
    {
        fonts.push_back(font);
        return font;
    }
    else
    {
        delete font;
        printf("Error: unable to create font\n");
        return NULL;
    }
}

void ol_GUIManager::delControl(ol_GUIControl *control)
{
    if(control == NULL) return;
    controls.remove(control);
    delete control;
    controlsCount--;
}

void ol_GUIManager::addControl(ol_GUIPanel *parent, ol_GUIControl *control)
{
    if(!control)
    {
        printf("Failed to add GUI control\n");
        return;
    }

    if(parent)
    {
        parent->addItem(control);
    }
    else
    {
        controls.push_back(control);
        controlsCount++;
    }
}

ol_GUIControl* ol_GUIManager::addGUIControl(ol_GUIPanel *parent, ol_Vector2df origin, float width, float height, ol_GUIPositionPoints keyPoint, const char *name)
{
    ol_GUITheme *theme = this->getDefaultTheme();
    if(!theme)
    {
        printf("Error: GUI Default theme is not inited\n");
        return NULL;
    }

    ol_GUIControl *control = new ol_GUIControl(graph, theme, origin, width, height, keyPoint, name);
    addControl(parent, control);
    return control;
}

ol_GUIPanel* ol_GUIManager::addGUIPanel(ol_GUIPanel *parent, ol_Vector2df origin, float width, float height, ol_GUIPositionPoints keyPoint, const char *name)
{
    ol_GUITheme *theme = this->getDefaultTheme();
    if(!theme)
    {
        printf("Error: GUI Default theme is not inited\n");
        return NULL;
    }

    ol_GUIPanel *panel = new ol_GUIPanel(graph, theme, origin, width, height, keyPoint, name);
    addControl(parent, panel);
    return panel;
}

ol_GUIWindow* ol_GUIManager::addGUIWindow(ol_Vector2df origin, float width, float height, wstring caption, ol_GUIPositionPoints keyPoint, const char *name)
{
    ol_GUITheme *theme = this->getDefaultTheme();
    if(!theme)
    {
        printf("Error: GUI Default theme is not inited\n");
        return NULL;
    }

    ol_GUIWindow *window = new ol_GUIWindow(graph, theme, origin, width, height, caption, keyPoint, name);
    addControl(NULL, window);
    return window;
}

ol_GUIFileBrowser*  ol_GUIManager::addGUIFileBrowser(ol_GUIPanel *parent, ol_Vector2df origin, float width, float height, ol_GUIPositionPoints keyPoint, const char *name)
{
    ol_GUITheme *theme = this->getDefaultTheme();
    if(!theme)
    {
        printf("Error: GUI Default theme is not inited\n");
        return NULL;
    }

    ol_GUIFileBrowser *browser = new ol_GUIFileBrowser(graph, theme, origin, width, height, keyPoint, name);
    addControl(parent, browser);
    return browser;
}

ol_GUIFileDialog* ol_GUIManager::addGUIFileDialog(ol_GUIFileDialogType type)
{
    ol_GUITheme *theme = this->getDefaultTheme();
    if(!theme)
    {
        printf("Error: GUI Default theme is not inited\n");
        return NULL;
    }

    wstring cap;
    switch(type)
    {
    case OL_GUI_FILEDIALOG_OPEN:
        cap = theme->fileDiagOpenCaption;
        break;
    case OL_GUI_FILEDIALOG_SAVE:
        cap = theme->fileDiagSaveCaption;
        break;
    }

    ol_GUIFileDialog *dialog = new ol_GUIFileDialog(graph, theme, winArea.centMid, theme->fileDialogWidth, theme->fileDialogHeight, cap, type, OL_GUI_POS_CENTMID);
    addControl(NULL, dialog);
    return dialog;
}

ol_GUIImage* ol_GUIManager::addGUIImage(ol_GUIPanel *parent, ol_Vector2df origin, float width, float height, ol_Texture tex, ol_GUIPositionPoints keyPoint, const char *name)
{
    ol_GUITheme *theme = this->getDefaultTheme();
    if(!theme)
    {
        printf("Error: GUI Default theme is not inited\n");
        return NULL;
    }
    ol_GUIImage *image = new ol_GUIImage(graph, theme, origin, width, height, tex, keyPoint, name);
    addControl(parent, image);
    return image;
}

ol_GUIDiagram* ol_GUIManager::addGUIDiagram(ol_GUIPanel *parent, ol_Vector2df origin, float width, float height, ol_GUIPositionPoints keyPoint, const char *name)
{
    ol_GUITheme *theme = this->getDefaultTheme();
    if(!theme)
    {
        printf("Error: GUI Default theme is not inited\n");
        return NULL;
    }

    ol_GUIDiagram* diag = new ol_GUIDiagram(graph, theme, origin, width, height, keyPoint, name);
    addControl(parent, diag);
    return diag;
}

ol_GUILabel* ol_GUIManager::addGUILabel(ol_GUIPanel *parent, ol_Vector2df origin, wstring text, ol_GUIPositionPoints keyPoint, const char *name)
{
    ol_GUITheme *theme = this->getDefaultTheme();
    if(!theme)
    {
        printf("Error: GUI Default theme is not inited\n");
        return NULL;
    }

    ol_GUILabel* lab = new ol_GUILabel(graph, theme, origin, 1, 1, text, keyPoint, name);
    addControl(parent, lab);
    return lab;
}

ol_GUIButton* ol_GUIManager::addGUIButton(ol_GUIPanel *parent, ol_Vector2df origin, float width, float height, wstring caption, ol_GUIButtonStyle style, ol_GUIPositionPoints keyPoint, const char *name)
{
    ol_GUITheme *theme = this->getDefaultTheme();
    if(!theme)
    {
        printf("Error: GUI Default theme is not inited\n");
        return NULL;
    }

    ol_GUIButton* butt = new ol_GUIButton(graph, theme, origin, width, height, caption, style, keyPoint, name);
    addControl(parent, butt);
    return butt;
}

ol_GUIEditBox* ol_GUIManager::addGUIEditBox(ol_GUIPanel *parent, ol_Vector2df origin, float width, float height, ol_GUIPositionPoints keyPoint, const char *name)
{
    ol_GUITheme *theme = this->getDefaultTheme();
    if(!theme)
    {
        printf("Error: GUI Default theme is not inited\n");
        return NULL;
    }

    ol_GUIEditBox* edit = new ol_GUIEditBox(graph, theme, origin, width, height, keyPoint, name);
    addControl(parent, edit);
    return edit;
}

ol_GUILabeledEditBox* ol_GUIManager::addGUILabeledEditBox(ol_GUIPanel *parent, ol_Vector2df origin, float width, float height, wstring caption, ol_GUIPositionPoints labelPoint, ol_GUIPositionPoints keyPoint, const char *name)
{
    ol_GUITheme *theme = this->getDefaultTheme();
    if(!theme)
    {
        printf("Error: GUI Default theme is not inited\n");
        return NULL;
    }

    ol_GUILabeledEditBox* edit = new ol_GUILabeledEditBox(graph, theme, origin, width, height, caption, labelPoint, keyPoint, name);
    addControl(parent, edit);
    return edit;
}

ol_GUITextBox* ol_GUIManager::addGUITextBox(ol_GUIPanel *parent, ol_Vector2df origin, float width, float height, ol_GUIPositionPoints keyPoint, const char *name)
{
    ol_GUITheme *theme = this->getDefaultTheme();
    if(!theme)
    {
        printf("Error: GUI Default theme is not inited\n");
        return NULL;
    }

    ol_GUITextBox* textbox = new ol_GUITextBox(graph, theme, origin, width, height, keyPoint, name);
    addControl(parent, textbox);
    return textbox;
}

ol_GUIScrollBox* ol_GUIManager::addGUIScrollBox(ol_GUIPanel *parent, ol_Vector2df origin, float width, float height, ol_GUIScrollBoxType type, ol_GUIPositionPoints keyPoint, const char *name)
{
    ol_GUITheme *theme = this->getDefaultTheme();
    if(!theme)
    {
        printf("Error: GUI Default theme is not inited\n");
        return NULL;
    }

    ol_GUIScrollBox* scrollbox = new ol_GUIScrollBox(graph, theme, origin, width, height, type, keyPoint, name);
    addControl(parent, scrollbox);
    return scrollbox;
}

ol_GUIEdScrollBox* ol_GUIManager::addGUIEdScrollBox(ol_GUIPanel *parent, ol_Vector2df origin, float width, float height, float scSize, float edSize, ol_GUIScrollBoxType type, ol_GUIEdScrollPos relPos, ol_GUIPositionPoints keyPoint, const char *name)
{
    ol_GUITheme *theme = this->getDefaultTheme();
    if(!theme)
    {
        printf("Error: GUI Default theme is not inited\n");
        return NULL;
    }

    ol_GUIEdScrollBox* scrollbox = new ol_GUIEdScrollBox(graph, theme, origin, width, height, scSize, edSize, type, relPos, keyPoint, name);
    addControl(parent, scrollbox);
    return scrollbox;
}

void ol_GUIManager::DrawAll(void)
{
    if(controlsCount<=0) return;

    for(list<ol_GUIControl*>::iterator it=controls.begin(); it!=controls.end(); ++it)
    {
        (*it)->Draw();
    }
}

void ol_GUIManager::UpdateAll(void)
{
    if(controlsCount<=0) return;
    ol_GUIControl *buff = NULL;
    for(list<ol_GUIControl*>::reverse_iterator it=controls.rbegin(); it!=controls.rend(); ++it)
    {
        (*it)->Update();
	if((*it)->justShowed)
	{
	    buff = (*it);
	    buff->justShowed = false;
	}
    }

    if(buff)
    {
        controls.remove(buff);
        controls.push_back(buff);
    }
}

ol_GUIEvent* ol_GUIManager::getLastEvent(void)
{
    return guiEvent;
}

ol_GUIEvent* ol_GUIManager::CheckGUIEvents(ol_Event *event)
{
    if(controlsCount<=0) return NULL;
    guiEvent->winEvent = false;
    ol_GUIControl *buff = NULL;
    bool eventHappened = false;

    for(list<ol_GUIControl*>::reverse_iterator it=controls.rbegin(); it!=controls.rend(); ++it)
    {
	if(eventHappened)
	{
	    if((*it)->state == OL_GUI_STATE_SELECTED) (*it)->setState(OL_GUI_STATE_STANDARD);
	    if((*it)->state == OL_GUI_STATE_FOCUSED && buff) (*it)->setState(OL_GUI_STATE_STANDARD);
	}
        
	if(buff) continue;
	
	if((*it)->CheckEvents(event, guiEvent))
        {
            if(guiEvent->winEvent) buff = (*it);

	    eventHappened = true;
        }
    }

    if(buff)
    {
        controls.remove(buff);
        controls.push_back(buff);
    }

    if(eventHappened) return guiEvent;
    else return NULL;
}
