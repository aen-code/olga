/**
------------------------------------------------------------------------
  This file is part of the OLGA project.
  Copyright © 2017 by Enver Akhmedov.
  E-mail: aen-code@yandex.ru
  For conditions of distribution and use, see copyright notice in OLGA.h
------------------------------------------------------------------------
**/

#include "ol_Geometry.h"

/**
    ------------------------------------ ol_Mesh2d ------------------------------------------
    >                                                                                      <
    >  Набор вершин, нормалей, на плоскости, представляющих собой контур объекта.          <
    >  Позволяет последовательно перебирать вершины. Заполнять нужно последовательно по    <
    >  часовой стрелке                                                                     <
    >                                                                                      <
    ----------------------------------------------------------------------------------------
**/

ol_Mesh2d::ol_Mesh2d()
{
    frame = 0;
    vertCount = 0;
}
ol_Mesh2d::~ol_Mesh2d() {}

void ol_Mesh2d::addVertex(ol_Vector2df vert)
{
    ol_Vertex2d vt(vert);
    verts.push_back(vt);
    vertCount++;
}

void ol_Mesh2d::Reset()
{
    frame = 0;
}

unsigned short ol_Mesh2d::getEdge(ol_Edge2d &edge)
{
    edge.left = verts[frame];
    if(frame == vertCount-1) edge.right = verts[0];
    else edge.right = verts[frame + 1];

    edge.normal = edgeNorms[frame];

    frame++;
    if(frame >= vertCount) frame = 0;

    return frame;
}

float ol_Mesh2d::DistanceToPoint(ol_Vector2df &p, ol_Vector2df &norm, ol_Vector2df &closest_point)
{
    float min_dist=0, dist=0;
    ol_Point2dRelPos rel_pos;
    ol_Vector2df cp;
    unsigned short nextedge;
    ol_Edge2d tedge;

    Reset();
    do
    {
        nextedge = getEdge(tedge);
        dist = DistanceToSegment(p, tedge.left.point, tedge.right.point, cp, rel_pos);

        if(nextedge == 1)
        {
            min_dist = dist;
            closest_point = cp;
            if(rel_pos == OL_POINT_REL_POS_LEFT) norm = tedge.left.normal;
            if(rel_pos == OL_POINT_REL_POS_RIGHT) norm = tedge.right.normal;
            if(rel_pos == OL_POINT_REL_POS_BETWEEN) norm = tedge.normal;
        }
        else if(dist<=min_dist)
        {
            min_dist = dist;
            closest_point = cp;
            if(rel_pos == OL_POINT_REL_POS_LEFT) norm = tedge.left.normal;
            if(rel_pos == OL_POINT_REL_POS_RIGHT) norm = tedge.right.normal;
            if(rel_pos == OL_POINT_REL_POS_BETWEEN) norm = tedge.normal;
        }

    }
    while(nextedge != 0);

    return min_dist;
}

void ol_Mesh2d::CalcNormals(void)
{
    ol_Vector2df norm;
    ol_Vector2df v;

    for(unsigned int i=0; i<vertCount; i++)
    {
        if(i == vertCount-1) v = ol_Vector2df(verts[i].point, verts[0].point);
        else v = ol_Vector2df(verts[i].point, verts[i+1].point);

        norm = v.getNormal();
        edgeNorms.push_back(norm);
    }

    for(unsigned int i=0; i<edgeNorms.size(); i++)
    {
        if(i==0) norm = edgeNorms[i] + edgeNorms[edgeNorms.size()-1];
        else norm = edgeNorms[i] + edgeNorms[i-1];

        norm.Normalize();
        verts[i].normal = norm;
    }
}

/**
    ------------------------------------ Mesh 3D -------------------------------------------
    >                                                                                      <
    >                        Трехмерная геометрическая модель.                             <
    >                                                                                      <
    ----------------------------------------------------------------------------------------
**/

ol_Mesh3D::ol_Mesh3D()
{
    vertCount = 0;
    faceCount = 0;
    texcoordCount = 0;
    normCount = 0;
    pVerts = NULL;
    pNormals = NULL;
    pTexVerts = NULL;
    pVertInd = NULL;
//    pFaces = NULL;
}

ol_Mesh3D::~ol_Mesh3D()
{
    if(pVerts) delete [] pVerts;
    if(pNormals) delete [] pNormals;
    if(pTexVerts) delete [] pTexVerts;
    if(pVertInd) delete [] pVertInd;
//    if(pFaces) delete [] pFaces;
}

void ol_Mesh3D::InitVerts(vector<ol_Vector3d> verts)
{
    vertCount = verts.size();

    if(vertCount>0 && pVerts == NULL)
    {
        pVerts = new ol_Vector3d [vertCount];

        for(unsigned int i=0; i<vertCount; i++)
            pVerts[i] = verts[i];
    }
    else cout<<"Error Initing Vertexes!\n";
}

void ol_Mesh3D::InitFaces(vector<ol_Face> faces)
{
/*
    faceCount = faces.size();

    if(faceCount>0 && pFaces == NULL)
    {
        pFaces = new ol_Face [faceCount];

        for(unsigned int i=0; i<faceCount; i++)
            pFaces[i] = faces[i];
    }
    else cout<<"Error Initing Faces!\n";
*/
}


void ol_Mesh3D::InitTexCoords(vector<ol_Vector2df> coords)
{
    texcoordCount = coords.size();

    if(texcoordCount>0 && pTexVerts == NULL)
    {
        pTexVerts = new ol_Vector2df [texcoordCount];

        for(unsigned int i=0; i<texcoordCount; i++)
            pTexVerts[i] = coords[i];
    }
    else cout<<"Error Initing Texture Coordinates!\n";
}

void ol_Mesh3D::InitIndexes(vector<GLuint> ind)
{
    indCount = ind.size();
    faceCount = indCount/3;

    if(indCount>0 && pVertInd == NULL)
    {
        pVertInd = new GLuint [indCount];

        for(unsigned int i=0; i<indCount; i++)
            pVertInd[i] = ind[i];
    }
    else cout<<"Error Initing Indexes!\n";
}

void ol_Mesh3D::InitNormals(vector<ol_Vector3d> norm)
{
    normCount = norm.size();

    if(normCount>0 && pNormals == NULL)
    {
        pNormals = new ol_Vector3d [normCount];

        for(unsigned int i=0; i<normCount; i++)
            pNormals[i] = norm[i];
    }
    else cout<<"Error Initing Normals!\n";
}

void ol_Mesh3D::InitVertInd(void)
{
/*
    indCount = faceCount*3;
    unsigned int vn = 0;

    // Инициализируем массив индексов
    if(indCount>0 && pVertInd==NULL)
    {
        pVertInd = new unsigned int [indCount];

        for(unsigned int i=0; i<faceCount; i++)
            for(unsigned int j=0; j<3; j++)
            {
                pVertInd[vn] = pFaces[i].vertIndex[j];
                vn++;
            }
    }
    else cout<<"Error Initing Indexes!\n";

    
//        Переставляем элементы в массиве текстурных координат так, чтобы индексы
//        массива текстурных координат совпадали с индексами массива вершин.
//        Работает только для идеально анврапнутой модели
    
    if(texcoordCount>0)
    {
        ol_Vector2df *texTMP = new ol_Vector2df [vertCount];

        int un=0;
        for(unsigned int i=0; i<faceCount; i++)
            for(unsigned int j=0; j<3; j++)
            {
                texTMP[pFaces[i].vertIndex[j]] = pTexVerts[pFaces[i].coordIndex[j]];
                un++;
            }

        delete pTexVerts;
        pTexVerts = new ol_Vector2df [vertCount];
        texcoordCount = vertCount;

        for(unsigned int i=0; i<vertCount; i++)
            pTexVerts[i] = texTMP[i];

        delete texTMP;
    }
*/
}


void ol_Mesh3D::CalcNormals(void)
{
	if(faceCount <= 0)	return;

	vector<ol_Vector3d> neighNorms;

	ol_Vector3d vVector1, vVector2, vNormal, vPoly[3];

	// Here we allocate all the memory we need to calculate the normals
	ol_Vector3d *pTempNormals = new ol_Vector3d [indCount]; // initialized will be only eahch third element of the array
	normCount = vertCount;
	pNormals = new ol_Vector3d [normCount];

	// Go though all of the faces of this object
	for(unsigned int i=0; i <indCount; i+=3)
	{
		// To cut down LARGE code, we extract the 3 points of this face
		vPoly[0] = pVerts[pVertInd[i]];
			vPoly[1] = pVerts[pVertInd[i+1]];
			vPoly[2] = pVerts[pVertInd[i+2]];
	
			// Now let's calculate the face normals (Get 2 vectors and find the cross product of those 2)
			vVector1 = vPoly[0] - vPoly[2];			// Get the vector of the polygon (we just need 2 sides for the normal)
			vVector2 = vPoly[2] - vPoly[1];			// Get a second vector of the polygon

			vNormal  = Cross(vVector1, vVector2);	// Return the cross product of the 2 vectors (normalize vector, but not a unit vector)

			vNormal  = Normalize(vNormal);			// Normalize the cross product to give us the polygons normal
			pTempNormals[i] = vNormal;				// Save the un-normalized normal for the vertex normals
		}
	
		ol_Vector3d vSum;
		ol_Vector3d vZero;
	
		for (unsigned int i = 0; i <vertCount; i++)		// Go through all of the vertices
		{
			for (unsigned int j = 0; j <indCount; j+=3)	// Go through all of the triangles
			{
				// Check if the vertex is shared by another face
				if (pVertInd[j] == i || pVertInd[j+1] == i || pVertInd[j+2] == i)
				{
				/* There is a problem! If there are two or more faces lying in the same plane sharing
				 * particular vertex, then two or more equal normals will be added and the resulting
				 * smoothed normal will be incorrect. Thereby we need to find all neighbor normals and 
				 * exclude repeated ones. But, due to the error of machine roundings there are no 
				 * mathematically equal normals. The same normals differ by small amount. Therefore 
				 * we find the magnitude of the difference of each pair. If the difference equals to 0, 
				 * then the vectors are the same. In practice we assume that if this value less than
				 * 0.00001 then it is 0 and normals are considered to be the same.
				 * 
				 * There is one more problem! The same vertex having different uv coordinates for
				 * different polygons (i.e. lying on the uv seam) is treated by OLGA as two different
				 * vertexes due to the using of glDrawElements() function for rendering. Therefore the
				 * uv seam will be shown unsmoothed. It is possible to account this case, but it requires
				 * additional calculations. In modern games normals are usually given using the normal map,
				 * and automatic normals calculation will not be used in majority of the cases. Besides,
				 * uv seam often lies on the edge that should not be smoothed. Thus I assume this technique
				 * to be sufficient for calculating normals.*/

				bool nf = false; // becomes true if this normal has been already added.
				float mag = 0;
				// We take the current normal and compare it with all normals that have been already added.
				for(unsigned int p=0; p<neighNorms.size(); p++) 
				{
					mag = Magnitude(pTempNormals[j] - neighNorms[p]);
					if(mag<=0.00001) nf = true;
				}
				if(!nf) neighNorms.push_back(pTempNormals[j]);
            }
        }
		vSum = vZero;	// Reset the sum.
		// Calculating the smoothed normal. 
		for(unsigned int p=0; p<neighNorms.size(); p++)
			vSum = vSum + neighNorms[p];
		neighNorms.clear();
		// We negate the shared so it has the normals pointing out.
		pNormals[i] = Invert(vSum);
		// Normalize the normal for the final vertex normal.
		pNormals[i] = Normalize(pNormals[i]);
	}
	// Free our memory and start over on the next object.
	delete [] pTempNormals;
}

void ol_Mesh3D::CleanUp(void)
{
//	delete [] pFaces;
//    pFaces = NULL;
}

void ol_Mesh3D::CalcCenter(void)
{
    ol_Vector3d min, max; // минимальные и максимальные значения координат вершин

    if(vertCount>0)
    {
        min = max = pVerts[0];
        for(unsigned int i=1; i<vertCount; i++)
        {
            if(pVerts[i].x <= min.x) min.x = pVerts[i].x;
            if(pVerts[i].y <= min.y) min.y = pVerts[i].y;
            if(pVerts[i].z <= min.z) min.z = pVerts[i].z;

            if(pVerts[i].x >= max.x) max.x = pVerts[i].x;
            if(pVerts[i].y >= max.y) max.y = pVerts[i].y;
            if(pVerts[i].z >= max.z) max.z = pVerts[i].z;
        }

        center.x = min.x + (max.x - min.x)/2;
        center.y = min.y + (max.y - min.y)/2;
        center.z = min.z + (max.z - min.z)/2;
    }
    else cout<<"Error: Unable to calculate center of the model. Vetrs num = 0\n";
}

unsigned int ol_Mesh3D::getVertCount(void)
{
    return vertCount;
}

unsigned int ol_Mesh3D::getPolyCount(void)
{
    return faceCount;
}

/**
    --------------------------------- ol_TriangleMesh ---------------------------------------
    >                                                                                       <
    >   Создан как вспомогательный класс. Изначально чтение модели было заточено под        <
    >   ol_Mesh3d, но оказалось, что вершины нужно дополнительно преобразовать. Чтобы не    <
    >   перебирать уже работающий код, создан дополнительный класс.                         <
    >                                                                                       <
    -----------------------------------------------------------------------------------------
**/

void ol_TriangleMesh::AddVertex(ol_Vertex ver)
{
    ol_Vertex vt;
    unsigned int size = verts.size();

    for(register unsigned int i=0; i<size; i++)
    {
        vt.vert = verts[i];
        vt.norm = norms[i];
        vt.tex = texcoords[i];

        if(vt == ver)
        {
            indexes.push_back(i);
            return;
        }
    }

    verts.push_back(ver.vert);
    norms.push_back(ver.norm);
    texcoords.push_back(ver.tex);
    indexes.push_back(verts.size()-1);
}
