/**
------------------------------------------------------------------------
  This file is part of the OLGA project.
  Copyright © 2017 by Enver Akhmedov.
  E-mail: aen-code@yandex.ru
  For conditions of distribution and use, see copyright notice in OLGA.h
------------------------------------------------------------------------
**/

#ifndef OL_SCENE_H_INCLUDED
#define OL_SCENE_H_INCLUDED

#include <GL/gl.h>
#include <GL/glu.h>
#include <vector>
#include <iostream>
#include "ol_Vector_Math.h"
#include "ol_Quaternion.h"
#include "core/ol_System.h"
#include "ol_Graphics.h"
#include "ol_Frustum.h"
#include "ol_Basic_Shapes.h"

using namespace std;

class ol_SceneManager; // Декларация SceneManagera

/**
    --------------------------------- Scene Object -----------------------------------------
    >                                                                                      <
    >           Базовый класс для представления всех объектов на сцене.                    <
    >                                                                                      <
    ----------------------------------------------------------------------------------------
**/

class ol_SceneObject
{
protected:
    int ID;                 // Уникальный идентификатор
    const char *name;       // Имя объекта

    ol_SceneObject *parent;              // Указатель на родительский объект
    vector<ol_SceneObject *> childs;     // Массив указателей на подчиненные объекты
    unsigned int childNum;               // количество "детей". Чтобы не вызывать каждый раз childs.size()

    ol_Vector3d pos;                     // положение в 3х мерном пространстве
    ol_Matrix44f ModelViewMat;           // Матрица модели-вида. Служит для перемещений и поворотов

    ol_Vector3d forward;                 // направление "вперед". Куда смотрит объект z+
    ol_Vector3d up;                      // направление "вверх".                      y+
    ol_Vector3d side;                    // направление "в сторону"                   x+

    ol_Quaternion q_Pitch;               // Кватернион, представляющий вращение в вертикальной плоскости
    ol_Quaternion q_Yaw;                 // Кватернион, представляющий вращение в горизонтальной плоскости
    ol_Quaternion q_Rol;                 // Кватернион, представляющий вращение вектора вокруг собственной оси
    ol_Quaternion q_Res;                 // Результирующий кватернион

    ol_Vector3d parRig;                  // вектор соединяющий объект и родительский объект

    ol_Vector3d xAxis, yAxis, zAxis;     // мировые оси координат
    float yaw, pitch, roll;              // углы Эйлера. Задают суммарное вращение между апдэйтами
    float xAngle, yAngle, zAngle;        // углы между осями координат и повернутыми осями
    bool rotating;                       // флаг указывающий, было ли вращение в данном фрэйме
    bool moving;                         // флаг указывающий, было ли перемещение в данном фрэйме
    float angm;                          // допустимый угол отклонения от вертикали. Используется в RotateAround()

    ol_Material *material;               // материал
    bool frustumCulling;                 // Осуществлять отсечение по пирамиде видимости?
    vector<ol_Line> geomlines;           // линии для отрисовки нормалей, и т.д.

public:
    // направление движения. Вектор, который является результирующим
    // вектором для всех сил действующих на объект. По умолчанию нулевой
    ol_Vector3d moveDir;
    ol_Vector3d collCen;     // Центр сферы столкновений
    float collRad;           // Радиус сферы столкновений

    // Следующие 3 параметра представляют собой параметры столкновений. Какие объекты с какими должны сталкиваться
    // Каждый из 16 бит каждого значения показывает принадлежность к каком-либо классу объектов
    // Столкновение происходит, при выполнении 2 условий
    // 1. Хотя бы 1 бит параметра collType объекта с которым сталкиваются(принимающего) должен совпадать с хотя бы одним битом
    //    параметра collCheck сталкивающегося объекта
    // 2. Хотя бы 1 бит параметра collAccept объекта с которым сталкиваются(принимающего) должен совпадать с хотя бы одним битом
    //    параметра collType сталкивающегося объекта
    unsigned short collType;    // тип объекта, то есть к каким классам принадлежит
    unsigned short collCheck;   // набор типов объектов с которыми сталкивается объект
    unsigned short collAccept;  // набор типов объектов которые сталкиваются с ним

    // Конструктор и Деструктор
    ol_SceneObject(const char *name);
    virtual ~ol_SceneObject();

    void                setParent(ol_SceneObject *parent);		// Задает родительский объект
    void                addChild(ol_SceneObject *child);		// Добавляет подчиненный объект
    ol_SceneObject*     getParent(void);						// Возвращает указатель на родительский объект
    void                setID(int id);							// Задает идентификационный номер
    int                 getID(void);							// Возвращает идентификационный номер
    void                setName(const char *name);				// Задает имя объекта
    const char*         getName(void);							// Возвращает имя объекта
    virtual void        setPosition(ol_Vector3d pos);			// Задает положение объекта в пространстве

    virtual ol_Vector3d  getPosition(void) const
    {
        return pos;    // Возвращает положение объекта
    }
    ol_Vector3d getForward(void)
    {
        return forward;
    }
    ol_Vector3d getSide(void)
    {
        return side;
    }
    ol_Vector3d getUp(void)
    {
        return up;
    }

    // Yaw - вокруг up вектора; Pitch - вокруг side вектора; Roll - вокруг forward вектора
    // Правило правой руки: Если направить большой палец правой руки по направлению оси,
    // то вращение направлено в сторону сжимания руки в кулак
    virtual void Rotate(float yaw, float pitch, float roll);
    // Осуществляет непосредственный поворот. Вызывается в Update(). Rotate - всего лишь накапливает угол поворота.
    virtual void setRotation(void);
    // Врвщение вокруг заданной точки. hor_ang - угол вращения в горизонтальной плоскости, vert_ang - в вертикальной
    virtual void RotateAround(float hor_ang, float vert_ang, ol_Vector3d point);
    virtual void MakePointTheTarget(ol_Vector3d) {}
    virtual void GoForward(float speed);
    virtual void GoSide(float speed);
    virtual void GoUp(float speed);
    virtual void Move(ol_Vector3d dir);

    // Столкновения
    virtual bool CheckCollisionWith(ol_SceneObject *object, ol_SceneEvent *event)
    {
        return false;
    }
    virtual void PreventPassThrough(ol_Vector3d normal);      // изменяет вектор движения не давая пройти через другой объект

    virtual void Draw(void) {}                      // Вызов всех методов отрисовки
    virtual void Update(void);                      // Обновление параметров
    virtual void CheckEvents(ol_Event *event) {}     // Обработка событий

    // Работа с материалами
    void            setMaterial(ol_Material *mat);
    ol_Material*     getMaterial(void);
    void            EnableMaterial(void);
    void            DisableMaterial(void);

    // Отсечение по пирамиде видимости
    bool isFrustumCulling(void)
    {
        return frustumCulling;
    }
    void setFrustumCulling(bool frust)
    {
        frustumCulling = frust;
    }
    void setCollisionParams(unsigned short type, unsigned short check, unsigned short accept, float rad = 1.0f);

    virtual vector<ol_Line> getGeomInfo(void);   // Возвращает набор линий для отрисовки нормалей и т.д.
};

/**
    --------------------------- Static Mesh Scene Object -----------------------------------
    >                                                                                      <
    >  Класс статической модели. Предназначен для отображения неподвижных моделей. Знает   <
    >  точные координаты всех вершин модели и умеет рендерить сам себя. Для перемещения    <
    >  и поворота используется непосредственное преобразование координат всех вершин       <
    >  объекта. Более эффективен для отображения статичных объектов, так как в рендеринге  <
    >  не вызывается glMultMatrix.                                                         <
    >                                                                                      <
    ----------------------------------------------------------------------------------------
**/

class ol_StaticMeshSceneObject: public ol_SceneObject
{
protected:
    ol_Mesh3D *pMesh;

public:
    ol_StaticMeshSceneObject(const char *name, ol_Mesh3D *mesh);
    virtual ~ol_StaticMeshSceneObject();

    // При повороте происходит непосредственное преобразование координат и нормалей
    virtual void setRotation(void);
    virtual void setPosition(ol_Vector3d newPos);
    virtual void Draw(void);
    virtual void Scale(ol_Vector3d k);
    virtual bool CheckCollisionWith(ol_SceneObject *object, ol_SceneEvent *event);
    virtual vector<ol_Line> getGeomInfo(void);
    ol_Mesh3D *getMesh(void);
};

/**
    ----------------------------- Dynamic Mesh Scene Object --------------------------------
    >                                                                                      <
    >  Класс динамической модели. Унаследован от статической. Для поворота и перемещения   <
    >  используется glMultMatrix. Геометрически все вершины находятся в начальной позиции  <
    >  и рендерятся средствами статичного объекта.                                         <
    >                                                                                      <
    ----------------------------------------------------------------------------------------
**/

class ol_DynamicMeshSceneObject: public ol_StaticMeshSceneObject
{
public:
    ol_DynamicMeshSceneObject(const char *name, ol_Mesh3D *mesh);
    virtual ~ol_DynamicMeshSceneObject();

    // Фактические значения координат и нормалей остаются неизменными
    // В каждом кадре объект из начальной позиции переносится в заданную средствами OpenGL (умножение на матрицу)
    virtual void setRotation(void);
    virtual void RotateAround(float hor_ang, float vert_ang, ol_Vector3d point);
    virtual void setPosition(ol_Vector3d pos);
    virtual void MakePointTheTarget(ol_Vector3d targ) {}
    virtual void Move(ol_Vector3d dir);
    virtual void GoForward(float speed);
    virtual void Draw(void);
    virtual vector<ol_Line> getGeomInfo(void);
};

/**
    ------------------------------- Camera Scene Objects -----------------------------------
    >                                                                                      <
    >                             Классы различных камер.                                  <
    >                                                                                      <
    ----------------------------------------------------------------------------------------
**/

class ol_CameraSceneObject: public ol_SceneObject
{
protected:
    ol_Vector3d targ;        // точка, куда смотрит камера
    float rotSpeed;         // скорость врашения

public:
    ol_CameraSceneObject(ol_Vector3d pos, ol_Vector3d targ, const char *name);
    virtual ~ol_CameraSceneObject();

    ol_Vector3d  getTarget(void) const
    {
        return targ;
    }
    void        setTarget(ol_Vector3d targ);

    virtual void Look(void);
};

/// Камера от третьего лица, как в Role Plaing Game (RPG)
class ol_CameraSceneObjectRPG: public ol_CameraSceneObject
{
private:
    float zoom;                         // скорость приближения

    void Rotate(ol_Vector2df rot);       // обработка вращения
    void Zoom(int del);                 // приближение/отдаление
public:
    ol_CameraSceneObjectRPG(ol_Vector3d pos, ol_Vector3d targ, const char *name);
    virtual ~ol_CameraSceneObjectRPG();

    virtual void CheckEvents(ol_Event *event);
};

/// Камера от первого лица, как в First Person Shooter (FPS)
class ol_CameraSceneObjectFPS: public ol_CameraSceneObject
{
private:
    float moveSpeed;                 // скорость движения камеры

    void Rotate(ol_Vector2df rot);    // обработка вращения

    bool first_rot; // используется для проверки первого вращения

public:
    ol_CameraSceneObjectFPS(ol_Vector3d pos, ol_Vector3d targ, const char *name);
    virtual ~ol_CameraSceneObjectFPS();

    virtual void CheckEvents(ol_Event *event);
    virtual void setPosition(ol_Vector3d pos);
    virtual void setRotation(void);
};

/**
    ------------------------------------- Lights -------------------------------------------
    >                                                                                      <
    >        GL-ный источник света. Знает все свои параметры и умеет вкл/выкл.             <
    >                                                                                      <
    ----------------------------------------------------------------------------------------
**/

class ol_LightSceneObject
{
private:
    GLfloat ambient[4];
    GLfloat diffuse[4];
    GLfloat specular[4];
    GLfloat position[4];
    GLenum lightNum;

public:
    ol_LightSceneObject(ol_Color amb, ol_Color diff, ol_Color spec, ol_Vector3d pos)
    {
        ambient[0] = amb.r;
        ambient[1] = amb.g;
        ambient[2] = amb.b;
        ambient[3] = amb.a;
        diffuse[0] = diff.r;
        diffuse[1] = diff.g;
        diffuse[2] = diff.b;
        diffuse[3] = diff.a;
        specular[0] = spec.r;
        specular[1] = spec.g;
        specular[2] = spec.b;
        specular[3] = spec.a;
        position[0] = pos.x;
        position[1] = pos.y;
        position[2] = pos.z;
        position[3] = 0.0f;
    }
    ~ol_LightSceneObject() {}

    bool setLightNumber(GLubyte num);
    void Light(void);
};

/**
    ------------------------------ Terrain Scene object ------------------------------------
    >                                                                                      <
    >             Ландшафт. Статический объект, создаваемый из карты высот.                <
    >                                                                                      <
    ----------------------------------------------------------------------------------------
**/

class ol_TerrainSceneObject: public ol_StaticMeshSceneObject
{
private:
    float horScale;             // Величина масштабирования поверхности в горизонтальной плоскости
    float vertScale;            // Масштабирование по вертикали
    unsigned int step;          // шаг выборки вершины из карты
    unsigned int smooth;        // степень сглаживания ландшафта

    // Вычисляет номер элемента одномерного массива, чтобы к нему можно было обращаться, как к двумерному
    GLuint CalcIndex(GLuint width, GLuint x, GLuint y);
    // Возвращает высоту из карты вершин
    int Height(ol_TextureImage *tex, unsigned int x, unsigned int y);
    // Заполняет массивы вершин, цветов и индексов
    void BuildTerrainMesh(ol_TextureImage *tex);
public:
    ol_TerrainSceneObject(ol_Graphics *graph, ol_Mesh3D *mesh, const char* fileName, unsigned int step, float horScale, float vertScale, unsigned int smooth, const char *name);
    virtual ~ol_TerrainSceneObject();
};

/**
    ----------------------------------- Sky Box --------------------------------------------
    >                                                                                      <
    >  Куб, на который наложены текстуры панорамы. Создает видимость окружающего           <
    >  пространства. Всегда движется вместе с камерой. Координаты центра совпадают с       <
    >  координатами камеры.                                                                <
    >                                                                                      <
    ----------------------------------------------------------------------------------------
**/

class ol_SkyBoxSceneObject: public ol_DynamicMeshSceneObject
{
public:
    ol_SkyBoxSceneObject(const char *name, ol_Mesh3D *mesh);
    virtual ~ol_SkyBoxSceneObject();
};

/**
    -------------------------------- Scene Manager -----------------------------------------
    >                                                                                      <
    >  Класс, который управляет созданием, удалением, отображением и обработкой событий    <
    >  всех объектов на сцене.                                                             <
    >                                                                                      <
    ----------------------------------------------------------------------------------------
**/

class ol_SceneManager
{
private:
    ol_Graphics *graph;      // указатель на графический интерфейс
    int activeCamera;

    ol_Frustum Frustum;

    vector<ol_SceneObject*>          objects;
    vector<ol_CameraSceneObject*>    cameras;
    vector<ol_LightSceneObject*>     lights;
    vector<ol_SceneEvent*>           events;

    unsigned int objectsRendered;
    unsigned int objectsCount;

    void addObject(ol_SceneObject *object);

    void addEvent(ol_SceneEvent *event);
    void ClearSceneEventStack(void);
    int lastEvent;

public:
    ol_SceneManager(ol_Graphics *graph);
    ~ol_SceneManager();

    void DrawAll(void);
    void UpdateAll(void);
    // обработка событий клавиатуры и мыши, генерация столкновений
    void CheckSceneEvents(ol_Event *event);

    ol_SceneObject*              addSceneObject(const char *name = "Scene Object");
    ol_CameraSceneObject*        addCameraSceneObject(ol_Vector3d pos, ol_Vector3d targ, const char *name = "Scene Object");
    ol_CameraSceneObject*        addCameraSceneObjectRPG(ol_Vector3d pos, ol_Vector3d targ, const char *name = "Scene Object");
    ol_CameraSceneObject*        addCameraSceneObjectFPS(ol_Vector3d pos, ol_Vector3d targ, const char *name = "Scene Object");
    ol_SkyBoxSceneObject*        addSkyBoxSceneObject(GLuint texture, const char *name = "Sky Box");
    ol_TerrainSceneObject*       addTerrainSceneObject(const char *fileName, unsigned int step, float horScale, float vertScale, float smooth, const char *name = "Scene Object");
    ol_StaticMeshSceneObject*    addStaticMeshSceneObject(ol_Mesh3D *mesh, const char *name = "Scene Object");
    ol_DynamicMeshSceneObject*   addDynamicMeshSceneObject(ol_Mesh3D *mesh, const char *name = "Scene Object");
    ol_LightSceneObject*         addLightSceneObject(ol_Color amb, ol_Color diff, ol_Color spec, ol_Vector3d pos);

    // Возвращает количество отрендереных объектов в данный момент
    unsigned int getRenderObjCount(void);
    // Возвращает общее количество объектов
    unsigned int getObjCount(void);
    // Возвращает указатель на текущую камеру
    ol_CameraSceneObject*  getActiveCamera(void);
    // Вычисляет мировые координаты концов селектирующего отрезка по оконным координатам курсора
    void CalcRayFromCursor(int mouse_x, int mouse_y, ol_Vector3d &p1, ol_Vector3d &p2);
    // Не доработано!
    bool exportSceneObject(ol_SceneObject *object, char *fileName);
    // Возвращает последнее игровое событие, если таковое есть или NULL в противном случае
    ol_SceneEvent* getLastEvent(void);
};

#endif // OL_SCENE_H_INCLUDED
