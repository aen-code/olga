/**
------------------------------------------------------------------------
  This file is part of the OLGA project.
  Copyright © 2017 by Enver Akhmedov.
  E-mail: aen-code@yandex.ru
  For conditions of distribution and use, see copyright notice in OLGA.h
------------------------------------------------------------------------
**/

#ifndef OL_TEXTURE_H_INCLUDED
#define OL_TEXTURE_H_INCLUDED

/**
    ------------------------------- Texture Image ------------------------------------------
    >                                                                                      <
    >                    Класс для хранения изображения.                                   <
    >                                                                                      <
    ----------------------------------------------------------------------------------------
**/

class ol_TextureImage
{
public:
    GLubyte  *imageData;                  // Данные изображения
    GLuint   bpp;                         // Глубина цвета в битах на пиксель
    GLuint   width;                       // Ширина изображения
    GLuint   height;                      // Высота изображения
    GLint    texID;                       // texID используется для выбора текстуры (идентификатор)
//---- Kamillorn added -----------------------
    int width_amount;                     // количество кадров в ширину
    int height_amount;                    // количество кадров в высоту
    string name;                          // имя анимации или изображения
    int cur_pos;                          // поле используемое для технических нужд при создании анимации
    unsigned short fps;                   // количество кадров в секунду. Используется только для анимации
//--------------------------------------------
    ol_TextureImage()
    {
        name = "Image";
        width = height = 0;
        width_amount = height_amount = 1;
        texID = -1;
        imageData = NULL;
        cur_pos = 0;
        fps = 1;
    }
    ~ol_TextureImage()
    {
        free(imageData);
    }
};

struct ol_Texture
{
    ol_Texture()
    {
        width = height = 0;
        texID = -1;
        width_amount = height_amount = 1;
        fps = 1;
    }
    ~ol_Texture() {}

    GLuint   width;                       // Ширина изображения
    GLuint   height;                      // Высота изображения
    GLint    texID;                       // texID используется для выбора текстуры (идентификатор)

    int width_amount;                     // количество кадров в ширину
    int height_amount;                    // количество кадров в высоту
    unsigned short fps;                   // количество кадров в секунду
};

#endif // OL_TEXTURE_H_INCLUDED
