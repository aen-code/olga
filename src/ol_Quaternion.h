/**
------------------------------------------------------------------------
  This file is part of the OLGA project.
  Copyright © 2017 by Enver Akhmedov.
  E-mail: aen-code@yandex.ru
  For conditions of distribution and use, see copyright notice in OLGA.h
------------------------------------------------------------------------
**/

#ifndef OL_QUATERNION_H_INCLUDED
#define OL_QUATERNION_H_INCLUDED

#define SLERP_TO_LERP_SWITCH_THRESHOLD 0.01f

#include "ol_Vector_Math.h"

/**
    --------------------------------- Quaternion -------------------------------------------
    >                                                                                      <
    >  Используется для вычисления трехмерного вращения. x, y, z - ось вращения, w - угол. <
    >  Кватернионы удобны, когда необходимо задать сложное вращение вокруг нескольких      <
    >  осей. Для каждого вращения задается отдельный кватернион. Результирующее вращение   <
    >  будет произведением отдельных кватернионов. Кроме того, из кватерниона можно        <
    >  получить матрицу вращения.                                                          <
    >                                                                                      <
    ----------------------------------------------------------------------------------------
**/

class ol_Quaternion
{
public:
    ol_Quaternion()
    {
        x=0.0f;
        y=0.0f;
        z=0.0f;
        w = 1.0f;
    }
    ~ol_Quaternion() {}

    ol_Quaternion(float x, float y, float z, float w)
    {
        this->x = x;
        this->y = y;
        this->z = z;
        this->w = w;
    }

    ol_Quaternion& operator= (ol_Quaternion q)
    {
        x = q.x;
        y = q.y;
        z = q.z;
        w = q.w;
        return *this;
    }

    ol_Quaternion operator*(const ol_Quaternion &q)
    {
        ol_Vector3d vector1, vector2, cross;
        float angle;

        vector1.x = this->x;
        vector1.y = this->y;
        vector1.z = this->z;
        vector2.x = q.x;
        vector2.y = q.y;
        vector2.z = q.z;
        angle = ((this->w * q.w) - (Dot(vector1, vector2)));

        cross = Cross(vector1, vector2);
        vector1.x *= q.w;
        vector1.y *= q.w;
        vector1.z *= q.w;
        vector2.x *= this->w;
        vector2.y *= this->w;
        vector2.z *= this->w;

        return ol_Quaternion(vector1.x + vector2.x + cross.x, vector1.y + vector2.y + cross.y, vector1.z + vector2.z + cross.z, angle);
    }

    float x,y,z,w;
};

ol_Quaternion QuaternionFromAxisAngle(ol_Vector3d axis, float angle);

void QuaternionToAxisAngle(ol_Quaternion quat, ol_Vector3d *axis, float *angle);

void QuaternionToMatrix(ol_Quaternion quat, float matrix[16]);

void QuaternionRotate(ol_Quaternion *quat, ol_Vector3d axis, float angle);

void QuaternionNormalize(ol_Quaternion *quat);

void QuaternionInvert(ol_Quaternion *quat);

ol_Vector3d QuaternionMultiplyVector(ol_Quaternion *quat, ol_Vector3d *vector);

ol_Quaternion QuaternionSLERP(ol_Quaternion start, ol_Quaternion end, float alpha);

ol_Quaternion QuaternionFromVector(ol_Vector3d v);

ol_Vector3d QuaternionToVector(ol_Quaternion q);

// поворачивает вектор vector на угол angle (градусы) вокруг оси axis
ol_Vector3d RotateVector(ol_Vector3d axis, ol_Vector3d vector, float angle);

#endif // OL_QUATERNION_H_INCLUDED
