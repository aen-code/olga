/**
------------------------------------------------------------------------
  This file is part of the OLGA project.
  Copyright © 2017 by Enver Akhmedov.
  E-mail: aen-code@yandex.ru
  For conditions of distribution and use, see copyright notice in OLGA.h
------------------------------------------------------------------------
**/

#ifndef OL_FILE_SYSTEM_H_INCLUDED
#define OL_FILE_SYSTEM_H_INCLUDED

#include <stdio.h>
#include <stdlib.h>
#include <dirent.h>
#include <string.h>
#include <ctype.h>
#include <locale>
#include <string>
#include <cwchar>
#include <algorithm>
#include <cctype>
#include <vector>
#include "core/ol_System.h"

using namespace std;

enum ol_FileType
{
    OL_IMAGE_TYPE_TGA,
    OL_IMAGE_TYPE_RAW,
    OL_IMAGE_TYPE_BMP,
    OL_MODEL_TYPE_OBJ,
    OL_RESOURCE_TYPE_OLR,
    OL_RESOURCE_TYPE_XML
};

enum ol_DirType
{
    OL_DIR_TYPE_DIR,
    OL_DIR_TYPE_FILE
};

struct ol_FileNames
{
    string name;
    ol_DirType type;
};

using namespace std;

// возвращает размер файла в байтах
unsigned long CalcFileSize(const char* namefile);
// проверяет, поддерживает ли движок заданный тип файлов
// если да - возвращает тип файла (ol_FileType), если нет - возвращает -1
int IsFileTypeSupported(const char *namefile);
// определяет количество групп символов в строке разделенных пробелами
// используется для определения количества вершин в полигоне при чтении *.obj
int CalcArgNum(char s[]);
// находит список каталогов и файлов в директории path
int ListDir(string path, vector<ol_FileNames> &files, bool showHidden);
// сортирует список названий файлов в files в алфавитном порядке
void SortFileNames(vector<ol_FileNames> &files);

typedef std::codecvt<wchar_t, char, std::mbstate_t> facet_type;

// преобразует простую строку к типу wstring на основе текущей локали
int String_to_Wstring(string &str_in, wstring &str_out);
// обратное действие
int Wstring_to_String(wstring &str_in, string &str_out);
// изменяет все символы в str_in на заглавные и сохраняет результат в str_out
void String_to_Uppercase(string &str_in, string &str_out);
// Разбивает строку inputStr на подстроки разделенные каким-нибудь символом из delimiters
// и сохраняет результат в массив строк tokens
void SplitSring(char *inputStr, const char *delimiters, vector<string> &tokens);
// тоже самое для wstring
void SplitSring(wchar_t *inputStr, const wchar_t *delimiters, vector<wstring> &tokens);
// A set of functions to work with strings
string IntToString(int val);
string DoubleToString(double val);
string LDoubleToString(long double val);
double StringToDouble(const char *str);
int StringToInt(const char *str);

#endif // OL_FILE_SYSTEM_H_INCLUDED
