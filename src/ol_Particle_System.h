/**
------------------------------------------------------------------------
  This file is part of the OLGA project.
  Copyright © 2017 by Enver Akhmedov.
  E-mail: aen-code@yandex.ru
  For conditions of distribution and use, see copyright notice in OLGA.h
------------------------------------------------------------------------
**/

#ifndef OL_PARTICLE_SYSTEM_H_INCLUDED
#define OL_PARTICLE_SYSTEM_H_INCLUDED

#include <stdlib.h> // для rand()

#include "ol_Mechanics.h"
#include "core/ol_System.h"
#include "ol_Graphics.h"
#include <list>

/**
    --------------------------------- Particle ---------------------------------------------
    >                                                                                      <
    >                     Класс частицы для создания спец. эффектов.                       <
    >                                                                                      <
    ----------------------------------------------------------------------------------------
**/

class ol_Particle: public ol_MechanicsObject2d
{
public:
    ol_Particle();
    ol_Particle(ol_Vector2df pos, ol_Vector2df speed, double bornTime);
    virtual ~ol_Particle();

    double bornTime;
};

/**
    --------------------------------- Particle Emitter -------------------------------------
    >                                                                                      <
    >     Излучатель частиц. Создает, удаляет, отрисовывает частицы.                       <
    >                                                                                      <
    ----------------------------------------------------------------------------------------
**/

class ol_ParticleEmitter: public ol_MechanicsObject2d
{
private:
    ol_Timer *timer;                         // указатель на системный таймер для отслеживания времени жизни частиц
    ol_Graphics *graph;                      // указатель на графический интерфейс
    list<ol_Particle *> particles;           // вектор частиц
    unsigned int partCount;                 // Общее количество частиц
    unsigned short freq;                    // частота генерации частиц  штук/с
    float lifeTime;                         // время жизни частицы
    ol_TimeMonitor *period;                  // засечки времени
    double curTime;                         // текущий замер времени
    ol_PhysicsAffector *affector;
public:
    ol_ParticleEmitter(ol_Graphics *graph, ol_Vector2df pos, double freq, double lifeTime);
    virtual ~ol_ParticleEmitter();

    void Draw(void);
    void Update(void);

    void addAffector(ol_PhysicsAffector *affector);
};

/**
    --------------------------------- Particle Manager -------------------------------------
    >                                                                                      <
    >                   Класс управляющий всеми частицами сцены.                           <
    >   Позволяет добавлять источники частиц, а также автоматически рендерит все частицы.  <
    >                                                                                      <
    ----------------------------------------------------------------------------------------
**/

class ol_ParticleManager
{
private:
    ol_Graphics *graph;
    vector<ol_ParticleEmitter *> emitters;
    unsigned int emittersCount;
    void addObject(ol_ParticleEmitter *emi);
public:
    ol_ParticleManager(ol_Graphics *graph);
    ~ol_ParticleManager();

    ol_ParticleEmitter *addEmitter(ol_Vector2df pos, double freq, double lifeTime);
    void DrawAll(void);
    void UpdateAll(void);
};

#endif // OL_PARTICLE_SYSTEM_H_INCLUDED
