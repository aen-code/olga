/**
------------------------------------------------------------------------
  This file is part of the OLGA project.
  Copyright © 2017 by Enver Akhmedov.
  E-mail: aen-code@yandex.ru
  For conditions of distribution and use, see copyright notice in OLGA.h
------------------------------------------------------------------------
**/

#ifndef OL_RESOURCE_H_INCLUDED
#define OL_RESOURCE_H_INCLUDED

#include "core/ol_System.h"
#include <vector>
#include <GL/gl.h>
#include <stdio.h>
#include <string.h>
#include <math.h>
#include "ol_Vector_Math.h"
#include "ol_Geometry.h"
#include "ol_File_System.h" 
#include "ol_Texture.h"


#include "pugixml.hpp"
#include "../third_party/base64/src/base64.h"

using namespace pugi;

/**
    --------------------------------- Resource Manager -------------------------------------
    >                                                                                      <
    >   Класс для управления загрузкой и сохранением данных различных форматов, таких      <
    >   как 2д изображения и 3д модели.                                                    <
    >                                                                                      <
    ----------------------------------------------------------------------------------------
**/

class ol_ResourceManager
{
private:
    // для временного хранения вершин, нормалей и текстурных координат загружаемой модели
    // тоже самое есть и в классе ol_LoadObj. Можно вообще отказаться от ol_LoadObj перенеся его ф-ии сюда
    vector<ol_Vector3d>  vertBuf;
    vector<ol_Vector3d>  normBuf;
    vector<ol_Vector2df> texcBuf;
    vector<GLuint>       indBuf;

    void ClearBufs(void);
public:
    ol_ResourceManager() {}
    ~ol_ResourceManager() {}

    // загружает меш из *.olr файла
    bool LoadMeshOL(ol_Mesh3D *mesh, const char *fileName);
    // загружает *.obj
    bool LoadOBJ(ol_Mesh3D *mesh, const char *fileName);
    // загружает *.tga файл
    bool LoadTGA(const char *fileName, ol_TextureImage *texture);
    // загружает *.raw файл
    bool LoadRAW(const char *fileName, ol_TextureImage *texture);
    // загружает *.bmp файл
    bool LoadBMP(const char *fileName, ol_TextureImage *texture);
    // сохраняет текстуру в *.xml файл, содержащую несколько кадров анимации;
    // можно применять и для сохранения обычной текстуры, просто количество кадров будет 1
    void SaveAnimTexXML(const char *file_name, ol_TextureImage *im);
    // загружает анимационную текстуры из *.xml файла
    bool LoadAnimTexXML(const char *file_name, ol_TextureImage* texture);
};

#endif // OL_RESOURCE_H_INCLUDED
