/**
------------------------------------------------------------------------
  This file is part of the OLGA project.
  Copyright © 2017 by Enver Akhmedov.
  E-mail: aen-code@yandex.ru
  For conditions of distribution and use, see copyright notice in OLGA.h
------------------------------------------------------------------------
**/

#ifndef OL_MECHANICS_INCLUDED
#define OL_MECHANICS_INCLUDED

#include <vector>
#include "core/ol_System.h"
#include "ol_Geometry.h"
#include "stdio.h"

using namespace std;

class ol_FrictionAffector;
class ol_CircleCollision2d;
class ol_PhysicsManager2d;

/**
    --------------------------------- Collision Circle -------------------------------------
    >                                                                                      <
    >                             Окружность столкновений.                                 <
    >                                                                                      <
    ----------------------------------------------------------------------------------------
**/

class ol_CollisionCircle
{
public:
    ol_Vector2df pos;            // центр окружности столкновений
    float collR;                // радиус столкновения
    float touchR;               // радиус касания
    bool touching;              // принимает значение истина в случае касания другого объекта
    bool collided;              // истина в случае, если окружность только что столкнулась
    int type;                   // Зарезервированный параметр. Может быть использован в клиентском
    // приложении для задания дополнительных свойств объекта

    ol_CollisionCircle();
    ol_CollisionCircle(ol_Vector2df pos, float collR, float touchR);
    ~ol_CollisionCircle();
};

/**
    --------------------------------- Mechanics Object 2d ----------------------------------
    >                                                                                      <
    >  Простейший механический объект двумерной сцены. Не имеет визуального представления. <
    >         Изменяет свое положение и скорость в соответствии действующими силами.       <
    >                                                                                      <
    ----------------------------------------------------------------------------------------
**/

class ol_MechanicsObject2d
{
public:
    float           mass;           // масса
    float           elasticity;     // упругость. значение от 0 до 1. При 0 отталкивания нет совсем, при значении >1 при соударении приобретает "энергию"
    ol_Vector2df     pos;            // положение в пространстве
    ol_Vector2df     vel;            // скорость
    ol_Vector2df     accel;          // постоянное ускорение
    ol_Vector2df     affect;         // дополнительно ускорение приобретаемое вследствие разового воздействия силы

    bool active;                    // если значение ложь - не обрабатывается

    ol_MechanicsObject2d();
    ol_MechanicsObject2d(float mass, ol_Vector2df pos, ol_Vector2df vel);
    virtual ~ol_MechanicsObject2d();

    virtual void Update(void);
};

/**
    --------------------------------- Collision Object 2d ----------------------------------
    >                                                                                      <
    >      Интерфейсный класс для обработки столкновений механических объектов сцены.      <
    >                                                                                      <
    ----------------------------------------------------------------------------------------
**/

class ol_CollisionObject2d: public ol_MechanicsObject2d
{
    friend class ol_PhysicsManager2d;
protected:
    // Сбрасывает флаги столкновения и касания
    void ResetCollisionFlags(void) {}
public:

    // Параметры столкновений. Тоже самое что и для 3д объектов
    unsigned short collType;    // тип объекта, то есть к каким классам принадлежит
    unsigned short collCheck;   // набор типов объектов с которыми сталкивается объект
    unsigned short collAccept;  // набор типов объектов которые сталкиваются с ним

    ol_CollisionObject2d();
    ol_CollisionObject2d(float mass, ol_Vector2df pos, ol_Vector2df vel);
    virtual ~ol_CollisionObject2d();

    // Задает тип объекта и параметры столкновений
    void setCollisionParams(unsigned short type, unsigned short check, unsigned short accept);
    // интерфейс для обработки столкновений
    virtual bool CheckCollisionWith(ol_CollisionObject2d *object, ol_SceneEvent *event);
};

/**
    --------------------------------- Circle Collision -------------------------------------
    >                                                                                      <
    >      Механический объект сцены, который сталкивается с другими объектами с           <
    >                         помощью окружности столкновений.                             <
    >                                                                                      <
    ----------------------------------------------------------------------------------------
**/

class ol_CircleCollision2d:public ol_CollisionObject2d
{
    friend class ol_PhysicsManager2d;
protected:
    void ResetCollisionFlags(void);
public:
    vector<ol_CollisionCircle*> circles;
    ol_CollisionCircle *eventCircle; // сюда сохраняется окружность взаимодействовавшая только что

    ol_CircleCollision2d();
    ol_CircleCollision2d(float mass, ol_Vector2df pos, ol_Vector2df vel);
    virtual ~ol_CircleCollision2d();

    virtual void Update(void);
    virtual void Shift(ol_Vector2df &dir);
    virtual void setPosition(ol_Vector2df newPos);
    virtual ol_Vector2df getPosition(void);
    // Добавляет окружность столкновений. pos - координаты относительно центра объекта
    // collR радиус столкновения. touchDelta - на сколько окружность касания больше окр. столк.
    ol_CollisionCircle* addCollisionCircle(ol_Vector2df pos, float collR, float touchDelta);
    // проверяет столкновения
    virtual bool CheckCollisionWith(ol_CircleCollision2d *object, ol_SceneEvent *event);
    // действие, если произошло столкновение с другим объектом такого же типа
    virtual void OnCollision(ol_CircleCollision2d *obj);
};

/**
    --------------------------------- Mesh Collision ---------------------------------------
    >                                                                                      <
    >      Механический объект сцены, который сталкивается с другими объектами с           <
    >                         помощью полигона столкновений.                               <
    >                                                                                      <
    ----------------------------------------------------------------------------------------
**/

class ol_MeshCollision2d: public ol_CollisionObject2d
{
    friend class ol_PhysicsManager2d;
public:
    ol_Mesh2d *mesh;             // набор вершин по которым обрабатывается столкновение
    ol_Vector2df closestPoint;   // ближайшая точка до последнего обработанного объекта
    ol_FrictionAffector *frict;  // трение

    ol_MeshCollision2d(ol_Mesh2d* mesh, float friction, float threshold);
    ol_MeshCollision2d(float mass, ol_Vector2df pos, ol_Vector2df vel, ol_Mesh2d* mesh, float friction, float threshold);
    virtual ~ol_MeshCollision2d();

    void setMesh(ol_Mesh2d *mesh);
    ol_Mesh2d* getMesh(void);
    virtual bool CheckCollisionWith(ol_CircleCollision2d *object, ol_SceneEvent *event);
};

/**
    --------------------------------- 2D Physics Manager -----------------------------------
    >                                                                                      <
    >                Класс отвечающий за обработку 2д физики.                              <
    >                                                                                      <
    ----------------------------------------------------------------------------------------
**/

class ol_PhysicsManager2d
{
private:
    vector<ol_CircleCollision2d*> circleObjects;
    vector<ol_MeshCollision2d*>   meshObjects;
    vector<ol_SceneEvent*> events;
    vector<unsigned int> pairs; // запоминается пара столкнувшихся объектов, чтобы повторно не обрабатывать
    void addEvent(ol_SceneEvent *event);
    void ClearSceneEventStack(void);
    int lastEvent;

public:
    friend class ol_SceneManager2d;

    ol_PhysicsManager2d();
    ~ol_PhysicsManager2d();

    void addMeshObject(ol_MeshCollision2d *obj);
    void addCircleObject(ol_CircleCollision2d *obj);
    void CheckCollisionEvents(void);
    ol_SceneEvent* getLastEvent(void);
};

/**
    --------------------------------- Physics Affector -------------------------------------
    >                                                                                      <
    >    Среда существования механических объектов. Содержит набор сил, действующих на     <
    >          объекты. Имеет метод воздействия на объекты.                                <
    >                                                                                      <
    ----------------------------------------------------------------------------------------
**/

class ol_PhysicsAffector
{
protected:
public:
    ol_PhysicsAffector() {}
    virtual ~ol_PhysicsAffector() {}

    virtual void Affect(ol_MechanicsObject2d *obj) {}
    virtual void ConstantAffect(ol_MechanicsObject2d *obj) {}
};

class ol_GravityAffector: public ol_PhysicsAffector
{
protected:
    ol_Vector2df gravity;
public:
    ol_GravityAffector(ol_Vector2df grav);
    virtual ~ol_GravityAffector();

    virtual void ConstantAffect(ol_MechanicsObject2d *obj);
};

class ol_SimpleForceAffector: public ol_PhysicsAffector
{
public:
    ol_Vector2df force;

    ol_SimpleForceAffector();
    ol_SimpleForceAffector(ol_Vector2df force);
    virtual ~ol_SimpleForceAffector();

    virtual void Affect(ol_MechanicsObject2d *obj);
};

/**
    --------------------------------- Friction Affector ------------------------------------
    >                                                                                      <
    >                                      Трение.                                         <
    >  Не является трением в классическом понимании. Просто некая сила, действующая против <
    >  скорости динамического объекта при соприкосновении его со статическим объектом и    <
    >  пропорциональная проекции ускорения на нормаль к поверхности (реакция опоры).       <
    >  Пропорциональна скорости, то есть с увеличением скорости увеличивается. Кроме того, <
    >  есть пороговое значение скорости при котором пропорциональная зависимость           <
    >  отключается.                                                                        <
    >                                                                                      <
    ----------------------------------------------------------------------------------------
**/

class ol_FrictionAffector: public ol_PhysicsAffector
{
public:
    float friction;     // коэффициент трения. Должен быть от 0 до 1
    float threshold;    // пороговая скорость

    ol_FrictionAffector();
    ol_FrictionAffector(float friction, float threshold);
    virtual ~ol_FrictionAffector();

    virtual void Affect(ol_MechanicsObject2d *obj, ol_Vector2df &norm);
};

#endif // OL_MECHANICS_INCLUDED
