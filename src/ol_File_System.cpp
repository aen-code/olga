/**
------------------------------------------------------------------------
  This file is part of the OLGA project.
  Copyright © 2017 by Enver Akhmedov.
  E-mail: aen-code@yandex.ru
  For conditions of distribution and use, see copyright notice in OLGA.h
------------------------------------------------------------------------
**/

#include "ol_File_System.h"

unsigned long CalcFileSize(const char* namefile)
{
    unsigned long DL;
    FILE* fp = fopen(namefile,"rb");

    if (fp==0)
    {
        printf("Error opening file: %s\n", namefile);
        return -1;
    }
    else
    {
        fseek(fp,0,SEEK_END);
        DL=ftell(fp);
    }

    fclose(fp);
    return DL;
}

int IsFileTypeSupported(const char *namefile)
{
    int res = -1;

    if(strstr(namefile,".raw")||strstr(namefile,".RAW"))
    {
        res = OL_IMAGE_TYPE_RAW;
        return  res;
    }

    if(strstr(namefile,".tga")||strstr(namefile,".TGA"))
    {
        res = OL_IMAGE_TYPE_TGA;
        return  res;
    }

    if(strstr(namefile,".bmp")||strstr(namefile,".BMP"))
    {
        res = OL_IMAGE_TYPE_BMP;
        return  res;
    }

    if(strstr(namefile,".obj")||strstr(namefile,".OBJ"))
    {
        res = OL_MODEL_TYPE_OBJ;
        return  res;
    }

    if(strstr(namefile,".olr")||strstr(namefile,".OLR"))
    {
        res = OL_RESOURCE_TYPE_OLR;
        return  res;
    }

    if(strstr(namefile,".xml")||strstr(namefile,".XML"))
    {
        res = OL_RESOURCE_TYPE_XML;
        return  res;
    }

    return res;
}

int CalcArgNum(char s[])
{
    if(s==NULL) return -1;

    unsigned int n = strlen(s);
    unsigned int argNum = 0;
    bool f = true;

    for(unsigned int i=0; i<n; i++)
    {
        if(s[i]==' ') f = true;

        if(s[i]!=' ' && s[i]!= '\n' && f)
        {
            argNum++;
            f = false;
        }
    }
    return argNum;
}

int ListDir(string path, vector<ol_FileNames> &files, bool showHidden)
{
    struct dirent *entry;
    DIR *dp;
    ol_FileNames buf;

    vector<ol_FileNames> files_tmp;
    vector<ol_FileNames> dirs_tmp;
    bool isSubdir = false; // true если есть каталог выше

    dp = opendir(path.c_str());
    if (dp == NULL)
    {
        perror("ERROR: opendir");
        return -1;
    }

    files.clear();
    while((entry = readdir(dp)))
    {
        buf.name = entry->d_name;

        if(buf.name == ".") continue;
        if(buf.name == "..")
        {
            isSubdir = true;
            continue;
        }

        if(!showHidden)
        {
            if(buf.name[0] == '.') continue;
        }

        if(entry->d_type == DT_DIR)
        {
            buf.type = OL_DIR_TYPE_DIR;
            dirs_tmp.push_back(buf);
        }

        if(entry->d_type == DT_REG)
        {
            buf.type = OL_DIR_TYPE_FILE;
            files_tmp.push_back(buf);
        }
    }

    SortFileNames(dirs_tmp);
    SortFileNames(files_tmp);

    files = dirs_tmp;
    for(unsigned int i=0; i<files_tmp.size(); i++)
        files.push_back(files_tmp[i]);

    if(isSubdir)
    {
        buf.type = OL_DIR_TYPE_DIR;
        buf.name = "..";
        files.insert(files.begin(), buf);
    }

    closedir(dp);
    return 1;
}

int String_to_Wstring(string &str_in, wstring &str_out)
{
    std::locale mylocale = std::locale (ol_CoreParams::locale_name);
    const facet_type &myfacet = std::use_facet<facet_type>(mylocale);
    std::mbstate_t mystate = std::mbstate_t(); // the shift state object

    // prepare objects to be filled by codecvt::in :
    wchar_t *pwstr = new wchar_t [str_in.size()+1];     // the destination buffer (might be too short)
    const char* pc;                                     // from_next
    wchar_t* pwc;                                       // to_next

    // translate characters:
    facet_type::result myresult = myfacet.in (mystate, str_in.c_str(), str_in.c_str()+str_in.size(), pc, pwstr, pwstr+str_in.size()+1, pwc);
    *pwc = '\0';
    str_out = pwstr;

    if(myresult != facet_type::ok)
    {
        printf("ERROR translating from string to wide string\n");
        return -1;
    }

    delete [] pwstr;

    return 0;
}

int Wstring_to_String(wstring &str_in, string &str_out)
{
    std::locale mylocale = std::locale (ol_CoreParams::locale_name);
    const facet_type &myfacet = std::use_facet<facet_type>(mylocale);
    std::mbstate_t mystate = std::mbstate_t();

    const wchar_t *in_next = 0;
    char *out_next = 0;
    std::wstring::size_type len = str_in.size () << 2;
    char *chars = new char [len + 1];

    facet_type::result myresult = myfacet.out (mystate, str_in.c_str (), str_in.c_str () + str_in.size (), in_next, chars, chars + len, out_next);
    *out_next = '\0';
    str_out = chars;

    if(myresult != facet_type::ok)
    {
        printf("ERROR translating from string to wide string\n");
        return -1;
    }

    delete [] chars;

    return 0;
}

void SortFileNames(vector<ol_FileNames> &files)
{
    if(files.size()<=0) return;

    string upcased1, upcased2;

    for(unsigned int i=0; i<files.size()-1; i++)
        for(unsigned int j=i+1; j<files.size(); j++)
        {
            String_to_Uppercase(files[i].name, upcased1);
            String_to_Uppercase(files[j].name, upcased2);

            if(upcased1>upcased2)
            {
                ol_FileNames tmp;
                tmp = files[i];
                files[i] = files[j];
                files[j] = tmp;
            }
        }
}


void String_to_Uppercase(string &str_in, string &str_out)
{
    str_out = str_in;

    transform(str_in.begin(), str_in.end(), str_out.begin(), ::toupper);
}

void SplitSring(char *inputStr, const char *delimiters, vector<string> &tokens)
{
    string s;
    char * pch;

    tokens.clear();

    pch = strtok(inputStr, delimiters);
    while(pch != NULL)
    {
        s = pch;
        tokens.push_back(s);
        pch = strtok (NULL, delimiters);
    }
}

void SplitSring(wchar_t *inputStr, const wchar_t *delimiters, vector<wstring> &tokens)
{
    wstring ws;
    wchar_t *pwc;
    wchar_t* buffer;

    tokens.clear();

    pwc = wcstok(inputStr, delimiters, &buffer);
    while (pwc)
    {
        ws = pwc;
        tokens.push_back(ws);
        pwc = wcstok(NULL, delimiters, &buffer);
    }
}

string IntToString(int val)
{
    char buffer[sizeof(int)*8+1]; // в мануале написано, что так по-любому хватит места
    sprintf(buffer,"%d",val);

    string s(buffer);

    return s;
}

string DoubleToString(double val)
{
    char buffer[50]; // ну, надеюсь хватит места

    if((val>99999.0) || (val<-99999.0) || ((val>-0.0001)&&(val<0.0001)&&(val!=0)))
        sprintf(buffer,"%e",val);
    else sprintf(buffer,"%f",val);

    string s(buffer);

    size_t found_digit = s.find_last_of("123456789");
    size_t found_dot = s.find_last_of(".");

    int n;

    if(found_digit>found_dot) n = found_digit+1;
    else n = found_dot+2;

    return s.substr(0, n);
}

string LDoubleToString(long double val)
{
    char buffer[50]; // ну, надеюсь хватит места

    if((val>99999.0) || (val<-99999.0) || ((val>-0.0001)&&(val<0.0001)&&(val!=0)))
        sprintf(buffer,"%Le",val);
    else sprintf(buffer,"%Lf",val);

    string s(buffer);

    size_t found_digit = s.find_last_of("123456789");
    size_t found_dot = s.find_last_of(".");

    int n;

    if(found_digit>found_dot) n = found_digit+1;
    else n = found_dot+2;

    return s.substr(0, n);
}


double StringToDouble(const char *str)
{
    return atof(str);
}

int StringToInt(const char *str)
{
    return atoi(str);
}
