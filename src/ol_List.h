/**
------------------------------------------------------------------------
  This file is part of the OLGA project.
  Copyright © 2017 by Enver Akhmedov.
  E-mail: aen-code@yandex.ru
  For conditions of distribution and use, see copyright notice in OLGA.h
------------------------------------------------------------------------
**/

#ifndef OL_LIST_H_INCLUDED
#define OL_LIST_H_INCLUDED

/**
    --------------------------------------- Node -------------------------------------------
    >                                                                                      <
    >                           Узел кольцевого списка.                                    <
    >   Кольцевые списки удобны, например, в геометрии, где многоугольник хранится в       <
    >   виде кольцевого списка вершин в порядке обхода. Узел такого списка является        <
    >                           объектом класса Node:                                      <
    >                                                                                      <
    ----------------------------------------------------------------------------------------
**/

class ol_Node
{
protected:
    ol_Node *next;	// связь к последующему узлу
    ol_Node *prev;	// связь к предшествующему узлу

public:
    ol_Node();
    virtual ~ol_Node();

    ol_Node *Next(void);         // возвращает следующий узел
    ol_Node *Prev(void);         // возвращает предыдущий узел
    ol_Node *Insert(ol_Node*);    // вставить узел после текущего
    ol_Node *Remove(void);       // удалить узел из списка, возвратить его указатель
    void Splice(ol_Node*);       // cлияние списков
};

#endif // OL_LIST_H_INCLUDED
