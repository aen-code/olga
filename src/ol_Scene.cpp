/**
------------------------------------------------------------------------
  This file is part of the OLGA project.
  Copyright © 2017 by Enver Akhmedov.
  E-mail: aen-code@yandex.ru
  For conditions of distribution and use, see copyright notice in OLGA.h
------------------------------------------------------------------------
**/

#include "ol_Scene.h"

/**
    --------------------------------- Scene Object -----------------------------------------
    >                                                                                      <
    >           Базовый класс для представления всех объектов на сцене.                    <
    >                                                                                      <
    ----------------------------------------------------------------------------------------
**/

ol_SceneObject::ol_SceneObject(const char *name)
{
    xAngle = yAngle = zAngle = 0.0f;
    angm = 0.17f; // радианы, примерно 10 градусов
    yaw = pitch = roll = 0.0f;
    pos = ol_Vector3d(0.0f, 0.0f, 0.0f);
    moveDir = ol_Vector3d(0.0f, 0.0f, 0.0f);
    rotating = moving = false;
    childNum = 0;

    forward  = zAxis = pos + ol_Vector3d(0.0f, 0.0f, 1.0f);
    up       = yAxis = pos + ol_Vector3d(0.0f, 1.0f, 0.0f);
    side     = xAxis = pos + ol_Vector3d(1.0f, 0.0f, 0.0f);

    SetIdentityMatrix(ModelViewMat);
    q_Res = ol_Quaternion();

    this->name = name;

    parent = NULL;

    frustumCulling = true;

    collType = collCheck = collAccept = OL_COLLISION_MASK_NONE;

    collCen = pos;
    collRad = 1;
}

ol_SceneObject::~ol_SceneObject()
{
    delete material;
#ifdef _OL_DEBUG_BUILD_
    printf("Scene Object Destructor ( ID = %d )\n", ID);
#endif // _OL_DEBUG_BUILD_
}

void ol_SceneObject::setID(int id)
{
    this->ID = id;
}
int ol_SceneObject::getID(void)
{
    return ID;
}
void ol_SceneObject::setName(const char *name)
{
    this->name = name;
}
const char* ol_SceneObject::getName(void)
{
    return name;
}
void ol_SceneObject::setMaterial(ol_Material *mat)
{
    material = mat;
}
ol_Material* ol_SceneObject:: getMaterial(void)
{
    return material;
}

void ol_SceneObject::addChild(ol_SceneObject *child)
{
    for(unsigned int i=0; i<childNum; i++)
        if(childs[i]==child) return;

    this->childs.push_back(child);
    childNum = childs.size();
}

void ol_SceneObject::setParent(ol_SceneObject *parent)
{
    this->parent = parent;
    parent->addChild(this);
    if(parent)
        parRig = pos - parent->getPosition();
}

void ol_SceneObject::setPosition(ol_Vector3d pos)
{
    this->pos = collCen = pos;
    SetPositionToMatrix(ModelViewMat, pos);
    if(parent)
        parRig = pos - parent->getPosition();
}

void ol_SceneObject::Rotate(float yaw, float pitch, float roll)
{
    rotating = true;

    this->yaw += yaw;
    this->pitch += pitch;
    this->roll += roll;
}

void ol_SceneObject::setRotation(void)
{
    q_Yaw = QuaternionFromAxisAngle(up, yaw*DEGTORAD*ol_CoreParams::fdt);
    q_Pitch = QuaternionFromAxisAngle(side, pitch*DEGTORAD*ol_CoreParams::fdt);
    q_Rol = QuaternionFromAxisAngle(forward, roll*DEGTORAD*ol_CoreParams::fdt);

    q_Res = ol_Quaternion();
    q_Res = q_Res*q_Yaw;
    q_Res = q_Res*q_Pitch;
    q_Res = q_Res*q_Rol;

    QuaternionToMatrix(q_Res, ModelViewMat);

    side    = Normalize(QuaternionMultiplyVector(&q_Res, &side));
    up      = Normalize(QuaternionMultiplyVector(&q_Res, &up));
    forward = Normalize(QuaternionMultiplyVector(&q_Res, &forward));
}

void ol_SceneObject::RotateAround(float hor_ang, float vert_ang, ol_Vector3d point)
{
    ol_Vector3d v = pos - point; // вектор полученный из направления центра вращения (вращаемый вектор)
    float dist = Magnitude(v);  // расстояние до центра вращения

    ol_Vector3d uv = Invert(Normalize(Cross(v,yAxis))); // Горизонтальная ось

    hor_ang = hor_ang * DEGTORAD * ol_CoreParams::fdt;
    vert_ang =  vert_ang * DEGTORAD * ol_CoreParams::fdt;

    /*
        Вводим ограничение отклонения объекта от вертикали, так как в вертикальном положении
        невозможно вычислить горизонтальную ось. Вектор направления совпадает с вертикальным и
        не получается вычислить векторное произведение. При данном способе получения вращения по другому нельзя.
        Вычисляем угол между вертикальной осью вращения и вращаемым вектором переводим в градусы
    */

    float al;   // угол между вращаемым вектором и вертикальной осью
    float gam;  // угол между вращаемым вектором и допустимым отклонением

    // сначала определяем направление вращения

    // отрицательное. Угол уменьшается. Объект вращается вверх
    if(vert_ang < 0)
    {
        al =  AngleBetweenVectors(v, yAxis);
        gam = al - angm;

        if(gam < fabs(vert_ang)) vert_ang = -gam;
    }
    // положительное. Угол увеличивается. Объект движется вниз
    if(vert_ang > 0)
    {
        al = PI - AngleBetweenVectors(v, yAxis);
        gam = al - angm;

        if(gam < vert_ang) vert_ang = gam;
    }

    // Вращение вокруг вертикальной и горизонтальной осей. Yaw и Pitch используются чтобы не вводить новых переменных
    q_Yaw = QuaternionFromAxisAngle(yAxis, hor_ang);
    q_Pitch = QuaternionFromAxisAngle(uv, vert_ang);

    ol_Quaternion qr = ol_Quaternion(); // результирующий кватернион q_Res использовать нельзя, так как он хранит вращения
    qr = qr*q_Yaw;
    qr = qr*q_Pitch;

    v = Normalize(QuaternionMultiplyVector(&qr,&v));
    v = v*dist;
    v = point + v;

    setPosition(v);
}

void ol_SceneObject::Move(ol_Vector3d dir)
{
    moving = true;
    moveDir = moveDir + dir;
    if(childNum>0) for(unsigned int i=0; i<childNum; i++) childs[i]->Move(dir);
}

void ol_SceneObject::GoForward(float speed)
{
    Move(forward*speed);
}

void ol_SceneObject::GoSide(float speed)
{
    Move(side*speed);
}

void ol_SceneObject::GoUp(float speed)
{
    Move(up*speed);
}

void ol_SceneObject::Update(void)
{
    if(rotating)
    {
        setRotation();
        yaw = pitch = roll = 0.0f;
        rotating = false;
    }

    if(moving)
    {
        setPosition(pos + moveDir*ol_CoreParams::fdt);
        moveDir = ol_Vector3d(0.0f, 0.0f, 0.0f);
        moving = false;
    }
}

void ol_SceneObject::PreventPassThrough(ol_Vector3d normal)
{
//                ol_Vector3d objDir = object->moveDir;
//                float angle = AngleBetweenVectors(objDir, vNormal)*RADTODEG;
//                if(angle<90) return;
//                if((angle>170)&&(angle<190))
//                {
//                    object->Move(Invert(objDir));
//                    return;
//                }
//
//                ol_Vector3d vProjected = Invert(GetProjectionVector(objDir, vNormal));
//                vProjected = vProjected*Magnitude(objDir);
//                ol_Vector3d vNewOffset = vProjected - objDir;
//                object->Move(vNewOffset);


    float angle = AngleBetweenVectors(moveDir, normal)*RADTODEG;
    if(angle<90) return;
    if((angle>170)&&(angle<190))
    {
        Move(Invert(moveDir));
        return;
    }

    ol_Vector3d vProjected = Invert(GetProjectionVector(moveDir, normal));
    vProjected = vProjected*Magnitude(moveDir);
    Move(vProjected - moveDir);
}

void ol_SceneObject::EnableMaterial(void)
{
	/*
	// added to fix absence of transparency 01.07.2017 -----------------
	glEnable(GL_ALPHA_TEST);
	glEnable(GL_BLEND);
	glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);
	//------------------------------------------------------------------
    */
    if(material->render == OL_RENDER_SOLID) glPolygonMode(GL_FRONT_AND_BACK, GL_FILL);

    if(material->render == OL_RENDER_WIREFRAME) glPolygonMode(GL_FRONT_AND_BACK, GL_LINE);

    if(material->render == OL_RENDER_SOLID_N_WIRE)
    {
        glPushAttrib(GL_ALL_ATTRIB_BITS);
        glDisable(GL_LIGHTING);
        glDisable(GL_TEXTURE_2D);
        glPolygonMode(GL_FRONT_AND_BACK, GL_LINE);
        glColor3f(1.0f, 1.0f, 1.0f);
        Draw();
        glPopAttrib();
        glPolygonMode(GL_FRONT_AND_BACK, GL_FILL);
    }

    if(material->lightning)
    {
        glEnable(GL_LIGHTING);

        glMaterialfv(GL_FRONT_AND_BACK, GL_AMBIENT, material->ambient);
        glMaterialfv(GL_FRONT_AND_BACK, GL_DIFFUSE, material->diffuse);
        glMaterialfv(GL_FRONT_AND_BACK, GL_SPECULAR, material->specular);
        glMateriali(GL_FRONT_AND_BACK, GL_SHININESS, material->shine);
    }
    else glColor4fv(material->color);

    if(material->hasTexture)
    {
        glEnable(GL_TEXTURE_2D);
        glBindTexture(GL_TEXTURE_2D, material->tex);
        glEnableClientState(GL_TEXTURE_COORD_ARRAY);
    }
    else glDisable(GL_TEXTURE_2D);
}

vector<ol_Line> ol_SceneObject::getGeomInfo(void)
{
    ol_Line lb;

    geomlines.clear();

    if(material->renderDirections)
    {
        lb.start = pos;
        lb.end = pos + forward*5;
        lb.col = ol_Color(0.0f, 0.0f, 1.0f);
        geomlines.push_back(lb);
        lb.start = pos;
        lb.end = pos + up*5;
        lb.col = ol_Color(0.0f, 1.0f, 0.0f);
        geomlines.push_back(lb);
        lb.start = pos;
        lb.end = pos + side*5;
        lb.col = ol_Color(1.0f, 0.0f, 0.0f);
        geomlines.push_back(lb);
    }

    return geomlines;
}

void ol_SceneObject::DisableMaterial(void)
{
    if(material->lightning) glDisable(GL_LIGHTING);

	glDisable(GL_BLEND);
	glDisable(GL_ALPHA_TEST);
}

void ol_SceneObject::setCollisionParams(unsigned short type, unsigned short check, unsigned short accept, float rad)
{
    collType = type;
    collCheck = check;
    collAccept = accept;
    collRad = rad;
}

/**
    --------------------------- Static Mesh Scene Object -----------------------------------
    >                                                                                      <
    >  Класс статической модели. Предназначен для отображения неподвижных моделей. Знает   <
    >  точные координаты всех вершин модели и умеет рендерить сам себя. Для перемещения    <
    >  и поворота используется непосредственное преобразование координат всех вершин       <
    >  объекта. Более эффективен для отображения статичных объектов, так как в рендеринге  <
    >  не вызывается glMultMatrix.                                                         <
    >                                                                                      <
    ----------------------------------------------------------------------------------------
**/

ol_StaticMeshSceneObject::ol_StaticMeshSceneObject(const char *name, ol_Mesh3D *mesh): ol_SceneObject(name)
{
    this->pMesh = mesh;

    setPosition(ol_Vector3d(0.0f, 0.0f, 0.0f));
}

ol_StaticMeshSceneObject::~ol_StaticMeshSceneObject()
{
    if(pMesh) delete pMesh;
#ifdef _OL_DEBUG_BUILD_
    printf("Static Mesh destructor ( ID = %d )\n", ID);
#endif // _OL_DEBUG_BUILD_
}

void ol_StaticMeshSceneObject::setPosition(ol_Vector3d newPos)
{
    ol_SceneObject::setPosition(newPos);

    ol_Vector3d offset = pos - pMesh->center;
    pMesh->center = pos;

    for(unsigned int i=0; i<pMesh->vertCount; i++)
        pMesh->pVerts[i] = pMesh->pVerts[i] + offset;
}

void ol_StaticMeshSceneObject::Draw(void)
{
    glEnableClientState(GL_VERTEX_ARRAY);
    glEnableClientState(GL_NORMAL_ARRAY);

    glVertexPointer(3, GL_FLOAT, 0, pMesh->pVerts);
    glNormalPointer(GL_FLOAT, 0, pMesh->pNormals);
    if(pMesh->texcoordCount>0) glTexCoordPointer(2, GL_FLOAT, 0, pMesh->pTexVerts);

    glDrawElements(GL_TRIANGLES, pMesh->indCount, GL_UNSIGNED_INT, pMesh->pVertInd);

    glDisableClientState(GL_VERTEX_ARRAY);
    glDisableClientState(GL_NORMAL_ARRAY);
    glDisableClientState(GL_TEXTURE_COORD_ARRAY);
}

void ol_StaticMeshSceneObject::Scale(ol_Vector3d k)
{
    unsigned int n = pMesh->vertCount;

    for(unsigned int i=0; i<n; i++)
    {
        pMesh->pVerts[i].x = pMesh->pVerts[i].x * k.x;
        pMesh->pVerts[i].y = pMesh->pVerts[i].y * k.y;
        pMesh->pVerts[i].z = pMesh->pVerts[i].z * k.z;
    }
}

ol_Mesh3D* ol_StaticMeshSceneObject::getMesh(void)
{
    return pMesh;
}

vector<ol_Line> ol_StaticMeshSceneObject::getGeomInfo(void)
{
    ol_SceneObject::getGeomInfo();

    ol_Line lb;

    // прорисовка нормалей
    if(material->renderNormals)
    {
        for(unsigned int i=0; i<pMesh->vertCount; i++)
        {
            lb.start = pMesh->pVerts[i];
            lb.end = pMesh->pVerts[i] + pMesh->pNormals[i]*3;
            lb.col = ol_Color(1.0f, 0.0f, 1.0f);
            geomlines.push_back(lb);
        }
    }

    return geomlines;
}

void ol_StaticMeshSceneObject::setRotation(void)
{
    ol_SceneObject::setRotation();

    ol_Vector3d v;
    // В одном цикле так как количество вершин и нормалей совпадает
    for(unsigned int i=0; i<pMesh->vertCount; i++)
    {
        v = pMesh->pVerts[i] - pos;
        pMesh->pVerts[i] = pos + MatrixTransformVector(v, ModelViewMat);
        pMesh->pNormals[i] = MatrixTransformVector(pMesh->pNormals[i], ModelViewMat);
    }
}

bool ol_StaticMeshSceneObject::CheckCollisionWith(ol_SceneObject *object, ol_SceneEvent *gevent)
{
    // This function is pretty much a direct rip off of SpherePolygonCollision()
    // We needed to tweak it a bit though, to handle the collision detection once
    // it was found, along with checking every triangle in the list if we collided.
    // pVertices is the world data. If we have space partitioning, we would pass in
    // the vertices that were closest to the camera. What happens in this function
    // is that we go through every triangle in the list and check if the camera's
    // sphere collided with it.  If it did, we don't stop there.  We can have
    // multiple collisions so it's important to check them all.  One a collision
    // is found, we calculate the offset to move the sphere off of the collided plane.

    if(((collType & object->collCheck) == 0 ) || ((collAccept & object->collType) == 0) || (object == this)) return false;

    ol_Vector3d m_vPosition = object->collCen;
    float m_radius = object->collRad;

    // Go through all the triangles
    for(unsigned int i = 0; i < pMesh->indCount-3; i+= 3)
    {
        // Store of the current triangle we testing
        ol_Vector3d vTriangle[3] = { pMesh->pVerts[pMesh->pVertInd[i]], pMesh->pVerts[pMesh->pVertInd[i+1]], pMesh->pVerts[pMesh->pVertInd[i+2]] };

        // 1) STEP ONE - Finding the sphere's classification

        // We want the normal to the current polygon being checked
        ol_Vector3d vNormal = Normal(vTriangle);

        // This will store the distance our sphere is from the plane
        float distance = 0.0f;

        // This is where we determine if the sphere is in FRONT, BEHIND, or INTERSECTS the plane
        int classification = ClassifySphere(m_vPosition, vNormal, vTriangle[0], m_radius, distance);

        // If the sphere intersects the polygon's plane, then we need to check further
        if((classification == OL_COLLISION_INTERSECTS))
        {
            // 2) STEP TWO - Finding the psuedo intersection point on the plane

            // Now we want to project the sphere's center onto the triangle's plane
            ol_Vector3d vOffset = vNormal * distance;

            // Once we have the offset to the plane, we just subtract it from the center
            // of the sphere.  "vIntersection" is now a point that lies on the plane of the triangle.
            ol_Vector3d vIntersection = m_vPosition - vOffset;

            // 3) STEP THREE - Check if the intersection point is inside the triangles perimeter

            // We first check if our intersection point is inside the triangle, if not,
            // the algorithm goes to step 4 where we check the sphere again the polygon's edges.

            // We do one thing different in the parameters for EdgeSphereCollision though.
            // Since we have a bulky sphere for our camera, it makes it so that we have to
            // go an extra distance to pass around a corner. This is because the edges of
            // the polygons are colliding with our peripheral view (the sides of the sphere).
            // So it looks likes we should be able to go forward, but we are stuck and considered
            // to be colliding.  To fix this, we just pass in the radius / 2.  Remember, this
            // is only for the check of the polygon's edges.  It just makes it look a bit more
            // realistic when colliding around corners.  Ideally, if we were using bounding box
            // collision, cylinder or ellipses, this wouldn't really be a problem.

            if(InsidePolygon(vIntersection, vTriangle, 3) || EdgeSphereCollision(m_vPosition, vTriangle, 3, m_radius/2))
            {
                // If we get here, we have collided!  To handle the collision detection
                // all it takes is to find how far we need to push the sphere back.
                // GetCollisionOffset() returns us that offset according to the normal,
                // radius, and current distance the center of the sphere is from the plane.
//				vOffset = GetCollisionOffset(vNormal, m_radius, distance);

                // Now that we have the offset, we want to ADD it to the position and
                // view vector in our camera.  This pushes us back off of the plane.  We
                // don't see this happening because we check collision before we render
                // the scene.
//                object->Move(vOffset);


                gevent->type = OL_SEV_COLLISION;
                gevent->object1 = object; // кто столкнулся
                gevent->object2 = this;   // с кем столкнулся
                gevent->collisionPoint = vIntersection;
                gevent->collisionNormal = vNormal;
                gevent->collisionFace[0] = vTriangle[0];
                gevent->collisionFace[1] = vTriangle[1];
                gevent->collisionFace[2] = vTriangle[2];

                return true;

            }
        }
    }

    return false;
}

/**
    ----------------------------- Dynamic Mesh Scene Object --------------------------------
    >                                                                                      <
    >  Класс динамической модели. Унаследован от статической. Для поворота и перемещения   <
    >  используется glMultMatrix. Геометрически все вершины находятся в начальной позиции  <
    >  и рендерятся средствами статичного объекта.                                         <
    >                                                                                      <
    ----------------------------------------------------------------------------------------
**/

ol_DynamicMeshSceneObject::ol_DynamicMeshSceneObject(const char *name, ol_Mesh3D *mesh): ol_StaticMeshSceneObject(name, mesh)
{}

ol_DynamicMeshSceneObject::~ol_DynamicMeshSceneObject()
{
#ifdef _OL_DEBUG_BUILD_
    printf("Dynamic Mesh destructor ( ID = %d )\n", ID);
#endif // _OL_DEBUG_BUILD_
}

void ol_DynamicMeshSceneObject::setPosition(ol_Vector3d pos)
{
    ol_SceneObject::setPosition(pos);
}

void ol_DynamicMeshSceneObject::RotateAround(float hor_ang, float vert_ang, ol_Vector3d point)
{
    ol_SceneObject::RotateAround(hor_ang, vert_ang, point);
}

void ol_DynamicMeshSceneObject::setRotation(void)
{
    q_Yaw = QuaternionFromAxisAngle(yAxis, yaw*DEGTORAD*ol_CoreParams::fdt);
    q_Pitch = QuaternionFromAxisAngle(xAxis, pitch*DEGTORAD*ol_CoreParams::fdt);
    q_Rol = QuaternionFromAxisAngle(zAxis, roll*DEGTORAD*ol_CoreParams::fdt);

    q_Res = q_Res*q_Yaw;
    q_Res = q_Res*q_Pitch;
    q_Res = q_Res*q_Rol;

    QuaternionToMatrix(q_Res, ModelViewMat);

    SetPositionToMatrix(ModelViewMat, pos);

    side    = ol_Vector3d(ModelViewMat[0], ModelViewMat[1], ModelViewMat[2]);
    up      = ol_Vector3d(ModelViewMat[4], ModelViewMat[5], ModelViewMat[6]);
    forward = ol_Vector3d(ModelViewMat[8], ModelViewMat[9], ModelViewMat[10]);
}

void ol_DynamicMeshSceneObject::GoForward(float speed)
{
    ol_SceneObject::GoForward(speed);
}

void ol_DynamicMeshSceneObject::Move(ol_Vector3d dir)
{
    ol_SceneObject::Move(dir);
}

void ol_DynamicMeshSceneObject::Draw(void)
{
    glPushMatrix();
    glMatrixMode(GL_MODELVIEW);
    glMultMatrixf(ModelViewMat);
    ol_StaticMeshSceneObject::Draw();
    glPopMatrix();
}

vector<ol_Line> ol_DynamicMeshSceneObject::getGeomInfo(void)
{
    ol_SceneObject::getGeomInfo();

    ol_Line lb;
    ol_Vector3d nt, pt;
    ol_Matrix44f mvm;

    // прорисовка нормалей
    if(material->renderNormals)
    {
        CopyMatrix(mvm, ModelViewMat);
        SetPositionToMatrix(mvm, ol_Vector3d(0.0f, 0.0f, 0.0f));

        for(unsigned int i=0; i<pMesh->vertCount; i++)
        {
            nt = pMesh->pNormals[i];
            pt = pMesh->pVerts[i];

            pt = MatrixTransformVector(pt, ModelViewMat);
            nt = Normalize(MatrixTransformVector(nt, mvm));

            lb.start = pt;
            lb.end = pt + nt;
            lb.col = ol_Color(1.0f, 0.0f, 1.0f);
            geomlines.push_back(lb);
        }
    }

    return geomlines;
}

/**
    ------------------------------- Camera Scene Objects -----------------------------------
    >                                                                                      <
    >                             Классы различных камер.                                  <
    >                                                                                      <
    ----------------------------------------------------------------------------------------
**/

ol_CameraSceneObject::ol_CameraSceneObject(ol_Vector3d pos, ol_Vector3d targ, const char *name): ol_SceneObject(name)
{
    setPosition(pos);
    setTarget(targ);
}
ol_CameraSceneObject::~ol_CameraSceneObject()
{
#ifdef _OL_DEBUG_BUILD_
    printf("Camera destructor ( ID = %d )\n", ID);
#endif // _OL_DEBUG_BUILD_
}

void ol_CameraSceneObject::setTarget(ol_Vector3d targ)
{
    this->targ = targ;
}

void ol_CameraSceneObject::Look(void)
{
    if(parent) setTarget(parent->getPosition());
    gluLookAt(pos.x, pos.y, pos.z,   targ.x, targ.y, targ.z,   0, 1, 0);

    glGetFloatv(GL_MODELVIEW_MATRIX, ModelViewMat);   // узнаём видовую матрицу.

    side    = Invert(ol_Vector3d(ModelViewMat[0], ModelViewMat[4], ModelViewMat[8]));
    up      = Invert(ol_Vector3d(ModelViewMat[1], ModelViewMat[5], ModelViewMat[9]));
    forward = Invert(ol_Vector3d(ModelViewMat[2], ModelViewMat[6], ModelViewMat[10]));
}

///-----------------------------------------------------------------------------------
ol_CameraSceneObjectRPG::ol_CameraSceneObjectRPG(ol_Vector3d pos, ol_Vector3d targ, const char *name): ol_CameraSceneObject(pos, targ, name)
{
    rotSpeed = 20;
    zoom = 40;
}
ol_CameraSceneObjectRPG::~ol_CameraSceneObjectRPG()
{
#ifdef _OL_DEBUG_BUILD_
    printf("Camera RPG destructor ( ID = %d )\n", ID);
#endif // _OL_DEBUG_BUILD_
}

void ol_CameraSceneObjectRPG::Rotate(ol_Vector2df rot)
{
    rot = rot * rotSpeed;
    ol_SceneObject::RotateAround(-rot.x, rot.y, targ);
}

void ol_CameraSceneObjectRPG::Zoom(int del)
{
    ol_Vector3d v = pos - targ;
    float dist = fabsf(Magnitude(v));
//	printf("dist = %f\n", dist);
    dist = dist + del*zoom;

/*
    // ограничивает приближение
    if(dist<0) return;
*/
    v = Normalize(v);
    v = v*dist;

    Move(targ + (v - pos));
}

void ol_CameraSceneObjectRPG::CheckEvents(ol_Event *event)
{
    if(event->getKey(OL_KEY_RBUTTON))
    {
        Rotate(event->getCurShift());
    }
    if(event->getMouseWheel())
    {
        Zoom(event->getMouseWheel());
    }
}

///-----------------------------------------------------------------------------------
ol_CameraSceneObjectFPS::ol_CameraSceneObjectFPS(ol_Vector3d pos, ol_Vector3d targ, const char *name): ol_CameraSceneObject(pos, targ, name)
{
    rotSpeed = 5.01f;
    moveSpeed = 3.3f;
    setTarget(pos + forward);
    first_rot = true;
}

ol_CameraSceneObjectFPS::~ol_CameraSceneObjectFPS()
{
#ifdef _OL_DEBUG_BUILD_
    printf("Camera destructor FPS ( ID = %d )\n", ID);
#endif // _OL_DEBUG_BUILD_
}
void ol_CameraSceneObjectFPS::Rotate(ol_Vector2df rot)
{
    if((rot.x==0)&&(rot.y==0)) return;

    float rot_x = rot.x*rotSpeed*DEGTORAD;
    float rot_y = rot.y*rotSpeed;

    // ограничиваем отклонение камеры от вертикали
    float al =  AngleBetweenVectors(forward, yAxis)*RADTODEG;
    if((al + rot_y)>175) rot_y = 175 - al;      // приближается к верхней границе
    if((al + rot_y)<5)  rot_y = 5 - al ;        // приближается к нижней границе

    rot_y*=DEGTORAD;

    ol_SceneObject::Rotate(rot_x, rot_y, 0);
}

void ol_CameraSceneObjectFPS::setRotation(void)
{
    ol_SceneObject::setRotation();
    setTarget(pos + forward);
}

void ol_CameraSceneObjectFPS::setPosition(ol_Vector3d pos)
{
    ol_SceneObject::setPosition(pos);
    setTarget(pos + forward);
}

void ol_CameraSceneObjectFPS::CheckEvents(ol_Event *event)
{
    if((event->isMouseMoving()) && (event->getKey(OL_KEY_RBUTTON)) && !first_rot)
    {
//        POINT mousePos;					// This is a window structure that holds an X and Y
//        int middleX = ol_System->centerX;			// This is a binary shift to get half the width
//        int middleY = ol_System->centerY;			// This is a binary shift to get half the height
//
//        // Get the mouse's current X,Y position
//        GetCursorPos(&mousePos);
//
//        if((mousePos.x == middleX) && (mousePos.y == middleY)) return;
//
//        // If our cursor is still in the middle, we never moved... so don't update the screen
//        if(change_m_pos)
//        {
//            mousePos.x=lastm_x;
//            mousePos.y=lastm_y;
//
//            change_m_pos = false;
//        }
//
//        // Set the mouse position to the middle of our window
//        if(change_first_pos)
//        {
//            mousePos.x=middleX;
//            mousePos.y=middleY;
//
//            change_first_pos = false;
//        }
//
//        lastm_x=mousePos.x;
//        lastm_y=mousePos.y;
//
//        SetCursorPos(middleX, middleY);


        Rotate(event->getCurShift());
//        event->mParams->ResetCursor();
//        event->mParams->CenterMouseCursor();

    }

    if((event->keys->keyDown[OL_KEY_W]))    ol_SceneObject::GoForward(moveSpeed);
    if((event->keys->keyDown[OL_KEY_S]))    ol_SceneObject::GoForward(-moveSpeed);
    if((event->keys->keyDown[OL_KEY_A]))    ol_SceneObject::GoSide(moveSpeed);
    if((event->keys->keyDown[OL_KEY_D]))    ol_SceneObject::GoSide(-moveSpeed);
}

/**
    ----------------------------------- Sky Box --------------------------------------------
    >                                                                                      <
    >  Куб, на который наложены текстуры панорамы. Создает видимость окружающего           <
    >  пространства. Всегда движется вместе с камерой. Координаты центра совпадают с       <
    >  координатами камеры.                                                                <
    >                                                                                      <
    ----------------------------------------------------------------------------------------
**/

ol_SkyBoxSceneObject::ol_SkyBoxSceneObject(const char *name, ol_Mesh3D *mesh): ol_DynamicMeshSceneObject(name, mesh)
{
    frustumCulling = false;
}

ol_SkyBoxSceneObject::~ol_SkyBoxSceneObject()
{
#ifdef _OL_DEBUG_BUILD_
    printf("Sky Box destructor ( ID = %d )\n", ID);
#endif // _OL_DEBUG_BUILD_
}

/**
    ------------------------------ Terrain Scene object ------------------------------------
    >                                                                                      <
    >             Ландшафт. Статический объект, создаваемый из карты высот.                <
    >                                                                                      <
    ----------------------------------------------------------------------------------------
**/

ol_TerrainSceneObject::ol_TerrainSceneObject(ol_Graphics *graph, ol_Mesh3D *mesh, const char* fileName, unsigned int step, float horScale, float vertScale, unsigned int smooth, const char *name): ol_StaticMeshSceneObject(name, mesh)
{
    frustumCulling = false;
    this->horScale = horScale;
    this->vertScale = vertScale;
    this->step = step;
    this->smooth = smooth;
    ol_TextureImage *tex = graph->LoadImage(fileName);

    if(tex!=NULL)
    {
        BuildTerrainMesh(tex);
        cout<<"Height Map loaded: "<<fileName<<"\n";
        delete tex;
    }
    else
    {
        cout<<"Error loading Height Map!\n";
        delete pMesh;
    }
}

ol_TerrainSceneObject::~ol_TerrainSceneObject()
{
#ifdef _OL_DEBUG_BUILD_
    printf("Terrain destructor ( ID = %d )\n", ID);
#endif // _OL_DEBUG_BUILD_
}

GLuint ol_TerrainSceneObject::CalcIndex(GLuint width, GLuint x, GLuint y)
{
    return x+y*width;
}

int ol_TerrainSceneObject::Height(ol_TextureImage *tex, unsigned int x, unsigned int y)
{
    unsigned int step = tex->bpp/8; // количество значений (байт) хранящих инфу о высоте
    unsigned int height = 0;

    for(unsigned int i=0; i<step; i++)
        height+= tex->imageData[CalcIndex(tex->width, x*step+i, y*step)];  // Возвращаем значение высоты

    height/=step;

    return height;
}

void ol_TerrainSceneObject::BuildTerrainMesh(ol_TextureImage *tex)
{
	ol_Vector3d tmp;				// Vertex buffer.
	ol_Vector2df tmt;				// Texture coordinate buffer.
	vector<ol_Vector3d> points;		// Temporary vertexes array.
	vector<ol_Vector2df> texcoords;	// Temporary UV coordinates array.
	vector<GLuint> inds;			// Temporary face indexes array.
	GLuint tind;

	float sx = (float)8/tex->width;
	float sy = (float)8/tex->height;

	// Generate vertexes
	for (unsigned int y = 0; y<tex->height; y+=step )
		for (unsigned int x = 0; x<tex->width; x+=step )
		{
			// Calculate vertex coordinates.
			tmp.x = x*horScale;						//x
			tmp.y = Height(tex, x, y)*vertScale;	//y
			tmp.z = y*horScale;						//z
			// Calculate UV coordinates.
			tmt.x = x*sx;
			tmt.y = y*sy;

			points.push_back(tmp);
			texcoords.push_back(tmt);
		}

	// Smoothing geometry.
	unsigned int size = sqrt(points.size());
	for(unsigned int k=1; k<=smooth; k++)
	{
		for(unsigned int i=0; i<points.size()-3; i++)
			points[i+1].y = (points[i].y + points[i+1].y + points[i+2].y)/3;

		for(unsigned int i=0; i<size; i++)
			for(unsigned int j=0; j<size-3; j++)
			{
				unsigned int ind0 = CalcIndex(size, i, j);
				unsigned int ind1 = CalcIndex(size, i, j+1);
				unsigned int ind2 = CalcIndex(size, i, j+2);
				points[ind1].y = (points[ind0].y + points[ind1].y + points[ind2].y)/3;
			}
	}

	// Generate face indexes.
	for (unsigned int y = 1; y<size; y++ )
		for ( unsigned int x = 0; x<size-1; x++ )
		{
			// First face.
			tind = CalcIndex(size, x, y);   inds.push_back(tind);
			tind = CalcIndex(size, x+1, y); inds.push_back(tind);
			tind = CalcIndex(size, x, y-1); inds.push_back(tind);
			// Second face.
			tind = CalcIndex(size, x+1, y);   inds.push_back(tind);
			tind = CalcIndex(size, x+1, y-1); inds.push_back(tind);
			tind = CalcIndex(size, x, y-1);   inds.push_back(tind);
		}

	pMesh->InitVerts(points);
	pMesh->InitTexCoords(texcoords);
	pMesh->InitIndexes(inds);
	pMesh->CalcNormals();
	pMesh->CalcCenter();
}

/**
    ------------------------------------- Lights -------------------------------------------
    >                                                                                      <
    >        GL-ный источник света. Знает все свои параметры и умеет вкл/выкл.             <
    >                                                                                      <
    ----------------------------------------------------------------------------------------
**/

void ol_LightSceneObject::Light(void)
{
    glLightfv(lightNum, GL_POSITION, position);
    glLightfv(lightNum, GL_AMBIENT, ambient);
    glLightfv(lightNum, GL_DIFFUSE, diffuse);
    glLightfv(lightNum, GL_SPECULAR, specular);

    glEnable(lightNum);
}

bool ol_LightSceneObject::setLightNumber(GLubyte num)
{
    switch (num)
    {
    case 1:
        lightNum = GL_LIGHT0;
        break;
    case 2:
        lightNum = GL_LIGHT1;
        break;
    case 3:
        lightNum = GL_LIGHT2;
        break;
    case 4:
        lightNum = GL_LIGHT3;
        break;
    case 5:
        lightNum = GL_LIGHT4;
        break;
    case 6:
        lightNum = GL_LIGHT5;
        break;
    case 7:
        lightNum = GL_LIGHT6;
        break;
    case 8:
        lightNum = GL_LIGHT7;
        break;
    default:
        return false;
    }
    return true;
}

/**
    -------------------------------- Scene Manager -----------------------------------------
    >                                                                                      <
    >  Класс, который управляет созданием, удалением, отображением и обработкой событий    <
    >  всех объектов на сцене.                                                             <
    >                                                                                      <
    ----------------------------------------------------------------------------------------
**/

ol_SceneManager::ol_SceneManager(ol_Graphics *graph)
{
    this->graph = graph;
    activeCamera = -1;
    objectsCount = 0;
    lastEvent = 0;
}
ol_SceneManager::~ol_SceneManager()
{
    for(unsigned int i=0; i<objects.size(); i++) delete objects[i];
    objects.clear();

    for(unsigned int i=0; i<lights.size(); i++) delete lights[i];
    lights.clear();

    for(unsigned int i=0; i<events.size(); i++) delete events[i];
    events.clear();

    cameras.clear();
}

unsigned int ol_SceneManager::getRenderObjCount(void)
{
    return objectsRendered;
}
unsigned int ol_SceneManager::getObjCount(void)
{
    return objectsCount;
}

void ol_SceneManager::addObject(ol_SceneObject *object)
{
    ol_Material *mat = graph->CreateDefaultMaterial();

    if((!object)||(!mat))
    {
        cout<<"Failed to add object\n";
        return;
    }

    object->setID(objectsCount);
    object->setMaterial(mat);

    objects.push_back(object);
    objectsCount++;
}

ol_LightSceneObject* ol_SceneManager::addLightSceneObject(ol_Color amb, ol_Color diff, ol_Color spec, ol_Vector3d pos)
{
    if(lights.size()<=8)
    {
        ol_LightSceneObject *light = new ol_LightSceneObject(amb, diff, spec, pos);
        lights.push_back(light);
        if(!light->setLightNumber(lights.size()+1)) cout<<"Error setting light number\n";

        return light;
    }
    else
    {
        cout<<"Error: Can Not Add More Than 8 Light Objects!\n";
        return NULL;
    }
}

ol_SceneObject* ol_SceneManager::addSceneObject(const char *name)
{
    ol_SceneObject *sceneObject = new ol_SceneObject(name);
    addObject(sceneObject);

    return sceneObject;
}

ol_StaticMeshSceneObject* ol_SceneManager::addStaticMeshSceneObject(ol_Mesh3D *mesh, const char *name)
{
    if(mesh)
    {
        ol_StaticMeshSceneObject *staticMesh = new ol_StaticMeshSceneObject(name, mesh);
        addObject(staticMesh);

        return staticMesh;
    }
    else
    {
        cout<<"Failed to load model:\n";
        return NULL;
    }
}

ol_DynamicMeshSceneObject* ol_SceneManager::addDynamicMeshSceneObject(ol_Mesh3D *mesh, const char *name)
{
    if(mesh)
    {
        ol_DynamicMeshSceneObject *dynamicMesh = new ol_DynamicMeshSceneObject(name, mesh);
        addObject(dynamicMesh);

        return dynamicMesh;
    }
    else
    {
        cout<<"Failed to load model:\n";
        return NULL;
    }
}

ol_TerrainSceneObject* ol_SceneManager::addTerrainSceneObject(const char *fileName, unsigned int step, float horScale, float vertScale, float smooth, const char *name)
{
    ol_Mesh3D *mesh = new ol_Mesh3D();

    ol_TerrainSceneObject *terr = new ol_TerrainSceneObject(graph, mesh, fileName, step, horScale, vertScale, smooth, name);
    addObject(terr);

    return terr;
}

ol_SkyBoxSceneObject* ol_SceneManager::addSkyBoxSceneObject(GLuint texture, const char *name)
{
    ol_ShapeGenerator shapeGen;
    ol_Mesh3D *mesh = shapeGen.CreateSBCube(10000.0f);

    ol_SkyBoxSceneObject* sky = new ol_SkyBoxSceneObject(name, mesh);
    addObject(sky);
    sky->getMaterial()->setMaterialParam(OL_MATERIAL_TEXTURE, texture);
    if(cameras.size()==0)
    {
		printf("Error: No camera has been added. Add a camera before the Sky Box\n");
		return NULL;
	}
    sky->setPosition(cameras[activeCamera]->getPosition());
    sky->setParent(cameras[activeCamera]);

    return sky;
}

ol_CameraSceneObject* ol_SceneManager::addCameraSceneObject(ol_Vector3d pos, ol_Vector3d targ, const char *name)
{
    ol_CameraSceneObject* cam = new ol_CameraSceneObject(pos, targ, name);

    cameras.push_back(cam);
    activeCamera = cameras.size() -1;
    addObject(cam);

    return cam;
}

ol_CameraSceneObject* ol_SceneManager::addCameraSceneObjectRPG(ol_Vector3d pos, ol_Vector3d targ, const char *name)
{
    ol_CameraSceneObject* cam = new ol_CameraSceneObjectRPG(pos, targ, name);

    cameras.push_back(cam);
    activeCamera = cameras.size() -1;
    addObject(cam);

    return cam;
}

ol_CameraSceneObject* ol_SceneManager::addCameraSceneObjectFPS(ol_Vector3d pos, ol_Vector3d targ, const char *name)
{
    ol_CameraSceneObject* cam = new ol_CameraSceneObjectFPS(pos, targ, name);

    cameras.push_back(cam);
    activeCamera = cameras.size() -1;
    addObject(cam);

    return cam;
}

void ol_SceneManager::CalcRayFromCursor(int mouse_x, int mouse_y, ol_Vector3d &p1, ol_Vector3d &p2)
{
    /*
        mouse_x, mouse_y  - оконные координаты курсора мыши.
        p1, p2            - возвращаемые параметры - концы селектирующего отрезка,
                            лежащие соответственно на ближней и дальней плоскостях отсечения.
    */

    GLint    viewport[4];       // параметры viewport-a.
    GLdouble projection[16];    // матрица проекции.
    GLdouble modelview[16];     // видовая матрица.
    GLdouble vx, vy = 0.0, vz;  // координаты курсора мыши в системе координат viewport-a.
    GLdouble wx, wy, wz;        // возвращаемые мировые координаты.

    glGetIntegerv(GL_VIEWPORT,viewport);           // узнаём параметры viewport-a.
    glGetDoublev(GL_PROJECTION_MATRIX,projection); // узнаём матрицу проекции.
    glGetDoublev(GL_MODELVIEW_MATRIX,modelview);   // узнаём видовую матрицу.

    // переводим оконные координаты курсора в систему координат viewport-a.
    vx = mouse_x;
    // vy = ol_System->height - mouse_y - 1; // где height - текущая высота окна.

    // вычисляем ближний конец селектирующего отрезка.
    vz = -1;
    gluUnProject(vx, vy, vz, modelview, projection, viewport, &wx, &wy, &wz);
    p1 = ol_Vector3d(wx,wy,wz);

    // вычисляем дальний конец селектирующего отрезка.
    vz = 1;
    gluUnProject(vx, vy, vz, modelview, projection, viewport, &wx, &wy, &wz);
    p2 = ol_Vector3d(wx,wy,wz);
}

bool ol_SceneManager::exportSceneObject(ol_SceneObject *object, char *fileName)
{
    return true;
}

ol_CameraSceneObject* ol_SceneManager::getActiveCamera(void)
{
    if(activeCamera>=0) return cameras[activeCamera];
    else return NULL;
}

void ol_SceneManager::addEvent(ol_SceneEvent *event)
{
    events.push_back(event);
    lastEvent ++;
}

ol_SceneEvent* ol_SceneManager::getLastEvent(void)
{
    lastEvent --;
    if(lastEvent < 0) return NULL;

    return events[lastEvent];
}

void ol_SceneManager::ClearSceneEventStack(void)
{
    lastEvent = 0;
    unsigned int n = events.size();

    for(unsigned int i=0; i<n; i++)
        delete events[i];

    events.clear();
}

void ol_SceneManager::CheckSceneEvents(ol_Event *event)
{
    ClearSceneEventStack();
    if(objectsCount<=0) return;

    for(GLuint i=0; i<objectsCount; i++)
        objects[i]->CheckEvents(event);

    for(GLuint i=0; i<objectsCount; i++)
        for(GLuint j=0; j<objectsCount; j++)
        {
            ol_SceneEvent *gameEvent = new ol_SceneEvent();
            if(objects[i]->CheckCollisionWith(objects[j], gameEvent))
                addEvent(gameEvent);
            else delete gameEvent;
        }
}

void ol_SceneManager::UpdateAll(void)
{
    if(objectsCount<=0) return;

    for(GLuint i=0; i<objectsCount; i++)
        objects[i]->Update();
}

void ol_SceneManager::DrawAll(void)
{
    if(activeCamera>=0) cameras[activeCamera]->Look();
    Frustum.CalculateFrustum();

    if(lights.size()>0)
        for(GLuint i=0; i<lights.size(); i++)
            lights[i]->Light();

    if(objectsCount>0)
    {
        objectsRendered = 0;

        for(GLuint i=0; i<objectsCount; i++)
        {
            if(objects[i]->isFrustumCulling())
            {
                if(!Frustum.PointInFrustum(objects[i]->getPosition()))
                    continue;
            }

            objects[i]->EnableMaterial();
            objects[i]->Draw();
            objects[i]->DisableMaterial();
            graph->DrawLines3D(objects[i]->getGeomInfo());

            objectsRendered++;
        }
    }
}
