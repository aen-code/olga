/**
------------------------------------------------------------------------
  This file is part of the OLGA project.
  Copyright © 2017 by Enver Akhmedov.
  E-mail: aen-code@yandex.ru
  For conditions of distribution and use, see copyright notice in OLGA.h
------------------------------------------------------------------------
**/

#ifndef OL_VECTOR_MATH_H_INCLUDED
#define OL_VECTOR_MATH_H_INCLUDED

#include <math.h>
#include <string.h>

#define PI 3.1415926535897932 // The PI constant
#define DEGTORAD (PI/180)
#define RADTODEG (180/PI)
#define PI_HALF  (PI/2)

#define OL_COLLISION_BEHIND	0
#define OL_COLLISION_INTERSECTS	1
#define OL_COLLISION_FRONT	2

// 4x4 matrix - column major. X vector is 0, 1, 2, etc.
//	0	4	8	12
//	1	5	9	13
//	2	6	10	14
//	3	7	11	15
typedef float ol_Matrix44f[16]; // A 4 X 4 matrix, column major (floats) - OpenGL style

typedef int ol_Matrix22i[4];	// A 2 X 2 matrix

// Положение точки относительно отрезка
enum ol_Point2dRelPos {OL_POINT_REL_POS_LEFT, OL_POINT_REL_POS_RIGHT, OL_POINT_REL_POS_BEYOND, OL_POINT_REL_POS_BEHIND, OL_POINT_REL_POS_BETWEEN, OL_POINT_REL_POS_ORIGIN, OL_POINT_REL_POS_DESTINATION};

/**
    --------------------------------- ol_Vector2d -------------------------------------------
    >                                                                                      <
    >                   Класс двумерной точки или вектора.                                 <
    >   Вообще, можно было не создавать 2 почти одинаковых класса отличающихся лишь        <
    >   количеством координат. Вместо этого можно было бы использовать темплейты, а        <
    >   координаты хранить в массиве, тогда 2д или 3д  можно было бы задавать с помощью    <
    >   параметра. Но я слишком сильно уже привязал код к классу ol_Vector3d.               <
    >                                                                                      <
    ----------------------------------------------------------------------------------------
**/

template<class T>
class ol_Vector2d
{
public:
    ol_Vector2d(void): x(0), y(0) {}
    ol_Vector2d(const ol_Vector2d &v): x(v.x), y(v.y) {}
    ol_Vector2d(const ol_Vector2d &p0, const ol_Vector2d &p1)
    {
        x = p1.x - p0.x;
        y = p1.y - p0.y;
    }
    ol_Vector2d(T x, T y)
    {
        this->x = x;
        this->y = y;
    }
    ol_Vector2d& operator= (ol_Vector2d v)
    {
        x = v.x;
        y = v.y;

        return *this;
    }
    bool operator==(ol_Vector2d v)
    {
        if(x==v.x && y==v.y) return true;
        else return false;
    }
    ol_Vector2d operator+(ol_Vector2d v)
    {
        return ol_Vector2d(v.x + x, v.y + y);
    }
    ol_Vector2d operator-(ol_Vector2d v)
    {
        return ol_Vector2d(x - v.x, y - v.y);
    }
    ol_Vector2d operator*(T num)
    {
        return ol_Vector2d(x * num, y * num);
    }
    ol_Vector2d operator/(T num)
    {
        return ol_Vector2d(x / num, y / num);
    }
    bool operator!=(ol_Vector2d v)
    {
        if(x != v.x || y != v.y) return true;
        else return false;
    }
    //Операции "<" и ">" реализуют лексикографический порядок отношений, когда считается,
    //что точка а меньше точки b, если либо а.х < b.х, либо a.x = b.х и а.у < b.у.
    bool operator< (ol_Vector2d &p)
    {
        return ((x < p.x) || ((x == p.x) && (y<p.y)));
    }
    bool operator> (ol_Vector2d &p)
    {
        return ((x>p.x) || ((x == p.x) && (y > p.y)));
    }
    // возвращает тру, если данная точка левее и ниже точки v
    bool isBelow(ol_Vector2d v)
    {
        if((x < v.x)&&(y < v.y)) return true;
        else return false;
    }
    // возвращает тру, если данная точке правее и выше точки v
    bool isAbove(ol_Vector2d v)
    {
        if((x > v.x)&&(y > v.y)) return true;
        else return false;
    }
    void Invert(void)
    {
        x = -x;
        y = -y;
    }
    // Относительное положение точки и прямой линии
    int Classify(ol_Vector2d &p0, ol_Vector2d &p1)
    {
        ol_Vector2d p2 = *this;
        ol_Vector2d a = p1 - p0;
        ol_Vector2d b = p2 - p0;
        double sa = a. x * b.y - b.x * a.y;
        if (sa > 0.0)
            return OL_POINT_REL_POS_LEFT;
        if (sa < 0.0)
            return OL_POINT_REL_POS_RIGHT;
        if ((a.x * b.x < 0.0) || (a.y * b.y < 0.0))
            return OL_POINT_REL_POS_BEHIND;
        if (a.length() < b.length())
            return OL_POINT_REL_POS_BEYOND;
        if (p0 == p2)
            return OL_POINT_REL_POS_ORIGIN;
        if (p1 == p2)
            return OL_POINT_REL_POS_DESTINATION;
        return OL_POINT_REL_POS_BETWEEN;
    }
    // Сравнивает данный вектор с Min, и если значения х или у Min меньше чем у данного вектора,
    // то значения данного вектора заменяются значениями Min
    void CompareMin(ol_Vector2d &Min)
    {
        if(Min.x < x) x = Min.x;
        if(Min.y < y) y = Min.y;
    }
    // То же самое, только для максимума. Выбираются наибольшие значения
    void CompareMax(ol_Vector2d &Max)
    {
        if(Max.x > x) x = Max.x;
        if(Max.y > y) y = Max.y;
    }

    float Length(void)
    {
        return (float) sqrt(x*x + y*y);
    }

    void Normalize(void)
    {
        T l = Length();

        x/=l;
        y/=l;
    }

    ol_Vector2d getNormal(void)
    {
        ol_Vector2d res(-y, x);
        res.Normalize();

        return res;
    }

    T x;
    T y;
};

typedef ol_Vector2d<int> ol_Vector2di;
typedef ol_Vector2d<float> ol_Vector2df;
typedef ol_Vector2d<double> ol_Vector2dd;
typedef ol_Vector2d<long double> ol_Vector2dld;

//Функция orientation возвращает значение 1, если обрабатываемые три точки ориентированы положительно,
// -1, если они ориентированы отрицательно, или 0, если они коллинеарны.
template<class T>
int PointOrientation2d(ol_Vector2d<T> &p0, ol_Vector2d<T> &p1, ol_Vector2d<T> &p2);
// скалярное произведение двух двумерных векторов
float Dot(ol_Vector2df &v1, ol_Vector2df &v2);
// вычисляет расстояние от точки p до отрезка представленного вершинами p0, p1.
// А также, возвращает ближайшую точку на отрезке (closest_point) и её положение на отрезке(rel_pos):
// левая вершина отрезка, правая или посередине
float DistanceToSegment(ol_Vector2df &p, ol_Vector2df &p0, ol_Vector2df &p1, ol_Vector2df &closest_point, ol_Point2dRelPos &rel_pos);
// вычисляет отраженный вектор на основе нормали
ol_Vector2df ReflectionVector(ol_Vector2df &v, ol_Vector2df &n, float &elasticity);

/**
    --------------------------------- ol_Vector3d -------------------------------------------
    >                                                                                      <
    >                    Класс трехмерной точки или вектора.                               <
    >                                                                                      <
    ----------------------------------------------------------------------------------------
**/

// This is our basic 3D point/vector class
class ol_Vector3d
{
public:

    // A default constructor
    ol_Vector3d()
    {
        x = 0.0f;
        y = 0.0f;
        z = 0.0f;
    }

    // This is our constructor that allows us to initialize our data upon creating an instance
    ol_Vector3d(float x, float y, float z)
    {
        this->x = x;
        this->y = y;
        this->z = z;
    }

    // operator= sets values of v to this Vector3D. example: v1 = v2 means that values of v2 are set onto v1
    ol_Vector3d& operator= (ol_Vector3d v)
    {
        x = v.x;
        y = v.y;
        z = v.z;
        return *this;
    }

    // Here we overload the + operator so we can add vectors together
    ol_Vector3d operator+(ol_Vector3d vVector)
    {
        // Return the added vectors result.
        return ol_Vector3d(vVector.x + x, vVector.y + y, vVector.z + z);
    }

    // Here we overload the - operator so we can subtract vectors
    ol_Vector3d operator-(ol_Vector3d vVector)
    {
        // Return the subtracted vectors result
        return ol_Vector3d(x - vVector.x, y - vVector.y, z - vVector.z);
    }

    // Here we overload the * operator so we can multiply by scalars
    ol_Vector3d operator*(float num)
    {
        // Return the scaled vector
        return ol_Vector3d(x * num, y * num, z * num);
    }

    // Here we overload the / operator so we can divide by a scalar
    ol_Vector3d operator/(float num)
    {
        // Return the scale vector
        return ol_Vector3d(x / num, y / num, z / num);
    }

    // Here we overload the == operator so we can check weather vectors are equal o not
    bool operator==(ol_Vector3d vVector)
    {
        if(x==vVector.x && y==vVector.y && z==vVector.z) return true;
        else return false;
    }

    // The same as before, but returns true when vectors are not equal
    bool operator!=(ol_Vector3d vVector)
    {
        if(x!=vVector.x || y!=vVector.y || z!=vVector.z) return true;
        else return false;
    }

    float x, y, z;
};


// This returns the absolute value of "num"
float Absolute(float num);

// This inverts vector x=-x, y=-y, z=-z
ol_Vector3d Invert(ol_Vector3d vVector);

//	This returns a perpendicular vector from 2 given vectors by taking the cross product.
ol_Vector3d Cross(ol_Vector3d vVector1, ol_Vector3d vVector2);

//	This returns the magnitude of a normal (or any other vector)
float Magnitude(ol_Vector3d vNormal);

//	This returns a normalize vector (A vector exactly of length 1)
ol_Vector3d Normalize(ol_Vector3d vNormal);

//	This returns the normal of a polygon (The direction the polygon is facing)
ol_Vector3d Normal(ol_Vector3d vPolygon[]);

// This returns the distance between 2 3D points
float Distance(ol_Vector3d vPoint1, ol_Vector3d vPoint2);

// This returns the point on the line segment vA_vB that is closest to point vPoint
ol_Vector3d ClosestPointOnLine(ol_Vector3d vA, ol_Vector3d vB, ol_Vector3d vPoint);

// This returns the distance the plane is from the origin (0, 0, 0)
// It takes the normal to the plane, along with ANy point that lies on the plane (any corner)
float PlaneDistance(ol_Vector3d Normal, ol_Vector3d Point);

// This takes a triangle (plane) and line and returns true if they intersected
bool IntersectedPlane(ol_Vector3d vPoly[], ol_Vector3d vLine[], ol_Vector3d &vNormal, float &originDistance);

// This returns the dot product between 2 vectors
float Dot(ol_Vector3d vVector1, ol_Vector3d vVector2);

// This returns the angle between 2 vectors
double AngleBetweenVectors(ol_Vector3d Vector1, ol_Vector3d Vector2);

// This returns an intersection point of a polygon and a line (assuming intersects the plane)
ol_Vector3d IntersectionPoint(ol_Vector3d vNormal, ol_Vector3d vLine[], double distance);

// This returns true if the intersection point is inside of the polygon
bool InsidePolygon(ol_Vector3d vIntersection, ol_Vector3d Poly[], long verticeCount);

// Use this function to test collision between a line and polygon
bool IntersectedPolygon(ol_Vector3d vPoly[], ol_Vector3d vLine[], int verticeCount);

// This function classifies a sphere according to a plane. (BEHIND, in FRONT, or INTERSECTS)
int ClassifySphere(ol_Vector3d &vCenter, ol_Vector3d &vNormal, ol_Vector3d &vPoint, float radius, float &distance);

// This takes in the sphere center, radius, polygon vertices and vertex count.
// This function is only called if the intersection point failed.  The sphere
// could still possibly be intersecting the polygon, but on it's edges.
bool EdgeSphereCollision(ol_Vector3d &vCenter, ol_Vector3d vPolygon[], int vertexCount, float radius);

// This returns true if the sphere is intersecting with the polygon.
bool SpherePolygonCollision(ol_Vector3d vPolygon[], ol_Vector3d &vCenter, int vertexCount, float radius);

// This returns the offset the sphere needs to move in order to not intersect the plane
ol_Vector3d GetCollisionOffset(ol_Vector3d &vNormal, float radius, float distance);

// Находит проекцию единичной длины вектора v на плоскость, где norm - нормаль к плоскости.
ol_Vector3d GetProjectionVector(ol_Vector3d v, ol_Vector3d norm);

// Инициализирует матрицу mat Идендичной матрицей
void SetIdentityMatrix(ol_Matrix44f mat);

// Заполняет матрицу dst значениями матрицы src
inline void CopyMatrix(ol_Matrix44f dst, ol_Matrix44f src)
{
    memcpy(dst, src, sizeof(ol_Matrix44f));
}

// Записывает x,y,z координаты вектора pos в соответствующие ячейки матрицы модели-вида (mat)
void SetPositionToMatrix(ol_Matrix44f mat, ol_Vector3d pos);

// Осуществляет одновременное вращение и перенос вершины v путем умножения на матрицу m
inline ol_Vector3d MatrixTransformVector(const ol_Vector3d v, const ol_Matrix44f m)
{
    ol_Vector3d vOut;

    vOut.x = m[0] * v.x + m[4] * v.y + m[8] *  v.z + m[12];
    vOut.y = m[1] * v.x + m[5] * v.y + m[9] *  v.z + m[13];
    vOut.z = m[2] * v.x + m[6] * v.y + m[10] * v.z + m[14];

    return vOut;
}


#endif // OL_VECTOR_MATH_H_INCLUDED
