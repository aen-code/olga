/**
------------------------------------------------------------------------
  This file is part of the OLGA project.
  Copyright © 2017 by Enver Akhmedov.
  E-mail: aen-code@yandex.ru
  For conditions of distribution and use, see copyright notice in OLGA.h
------------------------------------------------------------------------
**/

#include "ol_Basic_Shapes.h"

void ol_ShapeGenerator::Refresh(void)
{
    points.clear();
    faces.clear();
    texcoords.clear();
}

void ol_ShapeGenerator::InitMesh(ol_Mesh3D *pMesh)
{
    pMesh->InitVerts(points);
    pMesh->InitFaces(faces);
    pMesh->InitTexCoords(texcoords);
    pMesh->CalcNormals();
    pMesh->InitVertInd();
    pMesh->CalcCenter();
    pMesh->CleanUp();
}

ol_Mesh3D* ol_ShapeGenerator::CreateQuad3D(float side)
{
    ol_Mesh3D *pMesh = new ol_Mesh3D();
    Refresh();

    float half = side/2;

    // генерируем вершины
    tmp.x = half;
    tmp.z = -half;
    tmp.y = 0.0f;
    points.push_back(tmp);
    tmp.x = -half;
    tmp.z = -half;
    tmp.y = 0.0f;
    points.push_back(tmp);
    tmp.x = -half;
    tmp.z = half;
    tmp.y = 0.0f;
    points.push_back(tmp);
    tmp.x = half;
    tmp.z = half;
    tmp.y = 0.0f;
    points.push_back(tmp);

    //генерируем текстурные координаты
    tmt.x = 0;
    tmt.y = 0;
    texcoords.push_back(tmt);
    tmt.x = 0;
    tmt.y = 1;
    texcoords.push_back(tmt);
    tmt.x = 1;
    tmt.y = 1;
    texcoords.push_back(tmt);
    tmt.x = 1;
    tmt.y = 0;
    texcoords.push_back(tmt);

    // Генерируем полигоны
    // первый полигон
    tmf.vertIndex[0] = tmf.coordIndex[0] = 0;
    tmf.vertIndex[1] = tmf.coordIndex[1] = 1;
    tmf.vertIndex[2] = tmf.coordIndex[2] = 2;
    faces.push_back(tmf);

    // второй полигон
    tmf.vertIndex[0] = tmf.coordIndex[0] = 0;
    tmf.vertIndex[1] = tmf.coordIndex[1] = 2;
    tmf.vertIndex[2] = tmf.coordIndex[2] = 3;
    faces.push_back(tmf);

    InitMesh(pMesh);
    Refresh();

    return pMesh;
}
ol_Mesh3D* ol_ShapeGenerator::CreateSBCube(float side)
{
	ol_Mesh3D *pMesh = new ol_Mesh3D();
	vector<ol_Quad2d*> frames;
	ol_TriangleMesh bufMesh;
	ol_Vertex bufVert;
	Refresh();
	float h = side/2.0f;
	
	/* Generating texture coordinates. We need to cut single texture 
	 * into 6 frames: left, front, right, back, top, bottom. Using
	 * the same technique as in ol_Animation class.*/
	float kw = 1.0f/3.0f; // 3 frames horizontally. 
	float kh = 1.0f/2.0f; // 2 frames vertically.

	for(int j=0; j<2; j++)
		for(int i=0; i<3; i++)
		{
			ol_Quad2d *rect = new ol_Quad2d();

			rect->botLeft.y = rect->botRight.y = j*kh;
			rect->topLeft.y = rect->topRight.y = (j+1)*kh;

			rect->botLeft.x = rect->topLeft.x = i*kw;
			rect->botRight.x = rect->topRight.x = (i+1)*kw;

			frames.push_back(rect);
		}
	/* Generating vertex array. Enumeration is clockwise.
	 * 0-3 is the bottom quad, 4-7 is the top quad.
	 * Bottom, top, left, right, front and back are the sides of the sky box.
	 * Y axis goes inside the monitor.
	 *            
	 *            front
	 *            z(axis) 
	 *    (5)1----|----2(6)
	 *       |    |    |
	 * left  |top 0----|---x(axis) right
	 *       |bottom   |
	 *    (4)0---------3(7)
	 *           back
	 * */

	tmp.x = -h; tmp.z = -h; tmp.y = -h; points.push_back(tmp); // 0
	tmp.x = -h; tmp.z =  h; tmp.y = -h; points.push_back(tmp); // 1
	tmp.x =  h; tmp.z =  h; tmp.y = -h; points.push_back(tmp); // 2
	tmp.x =  h; tmp.z = -h; tmp.y = -h; points.push_back(tmp); // 3
	tmp.x = -h; tmp.z = -h; tmp.y =  h; points.push_back(tmp); // 4
	tmp.x = -h; tmp.z =  h; tmp.y =  h; points.push_back(tmp); // 5
	tmp.x =  h; tmp.z =  h; tmp.y =  h; points.push_back(tmp); // 6
	tmp.x =  h; tmp.z = -h; tmp.y =  h; points.push_back(tmp); // 7

    /* Generating triangle faces array. 
     * 2 faces for each side of the cube. 12 faces totally. 
     * bottom(0,1), top(2,3), left(4,5), front(6,7), right(8,9), back(10,11)
     * */
    // 0 face  bottom --------------------------------------------------
    bufVert.vert = points[1]; bufVert.tex = frames[0]->topLeft;  bufMesh.AddVertex(bufVert);
    bufVert.vert = points[0]; bufVert.tex = frames[0]->topRight;  bufMesh.AddVertex(bufVert);
    bufVert.vert = points[3]; bufVert.tex = frames[0]->botRight; bufMesh.AddVertex(bufVert);
    // 1 face
    bufVert.vert = points[1]; bufVert.tex = frames[0]->topLeft;  bufMesh.AddVertex(bufVert);
    bufVert.vert = points[3]; bufVert.tex = frames[0]->botRight; bufMesh.AddVertex(bufVert);
    bufVert.vert = points[2]; bufVert.tex = frames[0]->botLeft; bufMesh.AddVertex(bufVert);
    // 2 face top ------------------------------------------------------
    bufVert.vert = points[4]; bufVert.tex = frames[1]->botRight;  bufMesh.AddVertex(bufVert);
    bufVert.vert = points[5]; bufVert.tex = frames[1]->botLeft;  bufMesh.AddVertex(bufVert);
    bufVert.vert = points[6]; bufVert.tex = frames[1]->topLeft; bufMesh.AddVertex(bufVert);
    // 3 face
    bufVert.vert = points[4]; bufVert.tex = frames[1]->botRight;  bufMesh.AddVertex(bufVert);
    bufVert.vert = points[6]; bufVert.tex = frames[1]->topLeft; bufMesh.AddVertex(bufVert);
    bufVert.vert = points[7]; bufVert.tex = frames[1]->topRight; bufMesh.AddVertex(bufVert);
    // 4 face left -----------------------------------------------------
    bufVert.vert = points[1]; bufVert.tex = frames[2]->botLeft;  bufMesh.AddVertex(bufVert);
    bufVert.vert = points[5]; bufVert.tex = frames[2]->topLeft;  bufMesh.AddVertex(bufVert);
    bufVert.vert = points[4]; bufVert.tex = frames[2]->topRight; bufMesh.AddVertex(bufVert);
    // 5 face
    bufVert.vert = points[1]; bufVert.tex = frames[2]->botLeft;  bufMesh.AddVertex(bufVert);
    bufVert.vert = points[4]; bufVert.tex = frames[2]->topRight; bufMesh.AddVertex(bufVert);
    bufVert.vert = points[0]; bufVert.tex = frames[2]->botRight; bufMesh.AddVertex(bufVert);
    // 6 face front ----------------------------------------------------
    bufVert.vert = points[2]; bufVert.tex = frames[3]->botLeft;  bufMesh.AddVertex(bufVert);
    bufVert.vert = points[6]; bufVert.tex = frames[3]->topLeft;  bufMesh.AddVertex(bufVert);
    bufVert.vert = points[5]; bufVert.tex = frames[3]->topRight; bufMesh.AddVertex(bufVert);
    // 7 face
    bufVert.vert = points[2]; bufVert.tex = frames[3]->botLeft;  bufMesh.AddVertex(bufVert);
    bufVert.vert = points[5]; bufVert.tex = frames[3]->topRight; bufMesh.AddVertex(bufVert);
    bufVert.vert = points[1]; bufVert.tex = frames[3]->botRight; bufMesh.AddVertex(bufVert);
    // 8 face right ----------------------------------------------------
    bufVert.vert = points[3]; bufVert.tex = frames[4]->botLeft;  bufMesh.AddVertex(bufVert);
    bufVert.vert = points[7]; bufVert.tex = frames[4]->topLeft;  bufMesh.AddVertex(bufVert);
    bufVert.vert = points[6]; bufVert.tex = frames[4]->topRight; bufMesh.AddVertex(bufVert);
    // 9 face
    bufVert.vert = points[3]; bufVert.tex = frames[4]->botLeft;  bufMesh.AddVertex(bufVert);
    bufVert.vert = points[6]; bufVert.tex = frames[4]->topRight; bufMesh.AddVertex(bufVert);
    bufVert.vert = points[2]; bufVert.tex = frames[4]->botRight; bufMesh.AddVertex(bufVert);
    // 10 face back ----------------------------------------------------
    bufVert.vert = points[0]; bufVert.tex = frames[5]->botLeft;  bufMesh.AddVertex(bufVert);
    bufVert.vert = points[4]; bufVert.tex = frames[5]->topLeft;  bufMesh.AddVertex(bufVert);
    bufVert.vert = points[7]; bufVert.tex = frames[5]->topRight; bufMesh.AddVertex(bufVert);
    // 11 face
    bufVert.vert = points[0]; bufVert.tex = frames[5]->botLeft;  bufMesh.AddVertex(bufVert);
    bufVert.vert = points[7]; bufVert.tex = frames[5]->topRight; bufMesh.AddVertex(bufVert);
    bufVert.vert = points[3]; bufVert.tex = frames[5]->botRight; bufMesh.AddVertex(bufVert);

	// Removing frames that we do not need anymore. --------------------
	for(unsigned int i=0; i<frames.size(); i++)
		delete frames[i];
	frames.clear();

    pMesh->InitVerts(bufMesh.verts);
    pMesh->InitTexCoords(bufMesh.texcoords);
    pMesh->InitIndexes(bufMesh.indexes);
    pMesh->CalcNormals();
    pMesh->CalcCenter();

	Refresh();

	return pMesh;
}

ol_Mesh3D* ol_ShapeGenerator::CreateSphere(float r, unsigned int n)
{
    ol_Mesh3D *pMesh = new ol_Mesh3D();
    float alpha, betta;

    Refresh();

    if(n<4) n=4;
    /*
        Полюса сферы представляют сбой n-1 треугольников имеющих общую вершину.
        Разбить вершины на полигоны в одном цикле не получается, поэтому триангуляция
        полюсов, а также вычисление самих вершин, производится отдельно.
    */
    // Генерируем вершины и текстурные координаты
    // 1 вершина
    tmp.x = 0.0f;
    tmp.y = 0.0f;
    tmp.z = r;
    points.push_back(tmp);
    tmt.x = tmt.y = 0;
    texcoords.push_back(tmt);

    // основные вершины
    for(unsigned int y=1; y<n-1; y++)
    {
        betta=PI*y/(n-1);

        for(unsigned int x=0; x<n-1; x++)
        {
            alpha=(2*PI)*x/(n-1);

            tmp.x=r*sin(alpha)*sin(betta);
            tmp.y=r*cos(alpha)*sin(betta);
            tmp.z=r*cos(betta);
            points.push_back(tmp);

            tmt.x=float(x)/(n-1);
            tmt.y=float(y)/(n-1);
            texcoords.push_back(tmt);
        }
    }

    // последняя вершина
    tmp.x = 0.0f;
    tmp.y = 0.0f;
    tmp.z = -r;
    points.push_back(tmp);
    tmt.x = tmt.y = 1;
    texcoords.push_back(tmt);

    // Генерируем полигоны
    unsigned int last = (n-2)*(n-1)+1;  // номер последней вершины
    unsigned int k;                     // вспомогательный коэффициент

    // полигоны полюсов
    for(unsigned int i=1; i<=n-1; i++)
    {
        k=1;
        if(i==n-1) k = 2-n;
        // полигон первого полюса
        tmf.vertIndex[0] = tmf.coordIndex[0] = 0;
        tmf.vertIndex[2] = tmf.coordIndex[2] = i;
        tmf.vertIndex[1] = tmf.coordIndex[1] = i+k;
        faces.push_back(tmf);

        // полигон последнего полюса
        tmf.vertIndex[0] = tmf.coordIndex[0] = last;
        tmf.vertIndex[2] = tmf.coordIndex[2] = last-i;
        tmf.vertIndex[1] = tmf.coordIndex[1] = last-i-k;
        faces.push_back(tmf);
    }

    // полигоны основной части
    for(unsigned int x=1; x<=last-n; x++)
    {
        k = 0;
        if(!(x%(n-1))) k = n-1;
        // первый полигон
        tmf.vertIndex[0] = tmf.coordIndex[0] = x;
        tmf.vertIndex[2] = tmf.coordIndex[2] = x+n-1;
        tmf.vertIndex[1] = tmf.coordIndex[1] = x+n-k;
        faces.push_back(tmf);

        // второй полигон
        tmf.vertIndex[0] = tmf.coordIndex[0] = x;
        tmf.vertIndex[2] = tmf.coordIndex[2] = x+n-k;
        tmf.vertIndex[1] = tmf.coordIndex[1] = x+1-k;
        faces.push_back(tmf);
    }

    InitMesh(pMesh);
    Refresh();

    return pMesh;
}
