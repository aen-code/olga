/**
------------------------------------------------------------------------
  This file is part of the OLGA project.
  Copyright © 2017 by Enver Akhmedov.
  E-mail: aen-code@yandex.ru
  For conditions of distribution and use, see copyright notice in OLGA.h
------------------------------------------------------------------------
**/

#ifndef OL_GEOMETRY_H_INCLUDED
#define OL_GEOMETRY_H_INCLUDED

#include <GL/gl.h>
#include <vector>
#include <iostream>
#include <stdio.h>
#include "ol_List.h"
#include "ol_Vector_Math.h"

using namespace std;

class ol_Vertex2d
{
public:
    ol_Vertex2d()
    {
        point.x = 0.0f;
        point.y = 0.0f;
        normal.x = 0.0f;
        normal.y = 0.0f;
    }
    ol_Vertex2d(ol_Vector2df &p)
    {
        point.x = p.x;
        point.y = p.y;
        normal.x = 0.0f;
        normal.y = 0.0f;
    }

    ~ol_Vertex2d() {}

    ol_Vector2df point;
    ol_Vector2df normal;
};

/**
    ------------------------------------ Edge 2D -------------------------------------------
    >                                                                                      <
    >           Класс удобен для передачи ребра полигона как параметра                     <
    >                                                                                      <
    ----------------------------------------------------------------------------------------
**/

class ol_Edge2d
{
public:
    ol_Vertex2d left;
    ol_Vertex2d right;
    ol_Vector2df normal;
};

/**
    ------------------------------------ Mesh 2D -------------------------------------------
    >                                                                                      <
    >  Набор вершин, нормалей, на плоскости, представляющих собой контур объекта.          <
    >  Позволяет последовательно перебирать вершины. Заполнять нужно последовательно по    <
    >  часовой стрелке                                                                     <
    >                                                                                      <
    ----------------------------------------------------------------------------------------
**/

class ol_Mesh2d
{
private:
    unsigned short frame; // "окно" для последовательного перебора ребер
public:
    vector<ol_Vertex2d> verts;
    vector<ol_Vector2df> edgeNorms; // нормали граней подряд по часовой стреке

    GLuint vertCount;

    ol_Mesh2d();
    ~ol_Mesh2d();

    // сбрасывает окно в начальную позицию
    void Reset();
    // ф-я для перебора ребер
    unsigned short getEdge(ol_Edge2d &edge);
    // добавляет вершину
    void addVertex(ol_Vector2df vert);
    // определяет расстояние от точки до полигона. В параметр норм записывается нормаль ребра
    // которое ближе всего к точке. closest_point - ближайшая точка
    float DistanceToPoint(ol_Vector2df &p, ol_Vector2df &norm, ol_Vector2df &closest_point);
    // расчет нормалей. Необходимо вызывать после того как все вершины добавлены
    void CalcNormals(void);
};

/**
    ------------------------------------ Face  ---------------------------------------------
    >                                                                                      <
    >  This is our face structure. This is is used for indexing into the vertex and        <
    >  texture coordinate arrays.  From this information we know which vertices from       <
    >  our vertex array go to which face, along with the correct texture coordinates.      <
    >                                                                                      <
    ----------------------------------------------------------------------------------------
**/

class ol_Face
{
public:
    unsigned int vertIndex[3];			// indicies for the verts that make up this triangle
    unsigned int coordIndex[3];			// indicies for the tex coords to texture this face

    ol_Face() {}
    ~ol_Face() {}
};

/**
    ------------------------------------ Vertex  -------------------------------------------
    >                                                                                      <
    >        Class that actually contains all vertex data, not indexes like ol_Face         <
    >                                                                                      <
    ----------------------------------------------------------------------------------------
**/

class ol_Vertex
{
public:
    ol_Vertex()
    {
        tex = ol_Vector2df();
        norm = ol_Vector3d();
        vert = ol_Vector3d();
    }
    ~ol_Vertex() {}

    bool operator==(ol_Vertex vVer)
    {
        if(tex==vVer.tex && norm==vVer.norm && vert==vVer.vert) return true;
        else return false;
    }

    ol_Vector2df tex;
    ol_Vector3d norm;
    ol_Vector3d vert;
};

/**
    --------------------------------- ol_TriangleMesh ---------------------------------------
    >                                                                                      <
    >   Создан как вспомогательный класс. Изначально чтение модели было заточено под       <
    >   ol_Mesh3d, но оказалось, что вершины нужно дополнительно преобразовать. Чтобы не   <
    >   перебирать уже работающий код, создан дополнительный класс                         <
    >                                                                                      <
    ----------------------------------------------------------------------------------------
**/

class ol_TriangleMesh
{
public:
    vector<ol_Vector3d> verts;
    vector<ol_Vector3d> norms;
    vector<ol_Vector2df> texcoords;
    vector<GLuint> indexes;

    // Добавляет вершину к мешу. При добавлении производится проверка:
    // если такая уже есть добавляется только индекс
    void AddVertex(ol_Vertex ver);
};

/**
    ------------------------------------ Mesh 3D -------------------------------------------
    >                                                                                      <
    >                        Трехмерная геометрическая модель.                             <
    >                                                                                      <
    ----------------------------------------------------------------------------------------
**/

class ol_Mesh3D
{
public:
    GLuint  vertCount;			// The number of verts in the model
    GLuint  normCount;			// The number of normals in the model
    GLuint  faceCount;			// The number of faces in the model
    GLuint  texcoordCount;		// The number of texture coordinates
    GLuint  indCount;			// The number of actual points to draw

    ol_Vector3d  *pVerts;		// The object's vertices
    ol_Vector3d  *pNormals;		// The object's normals
    ol_Vector2df *pTexVerts;	// The texture's UV coordinates
    GLuint       *pVertInd;		// The indexes for glDrawElements
    ol_Face      *pFaces;		// The faces information of the object

    ol_Vector3d  center;		// Geometric center of model

    ol_Mesh3D();
    ~ol_Mesh3D();

    // выделяет память под массив вершин и заполняет его из значений входного вектора
    void InitVerts(vector<ol_Vector3d> verts);
    // выделяет память под массив фасетов и заполняет его из значений входного вектора
    void InitFaces(vector<ol_Face> faces);
    // выделяет память память под массив текстурных координат и заполняет его из значений входного вектора
    void InitTexCoords(vector<ol_Vector2df> coords);
    // выделяет память память под массив нормалей и заполняет его из значений входного вектора
    void InitNormals(vector<ol_Vector3d> norm);
    // выделяет память память под массив индексов и заполняет его из значений входного вектора
    void InitIndexes(vector<GLuint> ind);
    // создает массив индексов из вспомогательного массива фасетов
    void InitVertInd(void);
    // расчет интерполированных нормалей с использованием массива фасетов
    void CalcNormals(void);
    // удаляет массив фасетов
    void CleanUp(void);
    // определяет геометрический центр трехмерного объекта
    void CalcCenter(void);
    // возвращает количество вершин меша
    unsigned int getVertCount(void);
    // возвращает количество полигонов меша
    unsigned int getPolyCount(void);
};

#endif // OL_GEOMETRY_H_INCLUDED
