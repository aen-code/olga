/**
------------------------------------------------------------------------
  This file is part of the OLGA project.
  Copyright © 2017 by Enver Akhmedov.
  E-mail: aen-code@yandex.ru
  For conditions of distribution and use, see copyright notice in OLGA.h
------------------------------------------------------------------------
**/

#ifndef OL_ENUMERATIONS_H_INCLUDED
#define OL_ENUMERATIONS_H_INCLUDED

#define _OL_DEBUG_BUILD_

#include "../ol_Vector_Math.h"

#ifdef __linux__
#include "linux/ol_Win_Bitmap.h"
#include "linux/ol_Keycodes.h"
#include "linux/ol_Timer.h"
#endif
#ifdef __WIN32__
//#include "Win/ol_Keycodes.h"
#endif

/**
    --------------------------------- Core Params-------------------------------------------
    >                                                                                      <
    >   Класс для хранения некоторых параметров приложения, которые используется           <
    >   разными классами.                                                                  <
    >                                                                                      <
    ----------------------------------------------------------------------------------------
**/

class ol_CoreParams
{
public:
    int left;					// x coordinate of upper-left corner of client part of the window
    int up;						// y coordinate of upper-left corner
    int centerX;				// x coordinate of the center of the window
    int centerY;				// y coordinate of the center of the window
    int width;					// Width of the window
    int height;					// Height of the window
    unsigned int bitsPerPixel;	// Bits Per Pixel
    float depth;				// Depth of view
    static unsigned int fps;	// Frames per second
    static unsigned int vfps;	// Virtual frames per second defines perfonance
    static float fdt;			// Frame delta time
    static ol_Timer *timer;		// системный таймер
    static char *locale_name;	// текущая локаль

    void calcCenter(void);
};

enum ol_CollisionMasks
{
    OL_COLLISION_MASK_NONE  = 0x0000,
    OL_COLLISION_MASK_0     = 0x0001,
    OL_COLLISION_MASK_1     = 0x0002,
    OL_COLLISION_MASK_2     = 0x0004,
    OL_COLLISION_MASK_3     = 0x0008,
    OL_COLLISION_MASK_4     = 0x0010,
    OL_COLLISION_MASK_5     = 0x0020,
    OL_COLLISION_MASK_6     = 0x0040,
    OL_COLLISION_MASK_7     = 0x0080,
    OL_COLLISION_MASK_8     = 0x0100,
    OL_COLLISION_MASK_9     = 0x0200,
    OL_COLLISION_MASK_10    = 0x0400,
    OL_COLLISION_MASK_11    = 0x0800,
    OL_COLLISION_MASK_12    = 0x1000,
    OL_COLLISION_MASK_13    = 0x2000,
    OL_COLLISION_MASK_14    = 0x4000,
    OL_COLLISION_MASK_15    = 0x8000
};

enum ol_MaterialParams { OL_MATERIAL_AMBIENT, OL_MATERIAL_DIFFUSE, OL_MATERIAL_SPECULAR, OL_MATERIAL_SHININESS, OL_MATERIAL_LIGHTNING, OL_MATERIAL_COLOR, OL_MATERIAL_TEXTURE };
enum ol_MaterialType { OL_MATERIAL_TYPE_STANDART, OL_MATERIAL_TYPE_SHADER };

enum ol_RenderParams { OL_RENDER_SOLID, OL_RENDER_WIREFRAME, OL_RENDER_SOLID_N_WIRE };
enum ol_RenderFlags { OL_RENDER_DIRS, OL_RENDER_NORMS };

enum ol_EventType { OL_IETYPE_NONE, OL_IETYPE_KEYBOARD, OL_IETYPE_MOUSE }; // Input Event TYPE
enum ol_KeyboardEvents { OL_KEV_NONE, OL_KEV_KEYDOWN, OL_KEV_KEYUP };     // Keyboard EVent
enum ol_MouseEvents { OL_MEV_NONE,/* OL_MEV_MOUSEMOVE,*/ OL_MEV_WHEEL, OL_MEV_LBUTTONDOWN, OL_MEV_LBUTTONUP, OL_MEV_RBUTTONDOWN, OL_MEV_RBUTTONUP, OL_MEV_MBUTTONDOWN, OL_MEV_MBUTTONUP }; // Mouse EVent

enum ol_GameEvents { OL_GETYPE_SCENE, OL_GETYPE_GUI }; // Game Event TYPE
enum ol_SceneEvents { OL_SEV_COLLISION, OL_SEV2D_COLLISION }; // Scene EVent
enum ol_GUIEvents { OL_GUIEV_LBUTTONDOWN, OL_GUIEV_LBUTTONUP, OL_GUIEV_MBUTTONDOWN, OL_GUIEV_MBUTTONUP, OL_GUIEV_RBUTTONDOWN, OL_GUIEV_RBUTTONUP, OL_GUIEV_MOUSEMOVE,
                   OL_GUIEV_BUTTONCLICKED, OL_GUIEV_BUTTONPRESSED,
                   OL_GUIEV_SCROLL,
                   OL_GUIEV_EDITBOXENTER,
                   OL_GUIEV_FBCHANGEDPATH, OL_GUIEV_FBFILESELECTED, OL_GUIEV_FILEOPENED, OL_GUIEV_FILESAVED,
                   OL_GUIEV_SCROLLBOXCHANGED
                 }; // GUI EVents

struct ol_Keys
{
    bool keyDown [256];	      // каждый элемент массива содержит состояние отдельной клавиши true/false
    ol_KeyboardEvents kType;  // событие клавиатуры
    unsigned short key_code;  // аппаратный код клавиши
    unsigned int char_code;   // код введенного символа
};

/**
    --------------------------------- Mouse Input ------------------------------------------
    >                                                                                      <
    >                          Класс для работы с мышью.                                   <
    >                                                                                      <
    ----------------------------------------------------------------------------------------
**/

class ol_MouseInput
{
public:
    ol_MouseInput() {}
    ~ol_MouseInput() {}

    ol_Vector2d<float> mPos;		// новые координаты курсора мыши
    ol_Vector2df last_mPos;			// старые координаты
    ol_Vector2df cShift;			// смещение курсора (скорость движения)
    int wheel;						// вращение колеса

    ol_MouseEvents mType;			// события мышки.

    void setMousePos(int x, int y);
    void CenterMouseCursor(void);

    void ResetCursor(void);			// сбрасывает значения координат курсора mPos в last_mPos
    void ResetShift(void);			// обнуляет вектор движения курсора cShift
    void UpdateShift(void);			// вычисляет вектор движения курсора
};

/**
    Существует 2 типа событий, которые обрабатываются отдельно. События ввода-вывода:
    клавиатура, мышь и т.д., и игровые события движка: столкновения на 3д и 2д сцене,
    выбор объектов, события ГУИ. События ввода-вывода обрабатываются в апдэйте, игровые
    события обрабатываются в онЭвент.
**/

/**
    --------------------------------- Event ------------------------------------------------
    >                                                                                      <
    >               События ввода вывода: клавиатура, мышь и тд.                           <
    >                                                                                      <
    ----------------------------------------------------------------------------------------
**/

class ol_Event
{
public:
    ol_Keys *keys;               // содержит всю информацию по клавишам
    ol_MouseInput *mParams;      // содержит всю информацию по мышке

    ol_EventType type;           // ol_EventType

    ol_Event()
    {
        mParams = new ol_MouseInput();
        keys = new ol_Keys();
    }
    ~ol_Event()
    {
        delete keys;
        delete mParams;
    }

    // Возвращает состояние клавиши
    bool getKey(ol_KeyCodes key);
    // Возвращает код последней нажатой клавиши
    unsigned short getKeyCode(void);
    // Возвращает код символа последней нажатой клавиши
    unsigned int getCharCode(void);
    // Возвращает тип события клавиатура, мышь
    ol_EventType getType(void);
    // Возвращает последнее клавиатурное событие
    ol_KeyboardEvents getKType(void);
    // Возвращает последнее событие мыши
    ol_MouseEvents getMType(void);
    // Обнуляет состояния всех клавиш и событий
    void ResetAll(void);
    // Сбрасывает последнее событие мыши
    void ResetMEvent(void);

    ol_Vector2df getCurPos(void);
    ol_Vector2df getCurShift(void);
    int getMouseWheel(void);
    bool isMouseMoving(void);
    void setCurPos(ol_Vector2df pos);

    // возможно в дальнейшем будет удалена
    void RefreshEventState(void);
};

/**
    --------------------------------- Scene Even -------------------------------------------
    >                                                                                      <
    >                               Игровые события.                                       <
    >                                                                                      <
    ----------------------------------------------------------------------------------------
**/

class ol_SceneObject;  // декларация SceneObjecta
class ol_GUIControl;   // декларация ГУИ контрола
class ol_SceneObject2d;

struct ol_SceneEvent
{
    ol_SceneEvents type;

    ol_SceneObject *object1;
    ol_SceneObject *object2;

    ol_Vector3d collisionPoint;
    ol_Vector3d collisionNormal;
    ol_Vector3d collisionFace[3];

    ol_SceneObject2d *obj2d_1;
    ol_SceneObject2d *obj2d_2;
};

/**
    --------------------------------- GUI Event --------------------------------------------
    >                                                                                      <
    >                   События пользовательского интерфейса.                              <
    >                                                                                      <
    ----------------------------------------------------------------------------------------
**/

struct ol_GUIEvent
{
    ol_GUIEvents type;
    // ГУИ менеджеру нужно знать был ли клик по окну, даже если не произошло ни одного ГУИ события.
    // Например, клик мог быть по заголовку или по пустой части окна
    bool winEvent;

    ol_GUIControl *control1;
    ol_GUIControl *control2;
};

class ol_GameEvent
{
public:
    ol_GameEvents type;

    ol_SceneEvent *sevent;
    ol_GUIEvent   *gevent;

    ol_GameEvent() {}
    ~ol_GameEvent() {}
};

#endif // OL_ENUMERATIONS_H_INCLUDED
