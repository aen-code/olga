#ifndef VERSION_H
#define VERSION_H

namespace ol_Version
{
	//Date Version Types
	static const char DAY[] = "13";
	static const char MONTH[] = "10";
	static const char YEAR[] = "2017";

	//Software Status
	static const char STATUS[] =  "alpha";
	static const char STATUS_SHORT[] =  "a";

	//Standard Version Type
	static const long MAJOR  = 0;
	static const long MINOR  = 1;
	static const long PATCH  = 0;

	static const char FULLVERSION_STRING [] = "v.0.1.0-alpha";
}
#endif //VERSION_H
