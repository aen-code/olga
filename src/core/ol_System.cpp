/**
------------------------------------------------------------------------
  This file is part of the OLGA project.
  Copyright © 2017 by Enver Akhmedov.
  E-mail: aen-code@yandex.ru
  For conditions of distribution and use, see copyright notice in OLGA.h
------------------------------------------------------------------------
**/

#include "ol_System.h"
#include <string.h>

/**
    --------------------------------- Event ------------------------------------------------
    >                                                                                      <
    >               События ввода вывода: клавиатура, мышь и тд.                           <
    >                                                                                      <
    ----------------------------------------------------------------------------------------
**/

void ol_Event::ResetAll(void)
{
    memset(keys->keyDown, 0, sizeof(keys->keyDown));

    type = OL_IETYPE_NONE;
    keys->kType = OL_KEV_NONE;
    mParams->mType = OL_MEV_NONE;
    mParams->wheel = 0;
}

void ol_Event::ResetMEvent(void)
{
    type = OL_IETYPE_NONE;
    mParams->mType = OL_MEV_NONE;
}

void ol_Event::RefreshEventState(void)
{
#ifdef __WIN32__
    if(type == OL_ETYPE_KEYBOARD)
    {
        if(event == OL_EV_KEYDOWN) keys->keyDown[key_code] = true;
        if(event == OL_EV_KEYUP)   keys->keyDown[key_code] = false;
    }

    if(type == OL_ETYPE_MOUSE)
    {
        if(event == OL_EV_KEYDOWN) keys->keyDown[key_code] = true;
        if(event == OL_EV_KEYUP)   keys->keyDown[key_code] = false;

        if(event == OL_EV_MOUSEMOVE)
        {
            mParams->dx =  mParams->x - mParams->last_x;
            mParams->dy =  mParams->y - mParams->last_y;

            mParams->last_x = mParams->x;
            mParams->last_y = mParams->y;
        }
    }
#endif
}

unsigned short ol_Event::getKeyCode(void)
{
    return keys->key_code;
}

unsigned int ol_Event::getCharCode(void)
{
    return keys->char_code;
}

bool ol_Event::getKey(ol_KeyCodes key)
{
    return keys->keyDown[key];
}

ol_EventType ol_Event::getType(void)
{
    return this->type;
}

ol_KeyboardEvents ol_Event::getKType(void)
{
    return keys->kType;
}

ol_MouseEvents ol_Event::getMType(void)
{
    return mParams->mType;
}

ol_Vector2df ol_Event::getCurPos(void)
{
    return mParams->mPos;
}

ol_Vector2df ol_Event::getCurShift(void)
{
    return mParams->cShift;
}

int ol_Event::getMouseWheel(void)
{
    return mParams->wheel;
}

bool ol_Event::isMouseMoving(void)
{
    if((mParams->cShift.x!=0) || (mParams->cShift.y!=0)) return true;
    else return false;
}

void ol_Event::setCurPos(ol_Vector2df pos)
{
    mParams->mPos = pos;
}

/**
    --------------------------------- Core Params-------------------------------------------
    >                                                                                      <
    >   Класс для хранения некоторых параметров приложения, которые используется           <
    >   разными классами.                                                                  <
    >                                                                                      <
    ----------------------------------------------------------------------------------------
**/

void ol_CoreParams::calcCenter(void)
{
    centerX = left + width/2;
    centerY = up + height/2;
}

/**
    --------------------------------- Mouse Input ------------------------------------------
    >                                                                                      <
    >                          Класс для работы с мышью.                                   <
    >                                                                                      <
    ----------------------------------------------------------------------------------------
**/

void ol_MouseInput::setMousePos(int x, int y)
{
//    SetCursorPos(x, y);
}

void ol_MouseInput::ResetCursor(void)
{
    last_mPos = mPos;
}

void ol_MouseInput::ResetShift(void)
{
    cShift.x = 0.0f;
    cShift.y = 0.0f;
}

void ol_MouseInput::UpdateShift(void)
{
    cShift = mPos - last_mPos;
}

void ol_MouseInput::CenterMouseCursor(void)
{
//    setMousePos(ol_System->centerX, ol_System->centerY);

//        static bool change_m_pos = true;
//
//        if((x == ol_System->centerX) && (y == ol_System->centerY))
//        {
//            cout<<"Center - Return\n";
//            change_m_pos = false;
//            return;
//        }
//        else change_m_pos = true;
//
//        if(change_m_pos)
//        {
//            setMousePos(ol_System->centerX, ol_System->centerY);
////            dx = dy =0;
//            change_m_pos = false;
//        }
// //       else change_m_pos = true;
//
////        // Set the mouse position to the middle of our window
////        if(change_first_pos)
////        {
////            posx = ol_System->centerX;
////            posy = ol_System->centerY;
////
////            change_first_pos = false;
////        }
////
////        lastm_x = posx;
////        lastm_y = posy;
}
