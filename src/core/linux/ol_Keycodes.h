/**
------------------------------------------------------------------------
  This file is part of the OLGA project.
  Copyright © 2017 by Enver Akhmedov.
  E-mail: aen-code@yandex.ru
  For conditions of distribution and use, see copyright notice in OLGA.h
------------------------------------------------------------------------
**/

#ifndef OL_KEYCODES_H_INCLUDED
#define OL_KEYCODES_H_INCLUDED

// Константы для определения состояний клавиш из вектора прерываний
const char b_0 = 0x01;
const char b_1 = 0x02;
const char b_2 = 0x04;
const char b_3 = 0x08;
const char b_4 = 0x10;
const char b_5 = 0x20;
const char b_6 = 0x40;
const char b_7 = 0x80;

// Маски для определения дополнительных кнопок на мыши
#define Button6Mask ( 1 << 13 )
#define Button7Mask ( 1 << 14 )

enum ol_KeyCodes
{
    OL_KEY_LBUTTON = 0x01,
    OL_KEY_RBUTTON,
    OL_KEY_MBUTTON,

    OL_KEY_F1 = 0x43,
    OL_KEY_F2 = 0x44,
    OL_KEY_F3 = 0x45,
    OL_KEY_F4 = 0x46,
    OL_KEY_F5 = 0x47,
    OL_KEY_F6 = 0x48,
    OL_KEY_F7 = 0x49,
    OL_KEY_F8 = 0x4a,
    OL_KEY_F9 = 0x4b,
    OL_KEY_F10 = 0x4c,
    OL_KEY_F11 = 0x5f,
    OL_KEY_F12 = 0x60,

    OL_KEY_1 = 0xa0,
    OL_KEY_2 = 0xb0,
    OL_KEY_3 = 0xc0,
    OL_KEY_4 = 0xd0,
    OL_KEY_5 = 0xe0,
    OL_KEY_6 = 0xf0,
    OL_KEY_7 = 0x10,
    OL_KEY_8 = 0x11,
    OL_KEY_9 = 0x12,
    OL_KEY_0 = 0x13,

    OL_KEY_Q = 0x18,
    OL_KEY_W = 0x19,
    OL_KEY_E = 0x1a,
    OL_KEY_R = 0x1b,
    OL_KEY_T = 0x1c,
    OL_KEY_Y = 0x1d,
    OL_KEY_U = 0x1e,
    OL_KEY_I = 0x1f,
    OL_KEY_O = 0x20,
    OL_KEY_P = 0x21,
    OL_KEY_A = 0x26,
    OL_KEY_S = 0x27,
    OL_KEY_D = 0x28,
    OL_KEY_F = 0x29,
    OL_KEY_G = 0x2a,
    OL_KEY_H = 0x2b,
    OL_KEY_J = 0x2c,
    OL_KEY_K = 0x2d,
    OL_KEY_L = 0x2e,
    OL_KEY_Z = 0x34,
    OL_KEY_X = 0x35,
    OL_KEY_C = 0x36,
    OL_KEY_V = 0x37,
    OL_KEY_B = 0x38,
    OL_KEY_N = 0x39,
    OL_KEY_M = 0x3a,

//    bracketleft = 0x22,
//    bracketright = 0x23,
//    semicolon = 0x2f,
//    apostrophe = 0x30,

    OL_KEY_COMMA = 0x3b,
    OL_KEY_PERIOD = 0x3c,

//    slash = 0x3d,
//    backslash = 0x33,
//    grave = 0x31,

    OL_KEY_TAB = 0x17,

//    Caps_Lock = 0x42,

    OL_KEY_LSHIFT = 0x32,
    OL_KEY_LCONTROL = 0x25,

//    Alt_L = 0x40,

    OL_KEY_BACK = 0x16,
    OL_KEY_RETURN = 0x24,
    OL_KEY_RSHIFT = 0x3e,
    OL_KEY_RCONTROL = 0x69,

//    Alt_R = 0x6c,

    OL_KEY_LEFT = 0x71,
    OL_KEY_UP = 0x6f,
    OL_KEY_RIGHT = 0x72,
    OL_KEY_DOWN = 0x74,

    OL_KEY_PRIOR = 0x70,
    OL_KEY_NEXT = 0x75,
    OL_KEY_HOME = 0x6e,
    OL_KEY_END = 0x73,
    OL_KEY_INSERT = 0x76,
    OL_KEY_DELETE = 0x77,

    OL_KEY_NUMPAD0 = 0x5a,
    OL_KEY_NUMPAD1 = 0x57,
    OL_KEY_NUMPAD2 = 0x58,
    OL_KEY_NUMPAD3 = 0x59,
    OL_KEY_NUMPAD4 = 0x53,
    OL_KEY_NUMPAD5 = 0x54,
    OL_KEY_NUMPAD6 = 0x55,
    OL_KEY_NUMPAD7 = 0x4f,
    OL_KEY_NUMPAD8 = 0x50,
    OL_KEY_NUMPAD9 = 0x51,
    OL_KEY_DECIMAL = 0x5b,
    OL_KEY_DIVIDE = 0x6a,
    OL_KEY_MULTIPLY = 0x3f,
    OL_KEY_SUBTRACT = 0x52,
    OL_KEY_ADD = 0x56,

//    KP_Enter = 0x68,

    OL_KEY_PAUSE = 0x7f,
    OL_KEY_SCROLL = 0x4e,
    OL_KEY_LWIN = 0x85,
    OL_KEY_RWIN = 0x86,
    OL_KEY_ESCAPE = 0x9
};

#endif // OL_KEYCODES_H_INCLUDED
