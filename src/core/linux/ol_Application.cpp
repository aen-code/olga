/**
------------------------------------------------------------------------
  This file is part of the OLGA project.
  Copyright © 2017 by Enver Akhmedov.
  E-mail: aen-code@yandex.ru
  For conditions of distribution and use, see copyright notice in OLGA.h
------------------------------------------------------------------------
**/

#include "ol_Application.h"

#include <stdio.h>
#include <stdlib.h>

ol_Application::ol_Application() {}

ol_Application::~ol_Application()
{
    delete ioEvent;
    delete gEvent;
    delete smgr;
    delete smgr2d;

    DestroyWindowGL();
    printf("OLGA is Terminating ...\n");
}

void ol_Application::Init(ol_ApplInitParams prms)
{
    window.title			        = prms.title;
    window.isFullScreen	            = prms.isFullScreen;
    window.clearColor               = prms.bkColor;

    window.system.width			    = prms.width;
    window.system.height			= prms.height;
    window.system.bitsPerPixel	    = prms.bitsPerPixel;
    window.system.depth             = prms.depth;

    smgr = new ol_SceneManager(graph);
    ioEvent = new ol_Event();
    gEvent = new ol_GameEvent();
    smgr2d = new ol_SceneManager2d(graph);

    isProgramLooping = false;
}

void ol_Application::setWindowTitle(const char *tit)
{
    XStoreName(window.application.dpy, window.application.win, tit);
}

void ol_Application::setVersion(const char stat[], const char ver[])
{
    strcpy(version, ver);
    /*
    char buf[10];
    strcpy(version, stat);
    strcat(version, " ");
    strcat(version, ver);
    strcat(version, "; Builds Count - ");
    sprintf( buf, "%ld", AutoVersion::BUILDS_COUNT);
    strcat(version, buf);
    */
}

char *ol_Application::getVersion(void)
{
    return version;
}

ol_GLWindow* ol_Application::getWindow(void)
{
    return &window;
}

ol_Event* ol_Application::getEvent(void) const
{
    return ioEvent;
}

ol_GameEvent* ol_Application::getGameEvent(void) const
{
    return gEvent;
}

ol_SceneManager* ol_Application::getSceneManager(void) const
{
    return smgr;
}

ol_SceneManager2d* ol_Application::getSceneManager2d(void) const
{
    return smgr2d;
}

ol_Graphics* ol_Application::getGraphicsInterface(void) const
{
    return graph;
}

ol_GUIManager* ol_Application::getGUIManager(void) const
{
    return guimgr;
}

void ol_Application::ToggleFullscreen(void)								// Toggle Fullscreen/Windowed
{
}

bool ol_Application::ChangeScreenResolution (int width, int height, int bitsPerPixel)	// Change The Screen Resolution
{
    return true;														// Display Change Was Successful, Return True
}

void EarlyInitGLXfnPointers()
{
//
//    glGenVertexArraysAPPLE = (void(*)(GLsizei, const GLuint*))glXGetProcAddressARB((GLubyte*)"glGenVertexArrays");
//    glBindVertexArrayAPPLE = (void(*)(const GLuint))glXGetProcAddressARB((GLubyte*)"glBindVertexArray");
//    glDeleteVertexArraysAPPLE = (void(*)(GLsizei, const GLuint*))glXGetProcAddressARB((GLubyte*)"glGenVertexArrays");

    glXCreateContextAttribsARB = (GLXContext(*)(Display* dpy, GLXFBConfig config, GLXContext share_context, Bool direct, const int *attrib_list))glXGetProcAddressARB((GLubyte*)"glXCreateContextAttribsARB");
// glXChooseFBConfig = (GLXFBConfig*(*)(Display *dpy, int screen, const int *attrib_list, int *nelements))glXGetProcAddressARB((GLubyte*)"glXChooseFBConfig");
// glXGetVisualFromFBConfig = (XVisualInfo*(*)(Display *dpy, GLXFBConfig config))glXGetProcAddressARB((GLubyte*)"glXGetVisualFromFBConfig");
}

bool ol_Application::CreateWindowGL(int argc, char **argv)									// This Code Creates Our OpenGL Window
{
    GLint winmask;
    GLint nMajorVer = 0;
    GLint nMinorVer = 0;

    GLXFBConfig *fbConfigs;
    int numConfigs = 0;

    static int fbAttribs[] =
    {
        GLX_RENDER_TYPE,   GLX_RGBA_BIT,
        GLX_X_RENDERABLE,  True,
        GLX_DRAWABLE_TYPE, GLX_WINDOW_BIT,
        GLX_DOUBLEBUFFER,  True,
        GLX_RED_SIZE,      8,
        GLX_BLUE_SIZE,     8,
        GLX_GREEN_SIZE,    8,
        0
    };

    EarlyInitGLXfnPointers();

    /*** (1) open a connection to the X server ***/

    window.application.dpy = XOpenDisplay(NULL);
    if (window.application.dpy == NULL) fatalError("could not open display");

    /*** (2) make sure OpenGL's GLX extension supported ***/

    if(!glXQueryExtension(window.application.dpy, &window.application.dummy, &window.application.dummy)) fatalError("X server has no OpenGL GLX extension");


    // Get Version info
    glXQueryVersion(window.application.dpy, &nMajorVer, &nMinorVer);
    printf("Supported GLX version - %d.%d\n\n", nMajorVer, nMinorVer);

    if(nMajorVer == 1 && nMinorVer < 2)
    {
        XCloseDisplay(window.application.dpy);
        fatalError("ERROR: GLX 1.2 or greater is necessary");
    }

    // Get a new fb config that meets our attrib requirements
    fbConfigs = glXChooseFBConfig(window.application.dpy, DefaultScreen(window.application.dpy), fbAttribs, &numConfigs);

    window.application.vi = glXGetVisualFromFBConfig(window.application.dpy, fbConfigs[0]);

    // Now create an X window
    window.application.swa.event_mask = ExposureMask | VisibilityChangeMask |
                                        KeyPressMask | KeyReleaseMask |
                                        ButtonPressMask | ButtonReleaseMask |
                                        //    ButtonMotionMask | PointerMotionMask |
                                        StructureNotifyMask ;

    window.application.swa.border_pixel = 0;
    window.application.swa.bit_gravity = StaticGravity;
    window.application.swa.colormap = XCreateColormap(window.application.dpy, RootWindow(window.application.dpy, window.application.vi->screen), window.application.vi->visual, AllocNone);

    winmask = CWBorderPixel | CWBitGravity | CWEventMask| CWColormap;

    window.application.win = XCreateWindow(window.application.dpy, DefaultRootWindow(window.application.dpy), 0, 0,  window.system.width, window.system.height, 0,
                                           window.application.vi->depth, InputOutput, window.application.vi->visual, winmask, &window.application.swa);

    XSetStandardProperties(window.application.dpy, window.application.win, window.title, "main", None, argv, argc, NULL);
    XMapWindow(window.application.dpy, window.application.win);

    /*
        The OpenGL version of the context created will be up to OpenGL 3.1 if your implementa-
        tion supports that version or any newer context version if it is 100% backward-compatible
        with OpenGL 3.1. Because you can’t be sure what version of the OpenGL context you are
        going to get when calling glXCreateNewContext, this is not the preferred method. Instead
        use the newer version, glXCreateContextAttribsARB.

        Before using glXCreateContextAttribsARB, you should check that the extension string
        GLX_ARB_create_context_profile is in the list of GLX extensions. Then, you need to get
        the function pointer for this extension. After that, you are all set to use the preferred way
        of creating contexts.
    */

    // Also create a new GL context for rendering
    GLint attribs[] =
    {
        GLX_CONTEXT_MAJOR_VERSION_ARB, 3,
        GLX_CONTEXT_MINOR_VERSION_ARB, 0,
        0
    };

    window.application.cx = glXCreateContextAttribsARB(window.application.dpy, fbConfigs[0], 0, True, attribs);

//    window.application.cx = glXCreateNewContext(window.application.dpy, fbConfigs[0], GLX_RGBA_TYPE, NULL, false);

    if (window.application.cx == NULL) fatalError("could not create rendering context");

    glXMakeCurrent(window.application.dpy, window.application.win, window.application.cx);

    if(GLEE_VERSION_3_0) printf("Supported OpenGL version - 3.0\n\n");
    else fatalError("ERROR: OpenGL 3.0 is not supported");

    InitGL();
    ReshapeGL(window.system.width, window.system.height);				// Reshape Our GL Window

    window.system.fps=0;

    return true;														// Window Creating Was A Success
}

bool ol_Application::DestroyWindowGL(void)								// Destroy The OpenGL Window & Release Resources
{
    glXMakeCurrent(window.application.dpy, None, NULL);
    glXDestroyContext(window.application.dpy, window.application.cx);
    XDestroyWindow(window.application.dpy, window.application.win);
    XCloseDisplay(window.application.dpy);

    return true;														// Return True
}

void ol_Application::TerminateApplication(void)							// Terminate The Application
{
    isProgramLooping = false;
}

void ol_Application::ReshapeGL (int width, int height)				    // Reshape The Window When It's Moved Or Resized
{
    window.system.width = width;
    window.system.height = height;
    guimgr->RefreshWindowArea(width, height);

    glViewport (0, 0, (GLsizei)(width), (GLsizei)(height));				// Reset The Current Viewport
    glMatrixMode (GL_PROJECTION);										// Select The Projection Matrix
    glLoadIdentity ();													// Reset The Projection Matrix
    gluPerspective (45.0f, (GLfloat)(width)/(GLfloat)(height), 1.0f, window.system.depth);	// Calculate The Aspect Ratio Of The Window

    glMatrixMode (GL_MODELVIEW);										// Select The Modelview Matrix
    glLoadIdentity ();													// Reset The Modelview Matrix
}

void ol_Application::InitGL(void)
{
    ol_Color col = window.clearColor;
    glClearColor (col.r, col.g, col.b, col.a);					// Background clear color
    glClearDepth (1.0f);										// Depth Buffer Setup
    glDepthFunc (GL_LEQUAL);									// The Type Of Depth Testing (Less Or Equal)
    glEnable (GL_DEPTH_TEST);									// Enable Depth Testing
    glShadeModel (GL_SMOOTH);									// Select Smooth Shading
    glHint (GL_PERSPECTIVE_CORRECTION_HINT, GL_NICEST);			// Set Perspective Calculations To Most Accurate
}

void fatalError(const char *message)
{
    fprintf(stderr, "%s\n", message);
    exit(1);
}
