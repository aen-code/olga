/**
------------------------------------------------------------------------
  This file is part of the OLGA project.
  Copyright © 2017 by Enver Akhmedov.
  E-mail: aen-code@yandex.ru
  For conditions of distribution and use, see copyright notice in OLGA.h
------------------------------------------------------------------------
**/

#ifndef OL_APPLICATION_H_INCLUDED
#define OL_APPLICATION_H_INCLUDED

#include "GLee.h"
#include "../../ol_Scene.h"
#include "../../ol_2D_Scene.h"
#include "../../ol_Basic_Shapes.h"
#include "../ol_System.h"
#include "../version.h"

using namespace std;

// Contains Information Vital To Applications
struct ol_ApplData
{
    Display   *dpy;
    Window     win;

    XVisualInfo         *vi;
    Colormap             cmap;
    XSetWindowAttributes swa;
    GLXContext           cx;
    int                  dummy;

};

// Contains Information Vital To A Window
class ol_GLWindow
{
public:
    ol_ApplData			application;				// Application Structure
    const char*			title;						// Window Title
    ol_Color			clearColor;					// Clear color
    bool				isFullScreen;				// FullScreen?
    bool				isVisible;					// Window Visible?
    ol_CoreParams		system;                     // Object containing some params that are used by other objects conjointly

    ol_GLWindow() {}
    ~ol_GLWindow() {}
};

// User Initialization parameters
struct ol_ApplInitParams
{
    const char* 	title;							// Window Title
    int				width;							// Width
    int				height;							// Height
    int				bitsPerPixel;					// Bits Per Pixel
    float			depth;							// Depth of view
    ol_Color		bkColor;						// Background color
    bool			isFullScreen;					// FullScreen?
};

class ol_Application
{
private:
    char version[128];
    ol_GLWindow          window;
    ol_Event             *ioEvent;   // события ввода - вывода (клавиатура, мышь и т.д.)
    ol_GameEvent         *gEvent;    // игровые события (объекты сцены, гуи элементы)
    ol_SceneManager      *smgr;      // управление сценой
    ol_SceneManager2d    *smgr2d;    // управление объектами 2Д сцены

public:
    static ol_Graphics *graph;       // загрузка текстур, материалы, графические режимы, некоторые ф-ии отрисовки
    static ol_GUIManager *guimgr;    // управление гуи элементами управления

    ol_Application();
    ~ol_Application();

    void Init(ol_ApplInitParams prms);
    void TerminateApplication(void);	            // Terminate The Application
    void ToggleFullscreen(void);		            // Toggle Fullscreen / Windowed Mode
    bool CreateWindowGL(int argc, char **argv);
    bool DestroyWindowGL();
    bool ChangeScreenResolution(int width, int height, int bitsPerPixel);	// Change The Screen Resolution

    void InitGL(void);
    void ReshapeGL(int width, int height);

    ol_GLWindow*         getWindow(void);
    ol_SceneManager*     getSceneManager(void) const;
    ol_Graphics*         getGraphicsInterface(void) const;
    ol_Event*            getEvent(void) const;
    ol_GameEvent*        getGameEvent(void) const;
    ol_GUIManager*       getGUIManager(void) const;
    ol_SceneManager2d*   getSceneManager2d(void) const;

    void setVersion(const char stat[], const char ver[]);
    char *getVersion(void);

    void setWindowTitle(const char tit[]);  // Sets the title of window

    bool isProgramLooping;	 // Если становится false, главный цикл отключается
};

void fatalError(const char *message);

/**
    Вызывается внутри main/winmain но определяется внутри главного файла приложения.
    Сделана, для того, чтобы скрыть main и весь каркас glx/wgl приложения,
    а пользователю предоставить простой интерфейс главного файла приложения
**/
void InitOLGA(ol_Application *appl);

bool UserInitialize(void);
void UserDeinitialize(void);
void UserDraw(void);
void UserUpdate(ol_Event *event);
void UserOnEvent(ol_GameEvent *event);

#endif // OL_APPLICATION_H_INCLUDED
