/**
------------------------------------------------------------------------
  This file is part of the OLGA project.
  Copyright © 2017 by Enver Akhmedov.
  E-mail: aen-code@yandex.ru
  For conditions of distribution and use, see copyright notice in OLGA.h
------------------------------------------------------------------------
**/

#include "ol_Application.h"

// Инициализация статических параметров --------------------
unsigned int    ol_CoreParams::fps = 0;
unsigned int    ol_CoreParams::vfps = 0;
float           ol_CoreParams::fdt = 0.01f;
ol_Timer*       ol_CoreParams::timer = new ol_Timer(); // выделяем память под системный таймер
char*           ol_CoreParams::locale_name = new char [256];
ol_Graphics*    ol_Application::graph = new ol_Graphics();
ol_GUIManager*  ol_Application::guimgr = new ol_GUIManager(ol_Application::graph);    // управление гуи элементами управления
//----------------------------------------------------------

ol_Application       *mainApplication;
ol_CoreParams        *olga_system;
ol_Event             *olga_event;
ol_GameEvent         *olga_gEvent;
ol_Timer             *olga_timer;
ol_SceneManager      *olga_smgr;
ol_SceneManager2d    *olga_smgr2d;
ol_GUIManager        *olga_guimgr;
ol_Graphics          *olga_graph;
ol_GLWindow          *olga_window;

XEvent  event;

void UpdateKeyboardEvents(ol_Event *event, char keys[32], char sample[8])
{
    short keycode;

    for(unsigned int i = 0; i<32; i++)
    {
        if(0 == (int)keys[i]) continue;

        for(unsigned int j = 0; j<8; j++)
        {
            if( (sample[j]&keys[i]) == sample[j] ) // Клавиша нажата
            {
                keycode = i*8 + j;
                event->keys->keyDown[keycode] = true; // устанавливаем флаг клавиши в значение истина
            }
        }
    }
}

void UpdateMouseEvents(ol_Event *event, ol_Vector2df &cur_pos, unsigned int &mbuttons)
{
    // Обработка движения курсора ---------------------------------------------------------
    if(cur_pos!=event->getCurPos())
    {
        event->mParams->ResetCursor();
        event->setCurPos(cur_pos);
        event->mParams->UpdateShift();
//        event->mParams->mouseEvents[OL_EV_MOUSEMOVE] = true;
//        event->type[OL_ETYPE_MOUSE] = true;
    }
    else
    {
//        event->mParams->mouseEvents[OL_EV_MOUSEMOVE] = false;
        event->mParams->ResetShift();
    }

    // Обработка нажатия левой клавиши мышки ------------------------------------------------
    if((mbuttons&Button1Mask) == Button1Mask)
    {
        event->keys->keyDown[OL_KEY_LBUTTON] = true;
    }

    // Обработка нажатия средней клавиши мышки ------------------------------------------------
    if((mbuttons&Button2Mask) == Button2Mask)
    {
        event->keys->keyDown[OL_KEY_MBUTTON] = true;
    }

    // Обработка нажатия правой клавиши мышки ------------------------------------------------
    if((mbuttons&Button3Mask) == Button3Mask)
    {
        event->keys->keyDown[OL_KEY_RBUTTON] = true;
    }
}
/**
    ----------------------------------------------------------------------------------------
    >                                                                                      <
    >                                      DRAW                                            <
    >                                                                                      <
    ----------------------------------------------------------------------------------------
**/

void DrawAll(void)
{
    glClear (GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);		// Clear Screen And Depth Buffer
    glLoadIdentity();											// Reset The Modelview Matrix

    olga_smgr->DrawAll();
    UserDraw();

    olga_graph->PushScreenCoordinateMatrix();
    olga_smgr2d->DrawAll();
    olga_guimgr->DrawAll();
    olga_graph->PopProjectionMatrix();

    glXSwapBuffers(olga_window->application.dpy, olga_window->application.win);
}

/**
    ----------------------------------------------------------------------------------------
    >                                                                                      <
    >                                      UPDATE                                          <
    >                                                                                      <
    ----------------------------------------------------------------------------------------
**/

void UpdateAll(ol_Event *event)
{
// Генерация и обработка событий пользовательского интерфейса ----------
    ol_GUIEvent *guiEvent = olga_guimgr->CheckGUIEvents(event);
    if(guiEvent)
    {
        olga_gEvent->gevent = guiEvent;
        olga_gEvent->type = OL_GETYPE_GUI;
        UserOnEvent(olga_gEvent);
    }

// Генерация и обработка событий 3д сцены-------------------------------
    olga_smgr->CheckSceneEvents(event);
    ol_SceneEvent *sEvent = olga_smgr->getLastEvent();
    while(sEvent)
    {
        olga_gEvent->sevent = sEvent;
        olga_gEvent->type = OL_GETYPE_SCENE;

        UserOnEvent(olga_gEvent);
        sEvent = olga_smgr->getLastEvent();
    }

// Генерация и обработка событий 2д сцены-------------------------------
    olga_smgr2d->CheckSceneEvents(event);
    sEvent = olga_smgr->getLastEvent();
    while(sEvent)
    {
        olga_gEvent->sevent = sEvent;
        olga_gEvent->type = OL_GETYPE_SCENE;

        UserOnEvent(olga_gEvent);
        sEvent = olga_smgr->getLastEvent();
    }

// ---------------------------------------------------------------------

    UserUpdate(event);

    olga_guimgr->UpdateAll();
    olga_smgr2d->UpdateAll();
    olga_smgr->UpdateAll();
}

/**
    ----------------------------------------------------------------------------------------
    >                                                                                      <
    >                                      MAIN                                            <
    >                                                                                      <
    ----------------------------------------------------------------------------------------
**/

int main(int argc, char **argv)
{
//    double timerT1, timerT2, perfomance; // для определения virtual fps
    unsigned int frameConuter = 0;       // счетчик кадров для определения реального fps

// Для обработки событий мышки ---------------------------------------------------------------------------------------------
    int root_x, root_y;         // Координаты курсора относительно главного окна(или относительно экрана, если окно главное)
    int pos_x, pos_y;           // Координаты курсора относительно окна
    ol_Vector2df cur_pos;        // для передачи координат в UpdateMouseEvents()
    Window root, child;         // Сюда возвращаются указатели на главное окно и дочернее(если такое имеется)
    unsigned int keys_mbuttons; // Состояние кнопок мыши

// Для обработки событий клавиатуры ----------------------------------------------------------------------------------------
    char keys_keyboard[32];
    char key_sample[8] = {b_0, b_1, b_2, b_3, b_4, b_5, b_6, b_7};
    XIM xim; // Xlib input method
    XIC xic; // Xlib input context

    wchar_t buffer[1];
    Status stat;
    KeySym ks;
//    const char *s;
//    int xkeyinpresult;
// -------------------------------------------------------------------------------------------------------------------------

    mainApplication = new ol_Application();

    InitOLGA(mainApplication);

    if(mainApplication)
    {
        mainApplication->setVersion(ol_Version::STATUS, ol_Version::FULLVERSION_STRING);

        olga_timer    = ol_CoreParams::timer;

        olga_window   = mainApplication->getWindow();
        olga_system   = &olga_window->system;
        olga_event    = mainApplication->getEvent();
        olga_smgr     = mainApplication->getSceneManager();
        olga_smgr2d   = mainApplication->getSceneManager2d();
        olga_guimgr   = mainApplication->getGUIManager();
        olga_graph    = mainApplication->getGraphicsInterface();
        olga_gEvent   = mainApplication->getGameEvent();

        printf("OLGA - %s\n\n", mainApplication->getVersion());
    }
    else fatalError("Error (main): OLGA can not be inited!");

    if(!mainApplication->CreateWindowGL(argc, argv)) fatalError("Error (main): GL window creation failed.");

    xim = XOpenIM (olga_window->application.dpy, NULL, NULL, NULL);
    if(!xim) fatalError("Error (main): XOpenIM failed.\n");

    xic = XCreateIC (xim, XNInputStyle, XIMPreeditNothing | XIMStatusNothing, XNClientWindow, olga_window->application.win, XNFocusWindow, olga_window->application.win, NULL);
    if(!xic) fatalError("Error (main): XCreateIC failed.\n");

    setlocale(LC_NUMERIC, "en_US.utf8");

    strcpy(ol_CoreParams::locale_name, XLocaleOfIM(xim));
    printf("locale = %s  ", ol_CoreParams::locale_name);

    if(XSupportsLocale()) printf("Locale supported!\n");
    else printf("Locale is NOT supported!\n");

    printf("\nInitialization ---------------------------------------------\n");
    if(!UserInitialize())
    {
        mainApplication->TerminateApplication();
        printf("Error (main): User Initialization failed.\n");
    }

    olga_timer->startTimer();
    mainApplication->isProgramLooping = true;

    while (mainApplication->isProgramLooping)
    {
        olga_event->ResetAll();

        if(XCheckWindowEvent(olga_window->application.dpy, olga_window->application.win, KeyPressMask | KeyReleaseMask | ButtonPressMask | ButtonReleaseMask | ExposureMask | StructureNotifyMask, &event))
        {
            switch (event.type)
            {
            case KeyPress:
                olga_event->type = OL_IETYPE_KEYBOARD;
                olga_event->keys->kType = OL_KEV_KEYDOWN;
                olga_event->keys->key_code = event.xkey.keycode;

//              if (XFilterEvent(&event, olga_window->application.win) == True) break;

                /* xkeyinpresult = */XwcLookupString(xic, &event.xkey, buffer, 1, &ks, &stat);
                /*                    s = XKeysymToString (ks);
                                    printf("XwcLookupString = %d, KeySum = %s\n", xkeyinpresult, s);

                                    switch (stat)
                                    {
                                    case XBufferOverflow:
                                        printf ("stat = XBufferOverflow\n");
                                        break;
                                    case XLookupNone:
                                        printf ("stat = XLookupNone\n");
                                        break;
                                    case XLookupChars:
                                        printf ("stat = XLookupChars\n");
                                        break;
                                    case XLookupKeySym:
                                        printf ("stat = XLookupKeySym\n");
                                        break;
                                    case XLookupBoth:
                                        printf ("stat = XLookupBoth\n");
                                        break;
                                    }
                */
                if(((stat == XLookupChars) || (stat == XLookupBoth)) && (ks != NoSymbol) )
                {
                    olga_event->keys->char_code = (unsigned int)buffer[0];
                }
                else olga_event->keys->char_code = 0;

                break;

            case KeyRelease:
                olga_event->type = OL_IETYPE_KEYBOARD;
                olga_event->keys->kType = OL_KEV_KEYUP;
                olga_event->keys->key_code = event.xkey.keycode;
                break;

            case ButtonPress:
                olga_event->type = OL_IETYPE_MOUSE;

                switch (event.xbutton.button)
                {
                case 1:
                    olga_event->mParams->mType = OL_MEV_LBUTTONDOWN;
                    break;
                case 2:
                    olga_event->mParams->mType = OL_MEV_MBUTTONDOWN;
                    break;
                case 3:
                    olga_event->mParams->mType = OL_MEV_RBUTTONDOWN;
                    break;
                case 4:
                    olga_event->mParams->mType = OL_MEV_WHEEL;
                    olga_event->mParams->wheel = 1;
//                    olga_event->mParams->mouseEvents[OL_EV_MOUSEWHEEL] = true;
//                    olga_event->type[OL_ETYPE_MOUSE] = true;
                    break;
                case 5:
                    olga_event->mParams->mType = OL_MEV_WHEEL;
                    olga_event->mParams->wheel = -1;
//                    olga_event->mParams->mouseEvents[OL_EV_MOUSEWHEEL] = true;
//                    olga_event->type[OL_ETYPE_MOUSE] = true;
                    break;
                }
                break;

            case ButtonRelease:
                olga_event->type = OL_IETYPE_MOUSE;

                switch (event.xbutton.button)
                {
                case 1:
                    olga_event->mParams->mType = OL_MEV_LBUTTONUP;
                    break;
                case 2:
                    olga_event->mParams->mType = OL_MEV_MBUTTONUP;
                    break;
                case 3:
                    olga_event->mParams->mType = OL_MEV_RBUTTONUP;
                    break;
                }
                break;

//            case MotionNotify:
//                olga_event->type = OL_ETYPE_MOUSE;
//                olga_event->event = OL_EV_MOUSEMOVE;
//                break;

            case MappingNotify:
                XRefreshKeyboardMapping(&event.xmapping);
                break;

            case ConfigureNotify:
                mainApplication->ReshapeGL(event.xconfigure.width,  event.xconfigure.height);
                break;

            case Expose:

                break;

            case DestroyNotify:
//                mainApplication->TerminateApplication();
                break;

            case ClientMessage:

                break;

            default:
                break;
            }
        }

        XQueryKeymap(olga_window->application.dpy, keys_keyboard);		// получаем вектор прерываний клавиатуры
        UpdateKeyboardEvents(olga_event, keys_keyboard, key_sample);	// обрабатываем вектор прерываний

        // получаем состояния мышки
        if(XQueryPointer(olga_window->application.dpy, olga_window->application.win, &root, &child, &root_x, &root_y, &pos_x, &pos_y, &keys_mbuttons))
        {
            // Обрабатываем состояния мышки
            cur_pos.x = pos_x;
            cur_pos.y = olga_system->height - pos_y; // переводим в сист. коорд. где ось У направлена вверх и начинается в левом нижнем углу окна

            UpdateMouseEvents(olga_event, cur_pos, keys_mbuttons);
        }

        olga_timer->UpdateMonitors();

//      timerT1 = olga_timer->readTimer();
        UpdateAll(olga_event);
        DrawAll();
//      timerT2 = olga_timer->readTimer();
//      perfomance = 1.0f/(timerT2 - timerT1);

        frameConuter++;
        if(olga_timer->getTimeInterval()>=1.0)
        {
            ol_CoreParams::fps = frameConuter;
            ol_CoreParams::fdt = 1.0/frameConuter;
//            ol_CoreParams::vfps = perfomance;
            frameConuter = 0;
            olga_timer->updateTimer();
        }
    }

    printf("Clean Up ---------------------------------------------\n");

    delete mainApplication;
    delete ol_Application::graph;
    delete ol_Application::guimgr;
    delete ol_CoreParams::timer;

    UserDeinitialize();

    XDestroyIC (xic);
    XCloseIM (xim);

	setlocale(LC_ALL, "");   // Restore default locale settings
    printf("Default locale settings have been restored.\n");

    printf("OLGA is closed. :'( \n");

    return 0;
}
