/**
------------------------------------------------------------------------
  This file is part of the OLGA project.
  Copyright © 2017 by Enver Akhmedov.
  E-mail: aen-code@yandex.ru
  For conditions of distribution and use, see copyright notice in OLGA.h
------------------------------------------------------------------------
**/

#ifndef OL_TIMER_H_INCLUDED
#define OL_TIMER_H_INCLUDED

#include <sys/time.h>
#include <list>

using namespace std;

class ol_TimeMonitor
{
private:
    double lastTime;

public:
    friend class ol_Timer;
    ol_TimeMonitor()
    {
        itIsTime = false;
    }
    ~ol_TimeMonitor() {}

    double timeInterval;
    bool itIsTime;
};

//класс таймера. Значения всех интервалов времени измеряются в секундах с точностью до микросекунды
class ol_Timer
{
    struct timeval tval;        // предается в качестве аргумента в gettimeofday
    double currentCheckTime;    // текущий замер времени
    double lastCheckTime;       // предыдущий замер времени
    double globalTime;          // время прошедшее с момента включения таймера. Обновляется рефреш таймером
    list<ol_TimeMonitor *> monitors;

public:
    ol_Timer() {}
    ~ol_Timer() {}

    // вернуть время (от начального времени)
    double readTimer(void);
    // запустить таймер
    void startTimer(void);
    // обновить таймер
    void updateTimer(void);
    // обновляет значение времени прошедшего с момента старта или апдейта
    void refreshTimer(void);
    // получить временной отрезок между текущим замером и предыдущим
    double getTimeInterval(void);
    // возвращает время в с момента старта таймера
    double getTime(void);
    // создает монитор временного интервала и возвращает на него указатель
    ol_TimeMonitor *addTimeMonitor(double interval);
    // удаляет монитор
    void RemoveTimeMonitior(ol_TimeMonitor *mon);
    // проверяет временные засечки всх мониторов
    void UpdateMonitors(void);
};

#endif // OL_TIMER_H_INCLUDED
