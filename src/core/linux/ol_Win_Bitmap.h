/**
------------------------------------------------------------------------
  This file is part of the OLGA project.
  Copyright © 2017 by Enver Akhmedov.
  E-mail: aen-code@yandex.ru
  For conditions of distribution and use, see copyright notice in OLGA.h
------------------------------------------------------------------------
**/

#ifndef OL_WIN_BITMAP_H_INCLUDED
#define OL_WIN_BITMAP_H_INCLUDED

#include <stdint.h>

typedef uint8_t     BYTE;     // 1
typedef uint16_t    WORD;     // 2
typedef uint32_t    DWORD;    // 4
typedef long        LONG;     // signed 4

typedef struct
{
    WORD    bfType;
    WORD    bfReserved1;
    DWORD   bfSize;
    DWORD   bfOffBits;
    WORD    bfReserved2;
} BITMAPFILEHEADER, *PBITMAPFILEHEADER;

typedef struct
{
    DWORD  biSize;
    LONG   biWidth;
    LONG   biHeight;
    WORD   biPlanes;
    WORD   biBitCount;
    DWORD  biCompression;
    DWORD  biSizeImage;
    LONG   biXPelsPerMeter;
    LONG   biYPelsPerMeter;
    DWORD  biClrUsed;
    DWORD  biClrImportant;
} BITMAPINFOHEADER, *PBITMAPINFOHEADER;

#endif // OL_WIN_BITMAP_H_INCLUDED
