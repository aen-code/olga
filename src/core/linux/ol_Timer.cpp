/**
------------------------------------------------------------------------
  This file is part of the OLGA project.
  Copyright © 2017 by Enver Akhmedov.
  E-mail: aen-code@yandex.ru
  For conditions of distribution and use, see copyright notice in OLGA.h
------------------------------------------------------------------------
**/

#include "ol_Timer.h"
#include <stddef.h>  //для NULL

void ol_Timer::startTimer(void)
{
    lastCheckTime = currentCheckTime = globalTime = readTimer();
}

double ol_Timer::readTimer(void)
{
    gettimeofday(&tval, NULL);

    return tval.tv_sec + 0.000001*tval.tv_usec;
}

double ol_Timer::getTimeInterval(void)
{
    currentCheckTime = readTimer();

    return currentCheckTime - lastCheckTime;
}

double ol_Timer::getTime(void)
{
    return  readTimer() - globalTime;
}

void ol_Timer::refreshTimer(void)
{
    globalTime = readTimer();
}

void ol_Timer::updateTimer(void)
{
    //текущий замер становится прошлым
    lastCheckTime = currentCheckTime;
}

ol_TimeMonitor* ol_Timer::addTimeMonitor(double interval)
{
    ol_TimeMonitor *mon = new ol_TimeMonitor();

    mon->timeInterval = interval;
    mon->lastTime = readTimer();

    monitors.push_back(mon);
    return mon;
}

void ol_Timer::RemoveTimeMonitior(ol_TimeMonitor *mon)
{
    monitors.remove(mon);
    delete mon;
}

void ol_Timer::UpdateMonitors(void)
{
    if(monitors.empty()) return;

    list<ol_TimeMonitor *>::iterator it;
    double curTime = readTimer();

    for(it = monitors.begin(); it!=monitors.end(); ++it)
    {
        if((*it)->itIsTime)
        {
            (*it)->itIsTime = false;
        }
        else if((curTime - (*it)->lastTime)>=(*it)->timeInterval)
        {
            (*it)->itIsTime = true;
            (*it)->lastTime = curTime;
        }
    }
}
