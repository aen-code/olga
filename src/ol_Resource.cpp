/**
------------------------------------------------------------------------
  This file is part of the OLGA project.
  Copyright © 2017 by Enver Akhmedov.
  E-mail: aen-code@yandex.ru
  For conditions of distribution and use, see copyright notice in OLGA.h
------------------------------------------------------------------------
**/

#include "ol_Resource.h"
#include <stdlib.h>

/**
    --------------------------------- Resource Manager -------------------------------------
    >                                                                                      <
    >   Класс для управления загрузкой и сохранением данных различных форматов, таких      <
    >   как 2д изображения и 3д модели.                                                    <
    >                                                                                      <
    ----------------------------------------------------------------------------------------
**/

void ol_ResourceManager::ClearBufs(void)
{
    vertBuf.clear();
    normBuf.clear();
    texcBuf.clear();
    indBuf.clear();
}

bool ol_ResourceManager::LoadMeshOL(ol_Mesh3D *mesh, const char *fileName)
{
    if(!mesh || !fileName) return false;

    FILE *fp = fopen(fileName, "rt");
    if(fp==NULL)
    {
        cout<<"Unable to find or open the file: "<<fileName<<"\n";
        return false;
    }

    char strLine[255] = {0};
    char ch = 0;

    ol_Vector3d vnbuf;
    ol_Vector2df tcbuf;
    unsigned int ibuf;

    ClearBufs();

    while(!feof(fp))
    {
        ch = fgetc(fp);

        switch(ch)
        {
        case 'v':
            fscanf(fp, "%f %f %f", &vnbuf.x, &vnbuf.y, &vnbuf.z);
            fgets(strLine, 100, fp);
            vertBuf.push_back(vnbuf);
            break;

        case 'n':
            fscanf(fp, "%f %f %f", &vnbuf.x, &vnbuf.y, &vnbuf.z);
            fgets(strLine, 100, fp);
            normBuf.push_back(vnbuf);
            break;

        case 't':
            break;

        case 'i':
            for(unsigned int i=0; i<3; i++)
            {
                fscanf(fp, "%d", &ibuf);
                indBuf.push_back(ibuf);
            }
            fgets(strLine, 100, fp);
            break;

        case '\n':
            break;

        default:
            // If we get here then we don't care about the line being read, so read past it.
            fgets(strLine, 100, fp);
            break;
        }
    }

    fclose(fp);

    if(vertBuf.size()!=normBuf.size()) printf("Error: Different length of vertex and normal arrays!\n");

    mesh->InitVerts(vertBuf);
    mesh->InitNormals(normBuf);
    mesh->InitIndexes(indBuf);
    mesh->CalcCenter();

    ClearBufs();

    return true;
}

bool ol_ResourceManager::LoadOBJ(ol_Mesh3D *mesh, const char *fileName)
{
	if(!mesh || !fileName) return false;

	char strLine[255] = {0}; 	// Buffer to read string line from the input file.
	char ch;					// Buffer to read single character.
	char ch1;					// One more buffer, because we need to parse a pair of characters: "vt", "vn" ...
	int res = 1;				// Variable to catch error if it happens.
	char *cr;					// One more variable to hold file reading result.
	ol_Vector3d vb;				// Buffer variable to store 3D vertex or normal.
	ol_Vector2df tb;			// Buffer variable to store 2D texture coordinate.
	int argNum;					// Used to find out weather we have read triangle or a quad.
	// Following are the three arrays to store vertex, UV and texture indexes of the single face that we have read.
	int vi[4] = {0};
	int ti[4] = {0};
	int ni[4] = {0};
	// The order of indexes that will cut a quad face into two triangles.
	int fi[6] = {0, 1, 2, 0, 2, 3};
	ol_TriangleMesh trMesh; // Buffer mesh that helps us to prepare mesh for the format that OLGA uses.
	ol_Vertex vertex;		// A vertex of the buffer mesh.

	bool hasUV = false;		// Becomes true if we have met a texture coordinate in the OBJ file.
	bool hasNorms = false;	// The same for the normal.

	FILE *fp = fopen(fileName, "rt");
	if(fp==NULL)
	{
		cout<<"Unable to find or open the file: "<<fileName<<"\n";
		return false;
	}

	while(!feof(fp))
	{
		// Get the beginning character of the current line in the .obj file
		ch = fgetc(fp);

		/* Check if we just read in a 'v' (Could be a vertice/normal/textureCoord).
		 * Decipher this line to see if it's a vertex ("v"), normal ("vn"), or UV coordinate ("vt"). */
		if(ch == 'v')
		{
			// Read the next character in the file to see if it's a vertice/normal/UVCoord
			ch1 = fgetc(fp);
			switch (ch1)
			{
			case ' ': // If we get a space it must have been a vertex ("v")
				// Here we read in a vertice.  The format is "v x y z"
				res = fscanf(fp, "%f %f %f", &vb.x, &vb.y, &vb.z);
				// Read the rest of the line so the file pointer returns to the next line.
				cr = fgets(strLine, 100, fp);
				// Add a new vertice to our list
				vertBuf.push_back(vb);
				break;

			case 't': // If we get a 't' then it must be a texture coordinate ("vt")
				// Here we read in a texture coordinate.  The format is "vt u v"
				res = fscanf(fp, "%f %f", &tb.x, &tb.y);
				// Read the rest of the line so the file pointer returns to the next line.
				cr = fgets(strLine, 100, fp);
				// Add a new texture coordinate to our list
				texcBuf.push_back(tb);
				/* Set the flag that tells us this object has texture coordinates.
				 * Now we know that the face information will list the vertice AND UV index.
				 * For example, ("f 1 3 2" verses "f 1/1 2/2 3/3") */
				hasUV = true;
				break;

			case 'n': // If we get a 'n' then it must be a normal ("vn")
				res = fscanf(fp, "%f %f %f", &vb.x, &vb.y, &vb.z);
				cr = fgets(strLine, 100, fp);
				normBuf.push_back(vb);
				hasNorms = true;
				break;
		
			default:
				cr = fgets(strLine, 100, fp);
				break;
			}
			if(res<=0 || cr==NULL)
			{
				printf("Error: chto-to, blyaha-muha, stalo pri popitke chtenia v ol_LoadObj::ReadVertexInfo()");
				return false;
			} 
		}

		/* Check if we just read in a face header ('f').
		 * If we get here we then need to read in the face information.
		 * The face line holds indexes of vertex/UV/normal arrays that form single polygon. */
		if(ch == 'f')
		{
			// get the line for analyse
			fgets(strLine, 255, fp);
			argNum = CalcArgNum(strLine);
//			printf("argNum = %d; strLine = %s\n", argNum, strLine);
			// It is a triangle polygon. -------------------------------
			if(argNum == 3)
			{
				if(!hasUV && !hasNorms)
				{
					sscanf(strLine, "%d %d %d", &vi[0], &vi[1], &vi[2]);
					for(int i=0; i<=2; i++)
					{
						vertex.vert = vertBuf[vi[i]-1];
						trMesh.AddVertex(vertex);
					}
				}
				if(hasUV && !hasNorms)
				{
					sscanf(strLine, "%d/%d %d/%d %d/%d",  &vi[0], &ti[0], &vi[1], &ti[1], &vi[2], &ti[2]);
					for(int i=0; i<=2; i++)
					{
						vertex.vert = vertBuf[vi[i]-1];
						vertex.tex = texcBuf[ti[i]-1];
						trMesh.AddVertex(vertex);
					}
				}
				if(!hasUV && hasNorms)
				{
					sscanf(strLine, "%d//%d %d//%d %d//%d",  &vi[0], &ni[0], &vi[1], &ni[1], &vi[2], &ni[2]);
					for(int i=0; i<3; i++)
					{
						vertex.vert = vertBuf[vi[fi[i]]-1];
						vertex.norm = normBuf[ni[fi[i]]-1];
						trMesh.AddVertex(vertex);
					}
				}
				if(hasUV && hasNorms)
				{
					sscanf(strLine, "%d/%d/%d %d/%d/%d %d/%d/%d",  &vi[0], &ti[0], &ni[0], &vi[1], &ti[1], &ni[1], &vi[2], &ti[2], &ni[2]);
					for(int i=0; i<=2; i++)
					{
						vertex.vert = vertBuf[vi[i]-1];
						vertex.tex = texcBuf[ti[i]-1];
						vertex.norm = normBuf[ni[i]-1];
						trMesh.AddVertex(vertex);
					}
				}
			}
			// It is a quad polygon. -----------------------------------
			if(argNum == 4)
			{
				if(!hasUV && !hasNorms)
				{
					sscanf(strLine, "%d %d %d %d", &vi[0], &vi[1], &vi[2], &vi[3]);
					for(int i=0; i<6; i++)
					{
						vertex.vert = vertBuf[vi[fi[i]]-1];
						trMesh.AddVertex(vertex);
					}
				}
				if(hasUV && !hasNorms)
				{
					sscanf(strLine, "%d/%d %d/%d %d/%d %d/%d",  &vi[0], &ti[0], &vi[1], &ti[1], &vi[2], &ti[2], &vi[3], &ti[3]);
					for(int i=0; i<6; i++)
					{
						vertex.vert = vertBuf[vi[fi[i]]-1];
						vertex.tex = texcBuf[ti[fi[i]]-1];
						trMesh.AddVertex(vertex);
					}
				}
				if(!hasUV && hasNorms)
				{
					sscanf(strLine, "%d//%d %d//%d %d//%d %d//%d",  &vi[0], &ni[0], &vi[1], &ni[1], &vi[2], &ni[2], &vi[3], &ni[3]);
					for(int i=0; i<6; i++)
					{
						vertex.vert = vertBuf[vi[fi[i]]-1];
						vertex.norm = normBuf[ni[fi[i]]-1];
						trMesh.AddVertex(vertex);
					}
				}
				if(hasUV && hasNorms)
				{
					sscanf(strLine, "%d/%d/%d %d/%d/%d %d/%d/%d %d/%d/%d",  &vi[0], &ti[0], &ni[0], &vi[1], &ti[1], &ni[1], &vi[2], &ti[2], &ni[2], &vi[3], &ti[3], &ni[3]);
					for(int i=0; i<6; i++)
					{
						vertex.vert = vertBuf[vi[fi[i]]-1];
						vertex.tex = texcBuf[ti[fi[i]]-1];
						vertex.norm = normBuf[ni[fi[i]]-1];
						trMesh.AddVertex(vertex);
					}
				}
			}
			//if(argNum!=3 && argNum!=4) cr = fgets(strLine, 100, fp); 
		}

		/* If we read in a newline character, we've encountered a blank line in
		 * the *.obj file.  We don't want to do the default case and skip another
		 * line, so we just break and do nothing. */
		if(ch == '\n' || ch!='v' || ch!='f') continue;
	}

	mesh->InitIndexes(trMesh.indexes);
	mesh->InitVerts(trMesh.verts);
	if(hasUV) mesh->InitTexCoords(trMesh.texcoords);
	if(hasNorms) mesh->InitNormals(trMesh.norms);
	else mesh->CalcNormals();

	mesh->CalcCenter();

	ClearBufs();

	return true;
}

bool ol_ResourceManager::LoadTGA(const char *fileName, ol_TextureImage *texture )
{
    GLubyte  TGAheader[12]      = {0,0,2,0,0,0,0,0,0,0,0,0};    // Заголовок несжатого TGA файла 24 или 32-битного
    GLubyte  TGAheaderComp[12]  = {0,0,10,0,0,0,0,0,0,0,0,0};   // Заголовок сжатого TGA файла
    GLubyte  TGAcompare[12];                                    // Используется для сравнения заголовка TGA файла
    GLubyte  header[6];                                         // Первые 6 полезных байт заголовка
    GLuint   bytesPerPixel;                                     // Количество байтов на пиксель используемое в TGA файле
    GLuint   imageSize;                                         // Количество байтов, необходимое для хранения изображения в памяти
    bool     compressed = false;                                // Принимает значение true если файл со сжатием

    FILE *file = fopen(fileName, "rb");  // Открытие TGA файла

    // Если заголовок правильный, Если прочитаны следующие 6 байтов
    if(file!=NULL)
    {
        if(fread(TGAcompare,1,sizeof(TGAcompare),file)!=sizeof(TGAcompare))
        {
            fclose(file);                               // Если ошибка, закрываем файл
            cout<<"Error(LoadTGA): Invalid Header Size\n";
            return false;                               // Возвращаем false
        }

        if(memcmp(TGAheaderComp,TGAcompare,sizeof(TGAheaderComp)) == 0)
        {
            compressed = true;
        }
        else
        {
            if(memcmp(TGAheader,TGAcompare,sizeof(TGAheader)) != 0)
            {
                fclose(file);                               // Если ошибка, закрываем файл
                cout<<"Error(LoadTGA): Unknown Header\n";
                return false;                               // Возвращаем false
            }
        }

        if(fread(header,1,sizeof(header),file)!=sizeof(header))
        {
            fclose(file);                               // Если ошибка, закрываем файл
            cout<<"Error(LoadTGA): Error Reading Header\n";
            return false;                               // Возвращаем false
        }
    }
    else
    {
        cout<<"Error(LoadTGA): Error Opening File!\n";
        return false;
    }

    // Общие операции для сжатых и несжатых -----------------------
    // Определяем ширину TGA (старший байт*256+младший байт)
    texture->width  = header[1] * 256 + header[0];
    // Определяем высоту TGA (старший байт*256+младший байт)
    texture->height = header[3] * 256 + header[2];

    // Если ширина меньше или равна нулю; если высота меньше или равна нулю; является ли TGA 24 или 32-битным?
    if(texture->width<=0||texture->height<=0||(header[4]!=24 && header[4]!=32))
    {
        fclose(file);                                   // Если где-то ошибка, закрываем файл
        return false;                                   // Возвращаем false
    }

    texture->bpp  = header[4];                          // Получаем TGA бит/пиксель (24 или 32)
    bytesPerPixel = texture->bpp/8;                     // Делим на 8 для получения байт/пиксель

    // Подсчитываем размер памяти для данных TGA
    imageSize = texture->width*texture->height*bytesPerPixel;

    texture->imageData=(GLubyte *)malloc(imageSize);    // Резервируем память для хранения данных TGA
    //-------------------------------------------------------------
    if(compressed)
    {
        GLuint pixelcount = texture->height * texture->width;  // Количество пикселей в изображении
        GLuint currentpixel  = 0;     // Пиксель с который мы сейчас считываем
        GLuint currentbyte  = 0;      // Байт который мы зарисуем в Imagedata

        // Хранилище для одного пикселя
        GLubyte * colorbuffer = (GLubyte *)malloc(bytesPerPixel);

        do  // Начало цикла
        {
            GLubyte chunkheader = 0; // Значение для хранения идентификатора секции

            // Пытаемся считать идентификатора секции
            if(fread(&chunkheader, sizeof(GLubyte), 1, file) == 0)
            {
                //            ...Код ошибки...
                return false;        // Если неудача, возвращаем False
            }

            if(chunkheader < 128)   // Если секция является 'RAW' секцией
            {
                chunkheader++;        // Добавляем единицу для получения количества RAW пикселей
                // Начало цикла чтения пикселей
                for(short counter = 0; counter < chunkheader; counter++)
                {
                    // Пытаемся прочитать 1 пиксель
                    if(fread(colorbuffer, 1, bytesPerPixel, file) != bytesPerPixel)
                    {
                        //                    ...Код ошибки...
                        return false;      // Если ошибка, возвращаем False
                    }

                    texture->imageData[currentbyte] = colorbuffer[2];       // Записать байт 'R'
                    texture->imageData[currentbyte + 1  ] = colorbuffer[1]; // Записать байт 'G'
                    texture->imageData[currentbyte + 2  ] = colorbuffer[0]; // Записать байт 'B'

                    if(bytesPerPixel == 4)          // Если это 32bpp изображение...
                    {
                        texture->imageData[currentbyte + 3] = colorbuffer[3];  // Записать байт 'A'
                    }

                    // Увеличиваем счетчик байтов на значение равное количеству байт на пиксель
                    currentbyte += bytesPerPixel;
                    currentpixel++;          // Увеличиваем количество пикселей на 1
                }
            }
            else  // Если это RLE идентификатор
            {
                chunkheader -= 127; // Вычитаем 127 для получения количества повторений

                // Читаем следующий пиксель
                if(fread(colorbuffer, 1, bytesPerPixel, file) != bytesPerPixel)
                {
                    //                        ...Код ошибки...
                    return false;        // Если ошибка, возвращаем False
                }

                // Начинаем цикл
                for(short counter = 0; counter < chunkheader; counter++)
                {
                    // Копируем байт 'R'
                    texture->imageData[currentbyte] = colorbuffer[2];
                    // Копируем байт 'G'
                    texture->imageData[currentbyte + 1  ] = colorbuffer[1];
                    // Копируем байт 'B'
                    texture->imageData[currentbyte + 2  ] = colorbuffer[0];

                    if(bytesPerPixel == 4)    // Если это 32bpp изображение
                    {
                        // Копируем байт 'A'
                        texture->imageData[currentbyte + 3] = colorbuffer[3];
                    }

                    currentbyte += bytesPerPixel;  // Инкрементируем счетчик байтов
                    currentpixel++;        // Инкрементируем счетчик пикселей
                }
            }
        }
        while(currentpixel < pixelcount); // Еще есть пиксели для чтения? ... Начинаем цикл с начала
        fclose(file);        // Закрываем файл

        return true;        // Возвращаем true
    }


    // Удалось ли выделить память размер выделенной памяти равен  imageSize?
    if(texture->imageData==NULL||fread(texture->imageData, 1, imageSize, file)!=imageSize)
    {
        if(texture->imageData!=NULL)                    // Были ли загружены данные?
            free(texture->imageData);                   // Если да, то освобождаем память
        fclose(file);                                   // Закрываем файл

        return false;                                   // Возвращаем false
    }

    // замена 1 и 3 байта местами с помощью XOR (искл. лог. или)
    // a=a^b; b=a^b; a=a^b;
    for(GLuint i=0; i<GLuint(imageSize); i+=bytesPerPixel)     // Цикл по данным, описывающим изображение
    {
        texture->imageData[i] ^= texture->imageData[i+2];
        texture->imageData[i+2] ^= texture->imageData[i];
        texture->imageData[i] ^= texture->imageData[i+2];
    }

    fclose (file); // Закрываем файл

    return true;
}

bool ol_ResourceManager::LoadBMP(const char *fileName, ol_TextureImage *texture)
{
    FILE *fp=fopen(fileName,"rb");
    if(!fp)
    {
        cout<<"Error: Can't Find The file"<<fileName<<"\n";
        return false;
    }

    unsigned int imageSize;
    GLuint   bytesPerPixel;

//    BITMAPFILEHEADER bFh;
    BITMAPINFOHEADER bIh;

    WORD    word_buf;
    DWORD   dword_buf;
    LONG    long_buf;

// Две нижеследующие команды не работают так как с++ реально выделяет фиг знает сколько памяти под эти структуры
//    fread(&bFh, sizeof(BITMAPFILEHEADER), 1, fp);
//    fread(&bIh, sizeof(BITMAPINFOHEADER), 1, fp);

// Поэтому читаем вручную
    // reading  BITMAPFILEHEADER ------------------------------------
    fread(&word_buf, 2, 1, fp);     // bFh.bfType = word_buf;
    fread(&word_buf, 2, 1, fp);     // bFh.bfReserved1 = word_buf;
    fread(&dword_buf, 4, 1, fp);    // bFh.bfSize = dword_buf;
    fread(&dword_buf, 4, 1, fp);    // bFh.bfOffBits = dword_buf;
    fread(&word_buf, 2, 1, fp);     // bFh.bfReserved2 = word_buf;

    // reading  BITMAPINFOHEADER ------------------------------------
    fread(&dword_buf, 4, 1, fp);
    bIh.biSize = dword_buf;
    fread(&long_buf, 4, 1, fp);
    bIh.biWidth = long_buf;
    fread(&long_buf, 4, 1, fp);
    bIh.biHeight = long_buf;
    fread(&word_buf, 2, 1, fp);
    bIh.biPlanes = word_buf;
    fread(&word_buf, 2, 1, fp);
    bIh.biBitCount = word_buf;
    fread(&dword_buf, 4, 1, fp);
    bIh.biCompression = dword_buf;
    fread(&dword_buf, 4, 1, fp);
    bIh.biSizeImage = dword_buf;
    fread(&long_buf, 4, 1, fp);
    bIh.biXPelsPerMeter = long_buf;
    fread(&long_buf, 4, 1, fp);
    bIh.biYPelsPerMeter = long_buf;
    fread(&dword_buf, 4, 1, fp);
    bIh.biClrUsed = dword_buf;
    fread(&dword_buf, 4, 1, fp);
    bIh.biClrImportant = dword_buf;
    //---------------------------------------------------------------

    texture->width = bIh.biWidth;
    texture->height = bIh.biHeight;
    texture->bpp = bIh.biBitCount;
    bytesPerPixel = texture->bpp/8;  // Делим на 8 для получения байт/пиксель

    imageSize = texture->height*texture->width*bytesPerPixel;

    if(imageSize<=0)
    {
        cout<<"Error: Image size is zero!\n";
        fclose(fp);
        return false;
    }

    texture->imageData=(BYTE *)malloc(imageSize);
    fread(texture->imageData, 1, imageSize, fp);

    int result = ferror(fp);
    if (result)
    {
        cout<<"Error: Failed To Get Data!\n";
        fclose(fp);
        return false;
    }

    // замена 1 и 3 байта местами с помощью XOR (искл. лог. или)
    // a=a^b; b=a^b; a=a^b;
    for(GLuint i=0; i<GLuint(imageSize); i+=bytesPerPixel)     // Цикл по данным, описывающим изображение
    {
        texture->imageData[i] ^= texture->imageData[i+2];
        texture->imageData[i+2] ^= texture->imageData[i];
        texture->imageData[i] ^= texture->imageData[i+2];
    }

    fclose(fp);

    return true;
}

bool ol_ResourceManager::LoadRAW(const char *fileName, ol_TextureImage *texture)
{
    unsigned long nSize = CalcFileSize(fileName);

    if(nSize>0)
    {
        FILE *pFile = NULL;

        // открытие файла в режиме бинарного чтения
        pFile = fopen( fileName, "rb" );

        // Файл найден?
        if ( pFile == NULL )
        {
            // Выводим сообщение об ошибке и выходим из процедуры
            cout<<"Error: Can't Find The file"<<fileName<<"\n";
            return false;
        }

        texture->imageData=(GLubyte *)malloc(nSize); // выделяем память под данные текстуры

        // Каждый раз читаем по одному байту, размер = ширина * высота
        fread(texture->imageData, 1, nSize, pFile );

        // Проверяем на наличие ошибки
        int result = ferror( pFile );

        // Если произошла ошибка
        if (result)
        {
            cout<<"Error: Failed To Get Data!\n";
            fclose(pFile);
            return false;
        }
        else cout<<"File loaded: "<<fileName<<"\n";

        texture->bpp = 8;
        texture->width = texture->height = (GLuint)sqrtl(nSize);

        // Закрываем файл
        fclose(pFile);
        return true;
    }
    else return false;
}

void ol_ResourceManager::SaveAnimTexXML(const char *file_name, ol_TextureImage *im)
{
    int sizeofimage=im->width * im->height * im->bpp/8;

    FILE *f = fopen(file_name, "wt");
    fprintf(f, "%s\n", "<?xml version=\"1.0\"?>");

    fprintf(f, "%s", "<animation name=\"");
    fprintf(f, "%s", im->name.c_str());
    fprintf(f, "%s\n", "\"> ");

    fprintf(f, "%s", "<width>");
    fprintf(f, "%i", im->width);
    fprintf(f, "%s\n", "</width>");

    fprintf(f, "%s", "<height>");
    fprintf(f, "%i", im->height);
    fprintf(f, "%s\n", "</height>");

    fprintf(f, "%s", "<bpp>");
    fprintf(f, "%i", im->bpp);
    fprintf(f, "%s\n", "</bpp>");

    fprintf(f, "%s", "<wdimension>");
    fprintf(f, "%i", im->width_amount);
    fprintf(f, "%s\n", "</wdimension>");

    fprintf(f, "%s", "<hdimension>");
    fprintf(f, "%i", im->height_amount);
    fprintf(f, "%s\n", "</hdimension>");

    fprintf(f, "<fps>%i</fps>\n", im->fps);

    fprintf(f, "%s", "<pixels>");
    //----------------------------------------------------
    string enc;
    enc = base64_encode(im->imageData, sizeofimage);

    fprintf(f, "%s", enc.c_str());
    fprintf(f, "%s\n", "</pixels>");
    //----------------------------------------------------
    fprintf(f, "</animation>\n");
    fprintf(f, "</xml>");

    fclose(f);
}
// загружает анимационную текстуры из *.xml файла
bool ol_ResourceManager::LoadAnimTexXML(const char *file_name, ol_TextureImage* texture)
{
    pugi::xml_document doc;
    pugi::xml_parse_result result = doc.load_file(file_name);
    if(result!= status_ok)
    {
        printf("Error(LoadAnimTexXML): pugi: Can not load file\n");
        return false;
    }

    texture->width = atoi(doc.child("animation").child_value("width"));
    texture->height = atoi(doc.child("animation").child_value("height"));
    texture->bpp = atoi(doc.child("animation").child_value("bpp"));
    texture->width_amount = atoi(doc.child("animation").child_value("wdimension"));
    texture->height_amount = atoi(doc.child("animation").child_value("hdimension"));
    texture->name = (char*)doc.child("animation").attribute("name").value();
    texture->fps = atoi(doc.child("animation").child_value("fps"));

    int sizeofimage = texture->width * texture->height * texture->bpp/8;

    texture->imageData = new GLubyte[sizeofimage];
    memcpy(texture->imageData,(GLubyte*)base64_decode(doc.child("animation").child_value("pixels")).c_str(), sizeofimage);

    return true;
}
