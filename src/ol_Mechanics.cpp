/**
------------------------------------------------------------------------
  This file is part of the OLGA project.
  Copyright © 2017 by Enver Akhmedov.
  E-mail: aen-code@yandex.ru
  For conditions of distribution and use, see copyright notice in OLGA.h
------------------------------------------------------------------------
**/

#include "ol_Mechanics.h"

/**
    --------------------------------- Collision Circle -------------------------------------
    >                                                                                      <
    >                             Окружность столкновений.                                 <
    >                                                                                      <
    ----------------------------------------------------------------------------------------
**/

ol_CollisionCircle::ol_CollisionCircle()
{
    pos.x = 0.0f;
    pos.y = 0.0f;
    collR = 1.0f;
    touchR = 2.0f;
    touching = false;
    type = 0;
}

ol_CollisionCircle::ol_CollisionCircle(ol_Vector2df pos, float collR, float touchR)
{
    this->pos = pos;
    this->collR = collR;
    this->touchR = touchR;
    type = 0;
}

ol_CollisionCircle::~ol_CollisionCircle()
{}

/**
    --------------------------------- Mechanics Object 2d ----------------------------------
    >                                                                                      <
    >  Простейший механический объект двумерной сцены. Не имеет визуального представления. <
    >         Изменяет свое положение и скорость в соответствии действующими силами.       <
    >                                                                                      <
    ----------------------------------------------------------------------------------------
**/

ol_MechanicsObject2d::ol_MechanicsObject2d()
{
    mass = 1;
    elasticity = 1.0f;
    pos = ol_Vector2df(0.0f, 0.0f);
    vel = ol_Vector2df(0.0f, 0.0f);
    active = true;
}

ol_MechanicsObject2d::ol_MechanicsObject2d(float mass, ol_Vector2df pos, ol_Vector2df vel)
{
    this->mass = mass;
    this->pos = pos;
    this->vel = vel;
    accel = ol_Vector2df(0.0f, 0.0f);
    affect = ol_Vector2df(0.0f, 0.0f);
    elasticity = 1.0f;
    active = true;
}

ol_MechanicsObject2d::~ol_MechanicsObject2d()
{
}

void ol_MechanicsObject2d::Update(void)
{
    affect = affect + accel;
    vel = vel + affect * ol_CoreParams::fdt;
    affect = ol_Vector2df(0.0f, 0.0f);

    pos = pos + vel;
}

/**
    --------------------------------- Collision Object 2d ----------------------------------
    >                                                                                      <
    >      Интерфейсный класс для обработки столкновений механических объектов сцены.      <
    >                                                                                      <
    ----------------------------------------------------------------------------------------
**/

ol_CollisionObject2d::ol_CollisionObject2d() {}
ol_CollisionObject2d::ol_CollisionObject2d(float mass, ol_Vector2df pos, ol_Vector2df vel): ol_MechanicsObject2d(mass, pos, vel) {}
ol_CollisionObject2d::~ol_CollisionObject2d() {}

void ol_CollisionObject2d::setCollisionParams(unsigned short type, unsigned short check, unsigned short accept)
{
    collType = type;
    collCheck = check;
    collAccept = accept;
}

bool ol_CollisionObject2d::CheckCollisionWith(ol_CollisionObject2d *object, ol_SceneEvent *event)
{
    if(((collType & object->collCheck) == 0 ) || ((collAccept & object->collType) == 0)  || (object == this) || (!object->active)) return false;
    return true;
}

/**
    --------------------------------- Circle Collision -------------------------------------
    >                                                                                      <
    >      Механический объект сцены, который сталкивается с другими объектами с           <
    >                         помощью окружности столкновений.                             <
    >                                                                                      <
    ----------------------------------------------------------------------------------------
**/

ol_CircleCollision2d::ol_CircleCollision2d() {}
ol_CircleCollision2d::ol_CircleCollision2d(float mass, ol_Vector2df pos, ol_Vector2df vel): ol_CollisionObject2d(mass, pos, vel) {}
ol_CircleCollision2d::~ol_CircleCollision2d()
{
    for(unsigned int i=0; i<circles.size(); i++)
        delete circles[i];
}

void ol_CircleCollision2d::ResetCollisionFlags(void)
{
    for(unsigned int i=0; i<circles.size(); i++)
    {
        circles[i]->touching = false;
        circles[i]->collided = false;
    }
}

ol_CollisionCircle* ol_CircleCollision2d::addCollisionCircle(ol_Vector2df pos, float collR, float touchDelta)
{
    ol_CollisionCircle *circ = new ol_CollisionCircle(this->pos + pos, collR, collR + touchDelta);
    circles.push_back(circ);
    return circ;
}

ol_Vector2df ol_CircleCollision2d::getPosition(void)
{
    return pos;
}

void ol_CircleCollision2d::setPosition(ol_Vector2df newPos)
{
    ol_Vector2df dir = getPosition();
    dir = newPos - dir;
    Shift(dir);
}

void ol_CircleCollision2d::Shift(ol_Vector2df &dir)
{
    pos = pos + dir;

    for(unsigned int i=0; i<circles.size(); i++)
    {
        circles[i]->pos = circles[i]->pos + dir;
    }
}

void ol_CircleCollision2d::Update(void)
{
    ol_MechanicsObject2d::Update();
    for(unsigned int i=0; i<circles.size(); i++)
    {
        circles[i]->pos = circles[i]->pos + vel;
    }
}

bool ol_CircleCollision2d::CheckCollisionWith(ol_CircleCollision2d *object, ol_SceneEvent *event)
{
    if(!ol_CollisionObject2d::CheckCollisionWith(object, event)) return false;

    ol_Vector2df R;
    for(unsigned int i=0; i<circles.size(); i++)
        for(unsigned int j=0; j<object->circles.size(); j++)
        {
            R = circles[i]->pos - object->circles[j]->pos;
            if(R.Length() <= (circles[i]->collR + object->circles[j]->collR))
            {
                object->eventCircle = object->circles[j];
                object->eventCircle->collided = true;

                eventCircle = circles[i];
                eventCircle->collided = true;

                OnCollision(object);
                object->OnCollision(this);
                return true;
            }
        }

    return false;
}

void ol_CircleCollision2d::OnCollision(ol_CircleCollision2d *obj)
{
}

/**
    --------------------------------- Mesh Collision ---------------------------------------
    >                                                                                      <
    >      Механический объект сцены, который сталкивается с другими объектами с           <
    >                         помощью полигона столкновений.                               <
    >                                                                                      <
    ----------------------------------------------------------------------------------------
**/

ol_MeshCollision2d::ol_MeshCollision2d(ol_Mesh2d* mesh, float friction, float threshold)
{
    this->mesh = mesh;
    frict = new ol_FrictionAffector(friction, threshold);
}
ol_MeshCollision2d::ol_MeshCollision2d(float mass, ol_Vector2df pos, ol_Vector2df vel, ol_Mesh2d* mesh, float friction, float threshold): ol_CollisionObject2d(mass, pos, vel)
{
    this->mesh = mesh;
    frict = new ol_FrictionAffector(friction, threshold);
}
ol_MeshCollision2d::~ol_MeshCollision2d()
{
    delete frict;
}

void ol_MeshCollision2d::setMesh(ol_Mesh2d *mesh)
{
    this->mesh = mesh;
}
ol_Mesh2d* ol_MeshCollision2d::getMesh(void)
{
    return mesh;
}
bool ol_MeshCollision2d::CheckCollisionWith(ol_CircleCollision2d *object, ol_SceneEvent *event)
{
    if(!ol_CollisionObject2d::CheckCollisionWith(object, event)) return false;

    ol_Vector2df objPos;
    ol_Vector2df colNorm;
    float dist;

    for(unsigned int i=0; i<object->circles.size(); i++)
    {
        objPos = object->circles[i]->pos;

        dist = mesh->DistanceToPoint(objPos, colNorm, closestPoint);

        if((dist<object->circles[i]->touchR))
        {
            object->circles[i]->touching = true;
            frict->Affect(object, colNorm);
        }

        if(dist<object->circles[i]->collR)
        {
            object->circles[i]->collided = true;
            ol_Vector2df delta = colNorm * (object->circles[i]->collR - dist);
            object->Shift(delta);
            object->vel = ReflectionVector(object->vel, colNorm, object->elasticity);

//            event->type = OL_SEV2D_COLLISION;
//            event->obj2d_1 = object; // кто столкнулся
//            event->obj2d_2 = this;   // с кем столкнулся

//            return true;
        }
    }
    return false;
}

/**
    --------------------------------- 2D Physics Manager -----------------------------------
    >                                                                                      <
    >                Класс отвечающий за обработку 2д физики.                              <
    >                                                                                      <
    ----------------------------------------------------------------------------------------
**/


ol_PhysicsManager2d::ol_PhysicsManager2d() {}

ol_PhysicsManager2d::~ol_PhysicsManager2d() {}

void ol_PhysicsManager2d::addMeshObject(ol_MeshCollision2d *obj)
{
    if(!obj)
    {
        printf("Failed to add 2D collision object\n");
        return;
    }

    meshObjects.push_back(obj);
}

void ol_PhysicsManager2d::addCircleObject(ol_CircleCollision2d *obj)
{
    if(!obj)
    {
        printf("Failed to add 2D collision object\n");
        return;
    }

    circleObjects.push_back(obj);
}

void ol_PhysicsManager2d::CheckCollisionEvents(void)
{
    ClearSceneEventStack();
    for(unsigned int i=0; i<circleObjects.size(); i++)
        circleObjects[i]->ResetCollisionFlags();

    for(unsigned int i=0; i<meshObjects.size(); i++)
        for(unsigned int j=0; j<circleObjects.size(); j++)
        {
            ol_SceneEvent *gameEvent = new ol_SceneEvent();
            if(meshObjects[i]->CheckCollisionWith(circleObjects[j], gameEvent))
                addEvent(gameEvent);
            else delete gameEvent;
        }

    pairs.clear();
    for(unsigned int i=0; i<circleObjects.size(); i++)
        for(unsigned int j=0; j<circleObjects.size(); j++)
        {
            // исключаем повторное срабатывание столкновения
            for(unsigned int p=0; p<pairs.size(); p+=2)
                if( (i==pairs[p] && j==pairs[p+1] ) || (i==pairs[p+1] && j==pairs[p]) )
                    return;

            ol_SceneEvent *gameEvent = new ol_SceneEvent();
            if(circleObjects[i]->CheckCollisionWith(circleObjects[j], gameEvent))
            {
                addEvent(gameEvent);
                pairs.push_back(i);
                pairs.push_back(j);
            }

            else delete gameEvent;
        }
}

void ol_PhysicsManager2d::addEvent(ol_SceneEvent *event)
{
    events.push_back(event);
    lastEvent ++;
}

ol_SceneEvent* ol_PhysicsManager2d::getLastEvent(void)
{
    lastEvent --;
    if(lastEvent < 0) return NULL;

    return events[lastEvent];
}

void ol_PhysicsManager2d::ClearSceneEventStack(void)
{
    lastEvent = 0;
    unsigned int n = events.size();

    for(unsigned int i=0; i<n; i++)
        delete events[i];

    events.clear();
}

/**
    --------------------------------- Physics Affector -------------------------------------
    >                                                                                      <
    >    Среда существования механических объектов. Содержит набор сил, действующих на     <
    >          объекты. Имеет метод воздействия на объекты.                                <
    >                                                                                      <
    ----------------------------------------------------------------------------------------
**/

ol_GravityAffector::ol_GravityAffector(ol_Vector2df grav)
{
    this->gravity = grav;
}

ol_GravityAffector::~ol_GravityAffector()
{}

void ol_GravityAffector::ConstantAffect(ol_MechanicsObject2d *obj)
{
    obj->accel = obj->accel + gravity*obj->mass;
}

/**
    >--------------------------------------------------------------------------------------<
**/

ol_SimpleForceAffector::ol_SimpleForceAffector()
{
    force = ol_Vector2df(0.0f, 0.0f);
}

ol_SimpleForceAffector::ol_SimpleForceAffector(ol_Vector2df force)
{
    this->force = force;
}

ol_SimpleForceAffector::~ol_SimpleForceAffector() {}

void ol_SimpleForceAffector::Affect(ol_MechanicsObject2d *obj)
{
    obj->affect = obj->affect + force/obj->mass;
}

/**
    --------------------------------- Friction Affector ------------------------------------
    >                                                                                      <
    >                                      Трение.                                         <
    >  Не является трением в классическом понимании. Просто некая сила, действующая против <
    >  скорости динамического объекта при соприкосновении его со статическим объектом и    <
    >  пропорциональная проекции ускорения на нормаль к поверхности (реакция опоры).       <
    >  Пропорциональна скорости, то есть с увеличением скорости увеличивается. Кроме того, <
    >  есть пороговое значение скорости при котором пропорциональная зависимость           <
    >  отключается.                                                                        <
    >                                                                                      <
    ----------------------------------------------------------------------------------------
**/

ol_FrictionAffector::ol_FrictionAffector()
{
    friction = 0.000f;
    threshold = 0.0f;
}

ol_FrictionAffector::ol_FrictionAffector(float friction, float threshold)
{
    this->friction = friction;
    this->threshold = threshold;
}

ol_FrictionAffector::~ol_FrictionAffector()
{
}

void ol_FrictionAffector::Affect(ol_MechanicsObject2d *obj, ol_Vector2df &norm)
{
    ol_Vector2df A = obj->accel + obj->affect;           // суммарное ускорение
    ol_Vector2df V = norm * Dot(obj->vel, norm);         // нормальная проекция скорости
    V = obj->vel - V;                                   // тангенциальная проекция скорости
    float L = V.Length();                               // величина тангенциальной скорости
    if(L<=0) return;
    float scale = fabs(Dot(A, norm)) * friction;    	// реакция опоры
    if(L>threshold) scale*=L;

    // вычисление силы трения
    ol_Vector2df force = V;
    force.Invert();
    force.Normalize();
    force = force*scale;

    if(force.Length()>L)
    {
        force = V;
        force.Invert();
    }

    obj->vel = obj->vel + force;
}
