/**
------------------------------------------------------------------------
  This file is part of the OLGA project.
  Copyright © 2017 by Enver Akhmedov.
  E-mail: aen-code@yandex.ru
  For conditions of distribution and use, see copyright notice in OLGA.h
------------------------------------------------------------------------
**/

#ifndef OL_2D_SCENE_H_INCLUDED
#define OL_2D_SCENE_H_INCLUDED

#include "ol_GUI.h"
#include "ol_Particle_System.h"

class ol_DynamicObject2d;

/**
    -------------------------------- 2D Scene Object ---------------------------------------
    >                                                                                      <
    >                            объект 2-мерной сцены                                     <
    >                                                                                      <
    ----------------------------------------------------------------------------------------
**/

class ol_SceneObject2d
{
protected:
    ol_Graphics *graph;      // указатель на графический интерфейс; используется при рендеринге
    ol_Area2d *visual;       // визуальная часть - спрайт, который рендерится

    bool moving;            // показывает происходит ли движение в данном фрэйме
    ol_Vector2df moveDir;    // направление движения. Вектор, который является результирующим

    bool visible;
    bool enabled;

    // точка относительно которой отрисовывается объект
    ol_GUIPositionPoints origin;

public:
    int netID;
    int ID;                 // Уникальный идентификатор
    const char *name;       // Имя объекта

    ol_SceneObject2d(ol_Graphics *graph, ol_Vector2df pos, float width, float height, ol_GUIPositionPoints keyPoint = OL_GUI_POS_CENTMID, const char *name = "Scene Object 2D");
    virtual ~ol_SceneObject2d();

    virtual void Draw(void);
    virtual void Update(void);
    // стандартная функция движение. Фактически не сдвигает объект, а накапливает вектор движение.
    // Непосредственное изменение координат происходит в апдэйте.
    virtual void Move(ol_Vector2df dir);
    // Смещает объект в направлении dir путем изменения координат объекта
    virtual void Shift(ol_Vector2df &dir);

    virtual void Enable(void);
    virtual void Disable(void);

    // перемещает объект в заданную точку. Фактически просто вызывается ф-я шифт.
    virtual void setPosition(ol_Vector2df pos);
    // Возвращает точку относительно которой обрабатывается объект.
    // Необходимо использовать этот метод, так как в дальнейших классах точка отсчета может определяться физ. объектом или чем-то ещё.
    virtual ol_Vector2df getPosition(void);
    // Автоматическая реакция объекта на события клавиатуры и мышки
    virtual void CheckEvents(ol_Event *event) {}
};

/**
    -------------------------------- 2D Static Object --------------------------------------
    >                                                                                      <
    >   Объект двумерной сцены, который имеет меш для определения столкновений с ним.      <
    >                                                                                      <
    ----------------------------------------------------------------------------------------
**/
class ol_StaticObject2d: public ol_SceneObject2d, public ol_MeshCollision2d
{
public:
    ol_StaticObject2d(ol_Graphics *graph, ol_Vector2df pos, float width, float height, ol_Mesh2d* mesh, float friction = 0.0f, float threshold = 0.0f, ol_GUIPositionPoints keyPoint = OL_GUI_POS_CENTMID, const char *name = "Static Object 2D");
    virtual ~ol_StaticObject2d();

    virtual void Draw(void);
};

/**
    -------------------------------- 2D Dynamic Object -------------------------------------
    >                                                                                      <
    >                              Динамический объект.                                    <
    >          Представляет собой окружность, определяемую центром и радиусом.             <
    >   Имеет массу, скорость и ускорение. Подвержен воздействию физических сил.           <
    >               Может сталкиваться со статическими объектами                           <
    >                                                                                      <
    ----------------------------------------------------------------------------------------
**/

class ol_DynamicObject2d: public ol_SceneObject2d, public ol_CircleCollision2d
{
protected:
public:
    ol_DynamicObject2d(ol_Graphics *graph, ol_Vector2df pos, float width, float height, ol_GUIPositionPoints keyPoint = OL_GUI_POS_CENTMID, const char *name = "Dynamic Object 2D");
    virtual ~ol_DynamicObject2d();

    virtual void Draw(void);
    virtual void Update(void);
    virtual void CheckEvents(ol_Event *event);
    virtual void Shift(ol_Vector2df &dir);
    virtual void setPosition(ol_Vector2df pos);
    virtual ol_Vector2df getPosition(void);

};

/**
    --------------------------------- 2D Scene Manager -------------------------------------
    >                                                                                      <
    >                Класс управляющий всеми объектами двумерной сцены                     <
    >                                                                                      <
    ----------------------------------------------------------------------------------------
**/

class ol_SceneManager2d
{
private:
    ol_Graphics *graph;
    ol_ParticleManager *pmgr;
    ol_PhysicsManager2d *phymgr;

    vector<ol_SceneObject2d *> objects;

    unsigned int objectsCount;

public:
    ol_SceneManager2d(ol_Graphics *graph);
    ~ol_SceneManager2d();

    void addObject(ol_SceneObject2d *obj);
    void addStaticObject(ol_StaticObject2d *obj);
    void addDynamicObject(ol_DynamicObject2d *obj);

    ol_SceneObject2d* getObjectByNetID(int netID);

    ol_ParticleManager *getParticleManager(void);

    void DisableAll(void);
    void EnableAll(void);
    void DrawAll(void);
    void UpdateAll(void);

    // обработка событий клавиатуры и мыши, генерация столкновений
    void CheckSceneEvents(ol_Event *event);
    ol_SceneEvent* getLastEvent(void);
};

#endif // OL_2D_SCENE_H_INCLUDED
