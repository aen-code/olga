/**
------------------------------------------------------------------------
  This file is part of the OLGA project.
  Copyright © 2017 by Enver Akhmedov.
  E-mail: aen-code@yandex.ru
  For conditions of distribution and use, see copyright notice in OLGA.h
------------------------------------------------------------------------
**/

#include "ol_Graphics.h"
#include <math.h>
#include <iostream>
#include <stdio.h>

using namespace std;

/**
    --------------------------------- ol_Graphics -------------------------------------------
    >                                                                                      <
    >   Отвечает за загрузку текстур и моделей. Имеет несколько простых функций рисования  <
    >   геометрических примитивов. Отвечает за переход от 3Д рисования к 2Д и наоборот.    <
    >                                                                                      <
    ----------------------------------------------------------------------------------------
**/

ol_Graphics::ol_Graphics()
{
    colorPalette.push_back(OL_COLOR_RED);
    colorPalette.push_back(OL_COLOR_GREEN);
    colorPalette.push_back(OL_COLOR_BLUE);
    colorPalette.push_back(OL_COLOR_PURPLE);
    colorPalette.push_back(OL_COLOR_ORANGE);
    colorPalette.push_back(OL_COLOR_GREY);

    nextColor = -1;
}

ol_Color ol_Graphics::getNextColor(void)
{
    if(nextColor >= colorPalette.size()) nextColor = -1;
    nextColor++;
    return colorPalette[nextColor];
}

ol_Graphics::~ol_Graphics()
{
    map<const char*, ol_Texture>::const_iterator itr;

    for(itr=texMap.begin(); itr!=texMap.end(); itr++)
        glDeleteTextures(1, (GLuint*)&itr->second.texID);
}

GLuint ol_Graphics::getTexID(const char *fileName)
{
    GLuint result = -1;
    
    if(texMap.count(fileName)>0) result = texMap[fileName].texID;
    else
    {
		ol_TextureImage *tex = LoadImage(fileName);

		if(tex)
		{
			if(BuildTexture(tex))
			{
				result = tex->texID;
				texMap[fileName] = TextureImageToTexture(tex);
				printf("Image loaded: %s\n", fileName);
			}
			delete tex;
		}
		else printf("Could not Load Image: %s\n", fileName);
	}

    return result;
}

ol_Texture ol_Graphics::getTexture(const char *fileName)
{
    ol_Texture result;

    if(texMap.count(fileName)>0) result = texMap[fileName];
    else
    {
		ol_TextureImage *tex = LoadImage(fileName);

		if(tex)
		{
			if(BuildTexture(tex))
			{
				result.width = tex->width;
				result.height = tex->height;
				result.texID = tex->texID;
				result.width_amount = tex->width_amount;
				result.height_amount = tex->height_amount;
				result.fps = tex->fps;
				texMap[fileName] = result;
				printf("Image loaded: %s\n", fileName);
			}
			delete tex;
		}
		else printf("Could not Load Image: %s\n", fileName);
	}
    return result;
}

bool ol_Graphics::BuildTexture(ol_TextureImage *texture)
{
    if(!texture)
    {
        cout<<"Could not build texture!\n";
        return false;
    }

    GLuint   type = 0, format = 0;

    // Строим текстуру из данных
    glGenTextures(1, (GLuint*)&texture->texID);           // Сгенерировать OpenGL текстуру IDs
    glBindTexture(GL_TEXTURE_2D, texture->texID);         // Привязать нашу текстуру
    // Линейная фильтрация
    glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
    // Линейная фильтрация
    glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
    // Смешивание цвета текстуры с цветом вершины
    glTexEnvf(GL_TEXTURE_ENV, GL_TEXTURE_ENV_MODE, GL_MODULATE);

    switch(texture->bpp)
    {
    case 8:
        type = 1;
        format = GL_LUMINANCE;
        break;

    case 16:
        type = 2;
        format = GL_LUMINANCE_ALPHA;
        break;

    case 24:
        type = 3;
        format = GL_RGB;
        break;

    case 32:
        type = 4;
        format = GL_RGBA;
        break;
    default:
        cout<<"Error: Unsupported image bitrate!\n";
        return false;
        break;
    }
    
   // printf("format = %u(%u)\n", format, GL_RGBA);

    glTexImage2D(GL_TEXTURE_2D, 0, type, texture->width, texture->height, 0, format, GL_UNSIGNED_BYTE, texture->imageData);

    return true;
}

ol_TextureImage* ol_Graphics::LoadImage(const char *fileName)
{
    int ftype = IsFileTypeSupported(fileName);

    if(ftype == -1)
    {
        cout<<"Error: Image Format Is Not Supported!\n";
        return NULL;
    }
    else
    {
        ol_TextureImage *texture = new ol_TextureImage();

        switch (ftype)
        {
        case OL_IMAGE_TYPE_RAW:
            if(loaderRes.LoadRAW(fileName, texture)) return texture;
            else
            {
                delete texture;
                return NULL;
            }
            break;

        case OL_IMAGE_TYPE_TGA:
            if(loaderRes.LoadTGA(fileName, texture)) return texture;
            else
            {
                delete texture;
                return NULL;
            }
            break;

        case OL_IMAGE_TYPE_BMP:
            if(loaderRes.LoadBMP(fileName, texture)) return texture;
            else
            {
                delete texture;
                return NULL;
            }
            break;
        case OL_RESOURCE_TYPE_XML:
            if(loaderRes.LoadAnimTexXML(fileName, texture)) return texture;
            else
            {
                delete texture;
                return NULL;
            }
            break;

        default:
            delete texture;
            return NULL;
        }
    }
}

bool ol_Graphics::SaveImage(const char *fileName, ol_TextureImage *tex)
{
    if(!tex)
    {
        printf("Error(SaveImage): texture is not initialized\n");
        return false;
    }

    int ftype = IsFileTypeSupported(fileName);

    switch (ftype)
    {
    case OL_RESOURCE_TYPE_XML:
        loaderRes.SaveAnimTexXML(fileName, tex);
        return true;
        break;
    default:
        printf("Error(SaveImage): not supported image format\n");
        return false;
        break;
    }

    return false;
}

ol_Texture ol_Graphics::TextureImageToTexture(ol_TextureImage *tex)
{
    ol_Texture res;

    if(tex)
    {
        res.height = tex->height;
        res.width = tex->width;
        res.texID = tex->texID;
        res.width_amount = tex->width_amount;
        res.height_amount = tex->height_amount;
        res.fps = tex->fps;
    }

    return res;
}


ol_Mesh3D* ol_Graphics::getMesh(const char *fileName)
{
    int ftype = IsFileTypeSupported(fileName);

    ol_Mesh3D *mesh = new ol_Mesh3D();

    switch (ftype)
    {
    case -1:
        cout<<"Error: Model Format Is Not Supported!\n";
        delete mesh;
        return NULL;
        break;

    case OL_MODEL_TYPE_OBJ:

        if(!loaderRes.LoadOBJ(mesh, fileName))
        {
            cout<<"Error loadin model!\n";
            delete mesh;
            return NULL;
        }
        else
        {
            cout<<"Model loaded: "<<fileName<<"\n";
            return mesh;
        }

        break;

    case OL_RESOURCE_TYPE_OLR:
        if(!loaderRes.LoadMeshOL(mesh, fileName))
        {
            cout<<"Error loadin model!\n";
            delete mesh;
            return NULL;
        }
        else
        {
            cout<<"Model loaded: "<<fileName<<"\n";
            return mesh;
        }

        break;

    }
    return NULL;
}

bool ol_Graphics::saveMesh(ol_Mesh3D *mesh, const char *fileName)
{
//    loaderObj.ExportObj(mesh, fileName);

    return true;
}

void ol_Graphics::DrawLine3d(ol_Vector3d start, ol_Vector3d end, ol_Color col) const
{
    glPushAttrib(GL_ALL_ATTRIB_BITS);
    glDisable(GL_LIGHTING);
    glDisable(GL_TEXTURE_2D);

    glColor4f(col.r, col.g, col.b, col.a);

    glBegin(GL_LINES);
    glVertex3f(start.x, start.y, start.z);
    glVertex3f(end.x, end.y, end.z);
    glEnd();
    glPopAttrib();
}

void ol_Graphics::DrawLines3D(vector<ol_Line> lines)
{
    if(lines.size()<=0) return;

    glPushAttrib(GL_ALL_ATTRIB_BITS);
    glDisable(GL_LIGHTING);
    glDisable(GL_TEXTURE_2D);

    glBegin(GL_LINES);
    for(unsigned int i=0; i<lines.size(); i++)
    {
        glColor4f(lines[i].col.r, lines[i].col.g, lines[i].col.b, lines[i].col.a);

        glVertex3f(lines[i].start.x, lines[i].start.y, lines[i].start.z);
        glVertex3f(lines[i].end.x, lines[i].end.y, lines[i].end.z);
    }
    glEnd();

    glPopAttrib();
}

void ol_Graphics::DrawCoordSys(float length)
{
    ol_Color col;
    col = ol_Color(1.0f, 0.0f, 0.0f);
    DrawLine3d(ol_Vector3d(0.0f, 0.0f, 0.0f), ol_Vector3d(length, 0.0f, 0.0f), col);
    col = ol_Color(0.0f, 0.8f, 0.0f);
    DrawLine3d(ol_Vector3d(0.0f, 0.0f, 0.0f), ol_Vector3d(0.0f, length, 0.0f), col);
    col = ol_Color(0.0f, 0.0f, 1.0f);
    DrawLine3d(ol_Vector3d(0.0f, 0.0f, 0.0f), ol_Vector3d(0.0f, 0.0f, length), col);
}

void ol_Graphics::PushScreenCoordinateMatrix(void)
{
    ol_Matrix22i viewport;

    glPushAttrib(GL_LIST_BIT | GL_CURRENT_BIT  | GL_ENABLE_BIT |GL_TRANSFORM_BIT);
    glGetIntegerv(GL_VIEWPORT, viewport);
    glMatrixMode(GL_PROJECTION);
    glPushMatrix();
    glLoadIdentity();
    gluOrtho2D(viewport[0],viewport[2],viewport[1],viewport[3]);

    glPopAttrib();

    glDisable(GL_LIGHTING);
    glPolygonMode(GL_FRONT_AND_BACK, GL_FILL);
    glDisable(GL_DEPTH_TEST);
    glEnable(GL_TEXTURE_2D);
    glEnable(GL_BLEND);
    glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);
    glLoadIdentity();
}

void ol_Graphics::PopProjectionMatrix(void)
{
    glPushAttrib(GL_TRANSFORM_BIT);
    glMatrixMode(GL_PROJECTION);
    glPopMatrix();
    glPopAttrib();
    glEnable(GL_DEPTH_TEST);
    glDisable(GL_BLEND);
}

ol_Material* ol_Graphics::CreateDefaultMaterial(void)
{
    ol_Material *mat = new ol_Material();

    return mat;
}

void ol_Graphics::ol_GLColor(ol_Color color)
{
    glColor3f(color.r, color.g, color.b);
}

void ol_Graphics::DrawCircle(ol_Vector2df center, float radius, int vertNum)
{
    float alpha = 2*PI/vertNum;
    float angle = 0;

    glBegin(GL_LINE_LOOP);
    for(int i=1; i<=vertNum; i++)
    {
        glVertex2f(center.x + radius*sin(angle), center.y + radius*cos(angle));
        angle += alpha;
    }

    glEnd();
}

/**
    --------------------------------- ol_Material -------------------------------------------
    >                                                                                      <
    >           Содержит параметры материала и методы для работы с ним.                    <
    >                                                                                      <
    ----------------------------------------------------------------------------------------
**/

void ol_Material::setColorVec(GLfloat vec[4], ol_Color col)
{
    vec[0] = col.r;
    vec[1] = col.g;
    vec[2] = col.b;
    vec[3] = col.a;
}

void ol_Material::setAmbient(ol_Color col)
{
    setColorVec(ambient, col);
}

void ol_Material::setDiffuse(ol_Color col)
{
    setColorVec(diffuse, col);
}

void ol_Material::setSpecular(ol_Color col)
{
    setColorVec(specular, col);
}

void ol_Material::setColor(ol_Color col)
{
    setColorVec(color, col);
}

void ol_Material::setMaterialParam(ol_MaterialParams type, ol_Color param)
{
    if(type == OL_MATERIAL_AMBIENT) setAmbient(param);
    if(type == OL_MATERIAL_DIFFUSE) setDiffuse(param);
    if(type == OL_MATERIAL_SPECULAR) setSpecular(param);
    if(type == OL_MATERIAL_COLOR) setColor(param);
}

void ol_Material::setMaterialParam(ol_MaterialParams type, bool param)
{
    if(type == OL_MATERIAL_LIGHTNING) lightning = param;
}

void ol_Material::setMaterialParam(ol_MaterialParams type, int param)
{
    if(type == OL_MATERIAL_SHININESS) shine = (GLubyte)param;
}

void ol_Material::setMaterialParam(ol_MaterialParams type, GLuint param)
{
    if(type == OL_MATERIAL_TEXTURE)
    {
        tex = param;
        hasTexture = true;
    }
}

void ol_Material::setMaterialType(ol_MaterialType param)
{
    type = param;
}

void ol_Material::setRenderMode(ol_RenderParams param)
{
    render = param;
}

void ol_Material::setRenderFlags(ol_RenderFlags type, bool param)
{
    switch(type)
    {
    case OL_RENDER_DIRS:
        renderDirections = param;
        break;

    case OL_RENDER_NORMS:
        renderNormals = param;
        break;
    }
}
