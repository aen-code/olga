/**
------------------------------------------------------------------------
  This file is part of the OLGA project.
  Copyright © 2017 by Enver Akhmedov.
  E-mail: aen-code@yandex.ru
  For conditions of distribution and use, see copyright notice in OLGA.h
------------------------------------------------------------------------
**/

#include "ol_2D_Scene.h"

/**
    -------------------------------- 2D Scene Object ---------------------------------------
    >                                                                                      <
    >                            объект 2-мерной сцены                                     <
    >                                                                                      <
    ----------------------------------------------------------------------------------------
**/

ol_SceneObject2d::ol_SceneObject2d(ol_Graphics *graph, ol_Vector2df pos, float width, float height, ol_GUIPositionPoints keyPoint, const char *name)
{
    this->graph = graph;

    ID = netID = -1;

    enabled = true;
    visible = true;

    moving = false;
    moveDir = ol_Vector2df(0.0f, 0.0f);

    visual = new ol_Area2d();
    visual->Init(pos, width, height, keyPoint);
    origin = keyPoint;
}

ol_SceneObject2d::~ol_SceneObject2d()
{
    delete visual;
#ifdef _OL_DEBUG_BUILD_
    printf("Scene Object 2D destructor ( ID = %d )\n", ID);
#endif // _OL_DEBUG_BUILD_
}

void ol_SceneObject2d::Enable(void)
{
    enabled = true;
}
void ol_SceneObject2d::Disable(void)
{
    enabled = false;
}

void ol_SceneObject2d::setPosition(ol_Vector2df pos)
{
    ol_Vector2df dir(visual->getPosPoint(origin), pos);
    ol_SceneObject2d::Shift(dir);
}

ol_Vector2df ol_SceneObject2d::getPosition(void)
{
    return visual->centMid;
}

void ol_SceneObject2d::Draw(void)
{
    if(!enabled || !visible) return;

    glDisable(GL_TEXTURE_2D);

    graph->ol_GLColor(OL_COLOR_WHITE);
    glBegin(GL_QUADS);
    glVertex2f(visual->topLeft.x, visual->topLeft.y);
    glVertex2f(visual->topRight.x, visual->topRight.y);
    glVertex2f(visual->botRight.x, visual->botRight.y);
    glVertex2f(visual->botLeft.x, visual->botLeft.y);
    glEnd();

    graph->ol_GLColor(OL_COLOR_BLACK);
    glBegin(GL_LINE_LOOP);
    glVertex2f(visual->topLeft.x, visual->topLeft.y);
    glVertex2f(visual->topRight.x, visual->topRight.y);
    glVertex2f(visual->botRight.x, visual->botRight.y);
    glVertex2f(visual->botLeft.x, visual->botLeft.y);
    glEnd();

}

void ol_SceneObject2d::Move(ol_Vector2df dir)
{
    if(!enabled) return;

    moving = true;
    moveDir = moveDir + dir;
}

void ol_SceneObject2d::Shift(ol_Vector2df &dir)
{
    visual->Shift(dir);
}

void ol_SceneObject2d::Update(void)
{
    if(!enabled) return;

    if(moving)
    {
        ol_Vector2df tDir = moveDir*ol_CoreParams::fdt;
        Shift(tDir);
        moveDir = ol_Vector2df(0.0f, 0.0f);
        moving = false;
    }
}

/**
    -------------------------------- 2D Static Object --------------------------------------
    >                                                                                      <
    >   Объект двумерной сцены, который имеет меш для определения столкновений с ним.      <
    >                                                                                      <
    ----------------------------------------------------------------------------------------
**/

ol_StaticObject2d::ol_StaticObject2d(ol_Graphics *graph, ol_Vector2df pos, float width, float height, ol_Mesh2d* mesh, float friction, float threshold, ol_GUIPositionPoints keyPoint, const char *name):
    ol_SceneObject2d(graph, pos, width, height, keyPoint, name),
    ol_MeshCollision2d(mesh, friction, threshold)
{
    this->pos = visual->getPosPoint(keyPoint);
}

ol_StaticObject2d::~ol_StaticObject2d()
{}

void ol_StaticObject2d::Draw(void)
{
    if(!enabled || !visible) return;

    ol_SceneObject2d::Draw();

    graph->ol_GLColor(OL_COLOR_ORANGE);
    glBegin(GL_LINE_LOOP);

    for(unsigned int i=0; i<mesh->vertCount; i++)
        glVertex2f(mesh->verts[i].point.x, mesh->verts[i].point.y);

    glEnd();

    graph->ol_GLColor(OL_COLOR_PURPLE);
    glBegin(GL_LINES);
    for(unsigned int i=0; i<mesh->vertCount; i++)
    {
        glVertex2f(mesh->verts[i].point.x, mesh->verts[i].point.y);
        glVertex2f(mesh->verts[i].point.x + mesh->verts[i].normal.x*10, mesh->verts[i].point.y + mesh->verts[i].normal.y*10);

    }
    glEnd();

    graph->ol_GLColor(OL_COLOR_RED);
    glPointSize(4);
    glBegin(GL_POINTS);
    glVertex2f(closestPoint.x, closestPoint.y);
    glEnd();
}

/**
    -------------------------------- 2D Dynamic Object -------------------------------------
    >                                                                                      <
    >                              Динамический объект.                                    <
    >          Представляет набор окружностей, определяемых центром и радиусом.            <
    >   Имеет массу, скорость и ускорение. Подвержен воздействию физических сил.           <
    >          Может сталкиваться со статическими и динамическими объектами                <
    >                                                                                      <
    ----------------------------------------------------------------------------------------
**/

ol_DynamicObject2d::ol_DynamicObject2d(ol_Graphics *graph, ol_Vector2df pos, float width, float height, ol_GUIPositionPoints keyPoint, const char *name):
    ol_SceneObject2d(graph, pos, width, height, keyPoint, name)
{
    this->pos = visual->centMid;
}

ol_DynamicObject2d::~ol_DynamicObject2d() {}

void ol_DynamicObject2d::Draw(void)
{
    ol_SceneObject2d::Draw();

    // рисуем вектор скорости
    graph->ol_GLColor(OL_COLOR_PURPLE);
    ol_Vector2df speedVec = pos + vel*10;
    glBegin(GL_LINES);
    glVertex2f(pos.x, pos.y);
    glVertex2f(speedVec.x, speedVec.y);
    glEnd();

    // рисуем окружности столкновений
    for(unsigned int i=0; i<circles.size(); i++)
    {
        graph->ol_GLColor(OL_COLOR_BLUE);
        graph->DrawCircle(circles[i]->pos, circles[i]->collR);
        // рисуем окружность касания
        graph->ol_GLColor(OL_COLOR_PURPLE);
        graph->DrawCircle(circles[i]->pos, circles[i]->touchR);
    }

    // рисуем вектор ускорения
    graph->ol_GLColor(OL_COLOR_ORANGE);
    ol_Vector2df accelVec = pos + (affect + accel)*10;
    glBegin(GL_LINES);
    glVertex2f(pos.x, pos.y);
    glVertex2f(accelVec.x, accelVec.y);
    glEnd();
}

void ol_DynamicObject2d::Shift(ol_Vector2df &dir)
{
    ol_SceneObject2d::Shift(dir);
    ol_CircleCollision2d::Shift(dir);
}

void ol_DynamicObject2d::setPosition(ol_Vector2df pos)
{
    ol_SceneObject2d::setPosition(pos);
    ol_CircleCollision2d::setPosition(pos);
}

ol_Vector2df ol_DynamicObject2d::getPosition(void)
{
    return ol_CircleCollision2d::getPosition();
}

void ol_DynamicObject2d::Update(void)
{
    if(!enabled) return;

    ol_CircleCollision2d::Update();

    ol_SceneObject2d::setPosition(pos);
}

void ol_DynamicObject2d::CheckEvents(ol_Event *event)
{
}

/**
    --------------------------------- 2D Scene Manager -------------------------------------
    >                                                                                      <
    >                Класс управляющий всеми объектами двумерной сцены                     <
    >                                                                                      <
    ----------------------------------------------------------------------------------------
**/

ol_SceneManager2d::ol_SceneManager2d(ol_Graphics *graph)
{
    this->graph = graph;
    objectsCount = 0;

    pmgr = new ol_ParticleManager(graph);
    phymgr = new ol_PhysicsManager2d();
}

ol_SceneManager2d::~ol_SceneManager2d()
{
    for(unsigned int i=0; i<objects.size(); i++)
        delete objects[i];
    objects.clear();

    delete pmgr;
    delete phymgr;
}

ol_ParticleManager *ol_SceneManager2d::getParticleManager(void)
{
    return pmgr;
}

ol_SceneObject2d* ol_SceneManager2d::getObjectByNetID(int netID)
{
    for(unsigned int i=0; i<objectsCount; i++)
    {
        if(objects[i]->netID == netID) return objects[i];
    }
    return NULL;
}

void ol_SceneManager2d::addObject(ol_SceneObject2d *obj)
{
    if(!obj)
    {
        printf("Failed to add 2D object\n");
        return;
    }

    obj->ID = objectsCount;

    objects.push_back(obj);
    objectsCount++;
}

void ol_SceneManager2d::addStaticObject(ol_StaticObject2d *obj)
{
    addObject(obj);

    phymgr->addMeshObject(obj);
}

void ol_SceneManager2d::addDynamicObject(ol_DynamicObject2d *obj)
{
    addObject(obj);

    phymgr->addCircleObject(obj);
}

void ol_SceneManager2d::DisableAll(void)
{
    if(objectsCount<=0) return;

    for(unsigned int i=0; i<objectsCount; i++)
        objects[i]->Disable();
}

void ol_SceneManager2d::EnableAll(void)
{
    if(objectsCount<=0) return;

    for(unsigned int i=0; i<objectsCount; i++)
        objects[i]->Enable();
}

void ol_SceneManager2d::DrawAll(void)
{
    pmgr->DrawAll();
    if(objectsCount<=0) return;

    for(unsigned int i=0; i<objectsCount; i++)
        objects[i]->Draw();
}

void ol_SceneManager2d::UpdateAll(void)
{
    pmgr->UpdateAll();

    if(objectsCount<=0) return;

    for(unsigned int i=0; i<objectsCount; i++)
        objects[i]->Update();
}

void ol_SceneManager2d::CheckSceneEvents(ol_Event *event)
{
//    ClearSceneEventStack();
    if(objectsCount<=0) return;

    for(GLuint i=0; i<objectsCount; i++)
        objects[i]->CheckEvents(event);

    phymgr->CheckCollisionEvents();
}

