OLGA
(Object-oriented Library for Game Applications)
------------------------------------------------------------------------
OLGA is the C++ SDK designed as a platform (engine) for creating 
computer games. Library structure is built upon an object-oriented
approach. It mainly supports 3D game objects, 2D game objects and 
GUI controls. Visualization is performed by means of the OpenGL library.
All code is written in cross-platform manner and designed to have as
few dependencies as possible. Currently supported operating systems is
Linux.

![Screenshot](Screenshot.jpg)

------------------------------------------------------------------------
Installation and requirements
------------------------------------------------------------------------
To run OLGA projects you need a graphics card with support of the OpenGL
version 3.0 or higher. Originally OLGA is built under Linux x64 and 
stored in the "lib/" directory. The easiest way to rebuild the OLGA is
to use the Code::Blocks IDE: http://www.codeblocks.org/. First rebuild 
all third-party libraries that OLGA depend on. Install Code::Blocks, 
open corresponding CBP files of the projects in the "third_party/" 
directory, then press "Menu -> Build -> Rebuild". After that, open 
OLGA.cbp in the "project/" directory and rebuild it too. 
Platform specific additional requirements of the OLGA are:

Linux: 
    
    X11 library with header files.
    OpenGL library with header files.
    Freetype library with header files.

Most of the Linux distributions have all of these libraries 
pre-installed. Probably you will need just to add header packages.

------------------------------------------------------------------------
Directory structure
------------------------------------------------------------------------
    doc/         - All license files and documentation.
    examples/    - Some examples of using the OLGA. Projects with a 
                   number step-by-step explain features of the Library. 
                   Examples without a number are complete game projects 
                   that use the OLGA on advanced level.
    include/     - Main header file to include in your project.
    lib/         - The library itself.
    obj/         - Compiled object files.
    project/     - Code::Blocks project file to rebuild the library.
    src/         - Source files of the library.
    third_party/ - Libraries with their source files that the OLGA
                   depend on.

------------------------------------------------------------------------
Release Notes
------------------------------------------------------------------------
List of the main features and the history of all changes you can find in 
the "release_notes.txt" file.

------------------------------------------------------------------------
License and Contacts
------------------------------------------------------------------------
MIT license. 
The OLGA project is partially based on the code of the other projects. 
See corresponding license files in the "doc/licenses" directory. 

You can contact the author by e-mail:

    aen-code@yandex.ru
