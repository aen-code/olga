/**
                      OLGA 
   Object-oriented Library for Game Applications
   ---------------------------------------------

MIT License

Copyright (c) 2017 Enver Akhmedov

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.

Please note that the OLGA project is partially based on the code of 
the other projects:

   * GLee 5.4, Copyright © 2009 by Ben Woodhouse,
           http://elf-stone.com/glee.php;

   * Game Tutorials, Copyright © 2014 by Ben Humphrey,
           https://github.com/gametutorials;

   * FreeType Project, Copyright © 2017 by David Turner, Robert Wilhelm,
     and Werner Lemberg,
           https://www.freetype.org/

   * pugixml 1.5, Copyright © 2006-2014 by Arseny Kapoulkine,
           arseny.kapoulkine@gmail.com;

   * base64.cpp and base64.h, Copyright © 2004-2008 by René Nyffenegger,
           rene.nyffenegger@adp-gmbh.ch;

See corresponding licence files in the "doc/" directory of the OLGA
project. This means that if you use the OLGA project in your product,
you must acknowledge somewhere in your documentation that you've used
not only the OLGA project, but also the code of the projects 
mentioned above.
**/

#ifdef __linux__
#include "../src/core/linux/ol_Application.h"
#endif

#ifdef __WIN32__
#endif

#ifdef __MACOS__
#endif
