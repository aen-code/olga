/**
------------------------------------------------------------------------
  This file is part of the OLGA project.
  Copyright © 2017 by Enver Akhmedov.
  E-mail: aen-code@yandex.ru
  For conditions of distribution and use, see copyright notice in OLGA.h
------------------------------------------------------------------------
**/

#include "OLGA.h"

/// Define pointers to core variables ----------------------------------
ol_Application       *mainApp;
ol_SceneManager      *smgr;
ol_SceneManager2d    *smgr2d;
ol_Graphics          *graph;
ol_GUIManager        *gmgr;
ol_ParticleManager   *pmgr;

ol_SimpleFont *systemFont; // Font interface to print out text in UserDraw().
ol_CameraSceneObject *cam; // Camera object.

/// Initialization of the OLGA -----------------------------------------
void InitOLGA(ol_Application *appl)
{
	ol_ApplInitParams initParams;

	initParams.title		= "Example 1-04 SimpleFont and UserDraw";	// Window title.
	initParams.width		= 1024;								// X resolution.
	initParams.height		= 768;								// Y resolution.
	initParams.bitsPerPixel	= 32;								// Color depth.
	initParams.depth		= 100000.0f;						// Depth of view.
	initParams.bkColor		= ol_Color(0.95f, 0.95f, 0.95f);	// Default background color.
	initParams.isFullScreen	= false;							// Create full screen window? (Not implemented yet).

	appl->Init(initParams); // Send core parameters to OLGA.
	mainApp = appl;			// Save pointer to main OLGA application.
}

/// User defined initialization of all objects and parameters ----------
bool UserInitialize(void)
{
	// Save pointers to all core interfaces.
	smgr	= mainApp->getSceneManager();
	graph	= mainApp->getGraphicsInterface();
	gmgr	= mainApp->getGUIManager();
	smgr2d	= mainApp->getSceneManager2d();
	pmgr	= smgr2d->getParticleManager();

/// Creating a font from a *.ttf file ----------------------------------
	systemFont = gmgr->CreateSimpleFont("../../../media/fonts/Dejavu/DejaVuSans.ttf", 14);
	if(!systemFont) return false;

/// Adding a camera ----------------------------------------------------
	cam = smgr->addCameraSceneObjectRPG(ol_Vector3d(10.0f, 7.0f, 10.0f), ol_Vector3d(0.0f, 0.0f, 0.0f),"My Camera");

	return true; // Return true if initialization successful.
}

/// Removing of all user objects.
/// Objects that were added by core interfaces will be removed automatically.
void UserDeinitialize(void)
{}

/// All movements and input events are processed here ------------------
void UserUpdate(ol_Event *event)
{
	// Exit pogram if "Escape" button is pressed.
	if(event->getKey(OL_KEY_ESCAPE)) mainApp->TerminateApplication();
}

/// Processing of all GUI and Game events ------------------------------
void UserOnEvent(ol_GameEvent *event)
{
	switch(event->type)
	{
	case OL_GETYPE_SCENE:
		break;
	case OL_GETYPE_GUI:
		switch(event->gevent->type)
		{
		case OL_GUIEV_LBUTTONDOWN:
			break;
		case OL_GUIEV_LBUTTONUP:
			break;
		case OL_GUIEV_MBUTTONDOWN:
			break;
		case OL_GUIEV_MBUTTONUP:
			break;
		case OL_GUIEV_RBUTTONDOWN:
			break;
		case OL_GUIEV_RBUTTONUP:
			break;
		case OL_GUIEV_BUTTONPRESSED:
			break;
		case OL_GUIEV_SCROLL:
			break;
		case OL_GUIEV_MOUSEMOVE:
			break;
		case OL_GUIEV_FILEOPENED:
			break;
		case OL_GUIEV_FILESAVED:
			break;
		case OL_GUIEV_BUTTONCLICKED:
			break;
		case OL_GUIEV_EDITBOXENTER:
			break;
		case OL_GUIEV_FBCHANGEDPATH:
			break;
		case OL_GUIEV_FBFILESELECTED:
			break;
		case OL_GUIEV_SCROLLBOXCHANGED:
			break;
		}
		break;
	}
}

/// Calling OpenGL commands directly -----------------------------------
void UserDraw (void)
{
	// Drawing lines representing coordinate axes.
	graph->DrawCoordSys(50);
	// Drawing a pyramid.
	glDisable(GL_LIGHTING);
	glDisable(GL_TEXTURE_2D);
	glBegin(GL_QUADS);
		glColor3f(1.0f, 0.0f, 0.0f);
		glVertex3f(0.0f, 0.0f, 0.0f);
		glVertex3f(5.0f, 0.0f, 0.0f);
		glVertex3f(5.0f, 0.0f, 5.0f);
		glVertex3f(0.0f, 0.0f, 5.0f);
	glEnd();
	glBegin(GL_TRIANGLES);
		glColor3f(1.0f, 0.0f, 1.0f); glVertex3f(0.0f, 0.0f, 0.0f);
		glColor3f(1.0f, 1.0f, 0.0f); glVertex3f(5.0f, 0.0f, 0.0f);
		glColor3f(1.0f, 0.0f, 0.0f); glVertex3f(2.5f, 5.0f, 2.5f);

		glColor3f(1.0f, 0.0f, 0.0f); glVertex3f(5.0f, 0.0f, 0.0f);
		glColor3f(0.0f, 1.0f, 0.0f); glVertex3f(5.0f, 0.0f, 5.0f);
		glColor3f(0.0f, 0.0f, 1.0f); glVertex3f(2.5f, 5.0f, 2.5f);

		glColor3f(0.0f, 1.0f, 1.0f); glVertex3f(5.0f, 0.0f, 5.0f);
		glColor3f(1.0f, 1.0f, 0.0f); glVertex3f(0.0f, 0.0f, 5.0f);
		glColor3f(1.0f, 0.0f, 1.0f); glVertex3f(2.5f, 5.0f, 2.5f);

		glColor3f(1.0f, 1.0f, 0.0f); glVertex3f(0.0f, 0.0f, 0.0f);
		glColor3f(1.0f, 1.0f, 1.0f); glVertex3f(0.0f, 0.0f, 5.0f);
		glColor3f(0.0f, 1.0f, 0.0f); glVertex3f(2.5f, 5.0f, 2.5f);
	glEnd();
	// Printing some debug information.
	graph->ol_GLColor(OL_COLOR_BLACK); // Another way to call glColor3f().
	// Print out OLGA version and current FPS.
	systemFont->Print(10, 750, "OLGA - %s\nFPS: %d", mainApp->getVersion(), ol_CoreParams::fps);
	// Print out current camera position.
	ol_Vector3d p = cam->getPosition();
	systemFont->Print(10, 708, "Camera Position (%.2f; %.2f; %.2f)", p.x, p.y, p.z);
}
