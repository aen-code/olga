/**
------------------------------------------------------------------------
  This file is part of the OLGA project.
  Copyright © 2017 by Enver Akhmedov.
  E-mail: aen-code@yandex.ru
  For conditions of distribution and use, see copyright notice in OLGA.h
------------------------------------------------------------------------
**/

#include "OLGA.h"

/// Define pointers to core variables ----------------------------------
ol_Application       *mainApp;
ol_SceneManager      *smgr;
ol_SceneManager2d    *smgr2d;
ol_Graphics          *graph;
ol_GUIManager        *gmgr;
ol_ParticleManager   *pmgr;

/// Initialization of the OLGA -----------------------------------------
void InitOLGA(ol_Application *appl)
{
	ol_ApplInitParams initParams;

	initParams.title		= "Example 1-01 Helo OLGA";			// Window title.
	initParams.width		= 1024;								// X resolution.
	initParams.height		= 768;								// Y resolution.
	initParams.bitsPerPixel	= 32;								// Color depth.
	initParams.depth		= 100000.0f;						// Depth of view.
	initParams.bkColor		= ol_Color(0.95f, 0.95f, 0.95f);	// Default background color.
	initParams.isFullScreen	= false;							// Create full screen window? (Not implemented yet).

	appl->Init(initParams); // Send core parameters to OLGA.
	mainApp = appl;			// Save pointer to main OLGA application.
}

/// User defined initialization of all objects and parameters ----------
bool UserInitialize(void)
{
	// Save pointers to all core interfaces.
	smgr	= mainApp->getSceneManager();
	graph	= mainApp->getGraphicsInterface();
	gmgr	= mainApp->getGUIManager();
	smgr2d	= mainApp->getSceneManager2d();
	pmgr	= smgr2d->getParticleManager();

/// Adding a camera ----------------------------------------------------
	smgr->addCameraSceneObject(ol_Vector3d(10.0, 5.0, 10.0), ol_Vector3d(0.0, 0.0, 0.0),"My Camera");

/// Adding a light source ----------------------------------------------
	smgr->addLightSceneObject(ol_Color(0.3f, 0.3f, 0.3f), ol_Color(0.6f, 0.6f, 0.6f), ol_Color(1.0f, 1.0f, 1.0f), ol_Vector3d(0.0f, 400.0f, 0.0f));

/// Adding an a 3D scene object ----------------------------------------
	ol_Material *mat; // Pointer to material.
	//First load a mesh.
	ol_Mesh3D *migMesh = graph->getMesh("../../../media/models/MiG-3/MiG-3.obj");
	// And then add an object.
	ol_DynamicMeshSceneObject *fighter = smgr->addDynamicMeshSceneObject(migMesh);
	if(fighter) // If success customize rendering material parameters
	{
		mat = fighter->getMaterial();
		mat->setMaterialParam(OL_MATERIAL_LIGHTNING, true);
		mat->setMaterialParam(OL_MATERIAL_DIFFUSE, ol_Color(0.9f, 0.9f, 0.9f, 1.0f));
		mat->setMaterialParam(OL_MATERIAL_AMBIENT, ol_Color(1.0f, 1.0f, 1.0f, 1.0f));
		mat->setMaterialParam(OL_MATERIAL_TEXTURE, graph->getTexID("../../../media/models/MiG-3/MiG-3_tex.tga"));
	}
	else return false; // If something wrong return with error.

	return true; // Return true if initialization successful.
}

/// Removing all of the user objects.
/// Objects that were added by core interfaces will be removed automatically.
void UserDeinitialize(void)
{}

/// All movements and input events are processed here ------------------
void UserUpdate(ol_Event *event)
{}

/// Processing of all GUI and Game events ------------------------------
void UserOnEvent(ol_GameEvent *event)
{
	switch(event->type)
	{
	case OL_GETYPE_SCENE:
		break;
	case OL_GETYPE_GUI:
		switch(event->gevent->type)
		{
		case OL_GUIEV_LBUTTONDOWN:
			break;
		case OL_GUIEV_LBUTTONUP:
			break;
		case OL_GUIEV_MBUTTONDOWN:
			break;
		case OL_GUIEV_MBUTTONUP:
			break;
		case OL_GUIEV_RBUTTONDOWN:
			break;
		case OL_GUIEV_RBUTTONUP:
			break;
		case OL_GUIEV_BUTTONPRESSED:
			break;
		case OL_GUIEV_SCROLL:
			break;
		case OL_GUIEV_MOUSEMOVE:
			break;
		case OL_GUIEV_FILEOPENED:
			break;
		case OL_GUIEV_FILESAVED:
			break;
		case OL_GUIEV_BUTTONCLICKED:
			break;
		case OL_GUIEV_EDITBOXENTER:
			break;
		case OL_GUIEV_FBCHANGEDPATH:
			break;
		case OL_GUIEV_FBFILESELECTED:
			break;
		case OL_GUIEV_SCROLLBOXCHANGED:
			break;
		}
		break;
	}
}

/// Calling OpenGL commands directly -----------------------------------
void UserDraw (void)
{}
