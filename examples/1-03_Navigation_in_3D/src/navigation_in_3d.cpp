/**
------------------------------------------------------------------------
  This file is part of the OLGA project.
  Copyright © 2017 by Enver Akhmedov.
  E-mail: aen-code@yandex.ru
  For conditions of distribution and use, see copyright notice in OLGA.h
------------------------------------------------------------------------
**/

#include "OLGA.h"

/// Define pointers to core variables ----------------------------------
ol_Application       *mainApp;
ol_SceneManager      *smgr;
ol_SceneManager2d    *smgr2d;
ol_Graphics          *graph;
ol_GUIManager        *gmgr;
ol_ParticleManager   *pmgr;
/// User defined global variables --------------------------------------
// We define the poiter to our game object globaly because we will need 
// it to process events.
ol_DynamicMeshSceneObject *fighter;
float fSpeed = 80.0f;
float fYawSpeed = 50.0f;
float fPithchSpeed = 70.0f;

/// Initialization of the OLGA -----------------------------------------
void InitOLGA(ol_Application *appl)
{
	ol_ApplInitParams initParams;

	initParams.title		= "Example 1-03 Navigation in 3D";	// Window title.
	initParams.width		= 1024;								// X resolution.
	initParams.height		= 768;								// Y resolution.
	initParams.bitsPerPixel	= 32;								// Color depth.
	initParams.depth		= 100000.0f;						// Depth of view.
	initParams.bkColor		= ol_Color(0.95f, 0.95f, 0.95f);	// Default background color.
	initParams.isFullScreen	= false;							// Create full screen window? (Not implemented yet).

	appl->Init(initParams); // Send core parameters to OLGA.
	mainApp = appl;			// Save pointer to main OLGA application.
}

/// User defined initialization of all objects and parameters ----------
bool UserInitialize(void)
{
	// Save pointers to all core interfaces.
	smgr	= mainApp->getSceneManager();
	graph	= mainApp->getGraphicsInterface();
	gmgr	= mainApp->getGUIManager();
	smgr2d	= mainApp->getSceneManager2d();
	pmgr	= smgr2d->getParticleManager();

/// Adding a light source ----------------------------------------------
	smgr->addLightSceneObject(ol_Color(0.3f, 0.3f, 0.3f), ol_Color(0.6f, 0.6f, 0.6f), ol_Color(1.0f, 1.0f, 1.0f), ol_Vector3d(0.0f, 400.0f, 0.0f));

/// Building a terrain from height map ---------------------------------
	ol_Material *mat; // Pointer to material.
	ol_TerrainSceneObject *terr = smgr->addTerrainSceneObject("../../../media/terrains/Terrain1.tga", 4, 20.0, 4.0, 3, "Terrain");
	if(terr)
	{
		mat = terr->getMaterial(); // Get pointer to material.
		// Enable lightning to see the relief.
		mat->setMaterialParam(OL_MATERIAL_LIGHTNING, true);
		// Mix texture color with specific color to give the terrain more realistic view.
		mat->setMaterialParam(OL_MATERIAL_AMBIENT, ol_Color(0.95f, 0.45f, 0.0f));
		// Diffuse color is white. Play with this value to understand what it does.
		mat->setMaterialParam(OL_MATERIAL_DIFFUSE, ol_Color(1.0f, 1.0f, 1.0f));
		// No light reflection. Terrain has matt surface.
		mat->setMaterialParam(OL_MATERIAL_SPECULAR, ol_Color(0.0f, 0.0f, 0.0f));
		// Load a texture for the terrain.
		mat->setMaterialParam(OL_MATERIAL_TEXTURE, graph->getTexID("../../../media/terrains/Terrain1_tex.tga"));
		// Move 1200 points down.
		terr->setPosition(ol_Vector3d(0.0f, -1200.0f, 0.0f));
		// Disable frustum culling or terrain will disappear at some angle of view.
		terr->setFrustumCulling(false);
	}
	else return false;

/// Adding an a 3D scene object ----------------------------------------
	//First load a mesh.
	ol_Mesh3D *migMesh = graph->getMesh("../../../media/models/MiG-3/MiG-3.obj");
	// And then add an object.
	fighter = smgr->addDynamicMeshSceneObject(migMesh);
	if(fighter) // If success customize rendering material parameters
	{
		mat = fighter->getMaterial();
		mat->setMaterialParam(OL_MATERIAL_LIGHTNING, true);
		mat->setMaterialParam(OL_MATERIAL_DIFFUSE, ol_Color(0.9f, 0.9f, 0.9f, 1.0f));
		mat->setMaterialParam(OL_MATERIAL_AMBIENT, ol_Color(1.0f, 1.0f, 1.0f, 1.0f));
		mat->setMaterialParam(OL_MATERIAL_TEXTURE, graph->getTexID("../../../media/models/MiG-3/MiG-3_tex.tga"));
	}
	else return false; // If something wrong return with error.

/// Adding a camera ----------------------------------------------------
	/* This time we add a RPG camera. Unlike simple camera that has static
	 * position and target, RPG camera can rotate around a target (right mouse)
	 * and zoom (scroller). We want our camera to watch the fighter and follow it,
	 * therefore we use fighter's positon as a target for camera and make fighter
	 * a parent to camera. */
	ol_CameraSceneObject *cam =  smgr->addCameraSceneObjectRPG(ol_Vector3d(0.0, 5.0, -15.0), fighter->getPosition(),"My Camera");
	cam->setParent(fighter);

/// Adding a sky box ---------------------------------------------------
	// Keep in mind that you should add a camera before the Sky Box.
	smgr->addSkyBoxSceneObject(graph->getTexID("../../../media/sky_boxes/SkyBox1.tga"));

	return true; // Return true if initialization successful.
}

/// Removing of all user objects.
/// Objects that were added by core interfaces will be removed automatically.
void UserDeinitialize(void)
{}

/// All movements and input events are processed here ------------------
void UserUpdate(ol_Event *event)
{
	// Exit pogram if "Escape" button is pressed.
	if(event->getKey(OL_KEY_ESCAPE)) mainApp->TerminateApplication();

	// All scene objects "know" where forward is, so we just tell our object ro go forward.
	fighter->GoForward(fSpeed);
	if(event->getKey(OL_KEY_W)) fighter->Rotate(0, fYawSpeed, 0);
	if(event->getKey(OL_KEY_S)) fighter->Rotate(0, -fYawSpeed, 0);
	if(event->getKey(OL_KEY_D)) fighter->Rotate(0, 0, fPithchSpeed);
	if(event->getKey(OL_KEY_A)) fighter->Rotate(0, 0, -fPithchSpeed);
}

/// Processing of all GUI and Game events ------------------------------
void UserOnEvent(ol_GameEvent *event)
{
	switch(event->type)
	{
	case OL_GETYPE_SCENE:
		break;
	case OL_GETYPE_GUI:
		switch(event->gevent->type)
		{
		case OL_GUIEV_LBUTTONDOWN:
			break;
		case OL_GUIEV_LBUTTONUP:
			break;
		case OL_GUIEV_MBUTTONDOWN:
			break;
		case OL_GUIEV_MBUTTONUP:
			break;
		case OL_GUIEV_RBUTTONDOWN:
			break;
		case OL_GUIEV_RBUTTONUP:
			break;
		case OL_GUIEV_BUTTONPRESSED:
			break;
		case OL_GUIEV_SCROLL:
			break;
		case OL_GUIEV_MOUSEMOVE:
			break;
		case OL_GUIEV_FILEOPENED:
			break;
		case OL_GUIEV_FILESAVED:
			break;
		case OL_GUIEV_BUTTONCLICKED:
			break;
		case OL_GUIEV_EDITBOXENTER:
			break;
		case OL_GUIEV_FBCHANGEDPATH:
			break;
		case OL_GUIEV_FBFILESELECTED:
			break;
		case OL_GUIEV_SCROLLBOXCHANGED:
			break;
		}
		break;
	}
}

/// Calling OpenGL commands directly -----------------------------------
void UserDraw (void)
{}
