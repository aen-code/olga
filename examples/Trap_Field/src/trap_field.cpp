/**
------------------------------------------------------------------------
                      Trap Field 
------------------------------------------------------------------------

   Copyright © 2017 by Enver Akhmedov.

   E-mail: aen-code@yandex.ru

   This software is provided 'as-is', without any express or implied
   warranty. In no event will the authors be held liable for any
   damages arising from the use of this software.

   Permission is granted to anyone to use this software for any purpose,
   including commercial applications, and to alter it and redistribute
   it freely, subject to the following restrictions:

   1. The origin of this software must not be misrepresented; you must
      not claim that you wrote the original software. If you use this
      software in a product, an acknowledgement in the product
      documentation would be appreciated but is not required.
   2. Altered source versions must be plainly marked as such, and must
      not be misrepresented as being the original software.
   3. This notice may not be removed or altered from any source
      distribution.

   This game is developed upon the OLGA engine, which has additional 
   dependencies. See corresponding license files in the "doc" directory 
   of the project. All content presented in "media" directory is created
   by Enver Akhmedov, excluding the font, which has it's own license.
------------------------------------------------------------------------
**/

#include "gameplay.h"

/// Define pointers to core variables ----------------------------------
ol_Application       *mainApp;
ol_SceneManager      *smgr;
ol_SceneManager2d    *smgr2d;
ol_Graphics          *graph;
ol_GUIManager        *gmgr;
ol_ParticleManager   *pmgr;

GameField *game;			// The game object.

// Game GUI controls
ol_GUILabel *trapDisplay;	// Label to display number of traps marked/total.
ol_GUILabel *timeDisplay;	// Label to display seconds past from the beginning of the game.
ol_GUILabel *levelDisplay;	// Label to display current complexity level.

ol_GUIButton *newGameBut;	// Button to start a new game.
ol_GUIButton *aboutBut;		// Button to show about window.
ol_GUIButton *levelMinusBut;// Button to switch complexity level.
ol_GUIButton *levelPlusBut; // Button to switch complexity level.
ol_GUIWindow *aboutWin;		// About window.

// All possible complexity levels.
GameLevel levels[3] = {12, 12, 11, L"Easy",
					   16, 16, 50, L"Medium",
					   16, 32, 99, L"Hard"};
// Current complexity level. Points on an element of levels array.
unsigned int curLevel = 0;

// allocating memory for static members
unsigned int Cell::totalMarked = 0;
unsigned int Cell::totalOpened = 0;
bool Cell::died = false;
bool Cell::victory = false;

// To start a new game we ask GUI Manager to delete game object and create a new one.
void NewGame(void)
{
	gmgr->delControl(game);

	game = new GameField(graph, gmgr->getDefaultTheme(), gmgr->winArea.getPosPoint(OL_GUI_POS_CENTMID),1024, 480);
	/* As our game is also a GUI Panel (we inherited it from ol_GUIPanel), we add its 
	 * pointer to GUI Manager to handle all Draw and Event functions automatically. */
	gmgr->addControl(NULL, game); 
	// Begin a new game
	game->NewGame(levels[curLevel]);

	if(trapDisplay) trapDisplay->setText(game->getTrapStr());
}

/// Initialization of the OLGA -----------------------------------------
void InitOLGA(ol_Application *appl)
{
	ol_ApplInitParams initParams;

	// Fill out application data
	initParams.title        = "Trap Field";
	initParams.width        = 960;
	initParams.height       = 552;
	initParams.bitsPerPixel = 32;
	initParams.depth        = 100000.0f;
	initParams.bkColor      = ol_Color(0.95f, 0.95f, 0.95f);
	initParams.isFullScreen = false;

	appl->Init(initParams);
	mainApp = appl;
}

/// User defined initialization of all objects and parameters ----------
bool UserInitialize(void)
{
	smgr  =  mainApp->getSceneManager();
	graph =  mainApp->getGraphicsInterface();
	gmgr  =  mainApp->getGUIManager();
	smgr2d = mainApp->getSceneManager2d();
	pmgr = smgr2d->getParticleManager();

/// Adding a camera ----------------------------------------------------
	smgr->addCameraSceneObject(ol_Vector3d(20.0, 20.0, 20.0), ol_Vector3d(0.0, 0.0, 0.0),"My Camera");

/// Adding font, GUI controls and all game objects ---------------------
	if(!gmgr->InitDefaultTheme("../media/fonts/Dejavu/DejaVuSans-Bold.ttf")) return false;

	NewGame();

	trapDisplay = gmgr->addGUILabel(NULL, game->topMid + ol_Vector2df(0,18), L"", OL_GUI_POS_CENTMID);
	trapDisplay->setText(game->getTrapStr());

	timeDisplay = gmgr->addGUILabel(NULL, game->topRight + ol_Vector2df(-40, 18), L"", OL_GUI_POS_CENTRIGHT);
	timeDisplay->setText(game->getGameTime());

	newGameBut = gmgr->addGUIButton(NULL, gmgr->winArea.getPosPoint(OL_GUI_POS_TOPLEFT)+ ol_Vector2df(3, -18), 96, 30, L"New Game", OL_GUI_BUTTON_STANDARD, OL_GUI_POS_CENTLEFT);
	aboutBut = gmgr->addGUIButton(NULL, gmgr->winArea.getPosPoint(OL_GUI_POS_BOTRIGHT)+ ol_Vector2df(-3, 18), 85, 30, L"About", OL_GUI_BUTTON_STANDARD, OL_GUI_POS_CENTRIGHT);

	// Adding buttons to choose complexity level and a label to display current level.
	levelMinusBut = gmgr->addGUIButton(NULL, gmgr->winArea.getPosPoint(OL_GUI_POS_BOTLEFT)+ ol_Vector2df(3, 18), 30, 30, L"<", OL_GUI_BUTTON_STANDARD, OL_GUI_POS_CENTLEFT);
	levelPlusBut = gmgr->addGUIButton(NULL, levelMinusBut->centRight + ol_Vector2df(100, 0), 30, 30, L">", OL_GUI_BUTTON_STANDARD, OL_GUI_POS_CENTLEFT);
	levelDisplay = gmgr->addGUILabel(NULL, levelMinusBut->centRight+ ol_Vector2df(50, 0), levels[curLevel].name, OL_GUI_POS_CENTMID);

	// Constructing "About" window. It will have an icon, copyright notice and a short description of the game.
	aboutWin = gmgr->addGUIWindow(gmgr->winArea.getPosPoint(OL_GUI_POS_CENTMID), 400, 300, L"About", OL_GUI_POS_CENTMID);
	// We will not use the four following controls further, so we do not create variables and just ask GUI manager to add them.
	gmgr->addGUIImage(aboutWin, aboutWin->clientArea->topMid, 128, 96, graph->getTexture("../media/images/TrapField.tga"), OL_GUI_POS_TOPMID);
	gmgr->addGUILabel(aboutWin, aboutWin->clientArea->topMid + ol_Vector2df(0, -110), L"Trap Field", OL_GUI_POS_CENTMID);
	gmgr->addGUILabel(aboutWin, aboutWin->clientArea->topMid + ol_Vector2df(0, -130), L"Copyright (c) 2017 by Enver Akhmedov.", OL_GUI_POS_CENTMID);
	gmgr->addGUILabel(aboutWin, aboutWin->clientArea->topMid + ol_Vector2df(0, -150), L"aen-code@yandex.ru", OL_GUI_POS_CENTMID);
	// Game description is a quite long text, so we use a TextBox for it. ol_GUITextBox allows us to add a text with word wrapping.
	ol_GUITextBox *aboutText = gmgr->addGUITextBox(aboutWin, aboutWin->clientArea->botMid, aboutWin->clientArea->width-12, 100, OL_GUI_POS_BOTMID);
	aboutText->addText(L"Simple game illustrating capabilities of the OLGA engine. Some cells have a trap inside, others have a number indicating how many traps are around. Goal of the game is to mark all traps and open all other cells.");
	// We do not want the TextBox appear as a separate GUI control, so we ask not to render its border and background.
	aboutText->showBackground(false);
	aboutText->showBorder(false);
	// Hide "About" window. We will show it only by click on the "About" button.
	aboutWin->Hide();

	return true;	// Return true if initialization Successful.
}

/// Removing all of the user objects.
/// Objects that were added by core interfaces will be removed automatically.
void UserDeinitialize(void)
{}

/// All movements and input events are processed here ------------------
void UserUpdate(ol_Event *event)
{
	if(event->getKey(OL_KEY_ESCAPE)) mainApp->TerminateApplication();
	timeDisplay->setText(game->getGameTime());
}

/// Processing of all GUI and Game events ------------------------------
void UserOnEvent(ol_GameEvent *event)
{
	switch(event->type)
	{
	case OL_GETYPE_SCENE:
		break;
	case OL_GETYPE_GUI:
		switch(event->gevent->type)
		{
		case OL_GUIEV_LBUTTONDOWN:
			break;
		case OL_GUIEV_LBUTTONUP:
			break;
		case OL_GUIEV_MBUTTONDOWN:
			break;
		case OL_GUIEV_MBUTTONUP:
			break;
		case OL_GUIEV_RBUTTONDOWN:
			game->checkVict();
			if(Cell::victory)
			{
				game->GameOver(WINNER);
				trapDisplay->setText(L"Victory!");
			}
			if(!Cell::died && !Cell::victory)trapDisplay->setText(game->getTrapStr());
			break;
		case OL_GUIEV_RBUTTONUP:
			break;
		case OL_GUIEV_BUTTONPRESSED:
			break;
		case OL_GUIEV_SCROLL:
			break;
		case OL_GUIEV_MOUSEMOVE:
			break;
		case OL_GUIEV_FILEOPENED:
			break;
		case OL_GUIEV_FILESAVED:
			break;
		case OL_GUIEV_BUTTONCLICKED:
			game->checkVict();
			if(event->gevent->control1 == newGameBut) NewGame();

			if(Cell::died) 
			{
				game->GameOver(LOOSER);
				trapDisplay->setText(L"Game Over!");
			}
			if(Cell::victory)
			{
				game->GameOver(WINNER);
				trapDisplay->setText(L"Victory!");
			}

			if(event->gevent->control1 == levelMinusBut)
			{
				if(curLevel>0) curLevel --;
				levelDisplay->setText(levels[curLevel].name);
			}
			if(event->gevent->control1 == levelPlusBut)
			{
				if(curLevel<2) curLevel ++;
				levelDisplay->setText(levels[curLevel].name);
			}

			if(event->gevent->control1 == aboutBut) aboutWin->Show();
			break;
		case OL_GUIEV_EDITBOXENTER:
			break;
		case OL_GUIEV_FBCHANGEDPATH:
			break;
		case OL_GUIEV_FBFILESELECTED:
			break;
		case OL_GUIEV_SCROLLBOXCHANGED:
			break;
		}
		break;
	}
}

/// Calling OpenGL commands directly -----------------------------------
void UserDraw (void)
{}
