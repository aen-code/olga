/**
------------------------------------------------------------------------
  This file is part of the "Trap Field" game.
  Copyright © 2017 by Enver Akhmedov.
  E-mail: aen-code@yandex.ru
  For conditions of distribution and use, see copyright notice in
  "trap_field.cpp" or in "doc" directory of the project.
------------------------------------------------------------------------
**/

#include "gameplay.h"

Cell::Cell(ol_Graphics *graph, ol_GUITheme *theme, ol_Vector2df origin, float width, float height, wstring caption, ol_GUIButtonStyle style, ol_GUIPositionPoints keyPoint, const char *name): 
ol_GUIButton(graph, theme, origin, width, height, caption, style, keyPoint, name)
{
	isOpened = false;
	cellState = 0;
	trapsAround = 0;
}

Cell::~Cell()
{}

void Cell::InitColor(void)
{
	ol_Color color;
	switch(trapsAround)
	{
	case 1:
		color = OL_COLOR_WHITE;
	break;
	case 2:
		color = OL_COLOR_LIGHT_BLUE;
	break;
	case 3:
		color = OL_COLOR_GREEN;
	break;
	case 4:
		color = OL_COLOR_YELLOW;
	break;
	case 5:
		color = OL_COLOR_ORANGE;
	break;
	case 6:
		color = OL_COLOR_RED;
	break;
	case 7:
		color = OL_COLOR_PURPLE;
	break;
	case 8:
		color = OL_COLOR_BLACK;
	break;
	}

	labStandardColor = color;
	labSelectedColor = color;
	caption->color = color;
}

bool Cell::CheckEvents(ol_Event *event, ol_GUIEvent *guiEvent)
{
	if(ol_GUIButton::CheckEvents(event, guiEvent))
	{
		if(guiEvent->type == OL_GUIEV_BUTTONCLICKED)
		{
			if(isOpened)
			{
				// Open neighbors if all traps around are marked.
				int k = ConutMarkedAround();
				if(k == trapsAround)
				{
					for(unsigned int i=0; i<neighbors.size(); i++)
						if(neighbors[i]->isOpened == false && neighbors[i]->cellState!=1) 
							neighbors[i]->Open();
				}
			}
			else
			{
				if(cellState!=0) return false;
				Open();
				return false;
			}
		return false;
		}

		if(guiEvent->type == OL_GUIEV_RBUTTONDOWN)
		{
			if(isOpened || died || victory) return false;
			SwitchState();
		}
	}
	// Show all unopened neighbors.
	if(state == OL_GUI_STATE_PRESSED && isOpened) 
	{
		for(unsigned int i=0; i<neighbors.size(); i++)
				if(neighbors[i]->isOpened == false && neighbors[i]->cellState==0) 
					neighbors[i]->setState(OL_GUI_STATE_PRESSED);
	}

	return false;
}

void Cell::Open()
{
	isOpened = true;
	texID = skinOp;
	totalOpened ++;

	// The cell has no trap, but there are some traps around
	if(trapsAround > 0) setCaption(int(trapsAround));

	/* Cell is empty. There are no traps around.
	 * Check all neighbors and recursively open those having no traps around. */
	if(trapsAround == 0)
		for(unsigned int i=0; i<neighbors.size(); i++)
			if((neighbors[i]->isOpened == false))
				neighbors[i]->Open();

	// Here is a trap. Game over!
	if(trapsAround < 0) Blowup();
}

int Cell::ConutMarkedAround(void)
{
	int k = 0;
	for(unsigned int i=0; i<neighbors.size(); i++)
	{
		if(neighbors[i]->cellState == 1) k++;
	}

	return k;
}

void Cell::SwitchState(void)
{
	cellState++;
	if(cellState>2) cellState = 0;
	
	switch(cellState)
	{
	case 0: // not pressed
		texID = skinSt;
		setStyle(OL_GUI_BUTTON_STANDARD);
	break;
	case 1: // marked to be a trap
		texID = skinTr;
		setStyle(OL_GUI_BUTTON_LOCKED);
		totalMarked++;
	break;
	case 2: // marked by question
		texID = skinQe;
		setStyle(OL_GUI_BUTTON_LOCKED);
		totalMarked--;
	break;
	}
}

void Cell::Blowup(void)
{
	printf("Game Over! You have lost!\n");
	explosion->setPosition(centMid, OL_GUI_POS_CENTMID);
	explosion->Show();
	explosion->Animate(1);
	died = true;
}

GameField::GameField(ol_Graphics *graph, ol_GUITheme *theme, ol_Vector2df origin, float width, float height, ol_GUIPositionPoints keyPoint, const char *name): ol_GUIPanel(graph, theme, origin, width, height, keyPoint, name)
{}

GameField::~GameField()
{
	printf("GameField destructor\n");
}

void GameField::NewGame(GameLevel level)
{
	this->n = level.n;
	this->m = level.m;
	this->trc = level.trapNum;
	cellSize = 30;
	Cell::totalMarked = 0;
	Cell::totalOpened = 0;
	Cell::died = false;
	Cell::victory = false;
	firstTime = time(NULL);
	gameTime = 0;

/// Loading explosion animation ----------------------------------------
	ol_Texture expTex = graph->getTexture("../media/images/Explosion.tga");
	expTex.width_amount = 6;
	expTex.height_amount = 3;
	expTex.fps = 30;
	explImg = new ol_GUIImage(graph, theme, centMid, 96, 96, expTex, OL_GUI_POS_CENTMID);
	explImg->Hide();

/// Generating cells ---------------------------------------------------
	// initial position in GL coordinates of the first cell. Left bottom corner.
	ol_Vector2df origin((n-1)*cellSize/2, (m-1)*cellSize/2);
	origin = centMid - origin;

	// Allocating memory for all cells.
	cells.Allocate(m, n);

	Cell *pCell;
	/* For the first skin we use ol_Texture to assign animation. For other
	 * skins we need just texture identifier, so we use GLuint. We assume 
	 * that other skin textures contain the same number of frames. */
	ol_Texture skin1 = graph->getTexture("../media/images/ButStandard.tga");
	GLuint skin2 = graph->getTexID("../media/images/ButQuestion.tga");
	GLuint skin3 = graph->getTexID("../media/images/ButTrapMarked.tga");
	GLuint skin4 = graph->getTexID("../media/images/ButOpened.tga");
	GLuint skin5 = graph->getTexID("../media/images/ButTrap.tga");
	skin1.width_amount = 5;
	skin1.height_amount = 1;

	for(unsigned int i=0; i<m; i++)
		for(unsigned int j=0; j<n; j++)
		{
			pCell = new Cell(graph, theme, origin + ol_Vector2df(j*cellSize, i*cellSize), cellSize, cellSize);
			pCell->AssignAnimation(skin1);
			pCell->skinSt = skin1.texID;
			pCell->skinQe = skin2;
			pCell->skinTr = skin3;
			pCell->skinOp = skin4;
			pCell->skinDe = skin5;
			pCell->explosion = explImg;
			cells(i,j) = pCell;
			addItem(pCell);
		}
	/* Adding explosion animation after all cells because we want it 
	 * to render above all cells. */
	addItem(explImg);

/// Generating traps ---------------------------------------------------
	/* Initialize vector that contains all positions of all cells.
	 * We will use it to generate traps randomly. After random generator 
	 * returns a number in the range [0, trapCells.size()) we use it to 
	 * mark cell to be a trap. And then exclude this cell from the vector.
	 * This will allow us not to generate traps in the same place twice. */ 

	vector<CellPos> trapCells;
	CellPos buf;
	for(unsigned int i=0; i<m; i++)
		for(unsigned int j=0; j<n; j++)
			{
				buf.m = i; buf.n = j;
				trapCells.push_back(buf);
			}
	unsigned int p;
	srand(time(NULL)); // Initialize random generator.
	// Generate trc number of traps.
	for(unsigned int i=0; i<trc; i++)
	{
		// Prevent attempt to generate traps more than cells.
		if(trapCells.size()<=0) break;
		p = rand() % trapCells.size();
		cells(trapCells[p].m, trapCells[p].n)->trapsAround = -1;
		trapCells.erase(trapCells.begin() + p);
	}

/// Searching for neighbors around each cell ---------------------------
	// Not to type m-1, n-1 each time we need last row or column 
	m=m-1;
	n=n-1;
	unsigned int br, er, bc, ec; // search ranges for row and column 
	for(unsigned int i=0; i<=m; i++)
		for(unsigned int j=0; j<=n; j++)
		{
			if(cells(i, j)->trapsAround == -1) continue;
			// Process cases when cell is on the border separately.
			if((i==0) && (j==0))
			{
				br = i; er = i+1;
				bc = j; ec = j+1;
			}
			if((i==0) && (j>0) && (j<n))
			{
				br = i; er = i+1;
				bc = j-1; ec = j+1;
			}
			if((i==0) && (j==n))
			{
				br = i; er = i+1;
				bc = j-1; ec = j;
			}
			if((i>0) && (i<m) && (j==n))
			{
				br = i-1; er = i+1;
				bc = j-1; ec = j;
			}
			if((i==m) && (j==n))
			{
				br = i-1; er = i;
				bc = j-1; ec = j;
			}
			if((i==m) && (j>0) && (j<n))
			{
				br = i-1; er = i;
				bc = j-1; ec = j+1;
			}
			if((i==m) && (j==0))
			{
				br = i-1; er = i;
				bc = j; ec = j+1;
			}
			if((i>0) && (i<m) && (j==0))
			{
				br = i-1; er = i+1;
				bc = j; ec = j+1;
			}
			// The cell is inside matrix
			if((i>0) && (i<m) && (j>0) && (j<n))
			{
				br = i - 1; er = i + 1;
				bc = j - 1; ec = j + 1;
			}
			// Search all cells around the particular
			for(unsigned int r=br; r<=er; r++) 
				for(unsigned int c=bc; c<=ec; c++)
				{
					// Skip the same cell
					if((r==i) && (c==j)) continue;
					cells(i, j)->neighbors.push_back(cells(r,c));
				}
		}

/// Counting traps around each cell ------------------------------------
	for(unsigned int i=0; i<=m; i++)
		for(unsigned int j=0; j<=n; j++)
		{
			if(cells(i, j)->trapsAround == -1) continue;
			p = 0; // reset counter
			for(unsigned int k=0; k<cells(i, j)->neighbors.size(); k++)
				if(cells(i, j)->neighbors[k]->trapsAround == -1) p++;
			cells(i, j)->trapsAround = p;
			cells(i, j)->InitColor();
		}
}

void GameField::GameOver(GameOverType type)
{
	Cell *pCell; 
	
	switch(type)
	{
	case LOOSER:
	for(unsigned int i=0; i<=m; i++)
		for(unsigned int j=0; j<=n; j++)
		{
			pCell = cells(i, j);
			if(pCell->trapsAround == -1) pCell->texID = pCell->skinDe;
			else pCell->texID = pCell->skinOp;

			pCell->setStyle(OL_GUI_BUTTON_LOCKED);
		}
	break;
	case WINNER:
	for(unsigned int i=0; i<=m; i++)
		for(unsigned int j=0; j<=n; j++)
			cells(i, j)->setStyle(OL_GUI_BUTTON_LOCKED);
	break;
	}
}

wstring GameField::getTrapStr(void)
{
	wstring ws;
	string s = IntToString(Cell::totalMarked);
	s = s + "/";
	s = s + IntToString(trc);
	String_to_Wstring(s, ws);
	return ws;
}

wstring GameField::getGameTime(void)
{
	if(!Cell::died && !Cell::victory) gameTime = time(NULL) - firstTime;

	wstring ws;
	string s = IntToString(int(gameTime));
	String_to_Wstring(s, ws);
	return ws;
}

void GameField::checkVict(void)
{
	if(Cell::totalMarked == trc && (Cell::totalMarked + Cell::totalOpened) == (unsigned int)cells.Size())
		Cell::victory = true;
}
