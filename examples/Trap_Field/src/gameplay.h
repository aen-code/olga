/**
------------------------------------------------------------------------
  This file is part of the "Trap Field" game.
  Copyright © 2017 by Enver Akhmedov.
  E-mail: aen-code@yandex.ru
  For conditions of distribution and use, see copyright notice in
  "trap_field.cpp" or in "doc" directory of the project.
------------------------------------------------------------------------
**/

#include "OLGA.h"
#include <time.h>
#include <stdlib.h>

class Cell;

template<class T>
class Matrix
{
private:
	T* data;
	unsigned int rows, cols;
	int totalSize;
	
public:
	Matrix(){}
	~Matrix(){free(data); printf("Matrix Destructor\n");}
	
	int Allocate(unsigned int rows, unsigned int cols)
	{
		this->rows = rows;
		this->cols = cols;

		totalSize = rows*cols;

		data = (T*) calloc (totalSize, sizeof(T));
		
		if(data==NULL) return -1;

		return totalSize * sizeof(T);
	}

	int Size(void){ return totalSize; }

	T& operator()(unsigned int row, unsigned int col)
	{
		return data[row*cols + col];
	}
};

struct CellPos
{
	unsigned int m;
	unsigned int n;
};

struct GameLevel
{
	unsigned int m;
	unsigned int n;
	unsigned int trapNum;
	wstring name;
};

enum GameOverType{ WINNER, LOOSER };

class Cell: public ol_GUIButton
{
private:
	int cellState;

public:
	static unsigned int totalMarked;	// Number of all marked traps.
	static unsigned int totalOpened;	// Number of all opened cells.
	static bool died;					// Becomes true when user clicks on trap.
	static bool victory;				// Becomes true when user wins.

	/* Number of cells containing a trap around this cell.
	 * If the cell itself contains a trap, this value is -1. */
	int trapsAround;
	// True if the cell is opened.
	bool isOpened;

	// Pointer to GUI Image control with explosion animation.
	ol_GUIImage *explosion;
	
	vector<Cell*> neighbors;	// Each cell has an array of pointers to all of its neighbors.
	// OpenGL indexes of textures to change the skin of the cell.
	GLint skinSt, skinTr, skinQe, skinOp, skinDe;

	Cell(ol_Graphics *graph, ol_GUITheme *theme, ol_Vector2df origin, float width, float height, wstring caption = L"", ol_GUIButtonStyle style = OL_GUI_BUTTON_STANDARD, ol_GUIPositionPoints keyPoint = OL_GUI_POS_CENTMID, const char *name = "Cell");
	virtual ~Cell();

	// Redefinition of standard CheckEvents function
	virtual bool CheckEvents(ol_Event *event, ol_GUIEvent *guiEvent);
	// Cyclically switches the state of unopened cell: ->standard->trap mark->question mark->.
	void SwitchState(void);
	// Return the number of trap marks around.
	int ConutMarkedAround(void);
	// Opens cell. This is called when user clicks unmarked cell or when some cell opens its neighbor.
	void Open(void);
	// Called when user clicks trapped cell.
	void Blowup(void);
	/* Sets the color of the caption that depend on number of the traps around.
	 * Should be called after the number of the traps around have been counted. */ 
	void InitColor(void);
};

class GameField: public ol_GUIPanel
{
private:
	unsigned int n;			// Horizontal number of cells.
	unsigned int m;			// Vertical number of cells.
	float cellSize;			// Size of the cell in pixels.
	double firstTime;		// Saves global timer when game starts.
	double gameTime; 		// Stores game time in seconds.

	ol_GUIImage *explImg;	// GUI control to render explosion animation.

	Matrix<Cell*> cells;	// All cells.

public:
	unsigned int trc;		// Total number of traps in the field.

	GameField(ol_Graphics *graph, ol_GUITheme *theme, ol_Vector2df origin, float width, float height, ol_GUIPositionPoints keyPoint = OL_GUI_POS_CENTMID, const char *name = "Game Field");
	virtual ~GameField();

	// Returns string to display: marked traps/total traps
	wstring getTrapStr(void);
	// Returns string to display time of the game set
	wstring getGameTime(void);
	// Initialize field and start a game
	void NewGame(GameLevel level);
	// This function is called when user wins or fails with corresponding value of type variable
	void GameOver(GameOverType type);
	// Checks marked and opened cells and defines is user won.
	void checkVict(void);
};
