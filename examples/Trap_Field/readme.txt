------------------------------------------------------------------------
                             Trap Field
------------------------------------------------------------------------
Trap Field is a simple game illustrating capabilities of the OLGA 
engine. User has m x n field with a number of hidden traps. Goal of the 
game is to mark (right mouse click) all traps and open (left mouse 
click) all other cells. User loses the game if he opens a cell with a 
trap inside. If a cell has no trap it shows a number indicating how 
many traps are around. If opened cell is empty (no traps around) it 
opens all neighbor empty cells.

------------------------------------------------------------------------
                    Installation and requirements
------------------------------------------------------------------------
No installation is required. Just run executable file in the "bin" 
directory. Trap Field compiled for the x86_64 Linux platform. The game 
will run on most of the modern computers. The only requirement is 
hardware support of OpenGL 3.0 or higher. 

------------------------------------------------------------------------
                        Directory structure
------------------------------------------------------------------------
    bin/   - executable file.
    doc/   - all license files and documentation (if any).
    media/ - image and font resources of the game.
    obj/   - compiled object files.
    prj/   - make file if you want to recompile the game.
    src/   - source files of the game.

------------------------------------------------------------------------
                        License and Contacts
------------------------------------------------------------------------
All license files are in the "doc/licenses" directory. 
You can contact the author by e-mail:

    aen-code@yandex.ru
